package cucumber.ultragaz.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.ultragaz.AbstractFuncTest;
import cucumber.ultragaz.utils.PropertiesCpfCnpj;
import cucumber.ultragaz.utils.PropertiesFileReader;
import cucumber.ultragaz.utils.PropertiesTelefone;
import gherkin.deps.com.google.gson.JsonElement;

public class StepDefinitions extends AbstractFuncTest {

	private String encodedAuthorization;
	private String responseBody;
	private Integer responseCode;
	private LocalDate date;
	private String responseHeader;
	private int length;

	protected PropertiesFileReader fileReader = new PropertiesFileReader();

	@Before
	public void beforeScenario(Scenario scenario) {

		AbstractFuncTest.scenario = scenario;

	}

	@Given("^I use domain as \"(.*?)\"$")
	public void useDomainAs(String domain) {

		AbstractFuncTest.domain = domain;

	}

	@Given("^I use api name as \"(.*?)\"$")
	public void useApiName(String apiName) {

		AbstractFuncTest.apiName = apiName;

	}

	@Given("^System generate Authorization with APP clientId = \"([^\"]*)\" and APP secretKey = \"([^\"]*)\"$")
	public void calculeAuthorization(String appClientId, String appSecretKey) {

		appClientId = replaceVariablesValues(appClientId);

		appSecretKey = replaceVariablesValues(appSecretKey);

		String textHash = appClientId + ":" + appSecretKey;

		encodedAuthorization = Base64.getEncoder().encodeToString(textHash.getBytes());

		String estructuredAuthorization = "Basic " + encodedAuthorization;

		userParameter.put("${" + "authorization" + "}", estructuredAuthorization);

		scenario.write("Authorization = " + userParameter.get("${" + "authorization" + "}"));

	}

	@Given("^System generate Authorization with default APP information$")
	public void generateAuthorizationDefault() {

		String textHash = appClientId + ":" + appSecretKey;

		encodedAuthorization = Base64.getEncoder().encodeToString(textHash.getBytes());

		String estructuredAuthorization = "Basic " + encodedAuthorization;

		userParameter.put("${" + "authorization" + "}", estructuredAuthorization);

		scenario.write("Authorization = " + userParameter.get("${" + "authorization" + "}"));

	}

	@Then("^I save final value of header \"(.*?)\" as \"(.*?)\"$")
	public void getFinalValueOfUrl(String url, String variableUrl) throws Throwable {

		responseHeader = requestJSON.header(url);

		String value = (responseHeader.substring(responseHeader.lastIndexOf("/") + 1));

		userParameter.put("${" + variableUrl + "}", value);

		scenario.write(userParameter.get("${" + variableUrl + "}"));

	}

	@Given("^I save \"([^\"]*)\" as \"([^\"]*)\"$")
	public void saveStringAsString(String value, String parameter) {

		value = replaceVariablesValues(value);

		parameter = replaceVariablesValues(parameter);

		userParameter.put("${" + parameter + "}", value);

		scenario.write(parameter + " = " + userParameter.get("${" + parameter + "}"));

	}

	@Given("^I save \"([^\"]*)\" as ?([^\"]*)? days from now$")
	public void saveValueXDaysFromNow(String value, Integer days) {

		date = java.time.LocalDate.now().plusDays(days);

		userParameter.put("${" + "dueDate" + "}", date.toString());

		scenario.write("Due date: " + date);

		userParameter.put("${" + value + "}", date.toString());

		scenario.write(value + ": " + userParameter.get("${" + value + "}"));

	}

	@Given("^I save ?([^\"]*)? as \"([^\"]*)\"$")
	public void saveNumberAsString(Integer value, String parameter) {

		userParameter.put("${" + parameter + "}", value + "");

		scenario.write(parameter + " = " + userParameter.get("${" + parameter + "}"));

	}

	@Given("^System generate random number$")
	public void generateRandomNumber() {

		Random gerador = new Random();

		int random = gerador.nextInt();

		random = Math.abs(random);

		String randomS = random + "";

		userParameter.put("${" + "random" + "}", randomS);

		scenario.write(randomS);

	}

	@Given("^System generate random number with length ?([^\"]*)?$")
	public void generateRandomCepNumber(Integer length) {

		int random = (int) Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1));

		userParameter.put("${" + "random" + "}", random + "");

		scenario.write(random + "");

	}

	@Given("^System generate random telephone number$")
	public void generateRandomTelephoneNumber() {

		PropertiesTelefone geraTelefone = new PropertiesTelefone();

		String telefone = geraTelefone.geraTelefone();

		userParameter.put("${" + "phone" + "}", telefone);

		scenario.write(telefone);

	}

	@When("^I read file body \"(.*?)\"$")
	public String readFileBody(String filePath)
			throws JSONException, JsonParseException, JsonMappingException, IOException {

		filePath = replaceVariablesValues(filePath);

		String prettyJsonString = "";

		jsonFile = fileReader.lerArquivo(filePath);

		JsonElement je = jp.parse(jsonFile);

		prettyJsonString = gson.toJson(je);

		prettyJsonString = replaceVariablesValues(prettyJsonString);

		scenario.write(prettyJsonString);

		request.body(prettyJsonString);

		return prettyJsonString;

	}

	@Then("^I set request header \"([^\"]*)\" as \"([^\"]*)\"$")
	public void setRequestHeaderAuthorization(String headerName, String headerVariableValue) {

		headerVariableValue = replaceVariablesValues(headerVariableValue);

		request.header(headerName, headerVariableValue);

		scenario.write(headerName + ": " + headerVariableValue);

	}

	@Then("^I set request Content-Type header as \"([^\"]*)\"$")
	public void setRequestHeaderAuthorization(String headerVariableValue) {

		headerVariableValue = replaceVariablesValues(headerVariableValue);

		request.contentType(headerVariableValue);

		scenario.write("Content-Type: " + headerVariableValue);

	}

	@Then("^I save response header \"(.*?)\" as \"(.*?)\"$")
	public void saveResponseHeader(String searchResponseHeader, String userKey) throws Throwable {

		responseHeader = requestJSON.header(searchResponseHeader);

		userParameter.put("${" + userKey + "}", responseHeader);

		scenario.write(userParameter.get("${" + userKey + "}"));

	}

	@Given("^I set POST api endpoint as \"(.*?)\"$")
	public void setPostApiEndpoint(String currentAPIEndPoint) {

		currentAPIEndPoint = replaceVariablesValues(currentAPIEndPoint);

		requestJSON = request.post(domain + apiName + currentAPIEndPoint);

		scenario.write(requestJSON.getContentType() + " Content-Type");

		scenario.write(requestJSON.contentType() + " Content-Type");

		scenario.write(requestJSON.getHeader("Content-Type") + " Content-Type");

		scenario.write(requestJSON.getHeaders().toString());

		scenario.write(domain + apiName + currentAPIEndPoint);

	}

	@Given("^I set PATCH api endpoint as \"(.*?)\"$")
	public void setPatchApiEndpoint(String currentAPIEndPoint) {

		currentAPIEndPoint = replaceVariablesValues(currentAPIEndPoint);

		requestJSON = request.patch(domain + apiName + currentAPIEndPoint);

		scenario.write(domain + apiName + currentAPIEndPoint);

	}

	@Given("^I set PUT api endpoint as \"(.*?)\"$")
	public void setPutApiEndpoint(String currentAPIEndPoint) {

		currentAPIEndPoint = replaceVariablesValues(currentAPIEndPoint);

		requestJSON = request.put(domain + apiName + currentAPIEndPoint);

		scenario.write(domain + apiName + currentAPIEndPoint);

	}

	@When("^I get response body$")
	public String getResponseBody() {

		responseBody = requestJSON.getBody().asString();

		try {

			JsonElement je = jp.parse(responseBody);

			String prettyJsonString = gson.toJson(je);

			scenario.write(prettyJsonString);

		} catch (Exception e) {

			responseBody = requestJSON.getBody().asString();

			scenario.write(responseBody);

		}

		return responseBody;

	}

	@When("^I get response status code$")
	public Integer getResponseStatusCode() {

		responseCode = requestJSON.getStatusCode();

		scenario.write(responseCode.toString());

		return responseCode;

	}

	@When("^I save response value \"([^\"]*)\" as \"([^\"]*)\"$")
	public void saveJsonValue(String jsonKey, String userKey) throws Throwable {

		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseBody));

		String jsonValue = "";

		if (rootNode.at(jsonKey) != null) {

			jsonValue = rootNode.at(jsonKey).asText();

		}

		userParameter.put("${" + userKey + "}", jsonValue);

		scenario.write(userParameter.get("${" + userKey + "}"));

	}

	@Then("^I compare variable length \"([^\"]*)\" as ?([^\"]*)?$")
	public void variableXLengthIsEqualsY(String variable, int value) {

		variable = replaceVariablesValues(variable);

		length = variable.length();

		assertEquals(length, value);

	}

	@Then("^I compare variable \"([^\"]*)\" with \"?([^\"]*)\"?$")
	public void variableXEqualsY(String variable, String value) {

		variable = replaceVariablesValues(variable);

		value = replaceVariablesValues(value);

		assertEquals(variable, value);

	}

	@When("^I compare response value \"([^\"]*)\" with ?([^\"]*)?$")
	public void compareResponseValue(String jsonKey, Integer parameterValue) throws Throwable {

		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseBody));

		String jsonValue = "";

		if (rootNode.at(jsonKey) != null) {

			jsonValue = rootNode.at(jsonKey).asText();

		}

		assertEquals(jsonValue, parameterValue);

		scenario.write(jsonValue + " é igual a " + parameterValue);

	}

	@Then("^Response code must be (\\d+)$")
	public void responseCodeMustBe(Integer requiredResponseCode) {

		assertEquals(requiredResponseCode, responseCode);

	}

	@Then("^I get token on database by contact_id \"(.*?)\"$")
	public void getTokenOnDatabase(String phoneContact) {

		phoneContact = replaceVariablesValues(phoneContact);

		scenario.write("contact_id: " + phoneContact);

		String token = selectTokenByContactId(phoneContact);

		userParameter.put("${" + "TOKEN" + "}", token);

	}

	@Then("^Response body must be \"(.*?)\"$")
	public void responseBodyMustBe(String requiredResponseBody) {

		requiredResponseBody = replaceVariablesValues(requiredResponseBody);

		assertEquals(requiredResponseBody, responseBody);

	}

	@Given("^I set request body as \"(.*?)\"$")
	public void setRequestBody(String requiredRequestBody) {

		String jsonBodyString = replaceVariablesValues(requiredRequestBody);

		request.body(jsonBodyString);

		JsonElement je = jp.parse(jsonBodyString);

		String prettyJsonString = gson.toJson(je);

		scenario.write(prettyJsonString);

	}

	@Then("^I remove info from userId \"(.*?)\" and login \"(.*?)\"$")
	public void removeInfoFromUserId(String userId, String login) {

		userId = replaceVariablesValues(userId);
		login = replaceVariablesValues(login);

		deleteTokenLogByUserId(userId);

		deleteTokenByUserId(userId);

		deleteContactByLogin(login);

		deleteRelUserPortalCustomerByUserId(userId);

		deleteUserPortalById(userId);

		scenario.write("Deleted tokens from user_id: " + userId + " and login: " + login);

	}

	@Given("^I set GET api endpoint as \"(.*?)\"$")
	public void setGetApiEndpoint(String currentAPIEndPoint) {
		currentAPIEndPoint = replaceVariablesValues(currentAPIEndPoint);

		requestJSON = request.get(domain + apiName + currentAPIEndPoint);

		scenario.write(domain + apiName + currentAPIEndPoint);

	}

	@Given("^I set DELETE api endpoint as \"(.*?)\"$")
	public void setDeleteApiEndpoint(String currentAPIEndPoint) {

		currentAPIEndPoint = replaceVariablesValues(currentAPIEndPoint);

		requestJSON = request.delete(domain + apiName + currentAPIEndPoint);

		scenario.write(domain + apiName + currentAPIEndPoint);

	}

	@Given("^System generate random CPF$")
	public void systemGenerateRandomCpf() {

		String cpf = new PropertiesCpfCnpj().cpf();

		userParameter.put("${" + "randomCPF" + "}", cpf);

		scenario.write("CPF: " + cpf);

	}

	@When("^I compare if response value \"([^\"]*)\" contains \"([^\"]*)\"$")
	public void compareIfResponseValueContains(String jsonKey, String parameterValue) throws Throwable {

		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseBody));

		String jsonValue = "";

		if (rootNode.at(jsonKey) != null) {

			jsonValue = rootNode.at(jsonKey).asText();

		}

		parameterValue = replaceVariablesValues(parameterValue);

		if (jsonValue.contains(parameterValue)) {

			scenario.write(jsonValue + " contem " + parameterValue);

		} else {

			fail("The value " + jsonValue + " does not contains " + parameterValue);

			scenario.write("The value " + jsonValue + " does not contains " + parameterValue);

		}

	}

	@Then("^System get local date time")
	public void getDate() {

		date = java.time.LocalDate.now();

		userParameter.put("${" + "localDate" + "}", date.toString());

		scenario.write("Data local: " + date);

	}

	@Given("^System generate random CNPJ$")
	public void systemGenerateRandomCnpj() {

		String cnpj = new PropertiesCpfCnpj().cnpj();

		userParameter.put("${" + "randomCNPJ" + "}", cnpj);

		scenario.write("CNPJ: " + cnpj);

	}

	@Given("^I recover app clientId as \"([^\"]*)\"$")
	public void recoverAppClientId(String variableClientId) {

		variableClientId = replaceVariablesValues(variableClientId);

		userParameter.put("${" + variableClientId + "}", appClientId);

		scenario.write("App clientId = " + userParameter.get("${" + variableClientId + "}"));

	}

	@When("^I compare response value \"([^\"]*)\" with \"([^\"]*)\"$")
	public void compareResponseValue(String jsonKey, String parameterValue) throws Throwable {

		parameterValue = replaceVariablesValues(parameterValue);

		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseBody));

		String jsonValue = "";

		parameterValue = replaceVariablesValues(parameterValue);

		if (rootNode.at(jsonKey) != null) {

			try {
				jsonValue = rootNode.at(jsonKey).asText();
				System.out.printf(rootNode.asText());
				if (jsonValue.equals(parameterValue)) {
					assertEquals(jsonValue, parameterValue);

					scenario.write(jsonValue + " é igual a " + parameterValue);

				} else if (rootNode.toString().contains(parameterValue)) {
					scenario.write("Response body contém " + parameterValue);
				} else {
					fail("The value '" + parameterValue + "' is not found!");

					scenario.write("The value '" + parameterValue + "' must exist!");
				}

			} catch (Exception e) {

			}

		}
	}

	@Then("^I verify if response value \"([^\"]*)\" is not empty$")
	public void verifyIfResponseValueIsNotEmpty(String jsonKey) throws Throwable {

		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseBody));

		String jsonValue = "";

		if (rootNode.at(jsonKey) != null) {

			jsonValue = rootNode.at(jsonKey).asText();

			if (jsonValue.length() != 0 && !(jsonValue.isEmpty())) {

			} else {

				fail("The value " + jsonValue + " must not be empty!");

				scenario.write("The value " + jsonValue + " must not be empty!");

			}

		}

		scenario.write(jsonValue);

	}

	@Then("^I verify if response value \"([^\"]*)\" is empty$")
	public void verifyIfResponseValueIsEmpty(String jsonKey) throws Throwable {

		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseBody));

		String jsonValue = "";

		if (rootNode.at(jsonKey) != null) {

			jsonValue = rootNode.at(jsonKey).asText();

			if (jsonValue.length() != 0 && !(jsonValue.isEmpty())) {

				fail("The value " + jsonValue + " must be empty!");

				scenario.write("The value " + jsonValue + " must be empty!");

			} else {

				scenario.write("The value " + jsonValue + " is empty!");

			}

		}

	}

	@Then("^I wait ?([^\"]*)? seconds$")
	public void waitOneSecond(int seconds) throws Throwable {

		TimeUnit.SECONDS.sleep(seconds);

	}

	@Then("^I add ?([^\"]*)? days on local date$")
	public void getDate(int days) {

		date = java.time.LocalDate.now().plusDays(days);

		userParameter.put("${" + "dueDate" + "}", date.toString());

		scenario.write("Data local: " + date);

	}

	@Then("^I verify if response value \"([^\"]*)\" length is ?([^\"]*)?$")
	public void compareResponseValueLength(String jsonKey, int valueLength) throws Throwable {

		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseBody));

		String jsonValue = "";

		if (rootNode.at(jsonKey) != null) {

			jsonValue = rootNode.at(jsonKey).asText();

		}

		assertEquals(jsonValue.length(), valueLength);

		scenario.write(jsonValue.length() + " é igual a " + valueLength);

	}

	@Then("^I verify if response body is empty$")
	public void verifyIfResponseBodyIsEmpty() throws Throwable {

		if (responseBody == null || responseBody.isEmpty()) {

		} else {

			fail("The value " + responseBody + " must not be empty!");

			scenario.write("The value " + responseBody + " must not be empty!");

		}

	}

	@Then("^I verify if response body is not empty$")
	public void verifyIfResponseBodyIsNotEmpty() throws Throwable {

		if (responseBody != null && !responseBody.isEmpty()) {

		} else {

			fail("The value " + responseBody + " must be empty!");

			scenario.write("The value " + responseBody + " must be empty!");

		}

	}

	@When("^I verify if response parameter \"([^\"]*)\" contains value \"([^\"]*)\"$")
	public void verifyResponseParameterAndValue(String responseParameter, String parameterValue) throws Throwable {

		parameterValue = replaceVariablesValues(parameterValue);

		if (responseBody.contains("\"" + responseParameter + "\":\"" + parameterValue + "\"")) {
			scenario.write("The value '" + parameterValue + "' is present on " + responseParameter);
			assert (responseBody.contains("\"" + responseParameter + "\":\"" + parameterValue + "\""));
		} else {
			fail(parameterValue + " is not present on " + responseParameter);
			scenario.write(parameterValue + " is not present on " + responseParameter);
		}
	}

	@When("^I compare response value \"([^\"]*)\" with not found \"([^\"]*)\"$")
	public void compareResponseValueNotFound(String jsonKey, String parameterValue) throws Throwable {

		parameterValue = replaceVariablesValues(parameterValue);

		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseBody));

		String jsonValue = "";

		parameterValue = replaceVariablesValues(parameterValue);

		if (rootNode.at(jsonKey) != null) {

			try {
				jsonValue = rootNode.at(jsonKey).asText();
				if (jsonValue.equals(parameterValue)) {
					assertEquals(jsonValue, parameterValue);

					fail("The value '" + parameterValue + "' is  found!");
					scenario.write("The value '" + parameterValue + "' must exist!");

				} else {

					scenario.write(jsonValue + " não é igual a " + parameterValue);

				}

			} catch (Exception e) {

			}

		}

	}

}