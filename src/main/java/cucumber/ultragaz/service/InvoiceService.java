package cucumber.ultragaz.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import cucumber.ultragaz.domain.empresarial.invoice.CustomerInvoiceDomain;
import cucumber.ultragaz.domain.empresarial.invoice.InvoiceDomain;
import cucumber.ultragaz.repository.connection.DatabaseConnection;

public class InvoiceService {

	private static final String INSERT_CUSTOMER_INVOICE = "INSERT INTO customer_invoice (id, external_id, ship_site_id, bill_site_id, code, site_code, name, document, address, address_number, complement, county, city, state_code, zip_code, country, trx_id, site_code_bill, site_code_ship) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String INSERT_INVOICE = "INSERT INTO invoice (id, trx_id, organization, invoice_number, series, emission_date, source, `type`, access_key, status, xml, amount, creation_date, update_date) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String DELETE_CUSTOMER_INVOICE_BY_TRX_ID = "delete from customer_invoice where trx_id=\"%s\"";
	private static final String DELETE_INVOICE_BY_ID = "delete from invoice where id=\"%s\"";
	private static final String URL = "jdbc:mysql://invoicedev.cejk7gucxfmq.sa-east-1.rds.amazonaws.com/invoicedev";
	private static final String USERNAME = "admin";
	private static final String PASSWORD = "4lNCAuoGe6sOskirQmEn";

	private static DatabaseConnection invoiceConnection = new DatabaseConnection();

	public String insert(InvoiceDomain invoiceDomain) {

		try (Connection connection = invoiceConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INVOICE);

			preparedStatement.setString(1, invoiceDomain.getId());
			preparedStatement.setLong(2, invoiceDomain.getTrxId());
			preparedStatement.setString(3, invoiceDomain.getOrganization());
			preparedStatement.setString(4, invoiceDomain.getInvoiceNumber());
			preparedStatement.setString(5, invoiceDomain.getSeries());
			preparedStatement.setDate(6, invoiceDomain.getEmissionDate());
			preparedStatement.setString(7, invoiceDomain.getSource());
			preparedStatement.setString(8, invoiceDomain.getType());
			preparedStatement.setString(9, invoiceDomain.getAccessKey());
			preparedStatement.setString(10, invoiceDomain.getStatus());
			preparedStatement.setString(11, invoiceDomain.getXml());
			preparedStatement.setDouble(12, invoiceDomain.getAmount());
			preparedStatement.setDate(13, invoiceDomain.getCreationDate());
			preparedStatement.setDate(14, invoiceDomain.getUpdateDate());

			preparedStatement.execute();

			return invoiceDomain.getId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}

	public String insertCustomerInvoice(CustomerInvoiceDomain customerInvoiceDomain) {

		try (Connection connection = invoiceConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CUSTOMER_INVOICE);

			preparedStatement.setString(1, customerInvoiceDomain.getId());
			preparedStatement.setLong(2, customerInvoiceDomain.getExternalId());
			preparedStatement.setLong(3, customerInvoiceDomain.getShipSiteId());
			preparedStatement.setLong(4, customerInvoiceDomain.getBillSiteId());
			preparedStatement.setString(5, customerInvoiceDomain.getCode());
			preparedStatement.setString(6, customerInvoiceDomain.getSiteCode());
			preparedStatement.setString(7, customerInvoiceDomain.getName());
			preparedStatement.setString(8, customerInvoiceDomain.getDocument());
			preparedStatement.setString(9, customerInvoiceDomain.getAddress());
			preparedStatement.setString(10, customerInvoiceDomain.getAddressNumber());
			preparedStatement.setString(11, customerInvoiceDomain.getComplement());
			preparedStatement.setString(12, customerInvoiceDomain.getCounty());
			preparedStatement.setString(13, customerInvoiceDomain.getCity());
			preparedStatement.setString(14, customerInvoiceDomain.getStateCode());
			preparedStatement.setString(15, customerInvoiceDomain.getZipCode());
			preparedStatement.setString(16, customerInvoiceDomain.getCountry());
			preparedStatement.setLong(17, customerInvoiceDomain.getTrxId());
			preparedStatement.setString(18, customerInvoiceDomain.getSiteCodeBill());
			preparedStatement.setString(19, customerInvoiceDomain.getSiteCodeShip());

			preparedStatement.execute();

			return customerInvoiceDomain.getId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}

	public void deleteCustomerInvoiceByTrxId(Integer id) {

		try (Connection connection = invoiceConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_CUSTOMER_INVOICE_BY_TRX_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}
	
	public void deleteInvoiceById(String id) {

		try (Connection connection = invoiceConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_INVOICE_BY_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

}
