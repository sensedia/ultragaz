package cucumber.ultragaz.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import cucumber.ultragaz.domain.empresarial.customer.ContactDomain;
import cucumber.ultragaz.domain.empresarial.customer.CustomerAddressDomain;
import cucumber.ultragaz.domain.empresarial.customer.CustomerDomain;
import cucumber.ultragaz.domain.empresarial.customer.InstallBaseDomain;
import cucumber.ultragaz.domain.empresarial.customer.InstallbaseAddressDomain;
import cucumber.ultragaz.domain.empresarial.customer.UnitDomain;
import cucumber.ultragaz.domain.empresarial.customer.UserDomain;
import cucumber.ultragaz.repository.connection.DatabaseConnection;

public class CustomerService {

	private static final String SELECT_CUSTOMER_BY_ID = "select * from customer where id=\"%s\"";
	private static final String SELECT_TOKEN_BY_ID_CONTACT_ID = "select * from token where contact_id=\"%s\" order by created_at desc";
	private static final String SELECT_CONTACT_BY_DOCUMENT = "select * from contact inner join customer where contact.customer_id=customer.customer_id and customer.document=\"%s\"";
	private static final String SELECT_USER_BY_LOGIN = "select * from user_portal where login=\"%s\"";
	private static final String SELECT_USER_PORTAL_BY_ID = "select * from user_portal where id=\"%s\"";
	private static final String INSERT_CUSTOMER = "insert into customer (id, customer_id, code, name, fancy_name, document, status, source_organization, type, creation_date, update_date, birth_date) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String INSERT_CUSTOMER_ADDRESS = "INSERT INTO customer_address (id, site_use_id, site_id, code, address_line, address, address_number, complement, country, city, state_code, zip_code, address_type, customer_id, document, status, neighborhood, unit_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String INSERT_UNIT = "INSERT INTO unit (id, instance_id, name, due_day, delivery_type, has_gas_meter) values (?, ?, ?, ?, ?, ?)";
	private static final String INSERT_INSTALLBASE_ADDRESS = "INSERT INTO installbase_address (id, site_use_id, code, address_line, address, address_number, complement, country, city, state_code, zip_code, customer_id, customer_site_id) values (?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String INSERT_INSTALL_BASE = "INSERT INTO install_base (id, description, code_hierarchy, status, active_date_from, active_date_to, quantity, reference, nickname, creation_date, update_date, installbase_address_site_use_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String DELETE_CUSTOMER_BY_CUSTOMER_ID = "delete from customer where customer_id=\"%s\"";
	private static final String DELETE_CUSTOMER_ADDRESS_BY_ID = "delete from customer_address where id=\"%s\"";
	private static final String DELETE_CONTACT_BY_ID = "delete from contact where id=\"%s\"";
	private static final String DELETE_TOKEN_LOG_BY_USER_ID = "delete token_log from token_log INNER join token where token_log.token_id=token.id and token.user_id=\"%s\"";
	private static final String DELETE_TOKEN_BY_USER_ID = "delete from token where user_id=\"%s\"";
	private static final String DELETE_CONTACT_BY_LOGIN = "delete from contact where contact=\"%s\"";
	private static final String DELETE_REL_USER_PORTAL_CUSTOMER_BY_USER_ID = "delete from rel_user_portal_customer where user_id=\"%s\"";
	private static final String DELETE_USER_PORTAL_BY_ID = "delete from user_portal where id=\"%s\"";
	private static final String DELETE_INSTALL_BASE_BY_ID = "delete from install_base where id=\"%s\"";
	private static final String DELETE_INSTALLBASE_ADDRESS_BY_SITE_USE_ID = "delete from installbase_address where site_use_id=\"%s\"";
	private static final String DELETE_CUSTOMER_ADDRESS_BY_CUSTOMER_ID = "delete from customer_address where customer_id=\"%s\"";
	private static final String DELETE_CONTACT_BY_CUSTOMER_ID = "delete from contact where customer_id=\"%s\"";
	private static final String INSERT_CONTACT = "INSERT INTO contact (id, contact, contact_type, status, creation_date, update_date, customer_id, user_id, site_id, contact_id, name, origin, is_validated, contact_id_sf) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String INSERT_USER_PORTAL = "INSERT INTO user_portal (id, customer_code, document, password, login, created_at, is_active, customer_id) values (?, ?, ?, ?, ?, ?, ?, ?)";
	// INDUSTRIAL:
	private static final String URL = "jdbc:mysql://customerdev.cejk7gucxfmq.sa-east-1.rds.amazonaws.com/customerdev";
	private static final String USERNAME = "admin";
	private static final String PASSWORD = "Sb13icK83f8Mtm4yiGnv";
	// RESIDENTIAL:
//	private static final String URL = "jdbc:mysql://resaledev.cejk7gucxfmq.sa-east-1.rds.amazonaws.com/storedev";
//	private static final String USERNAME = "admin";
//	private static final String PASSWORD = "8VMPUIz0BQw70h2dUfKC";

	DatabaseConnection customerConnection = new DatabaseConnection();

	public void filterById(String id) {

		try {

			ResultSet resultSet = customerConnection.getStatement(URL, USERNAME, PASSWORD)
					.executeQuery(String.format(SELECT_CUSTOMER_BY_ID, id));

			resultSet.next();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteRelUserPortalCustomerByUserId(String id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_REL_USER_PORTAL_CUSTOMER_BY_USER_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteUserPortalById(String id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_USER_PORTAL_BY_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteInstallBase(String id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_INSTALL_BASE_BY_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteInstallbaseAddressBySiteUseId(Integer siteUseId) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_INSTALLBASE_ADDRESS_BY_SITE_USE_ID, siteUseId));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public String filterUserByLogin(String login) {

		try {

			ResultSet resultSet = customerConnection.getStatement(URL, USERNAME, PASSWORD)
					.executeQuery(String.format(SELECT_USER_BY_LOGIN, login));

			resultSet.next();

			return resultSet.getString("id");

		} catch (SQLException e) {

			e.printStackTrace();

		}
		return null;
	}

	public String filterUserPortalDocumentByUserId(String userId) {

		try {

			ResultSet resultSet = customerConnection.getStatement(URL, USERNAME, PASSWORD)
					.executeQuery(String.format(SELECT_USER_PORTAL_BY_ID, userId));

			resultSet.next();

			return resultSet.getString("document");

		} catch (SQLException e) {

			e.printStackTrace();

		}
		return null;
	}

	public String filterContactByDocument(String document) {

		try {

			ResultSet resultSet = customerConnection.getStatement(URL, USERNAME, PASSWORD)
					.executeQuery(String.format(SELECT_CONTACT_BY_DOCUMENT, document));

			resultSet.next();

			return resultSet.getString("contact");

		} catch (SQLException e) {

			e.printStackTrace();

		}
		return null;
	}

	public void deleteTokenByUserId(String id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_TOKEN_BY_USER_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteContactByLogin(String login) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_CONTACT_BY_LOGIN, login));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public String selectTokenByContactId(String contactId) {

		try {

			ResultSet resultSet = customerConnection.getStatement(URL, USERNAME, PASSWORD)
					.executeQuery(String.format(SELECT_TOKEN_BY_ID_CONTACT_ID, contactId));

			resultSet.next();

			return resultSet.getString("token");

		} catch (SQLException e) {

			e.printStackTrace();

		}

		return null;

	}

	public Integer insert(CustomerDomain customerDomain) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CUSTOMER);

			preparedStatement.setString(1, customerDomain.getId());
			preparedStatement.setLong(2, customerDomain.getCustomerId());
			preparedStatement.setString(3, customerDomain.getCode());
			preparedStatement.setString(4, customerDomain.getName());
			preparedStatement.setString(5, customerDomain.getFancyName());
			preparedStatement.setString(6, customerDomain.getDocument());
			preparedStatement.setString(7, customerDomain.getStatus());
			preparedStatement.setLong(8, customerDomain.getSourceOrganization());
			preparedStatement.setString(9, customerDomain.getType());
			preparedStatement.setDate(10, customerDomain.getCreationDate());
			preparedStatement.setDate(11, customerDomain.getUpdateDate());
			preparedStatement.setObject(12, customerDomain.getBirthDate());

			preparedStatement.execute();

			return customerDomain.getCustomerId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}

	public String insertUserPortal(UserDomain userDomain) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_PORTAL);

			preparedStatement.setString(1, userDomain.getId());
			preparedStatement.setString(2, userDomain.getCustomerCode());
			preparedStatement.setString(3, userDomain.getDocument());
			preparedStatement.setString(4, userDomain.getPassword());
			preparedStatement.setString(5, userDomain.getLogin());
			preparedStatement.setDate(6, userDomain.getCreatedAt());
			preparedStatement.setLong(7, userDomain.getIsActive());
			preparedStatement.setString(8, userDomain.getCustomerId());

			preparedStatement.execute();

			return userDomain.getId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}

	public void deleteById(Integer customerId) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_CUSTOMER_BY_CUSTOMER_ID, customerId));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteAddressById(String id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_CUSTOMER_ADDRESS_BY_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteContactById(String id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(String.format(DELETE_CONTACT_BY_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteTokenLogByUserId(String id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_TOKEN_LOG_BY_USER_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}
	
	public String insertUnit(UnitDomain unitDomain) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_UNIT);

			preparedStatement.setString(1, unitDomain.getId());
			preparedStatement.setLong(2, unitDomain.getInstanceId());
			preparedStatement.setString(3, unitDomain.getName());
			preparedStatement.setLong(4, unitDomain.getDueDay());
			preparedStatement.setObject(5, unitDomain.getDeliveryType());
			preparedStatement.setObject(6, unitDomain.getHasGasMeter());

			preparedStatement.execute();

			return unitDomain.getId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}
	
	public String insertAddress(CustomerAddressDomain customerAddressDomain) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CUSTOMER_ADDRESS);

			preparedStatement.setString(1, customerAddressDomain.getId());
			preparedStatement.setLong(2, customerAddressDomain.getSiteUseId());
			preparedStatement.setLong(3, customerAddressDomain.getSiteId());
			preparedStatement.setString(4, customerAddressDomain.getCode());
			preparedStatement.setString(5, customerAddressDomain.getAddressLine());
			preparedStatement.setString(6, customerAddressDomain.getAddress());
			preparedStatement.setString(7, customerAddressDomain.getAddressNumber());
			preparedStatement.setString(8, customerAddressDomain.getComplement());
			preparedStatement.setString(9, customerAddressDomain.getCountry());
			preparedStatement.setString(10, customerAddressDomain.getCity());
			preparedStatement.setString(11, customerAddressDomain.getStateCode());
			preparedStatement.setString(12, customerAddressDomain.getZipCode());
			preparedStatement.setString(13, customerAddressDomain.getAddressType());
			preparedStatement.setLong(14, customerAddressDomain.getCustomerId());
			preparedStatement.setString(15, customerAddressDomain.getDocument());
			preparedStatement.setString(16, customerAddressDomain.getStatus());
			preparedStatement.setObject(17, customerAddressDomain.getNeighborhood());
			preparedStatement.setObject(18, customerAddressDomain.getUnitId());

			preparedStatement.execute();

			return customerAddressDomain.getId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}

	public Integer insertInstallbaseAddress(InstallbaseAddressDomain installbaseAddressDomain) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INSTALLBASE_ADDRESS);

			preparedStatement.setString(1, installbaseAddressDomain.getId());
			preparedStatement.setLong(2, installbaseAddressDomain.getSiteUseId());
			preparedStatement.setString(3, installbaseAddressDomain.getCode());
			preparedStatement.setString(4, installbaseAddressDomain.getAddressLine());
			preparedStatement.setString(5, installbaseAddressDomain.getAddress());
			preparedStatement.setString(6, installbaseAddressDomain.getAddressNumber());
			preparedStatement.setString(7, installbaseAddressDomain.getComplement());
			preparedStatement.setString(8, installbaseAddressDomain.getCountry());
			preparedStatement.setString(9, installbaseAddressDomain.getCity());
			preparedStatement.setString(10, installbaseAddressDomain.getStateCode());
			preparedStatement.setString(11, installbaseAddressDomain.getZipCode());
			preparedStatement.setLong(12, installbaseAddressDomain.getCustomerId());
			preparedStatement.setLong(13, installbaseAddressDomain.getCustomerSiteId());

			preparedStatement.execute();

			return installbaseAddressDomain.getSiteUseId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}

	public String insertInstallBase(InstallBaseDomain installBaseDomain) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INSTALL_BASE);

			preparedStatement.setString(1, installBaseDomain.getId());
			preparedStatement.setString(2, installBaseDomain.getDescription());
			preparedStatement.setString(3, installBaseDomain.getCodeHierarchy());
			preparedStatement.setString(4, installBaseDomain.getStatus());
			preparedStatement.setDate(5, installBaseDomain.getActiveDateFrom());
			preparedStatement.setDate(6, installBaseDomain.getActiveDateTo());
			preparedStatement.setLong(7, installBaseDomain.getQuantity());
			preparedStatement.setString(8, installBaseDomain.getReference());
			preparedStatement.setString(9, installBaseDomain.getNickname());
			preparedStatement.setDate(10, installBaseDomain.getCreationDate());
			preparedStatement.setDate(11, installBaseDomain.getUpdateDate());
			preparedStatement.setLong(12, installBaseDomain.getInstallbaseAddressSiteUseId());

			preparedStatement.execute();

			return installBaseDomain.getId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}

	public String insertContact(ContactDomain contactDomain) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CONTACT);

			preparedStatement.setString(1, contactDomain.getId());
			preparedStatement.setString(2, contactDomain.getContact());
			preparedStatement.setString(3, contactDomain.getContactType());
			preparedStatement.setString(4, contactDomain.getStatus());
			preparedStatement.setDate(5, contactDomain.getCreationDate());
			preparedStatement.setDate(6, contactDomain.getUpdateDate());
			preparedStatement.setLong(7, contactDomain.getCustomerId());
			preparedStatement.setString(8, contactDomain.getUserId());
			preparedStatement.setLong(9, contactDomain.getSiteId());
			preparedStatement.setLong(10, contactDomain.getContactId());
			preparedStatement.setString(11, contactDomain.getName());
			preparedStatement.setString(12, contactDomain.getOrigin());
			preparedStatement.setObject(13, contactDomain.getIsValidated());
			preparedStatement.setString(14, contactDomain.getContactIdSf());

			preparedStatement.execute();

			return contactDomain.getId();

		} catch (Exception e) {

			e.printStackTrace();

		}

		return null;

	}

	public void deleteAddressByCustomerId(Integer id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_CUSTOMER_ADDRESS_BY_CUSTOMER_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteByCustomerId(Integer id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_CUSTOMER_BY_CUSTOMER_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void deleteContactByCustomerId(Integer id) {

		try (Connection connection = customerConnection.getConnection(URL, USERNAME, PASSWORD)) {

			PreparedStatement preparedStatement = connection
					.prepareStatement(String.format(DELETE_CONTACT_BY_CUSTOMER_ID, id));
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

}
