package cucumber.ultragaz.repository.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DatabaseConnection {

	private static final Logger LOG = LogManager.getLogger(DatabaseConnection.class);

	public Statement getStatement(String url, String username, String password) {

		try {
			return getConnection(url, username, password).createStatement();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return null;
	}

	public Connection getConnection(String url, String username, String password) {
		try {
			return DriverManager.getConnection(url + "?useSSL=false&user=" + username + "&password=" + password);
		} catch (SQLException e) {
			LOG.error(e.getMessage(), e);
		}

		return null;
	}
}