package cucumber.ultragaz.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import cucumber.ultragaz.AbstractFuncTest;

public class PropertiesFileReader extends AbstractFuncTest {

	Scanner ler = new Scanner(System.in);
	String part1 = "";
	String part2 = "";
	
	public String lerArquivo(String filePath) throws JsonParseException, JsonMappingException, IOException {
		
		
		try {
			
			FileReader arq = new FileReader(getClass().getResource(filePath).getFile());
			BufferedReader lerArq = new BufferedReader(arq);
			String linha = lerArq.readLine();
			
			jsonFile = linha;
			
			while (linha != null) {
			
				linha = lerArq.readLine();
				
				if (linha != null) {
				
					jsonFile += linha;
				
				}
				
			}
			
			arq.close();

		} catch (IOException e) {
			
			new IOException(e);
			
		}
				
		return jsonFile;
		
	}
	
	public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
	    
		Map<String, Object> retMap = new HashMap<String, Object>();

	    if(json != JSONObject.NULL) {
	    
	    	retMap = toMap(json);
	    
	    }
	    
	    return retMap;
	
	}

	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
	
		Map<String, Object> map = new HashMap<String, Object>();

	    Iterator<String> keysItr = object.keys();
	    
	    while(keysItr.hasNext()) {
	    
	    	String key = keysItr.next();
	        
	    	Object value = object.get(key);

	        if(value instanceof JSONArray) {
	        
	        	value = toList((JSONArray) value);
	        
	        }

	        else if(value instanceof JSONObject) {
	        
	        	value = toMap((JSONObject) value);
	        
	        }
	        
	        map.put(key, value);
	    
	    }
	    
	    return map;
	
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
	
		List<Object> list = new ArrayList<Object>();
	    
		for(int i = 0; i < array.length(); i++) {
	    
			Object value = array.get(i);
	        
			if(value instanceof JSONArray) {
	        
				value = toList((JSONArray) value);
	        
			}

	        else if(value instanceof JSONObject) {
	        
	        	value = toMap((JSONObject) value);
	        
	        }
	        
			list.add(value);
	    
		}
	    
		return list;
	
	}

}