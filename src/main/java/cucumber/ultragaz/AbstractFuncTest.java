package cucumber.ultragaz;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cucumber.api.Scenario;
import cucumber.ultragaz.domain.empresarial.customer.ContactDomain;
import cucumber.ultragaz.domain.empresarial.customer.CustomerAddressDomain;
import cucumber.ultragaz.domain.empresarial.customer.CustomerDomain;
import cucumber.ultragaz.domain.empresarial.customer.InstallBaseDomain;
import cucumber.ultragaz.domain.empresarial.customer.InstallbaseAddressDomain;
import cucumber.ultragaz.domain.empresarial.customer.UnitDomain;
import cucumber.ultragaz.domain.empresarial.customer.UserDomain;
import cucumber.ultragaz.domain.empresarial.invoice.CustomerInvoiceDomain;
import cucumber.ultragaz.domain.empresarial.invoice.InvoiceDomain;
import cucumber.ultragaz.service.CustomerService;
import cucumber.ultragaz.service.InvoiceService;
import cucumber.ultragaz.utils.PropertiesCpfCnpj;
import cucumber.ultragaz.utils.PropertiesUtil;
import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;
import gherkin.deps.com.google.gson.JsonParser;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.masterthought.cucumber.Configuration;

public abstract class AbstractFuncTest {

	private static final String CPF_LABEL = "CPF";
	public static Response requestJSON;
	public  RequestSpecification request = RestAssured.given();
	// protected static String domain = "https://api-ultragaz.sensedia.com";
	protected static String domain = "http://localhost:8088";
	protected static String apiName = "";
	protected String appClientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29";
	protected String appSecretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9";
	protected String responseAccessToken;
	protected String responseTokenType;
	protected String responseExpiresIn;
	protected String responseStatus;
	protected String responseError;
	protected String responseException;
	protected String responseMessage;
	protected String responsePath;
	protected String responseTimestamp;
	protected static LinkedHashMap<String, String> userParameter = new LinkedHashMap<String, String>();
	protected static LinkedHashMap<String, String> bodyParameter = new LinkedHashMap<String, String>();
	protected String jsonFile;
	protected JsonParser jp = new JsonParser();
	protected Gson gson = new GsonBuilder().setPrettyPrinting().create();
	protected static Scenario scenario;
	protected String url;
	protected String username;
	protected String password;
	protected static Integer CODE;
	protected static Integer CUSTOMER_ID;
	protected static Integer SITE_USE_ID;
	protected static String ID_CUSTOMER;
	protected static Integer EXTERNAL_ID = new PropertiesUtil().getRandomNumberByLength(6);
	protected static Integer SHIP_SITE_ID = new PropertiesUtil().getRandomNumberByLength(6);
	protected static String CNPJ;
	protected static String CPF;
	protected static Integer SITE_ID;
	protected static String UNIT_ID;
	protected static Integer UNIT_INSTANCE_ID = new PropertiesUtil().getRandomNumberByLength(7);
	protected static final String XML = "<nfeProc versao=\"4.00\"><NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infNFe versao=\"4.00\" Id=\"NFe35190761602199018917550990019473581127286535\"><ide><cUF>35</cUF><cNF>12728653</cNF><natOp>VENDA DE COMBUSTIVEL P/ CONSUMIDOR FINAL</natOp><mod>55</mod><serie>99</serie><nNF>1947358</nNF><dhEmi>2019-07-12T00:00:00-03:00</dhEmi><dhSaiEnt>2019-07-12T00:00:00-03:00</dhSaiEnt><tpNF>1</tpNF><idDest>1</idDest><cMunFG>3529401</cMunFG><tpImp>1</tpImp><tpEmis>1</tpEmis><cDV>5</cDV><tpAmb>2</tpAmb><finNFe>1</finNFe><indFinal>0</indFinal><indPres>9</indPres><procEmi>0</procEmi><verProc>Triangulus6.0.66.203</verProc></ide><emit><CNPJ>61602199018917</CNPJ><xNome>COMPANHIA ULTRAGAZ S A</xNome><enderEmit><xLgr>AV. ALBERTO S SAMPAIO</xLgr><nro>1098</nro><xBairro>CAPUAVA</xBairro><cMun>3529401</cMun><xMun>MAUA</xMun><UF>SP</UF><CEP>09380000</CEP><cPais>1058</cPais><xPais>BRASIL</xPais><fone>1140031616</fone></enderEmit><IE>442029223114</IE><CRT>3</CRT></emit><dest><CNPJ>44248862000103</CNPJ><xNome>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome><enderDest><xLgr>RUA GASTAO VIDIGAL</xLgr><nro>173</nro><xCpl>S C</xCpl><xBairro>ESTACAO</xBairro><cMun>3546405</cMun><xMun>SANTA CRUZ DO RIO PARDO</xMun><UF>SP</UF><CEP>18900000</CEP><cPais>1058</cPais><xPais>BRASIL</xPais></enderDest><indIEDest>1</indIEDest><IE>612007818113</IE></dest><entrega><CNPJ>44248862000103</CNPJ><xLgr>RUA GASTAO VIDIGAL</xLgr><nro>173</nro><xCpl>BR</xCpl><xBairro>ESTACAO</xBairro><cMun>3546405</cMun><xMun>SANTA CRUZ DO RIO PARDO</xMun><UF>SP</UF></entrega><det nItem=\"1\"><prod><cProd>0120014</cProd><cEAN>SEM GTIN</cEAN><xProd>ONU 1075 GAS(ES) DE PETROLEO, LIQUEFEITO(S), 2.1, GPL/GLP GRANEL</xProd><NCM>27111910</NCM><CEST>0601107</CEST><CFOP>5656</CFOP><uCom>KG</uCom><qCom>10.0000</qCom><vUnCom>5.9948000000</vUnCom><vProd>59.95</vProd><cEANTrib>SEM GTIN</cEANTrib><uTrib>KG</uTrib><qTrib>10.0000</qTrib><vUnTrib>5.9948000000</vUnTrib><indTot>1</indTot><comb><cProdANP>210203001</cProdANP><descANP>GLP</descANP><pGLP>52.4927</pGLP><pGNn>3.5406</pGNn><pGNi>43.9667</pGNi><vPart>5.32</vPart><UFCons>SP</UFCons></comb></prod><imposto><ICMS><ICMSST><orig>2</orig><CST>60</CST><vBCSTRet>37.48</vBCSTRet><pST>0.0000</pST><vICMSSubstituto>0.00</vICMSSubstituto><vICMSSTRet>6.75</vICMSSTRet><vBCSTDest>0.00</vBCSTDest><vICMSSTDest>0.00</vICMSSTDest><pRedBCEfet>33.3300</pRedBCEfet><vBCEfet>39.97</vBCEfet><pICMSEfet>18.0000</pICMSEfet><vICMSEfet>7.19</vICMSEfet></ICMSST></ICMS><IPI><cEnq>004</cEnq><IPINT><CST>53</CST></IPINT></IPI><PIS><PISNT><CST>04</CST></PISNT></PIS><COFINS><COFINSNT><CST>04</CST></COFINSNT></COFINS></imposto></det><total><ICMSTot><vBC>0.00</vBC><vICMS>0.00</vICMS><vICMSDeson>0.00</vICMSDeson><vICMSUFRemet>0.00</vICMSUFRemet><vFCP>0.00</vFCP><vBCST>0.00</vBCST><vST>0.00</vST><vFCPST>0.00</vFCPST><vFCPSTRet>0.00</vFCPSTRet><vProd>59.95</vProd><vFrete>0.00</vFrete><vSeg>0.00</vSeg><vDesc>0.00</vDesc><vII>0.00</vII><vIPI>0.00</vIPI><vIPIDevol>0.00</vIPIDevol><vPIS>0.00</vPIS><vCOFINS>0.00</vCOFINS><vOutro>0.00</vOutro><vNF>59.95</vNF></ICMSTot></total><transp><modFrete>3</modFrete><transporta><CNPJ>61602199018917</CNPJ><xNome>CIA ULTRAGAZ S/A</xNome><IE>442029223114</IE><xEnder>AV. ALBERTO SOARES SAMPAIO</xEnder><xMun>MAUA</xMun><UF>SP</UF></transporta><vol><qVol>10</qVol><esp>KG</esp><pesoL>10.000</pesoL><pesoB>10.000</pesoB></vol></transp><cobr><fat><nFat>1947358</nFat><vOrig>59.95</vOrig><vDesc>0.00</vDesc><vLiq>59.95</vLiq></fat><dup><nDup>001</nDup><dVenc>2019-07-19</dVenc><vDup>59.95</vDup></dup></cobr><pag><detPag><indPag>1</indPag><tPag>15</tPag><vPag>59.95</vPag></detPag></pag><infAdic><infAdFisco>ICMS recolhido por substituicao tributaria cf. art. 412, inc II e Anexo II, art 8 do RICMS/SP-Decreto 45490/00  DECLARO QUE OS PRODUTOS PERIGOSOS ESTAO ADEQUADAMENTE CLASSIFICADOS, EMBALADOS, IDENTIFICADOS, E ESTIVADOS PARA SUPORTAR OS RISCOS DAS OPERACOES DE TRANSPORTE E QUE ATENDEM AS EXIGENCIAS DA REGULAMENTACAO.</infAdFisco><infCpl>## BASE DE CALCULO DO ICMS ANTECIPADO :           37,48         ICMS RETIDO ANTECIPADO :            6,75 ## EQUIPE : 300 ## NFS TALOES QUE ACOMPANHAM :  NEM  194351  A  194400 NEM   198301  A  198350 ## PLACA : DZP5333 ## ORDEM DE VENDA : 69913393 ## CORRENTISTA : 300644 LEI DA TRANSPARENCIA FISCAL 12741/2012 - IMPOSTO FEDERAL R$ 167,70 POR TONELADA- IMPOSTO ESTADUAL CONFORME DESTACADO NO CAMPO ICMS-ST</infCpl></infAdic><infRespTec><CNPJ>01471899000193</CNPJ><xContato>Thiago Taffi</xContato><email>triangulus@config.com.br</email><fone>1155018300</fone></infRespTec></infNFe><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\" /><Reference URI=\"#NFe35190761602199018917550990019473581127286535\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\" /><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /><DigestValue>OxNPSoNirq2Ct4vT+842dsf8jpM=</DigestValue></Reference></SignedInfo><SignatureValue>NYOgjldcxKK4VvlVHljd3EaBxNOYE6qdE6kQK+qPS97oFxrpqtRupfSflzLqSELoVrE1il2p7kwytQjwST9NCOTfOTyTr+9sRl7j+53iiysmHxX4ZiGPoH+ICvGu9RauhPk9Dv1iJG833cOqE+3dr1WZyizu+P/889HCUv9ScVTbCQtROUB4NqGYxvN/ZGwAaOBp46XXISQ13nWjhNG5UIpcvDxmnESPAAULbCZP8VvPLQMEEmsF25c2c31PyWMXsiBJ1/sUqz1l7onpBoT31wzcLQrim3R6F5Wu4MnorJJJqsCrFSUnEdJ8Dtnq+wiozLYT9XCKZBwEdCsKZ9rkGw==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIHhDCCBWygAwIBAgIILaRGRpOzlPswDQYJKoZIhvcNAQELBQAwTDELMAkGA1UEBhMCQlIxEzARBgNVBAoMCklDUC1CcmFzaWwxKDAmBgNVBAMMH1NFUkFTQSBDZXJ0aWZpY2Fkb3JhIERpZ2l0YWwgdjUwHhcNMTkwMzI1MTc0ODAwWhcNMjAwMzI0MTc0ODAwWjCB4TELMAkGA1UEBhMCQlIxEzARBgNVBAoMCklDUC1CcmFzaWwxFDASBgNVBAsMCyhFTSBCUkFOQ08pMRgwFgYDVQQLDA8wMDAwMDEwMDg5MTk4MDQxFDASBgNVBAsMCyhFTSBCUkFOQ08pMRQwEgYDVQQLDAsoRU0gQlJBTkNPKTEUMBIGA1UECwwLKEVNIEJSQU5DTykxFDASBgNVBAsMCyhFTSBCUkFOQ08pMRQwEgYDVQQLDAsoRU0gQlJBTkNPKTEfMB0GA1UEAwwWQ09NUEFOSElBIFVMVFJBR0FaIFMgQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJonuWWOiPN6fHA3ShnO6tyTQF/l1EayJLRvKD20wADZ1Vv3yB39FcrVtDaeGNS1ikm3Aeb7/88MIgSPPm6fiv2snq8RaZUTzrkpCPG5wYEizofUPhhFaBHqmJiO9kvCY6vlr5kflVqD4ELBSYXnJTG4Yz1ZiADdyKovM+18qWB8I7Fz59+imslpvmauCZa6SKQg4FwaZ5DUvP2JuFPfBelp6XI2eKyNFtNoCrcz9c2xbtMQNsRfUpU3819d6yDnGdkMNhDAiZRvrt4Uwa/pLZg6yPJEw7u1wVOg2BDLE0veCoYekZXrCx/GVbBOYjTG1MrZ91UdORRMyvRdkCOTjr8CAwEAAaOCAtIwggLOMB8GA1UdIwQYMBaAFFZ1r0pzstgIxH37bCgcEdX3wajMMIGXBggrBgEFBQcBAQSBijCBhzBHBggrBgEFBQcwAoY7aHR0cDovL3d3dy5jZXJ0aWZpY2Fkb2RpZ2l0YWwuY29tLmJyL2NhZGVpYXMvc2VyYXNhY2R2NS5wN2IwPAYIKwYBBQUHMAGGMGh0dHA6Ly9vY3NwLmNlcnRpZmljYWRvZGlnaXRhbC5jb20uYnIvc2VyYXNhY2R2NTCBsQYDVR0RBIGpMIGmgRZBTkFHSUxAVUxUUkFHQVouQ09NLkJSoD4GBWBMAQMEoDUTMzE5MTIxOTYyMDIxNTU1Njk4NDAwMDAwMDAwMDAwMDAwMDAwMDAxMzU1MTc4OFNTUCBTUKAYBgVgTAEDAqAPEw1BTkEgTUFSSUEgR0lMoBkGBWBMAQMDoBATDjYxNjAyMTk5MDAwMTEyoBcGBWBMAQMHoA4TDDAwMDAwMDAwMDAwMDBxBgNVHSAEajBoMGYGBmBMAQIBBjBcMFoGCCsGAQUFBwIBFk5odHRwOi8vcHVibGljYWNhby5jZXJ0aWZpY2Fkb2RpZ2l0YWwuY29tLmJyL3JlcG9zaXRvcmlvL2RwYy9kZWNsYXJhY2FvLXNjZC5wZGYwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMIGbBgNVHR8EgZMwgZAwSaBHoEWGQ2h0dHA6Ly93d3cuY2VydGlmaWNhZG9kaWdpdGFsLmNvbS5ici9yZXBvc2l0b3Jpby9sY3Ivc2VyYXNhY2R2NS5jcmwwQ6BBoD+GPWh0dHA6Ly9sY3IuY2VydGlmaWNhZG9zLmNvbS5ici9yZXBvc2l0b3Jpby9sY3Ivc2VyYXNhY2R2NS5jcmwwHQYDVR0OBBYEFFjVmnz/CMhMfoEAnv8DSbZGueuqMA4GA1UdDwEB/wQEAwIF4DANBgkqhkiG9w0BAQsFAAOCAgEAeTAZBluT3qvUfKiCyhPPBkm5Icae9quY8FFtYrvL+5LaJVTTwjtSsrCdQrBPs1LRY8FHki2s8/TcNAcWM/eixKL/GBBJfYLDyVGpkRv8nMQi4R1ORbjQKX7rWD/EOunPAudofZhKbaOIrO9ZQBoXUqfkOCo41cyhnoG/GWFnBPdqkri3Szl8Rk3+ahtSXt3yEg/SneD6XgxSgQPnvRbTYmddi0Uht0U9K3i1+UD7brIEzI0i9Ia6UhECMXV+YHf6oS5NZMYZvKtcjYjfq51ZhJKTSDqvJBz0yn45gBtmclhZMEKAZqf9JCTEP5ZbHNf/sgab574mzDfyiOpPYpp5eNBtIRKLrlyn/oMVP+Kelz8o+K15gKR8sjG9EIE0iOFB2NK/8M9TyUxKbmQIlyAfpnzYLxLUqnxLW4U75VYgezG/RCtg38Rd25S/nxou8tolEv9guHJRvr24tOXDKSqk0spHYLUVz6E/3a6bK9yboIzaOQfDbs9zeGntsAnSghsbVIG/RS0T7DzBLkbEr8sG5uG5ETqV8SLakphVHQNfKosSi0FyyHxkhCeJu9U1dgQ7dljxggIG+i4Fpg+jLFmeIACis0hsC9Day1zfA7OP0eB5kFX/BbSmYd67yV3ms164AwwdBH3/A9/NbAkpjVcyoaUPtRIRPEJJ8EZgcg/h7KM=</X509Certificate></X509Data></KeyInfo></Signature></NFe><protNFe versao=\"4.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infProt><tpAmb>2</tpAmb><verAplic>SP_NFE_PL009_V4</verAplic><chNFe>35190761602199018917550990019473581127286535</chNFe><dhRecbto>2019-07-12T17:48:10-03:00</dhRecbto><nProt>135190004975085</nProt><digVal>OxNPSoNirq2Ct4vT+842dsf8jpM=</digVal><cStat>100</cStat><xMotivo>Autorizado o uso da NF-e</xMotivo></infProt></protNFe></nfeProc>";
	protected static Integer trxId = new PropertiesUtil().getRandomNumberByLength(9);


	protected String replaceVariablesValues(String text) {

		String xp = "(\\$\\{[^}]+\\})";

		StringBuffer stringBuffer = new StringBuffer();

		Pattern pattern = Pattern.compile(xp);

		Matcher matcher = pattern.matcher(text);

		while (matcher.find()) {

			String repString = userParameter.get(matcher.group(1));

			if (repString != null) {

				matcher.appendReplacement(stringBuffer, repString);

			}

		}

		matcher.appendTail(stringBuffer);

		return stringBuffer.toString();
	}

	protected void generateReport() {

		File reportOutputDirectory = new File("target");
		List<String> jsonFiles = new ArrayList<>();
		jsonFiles.add("json:target/cucumber.json");

		String buildNumber = "1";
		String projectName = "cucumberProject";

		Configuration configuration = new Configuration(reportOutputDirectory, projectName);
		// optional configuration - check javadoc for details
		configuration.setBuildNumber(buildNumber);

	}

	public static Integer insertCustomer(String name) {

		Date data = new Date(System.currentTimeMillis());
		CustomerService customerService = new CustomerService();
		CustomerDomain customerDomain = new CustomerDomain();
		CODE = new PropertiesUtil().getRandomNumberByLength(6);
		CUSTOMER_ID = new PropertiesUtil().getRandomNumberByLength(6);
		CNPJ = new PropertiesCpfCnpj().cnpj();
		SITE_ID = new PropertiesUtil().getRandomNumberByLength(7);

		customerDomain.setId(UUID.randomUUID().toString());
		customerDomain.setCustomerId(CUSTOMER_ID);
		customerDomain.setCode(CODE.toString());
		customerDomain.setName("CUSTOMER TEST" + name);
		customerDomain.setFancyName("CUSTOMER SENSEDIA TEST SA" + name);
		customerDomain.setDocument(CNPJ);
		customerDomain.setStatus("A");
		customerDomain.setSourceOrganization(430);
		customerDomain.setType("EMP");
		customerDomain.setCreationDate(data);
		customerDomain.setUpdateDate(data);
		customerDomain.setBirthDate(null);
		Integer idCustomer = customerService.insert(customerDomain);

		userParameter.put("${" + "CNPJ" + name + "}", CNPJ);
		userParameter.put("${" + "CODE" + name + "}", CODE.toString());

		return idCustomer;
	}

	public static Integer insertCustomerMi(String name, String document) {

		Date data = new Date(System.currentTimeMillis());
		CustomerService customerService = new CustomerService();
		CustomerDomain customerDomain = new CustomerDomain();
		CODE = new PropertiesUtil().getRandomNumberByLength(6);
		CUSTOMER_ID = new PropertiesUtil().getRandomNumberByLength(6);
		SITE_ID = new PropertiesUtil().getRandomNumberByLength(7);

		if (CPF_LABEL.equals(document)) {
			CPF = new PropertiesCpfCnpj().cpf();
			userParameter.put("${" + document + name + "}", CPF);
			customerDomain.setDocument(CPF);
		} else {
			CNPJ = new PropertiesCpfCnpj().cnpj();
			userParameter.put("${" + document + name + "}", CNPJ);
			customerDomain.setDocument(CNPJ);
		}

		customerDomain.setId(UUID.randomUUID().toString());
		customerDomain.setCustomerId(CUSTOMER_ID);
		customerDomain.setCode(CODE.toString());
		customerDomain.setName("CUSTOMER TEST" + name);
		customerDomain.setFancyName("CUSTOMER SENSEDIA TEST SA" + name);
		customerDomain.setStatus("A");
		customerDomain.setSourceOrganization(430);
		customerDomain.setType("MI");
		customerDomain.setCreationDate(data);
		customerDomain.setUpdateDate(data);
		customerDomain.setBirthDate(null);
		Integer idCustomer = customerService.insert(customerDomain);

		userParameter.put("${" + "CODE" + name + "}", CODE.toString());

		return idCustomer;
	}

	public static String insertUser(String name) {

		UserDomain userDomain = new UserDomain();
		CustomerService customerService = new CustomerService();
		Date data = new Date(System.currentTimeMillis());

		userDomain.setId(UUID.randomUUID().toString());
		userDomain.setCustomerCode(CODE.toString());
		userDomain.setDocument(CNPJ);
		userDomain.setPassword("FLm56+xDMYEdLULcwJB3GTgZTmhnpdWZEZLGUdQ5204=");
		userDomain.setLogin("usertestlogin" + name + "@sensedia.com");
		userDomain.setCreatedAt(data);
		userDomain.setIsActive(1);
		userDomain.setCustomerId(ID_CUSTOMER);
		String userId = customerService.insertUserPortal(userDomain);

		userParameter.put("${" + "userId" + name + "}", userId);

		return userId;
	}

	public void filterCustomerById(String id) {

		CustomerService customerService = new CustomerService();
		customerService.filterById(id);

	}

	public static void deleteCustomerById(Integer customerId) {

		CustomerService customerService = new CustomerService();
		customerService.deleteById(customerId);

	}

	public static String selectTokenByContactId(String contacId) {

		CustomerService customerService = new CustomerService();

		return customerService.selectTokenByContactId(contacId);

	}

	public static void deleteCustomerAddressById(String id) {

		CustomerService customerService = new CustomerService();
		customerService.deleteAddressById(id);

	}

	public static void deleteContactById(String id) {

		CustomerService customerService = new CustomerService();
		customerService.deleteContactById(id);

	}

	public String filterUserByLogin(String login) {

		CustomerService customerService = new CustomerService();
		return customerService.filterUserByLogin(login);

	}

	public String filterContactByDocument(String document) {

		CustomerService customerService = new CustomerService();
		return customerService.filterContactByDocument(document);

	}

	public String filterUserPortalDocumentByUserId(String userId) {

		CustomerService customerService = new CustomerService();
		return customerService.filterUserPortalDocumentByUserId(userId);

	}

	public static void deleteTokenLogByUserId(String userId) {

		CustomerService customerService = new CustomerService();
		customerService.deleteTokenLogByUserId(userId);

	}

	public void deleteTokenByUserId(String userId) {

		CustomerService customerService = new CustomerService();
		customerService.deleteTokenByUserId(userId);

	}

	public void deleteContactByLogin(String login) {

		CustomerService invoiceService = new CustomerService();
		invoiceService.deleteContactByLogin(login);

	}

	public static void deleteRelUserPortalCustomerByUserId(String userId) {

		CustomerService customerService = new CustomerService();
		customerService.deleteRelUserPortalCustomerByUserId(userId);

	}

	public static void deleteUserPortalById(String userId) {

		CustomerService customerService = new CustomerService();
		customerService.deleteUserPortalById(userId);

	}

	public static void deleteInstallBase(String installBaseId) {

		CustomerService customerService = new CustomerService();
		customerService.deleteInstallBase(installBaseId);

	}

	public static void deleteInstallbaseAddressBySiteUseId(Integer siteUseId) {

		CustomerService customerService = new CustomerService();
		customerService.deleteInstallbaseAddressBySiteUseId(siteUseId);

	}

	
	
	public static void insertUnit(String name, String document) {

		CustomerService customerService = new CustomerService();
		UNIT_ID = UUID.randomUUID().toString();

		UnitDomain unitDomain = new UnitDomain();

		unitDomain.setId(UNIT_ID);
		unitDomain.setInstanceId(UNIT_INSTANCE_ID);
		unitDomain.setName("APARTAMENTO" + name + document);
		unitDomain.setDueDay(15);
		unitDomain.setDeliveryType("CORREIO");
		unitDomain.setHasGasMeter(null);

		customerService.insertUnit(unitDomain);

	}
	
	public static void insertCustomerAddress(String addressType, String document) {

		CustomerService customerService = new CustomerService();

		CustomerAddressDomain customerAddressDomain = new CustomerAddressDomain();

		if (CPF_LABEL.equals(document)) {
			customerAddressDomain.setDocument(CPF);
		} else {
			customerAddressDomain.setDocument(CNPJ);
		}
		customerAddressDomain.setId(UUID.randomUUID().toString());
		customerAddressDomain.setSiteUseId(new PropertiesUtil().getRandomNumberByLength(7));
		customerAddressDomain.setSiteId(SITE_ID);
		customerAddressDomain.setCode(CODE.toString());
		customerAddressDomain.setAddressLine("Jardim Bom Retiro");
		customerAddressDomain.setAddress("Rua Domingos Conrado");
		customerAddressDomain.setAddressNumber("528");
		customerAddressDomain.setComplement("Casa");
		customerAddressDomain.setCountry("BRASIL");
		customerAddressDomain.setCity("Sumaré");
		customerAddressDomain.setStateCode("SP");
		customerAddressDomain.setZipCode("13181643");
		customerAddressDomain.setAddressType(addressType);
		customerAddressDomain.setCustomerId(CUSTOMER_ID);
		customerAddressDomain.setStatus("A");
		customerAddressDomain.setNeighborhood(null);
		customerAddressDomain.setUnitId(UNIT_ID);

		customerService.insertAddress(customerAddressDomain);

		userParameter.put("${" + "customerSiteId" + "}", SITE_ID.toString());

	}

	public static Integer insertInstallbaseAddress() {

		CustomerService customerService = new CustomerService();
		InstallbaseAddressDomain installbaseAddressDomain = new InstallbaseAddressDomain();

		installbaseAddressDomain.setId(UUID.randomUUID().toString());
		installbaseAddressDomain.setSiteUseId(SITE_USE_ID);
		installbaseAddressDomain.setCode(CODE.toString());
		installbaseAddressDomain.setAddressLine("Jardim Bom Retiro");
		installbaseAddressDomain.setAddress("Rua Domingos Conrado");
		installbaseAddressDomain.setAddressNumber("528");
		installbaseAddressDomain.setComplement("Casa");
		installbaseAddressDomain.setCountry("BRASIL");
		installbaseAddressDomain.setCity("Sumaré");
		installbaseAddressDomain.setStateCode("SP");
		installbaseAddressDomain.setZipCode("13181643");
		installbaseAddressDomain.setCustomerId(CUSTOMER_ID);
		installbaseAddressDomain.setCustomerSiteId(SITE_ID);

		return customerService.insertInstallbaseAddress(installbaseAddressDomain);

	}

	public static String insertInstallBase(Integer installbaseAddressSiteId) {

		CustomerService customerService = new CustomerService();
		InstallBaseDomain installBaseDomain = new InstallBaseDomain();
		Date date = new Date(System.currentTimeMillis());

		installBaseDomain.setId(UUID.randomUUID().toString());
		installBaseDomain.setDescription("BIM429321");
		installBaseDomain.setCodeHierarchy("INSTALACAO");
		installBaseDomain.setStatus("A");
		installBaseDomain.setActiveDateFrom(date);
		installBaseDomain.setActiveDateTo(null);
		installBaseDomain.setQuantity(1);
		installBaseDomain.setReference("BIM429321");
		installBaseDomain.setNickname("INSTALACAO UNICA");
		installBaseDomain.setCreationDate(date);
		installBaseDomain.setUpdateDate(date);
		installBaseDomain.setInstallbaseAddressSiteUseId(installbaseAddressSiteId);

		return customerService.insertInstallBase(installBaseDomain);

	}

	public static void insertContact(String contactType, String name) {

		CustomerService customerService = new CustomerService();
		ContactDomain contactDomain = new ContactDomain();
		Date date = new Date(System.currentTimeMillis());
		String contact = "19982722791";
		Integer isValidatedEmail = 0;

		if (contactType == "EMAIL") {

			contact = "emailtest" + name + "@sensedia.com";
			contactDomain.setIsValidated(0);

		} else if (contactType == "LOGIN") {
			contact = "usertestlogin" + name + "@sensedia.com";
			isValidatedEmail = 1;
		}

		contactDomain.setId(UUID.randomUUID().toString());
		contactDomain.setContact(contact);
		contactDomain.setContactType(contactType);
		contactDomain.setStatus("A");
		contactDomain.setCreationDate(date);
		contactDomain.setUpdateDate(date);
		contactDomain.setCustomerId(CUSTOMER_ID);
		contactDomain.setUserId(null);
		contactDomain.setSiteId(SITE_ID);
		contactDomain.setContactId(new PropertiesUtil().getRandomNumberByLength(7));
		contactDomain.setName("Sensedia Customer Test" + name);
		contactDomain.setOrigin(null);
		contactDomain.setIsValidated(isValidatedEmail);
		contactDomain.setContactIdSf(null);

		customerService.insertContact(contactDomain);

	}

	public static InvoiceDomain insertInvoice(String name) {

		InvoiceService invoiceService = new InvoiceService();
		InvoiceDomain invoiceDomain = new InvoiceDomain();
		Date date = new Date(System.currentTimeMillis());
		Integer invoiceNumber = new PropertiesUtil().getRandomNumberByLength(7);

		invoiceDomain.setId(UUID.randomUUID().toString());
		invoiceDomain.setTrxId(trxId = trxId + 1);
		invoiceDomain.setOrganization("5");
		invoiceDomain.setInvoiceNumber(invoiceNumber.toString());
		invoiceDomain.setSeries("99");
		invoiceDomain.setEmissionDate(date);
		invoiceDomain.setSource("NFF");
		invoiceDomain.setType("BENDER DIAS IND" + name);
		invoiceDomain.setAccessKey("3519076160219901891755099001947358012728653");
		invoiceDomain.setStatus("OP");
		invoiceDomain.setXml(XML);
		invoiceDomain.setAmount(59.95);
		invoiceDomain.setCreationDate(date);
		invoiceDomain.setUpdateDate(date);

		invoiceService.insert(invoiceDomain);

		return invoiceDomain;

	}

	public static void insertCustomerInvoice(Integer trxId, String name) {

		InvoiceService invoiceService = new InvoiceService();
		CustomerInvoiceDomain customerInvoiceDomain = new CustomerInvoiceDomain();

		customerInvoiceDomain.setId(UUID.randomUUID().toString());
		customerInvoiceDomain.setExternalId(EXTERNAL_ID);
		customerInvoiceDomain.setShipSiteId(SHIP_SITE_ID);
		customerInvoiceDomain.setBillSiteId(SHIP_SITE_ID);
		customerInvoiceDomain.setCode(CODE.toString());
		customerInvoiceDomain.setSiteCode(SITE_ID.toString());
		customerInvoiceDomain.setName("BENDER DIAS SA" + name);
		customerInvoiceDomain.setDocument(CNPJ);
		customerInvoiceDomain.setAddress("Rua Domingos Conrado");
		customerInvoiceDomain.setAddressNumber("528");
		customerInvoiceDomain.setComplement("Casa");
		customerInvoiceDomain.setCounty("Vila Velha");
		customerInvoiceDomain.setCity("Nova Odessa");
		customerInvoiceDomain.setStateCode("RR");
		customerInvoiceDomain.setZipCode("13181643");
		customerInvoiceDomain.setCountry("BRASIL");
		customerInvoiceDomain.setTrxId(trxId);
		customerInvoiceDomain.setSiteCodeBill(SITE_ID.toString());
		customerInvoiceDomain.setSiteCodeShip(CODE.toString());

		invoiceService.insertCustomerInvoice(customerInvoiceDomain);

	}

	public static void deleteCustomerInvoiceByTrxId(Integer id) {

		InvoiceService invoiceService = new InvoiceService();
		invoiceService.deleteCustomerInvoiceByTrxId(id);

	}

	public static void deleteInvoiceById(String id) {

		InvoiceService invoiceService = new InvoiceService();
		invoiceService.deleteInvoiceById(id);

	}

	public static Integer createUserCustomerContact(String name, String document) {

		// Cadastrar customer
		Integer customerId = insertCustomer(name);

		insertUnit(name, document);
		
		// Cadastrar endereços de cobrança para customer
		insertCustomerAddress("BILL_TO", document);
		insertCustomerAddress("SHIP_TO", document);
		insertCustomerAddress("CSTU_COBRANCA", document);

		// Cadastrar contatos para customer
		insertContact("PHONE", name);
		insertContact("EMAIL", name);
		

		return customerId;
	}

	public static Integer createUserMiCustomerContact(String name, String document) {

		// Cadastrar customer
		Integer customerId = insertCustomerMi(name, document);
		
		insertUnit(name, document);

		// Cadastrar endereços de cobrança para customer
		insertCustomerAddress("BILL_TO", document);
		insertCustomerAddress("SHIP_TO", document);
		insertCustomerAddress("CSTU_COBRANCA", document);

		// Cadastrar contatos para customer
		insertContact("PHONE", name);
		insertContact("EMAIL", name);

		return customerId;
	}

	public static String createUserLogin(String name) {

		insertContact("LOGIN", name);

		String userId = insertUser(name);

		return userId;
	}

	public static InvoiceDomain createInvoiceCustomerInvoice(String name) {

		// Cadastrar invoices para customer
		InvoiceDomain invoiceObject = insertInvoice(name);

		// Cadastrar relação de customer com invoices cadastradas
		insertCustomerInvoice(invoiceObject.getTrxId(), name);

		return invoiceObject;
	}

	public static void deleteUserCustomerContact(Integer customerId) {

		deleteCustomerByCustomerId(customerId);

		deleteCustomerAddressByCustomerId(customerId);

		deleteContactByCustomerId(customerId);

	}

	public static void deleteCustomerAddressByCustomerId(Integer id) {

		CustomerService customerService = new CustomerService();
		customerService.deleteAddressByCustomerId(id);

	}

	public static void deleteCustomerByCustomerId(Integer id) {

		CustomerService customerService = new CustomerService();
		customerService.deleteByCustomerId(id);

	}

	public static void deleteContactByCustomerId(Integer id) {

		CustomerService customerService = new CustomerService();
		customerService.deleteContactByCustomerId(id);

	}

	public static void setCucumberInvoiceEnvironment(String name, String penultimateInvoiceNumber,
			String lastInvoiceNumber) {

		userParameter.put("${" + "penultimateInvoiceNumber" + name + "}", penultimateInvoiceNumber);
		userParameter.put("${" + "lastInvoiceNumber" + name + "}", lastInvoiceNumber);

	}

	public static void deleteInvoiceCustomerInvoice(InvoiceDomain invoiceUser, InvoiceDomain invoiceDoisUser) {

		deleteCustomerInvoiceByTrxId(invoiceUser.getTrxId());
		deleteCustomerInvoiceByTrxId(invoiceDoisUser.getTrxId());

		deleteInvoiceById(invoiceUser.getId());
		deleteInvoiceById(invoiceDoisUser.getId());

	}

}
