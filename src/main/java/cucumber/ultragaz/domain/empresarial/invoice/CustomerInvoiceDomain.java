package cucumber.ultragaz.domain.empresarial.invoice;

public class CustomerInvoiceDomain {

	private String id;
	private Integer externalId;
	private Integer shipSiteId;
	private Integer billSiteId;
	private String code;
	private String siteCode;
	private String name;
	private String document;
	private String address;
	private String addressNumber;
	private String complement;
	private String county;
	private String city;
	private String stateCode;
	private String zipCode;
	private String country;
	private Integer trxId;
	private String siteCodeBill;
	private String siteCodeShip;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getExternalId() {
		return externalId;
	}

	public void setExternalId(Integer externalId) {
		this.externalId = externalId;
	}

	public Integer getShipSiteId() {
		return shipSiteId;
	}

	public void setShipSiteId(Integer shipSiteId) {
		this.shipSiteId = shipSiteId;
	}

	public Integer getBillSiteId() {
		return billSiteId;
	}

	public void setBillSiteId(Integer billSiteId) {
		this.billSiteId = billSiteId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getTrxId() {
		return trxId;
	}

	public void setTrxId(Integer trxId) {
		this.trxId = trxId;
	}

	public String getSiteCodeBill() {
		return siteCodeBill;
	}

	public void setSiteCodeBill(String siteCodeBill) {
		this.siteCodeBill = siteCodeBill;
	}

	public String getSiteCodeShip() {
		return siteCodeShip;
	}

	public void setSiteCodeShip(String siteCodeShip) {
		this.siteCodeShip = siteCodeShip;
	}

}