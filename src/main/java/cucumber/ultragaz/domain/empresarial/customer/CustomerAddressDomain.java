package cucumber.ultragaz.domain.empresarial.customer;

public class CustomerAddressDomain {

	private String id;
	private Integer siteUseId;
	private Integer siteId;
	private String code;
	private String addressLine;
	private String address;
	private String addressNumber;
	private String complement;
	private String country;
	private String city;
	private String stateCode;
	private String zipCode;
	private String addressType;
	private Integer customerId;
	private String document;
	private String status;
	private String unitId;
	private String unitName;
	private Integer dueDay;
	private String deliveryType;
	private String neighborhood;
	private Integer hasGasMeter;

	public Integer getHasGasMeter() {

		return hasGasMeter;

	}

	public void setHasGasMeter(Integer hasGasMeter) {

		this.hasGasMeter = hasGasMeter;

	}

	public Integer getDueDay() {

		return dueDay;

	}

	public void setDueDay(Integer dueDay) {

		this.dueDay = dueDay;

	}

	public String getUnitId() {

		return unitId;

	}

	public void setUnitId(String UNIT_ID) {

		this.unitId = UNIT_ID;

	}

	public String getNeighborhood() {

		return neighborhood;

	}

	public void setNeighborhood(String neighborhood) {

		this.neighborhood = neighborhood;

	}

	public String getDeliveryType() {

		return deliveryType;

	}

	public void setDeliveryType(String deliveryType) {

		this.deliveryType = deliveryType;

	}

	public String getUnitName() {

		return unitName;

	}

	public void setUnitName(String unitName) {

		this.unitName = unitName;

	}

	public String getId() {

		return id;

	}

	public void setId(String id) {

		this.id = id;

	}

	public Integer getSiteUseId() {

		return siteUseId;

	}

	public void setSiteUseId(Integer siteUseId) {

		this.siteUseId = siteUseId;

	}

	public Integer getSiteId() {

		return siteId;

	}

	public void setSiteId(Integer siteId) {

		this.siteId = siteId;

	}

	public String getCode() {

		return code;

	}

	public void setCode(String code) {

		this.code = code;

	}

	public String getAddressLine() {

		return addressLine;

	}

	public void setAddressLine(String addressLine) {

		this.addressLine = addressLine;

	}

	public String getAddress() {

		return address;

	}

	public void setAddress(String address) {

		this.address = address;

	}

	public String getAddressNumber() {

		return addressNumber;

	}

	public void setAddressNumber(String addressNumber) {

		this.addressNumber = addressNumber;

	}

	public String getComplement() {

		return complement;

	}

	public void setComplement(String complement) {

		this.complement = complement;

	}

	public String getCountry() {

		return country;

	}

	public void setCountry(String country) {

		this.country = country;

	}

	public String getCity() {

		return city;

	}

	public void setCity(String city) {

		this.city = city;

	}

	public String getStateCode() {

		return stateCode;

	}

	public void setStateCode(String stateCode) {

		this.stateCode = stateCode;

	}

	public String getZipCode() {

		return zipCode;

	}

	public void setZipCode(String zipCode) {

		this.zipCode = zipCode;

	}

	public String getAddressType() {

		return addressType;

	}

	public void setAddressType(String addressType) {

		this.addressType = addressType;

	}

	public Integer getCustomerId() {

		return customerId;

	}

	public void setCustomerId(Integer customerId) {

		this.customerId = customerId;

	}

	public String getDocument() {

		return document;

	}

	public void setDocument(String document) {

		this.document = document;

	}

	public String getStatus() {

		return status;

	}

	public void setStatus(String status) {

		this.status = status;

	}

}