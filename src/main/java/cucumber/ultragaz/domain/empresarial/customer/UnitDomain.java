package cucumber.ultragaz.domain.empresarial.customer;

public class UnitDomain {

	private String id;
	private Integer instanceId;
	private String name;
	private Integer dueDay;
	private String deliveryType;
	private Integer hasGasMeter;

	public Integer getHasGasMeter() {

		return hasGasMeter;

	}

	public void setHasGasMeter(Integer hasGasMeter) {

		this.hasGasMeter = hasGasMeter;

	}

	public Integer getDueDay() {

		return dueDay;

	}

	public void setDueDay(Integer dueDay) {

		this.dueDay = dueDay;

	}

	public Integer getInstanceId() {

		return instanceId;

	}

	public void setInstanceId(Integer instanceId) {

		this.instanceId = instanceId;

	}

	public String getDeliveryType() {

		return deliveryType;

	}

	public void setDeliveryType(String deliveryType) {

		this.deliveryType = deliveryType;

	}

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public String getId() {

		return id;

	}

	public void setId(String id) {

		this.id = id;

	}

}