package cucumber.ultragaz.domain.empresarial.customer;

import java.sql.Date;

public class CustomerDomain {

	private String id;
	private Integer customerId;
	private String code;
	private String name;
	private String fancyName;
	private String document;
	private String status;
	private Integer sourceOrganization;
	private String type;
	private Date creationDate;
	private Date updateDate;
	private Date birthDate;

	public String getId() {

		return id;
	
	}

	public void setId(String id) {
	
		this.id = id;
	
	}
	
	public Integer getCustomerId() {
	
		return customerId;
	
	}

	public void setCustomerId(Integer customerId) {
	
		this.customerId = customerId;
	
	}

	public String getCode() {
	
		return code;
	
	}

	public void setCode(String code) {
	
		this.code = code;
	
	}

	public String getName() {
	
		return name;
	
	}

	public void setName(String name) {
	
		this.name = name;
	
	}

	public String getFancyName() {
	
		return fancyName;
	
	}

	public void setFancyName(String fancyName) {
	
		this.fancyName = fancyName;
	
	}

	public String getDocument() {
	
		return document;
	
	}

	public void setDocument(String document) {
	
		this.document = document;
	
	}

	public String getStatus() {
	
		return status;
	
	}

	public void setStatus(String status) {
	
		this.status = status;
	
	}

	public Integer getSourceOrganization() {
	
		return sourceOrganization;
	
	}

	public void setSourceOrganization(Integer sourceOrganization) {
	
		this.sourceOrganization = sourceOrganization;
	
	}

	public String getType() {
	
		return type;
	
	}

	public void setType(String type) {
	
		this.type = type;
	
	}

	public Date getCreationDate() {
	
		return creationDate;
	
	}

	public void setCreationDate(Date creationDate) {
	
		this.creationDate = creationDate;
	
	}

	public Date getUpdateDate() {
	
		return updateDate;
	
	}

	public void setUpdateDate(Date updateDate) {
	
		this.updateDate = updateDate;
	
	}

	public Date getBirthDate() {
	
		return birthDate;
	
	}

	public void setBirthDate(Date birthDate) {
		
		this.birthDate = birthDate;
	
	}

}