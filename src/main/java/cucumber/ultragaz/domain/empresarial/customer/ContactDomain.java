package cucumber.ultragaz.domain.empresarial.customer;

import java.sql.Date;

public class ContactDomain {

	private String id;
	private String contact;
	private String contactType;
	private String status;
	private Date creationDate;
	private Date updateDate;
	private Integer customerId;
	private String userId;
	private Integer siteId;
	private Integer contactId;
	private String name;
	private String origin;
	private Integer isValidated;
	private String contactIdSf;

	public Integer getIsValidated() {

		return isValidated;

	}

	public void setIsValidated(Integer isValidated) {

		this.isValidated = isValidated;

	}
	
	public String getContactIdSf() {

		return contactIdSf;

	}

	public void setContactIdSf(String contactIdSf) {

		this.contactIdSf = contactIdSf;

	}
	
	public String getId() {

		return id;

	}

	public void setId(String id) {

		this.id = id;

	}

	public String getContact() {

		return contact;

	}

	public void setContact(String contact) {

		this.contact = contact;

	}

	public String getContactType() {

		return contactType;

	}

	public void setContactType(String contactType) {

		this.contactType = contactType;

	}

	public String getStatus() {

		return status;

	}

	public void setStatus(String status) {

		this.status = status;

	}

	public Date getCreationDate() {

		return creationDate;

	}

	public void setCreationDate(Date creationDate) {

		this.creationDate = creationDate;

	}

	public Date getUpdateDate() {

		return updateDate;

	}

	public void setUpdateDate(Date updateDate) {

		this.updateDate = updateDate;

	}

	public Integer getCustomerId() {

		return customerId;

	}

	public void setCustomerId(Integer customerId) {

		this.customerId = customerId;

	}

	public String getUserId() {

		return userId;

	}

	public void setUserId(String userId) {

		this.userId = userId;

	}

	public Integer getSiteId() {

		return siteId;

	}

	public void setSiteId(Integer siteId) {

		this.siteId = siteId;

	}

	public Integer getContactId() {

		return contactId;

	}

	public void setContactId(Integer contactId) {

		this.contactId = contactId;

	}

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public String getOrigin() {

		return origin;

	}

	public void setOrigin(String origin) {

		this.origin = origin;

	}

}
