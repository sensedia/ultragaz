package cucumber.ultragaz.domain.empresarial.customer;

import java.sql.Date;

public class InstallBaseDomain {

	private String id;
	private String description;
	private String codeHierarchy;
	private String status;
	private Date activeDateFrom;
	private Date activeDateTo;
	private Integer quantity;
	private String reference;
	private String nickname;
	private Date creationDate;
	private Date updateDate;
	private Integer installbaseAddressSiteUseId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCodeHierarchy() {
		return codeHierarchy;
	}

	public void setCodeHierarchy(String codeHierarchy) {
		this.codeHierarchy = codeHierarchy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getActiveDateFrom() {
		return activeDateFrom;
	}

	public void setActiveDateFrom(Date activeDateFrom) {
		this.activeDateFrom = activeDateFrom;
	}

	public Date getActiveDateTo() {
		return activeDateTo;
	}

	public void setActiveDateTo(Date activeDateTo) {
		this.activeDateTo = activeDateTo;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getInstallbaseAddressSiteUseId() {
		return installbaseAddressSiteUseId;
	}

	public void setInstallbaseAddressSiteUseId(Integer installbaseAddressSiteUseId) {
		this.installbaseAddressSiteUseId = installbaseAddressSiteUseId;
	}

}