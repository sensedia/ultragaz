package cucumber.ultragaz;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.ultragaz.domain.empresarial.invoice.InvoiceDomain;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/industrial/customer/users/POST_users_{userId}_tokens.feature", glue = "cucumber.ultragaz.steps", dryRun = false, strict = false, monochrome = true, plugin = {
		"pretty", "json:target/cucumber.json", "html:target/reports/cucumber-reports" }, tags ="@Definition, @Positivo" )
public class RunCucumberTest extends AbstractFuncTest {
	private static Integer customerId;
	private static Integer customerIdDois;
	private static Integer customerIdTres;
	private static Integer customerIdMi;
	private static Integer customerIdMiDois;
	private static InvoiceDomain invoiceUser;
	private static InvoiceDomain invoiceUserDois;
	private static InvoiceDomain invoiceUserTres;
	private static InvoiceDomain invoiceUserMi;
	private static InvoiceDomain invoiceUserMiDois;
	private static InvoiceDomain invoiceDoisUser;
	private static InvoiceDomain invoiceDoisUserDois;
	private static InvoiceDomain invoiceDoisUserTres;
	private static InvoiceDomain invoiceDoisUserMi;
	private static InvoiceDomain invoiceDoisUserMiDois;

	@BeforeClass
	public static void beforeClass() {

		String user = "";
		String userDois = "Segundo";
		String userTres = "Terceiro";
		String userMi = "MI";
		String userMiCnpj = "MICNPJ";

		customerId = createUserCustomerContact(user, null);
		invoiceUser = createInvoiceCustomerInvoice(user);
		invoiceDoisUser = createInvoiceCustomerInvoice(user);
		setCucumberInvoiceEnvironment(user, invoiceUser.getInvoiceNumber(), invoiceDoisUser.getInvoiceNumber());

		customerIdDois = createUserCustomerContact(userDois, null);
		invoiceUserDois = createInvoiceCustomerInvoice(userDois);
		invoiceDoisUserDois = createInvoiceCustomerInvoice(userDois);
		setCucumberInvoiceEnvironment(userDois, invoiceUserDois.getInvoiceNumber(),
				invoiceDoisUserDois.getInvoiceNumber());

		customerIdTres = createUserCustomerContact(userTres, null);
		invoiceUserTres = createInvoiceCustomerInvoice(userTres);
		invoiceDoisUserTres = createInvoiceCustomerInvoice(userTres);
		setCucumberInvoiceEnvironment(userTres, invoiceUserTres.getInvoiceNumber(),
				invoiceDoisUserTres.getInvoiceNumber());

		customerIdMi = createUserMiCustomerContact(userMi, "CPF");
		invoiceUserMi = createInvoiceCustomerInvoice(userMi);
		invoiceDoisUserMi = createInvoiceCustomerInvoice(userMi);
		setCucumberInvoiceEnvironment(userMi, invoiceUserMi.getInvoiceNumber(), invoiceDoisUserMi.getInvoiceNumber());

		customerIdMiDois = createUserMiCustomerContact(userMiCnpj, "CNPJ");
		invoiceUserMiDois = createInvoiceCustomerInvoice(userMiCnpj);
		invoiceDoisUserMiDois = createInvoiceCustomerInvoice(userMiCnpj);
		setCucumberInvoiceEnvironment(userMiCnpj, invoiceUserMiDois.getInvoiceNumber(),
				invoiceDoisUserMiDois.getInvoiceNumber());

		deleteContactByCustomerId(customerIdTres);

	}

	public static void main(String[] args) {

	}

	@AfterClass
	public static void afterClass() {

		deleteUserCustomerContact(customerId);
		deleteUserCustomerContact(customerIdDois);
		deleteUserCustomerContact(customerIdTres);
		deleteUserCustomerContact(customerIdMi);
		deleteUserCustomerContact(customerIdMiDois);

		deleteInvoiceCustomerInvoice(invoiceUser, invoiceDoisUser);
		deleteInvoiceCustomerInvoice(invoiceUserDois, invoiceDoisUserDois);
		deleteInvoiceCustomerInvoice(invoiceUserTres, invoiceDoisUserTres);
		deleteInvoiceCustomerInvoice(invoiceUserMi, invoiceDoisUserMi);
		deleteInvoiceCustomerInvoice(invoiceUserMiDois, invoiceDoisUserMiDois);

	}

}
