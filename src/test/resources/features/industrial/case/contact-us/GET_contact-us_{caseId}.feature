Feature: GET_contact-us_{caseId}.feature
  Operação responsável por consultar Caso de Dúvida e Esclarecimento por ID.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "/contact-us" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "c83134d8-d572-4a8f-8064-ea1fb413df65" as "caseId"

	@Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
    
  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar Casos de Fale Conosco <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/<caseId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | assistancyId |
      | com caseId inválido  |            1 |
      | com caseId incorreto | ${caseId}c   |
      
  @Positivo
  Scenario: Consultar Casos de Fale Conosco
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And I verify if response value "/isRead" is not empty
    And Response code must be 200

