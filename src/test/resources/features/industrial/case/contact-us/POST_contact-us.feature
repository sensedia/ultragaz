Feature: POST_contact-us.feature
  Operação responsável por registrar um Caso de Fale Conosco.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "/contact-us" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "331706" as "customerSiteId"
    And I save "Teste automação" as "detail"
    And I save "PORTAL_API" as "origin"
    And I save "OUTROS" as "reason"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":"${origin}"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":"${origin}"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":"${origin}"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":"${origin}"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Registrar um caso de Fale Conosco <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                           | body                                                                                                 | message                                          | code      | language |
      | sem customerSiteId                  | "{"reason":"${reason}","detail":"${detail}","origin":"${origin}"}"                                   | "Field customerSiteId is required."              | "400.001" | "en-US"  |
      | com customerSiteId vazio            | "{"customerSiteId":"","reason":"${reason}","detail":"${detail}","origin":"${origin}"}"               | "Field customerSiteId is required."              | "400.001" | "en-US"  |
      | com customerSiteId nulo             | "{"customerSiteId":null,"reason":"${reason}","detail":"${detail}","origin":"${origin}"}"             | "Field customerSiteId is required."              | "400.001" | "en-US"  |
      | com customerSiteId formato inválido | "{"customerSiteId":"banana","reason":"${reason}","detail":"${detail}","origin":"${origin}"}"         | "Field customerSiteId has an invalid format."    | "400.004" | "en-US"  |
      | sem reason                          | "{"customerSiteId":${customerSiteId},"detail":"${detail}","origin":"${origin}"}"                     | "Field reason is required."                      | "400.001" | "en-US"  |
      | com reason vazio                    | "{"customerSiteId":${customerSiteId},"reason":"","detail":"${detail}","origin":"${origin}"}"         | "Field reason is required."                      | "400.001" | "en-US"  |
      | com reason nulo                     | "{"customerSiteId":${customerSiteId},"reason":null,"detail":"${detail}","origin":"${origin}"}"       | "Field reason is required."                      | "400.001" | "en-US"  |
      | com reason inválido                 | "{"customerSiteId":${customerSiteId},"reason":"banana","detail":"${detail}","origin":"${origin}"}"   | "Field reason has an invalid format."            | "400.004" | "en-US"  |
      | com reason com apenas espaço        | "{"customerSiteId":${customerSiteId},"reason":" ","detail":"${detail}","origin":"${origin}"}"        | "Field reason has an invalid format."            | "400.004" | "en-US"  |
      | com reason 0                        | "{"customerSiteId":${customerSiteId},"reason":0,"detail":"${detail}","origin":"${origin}"}"          | "Field reason has an invalid format."            | "400.004" | "en-US"  |
      | com reason 9                        | "{"customerSiteId":${customerSiteId},"reason":9,"detail":"${detail}","origin":"${origin}"}"          | "Field reason has an invalid format."            | "400.004" | "en-US"  |
      | sem detail                          | "{"customerSiteId":${customerSiteId},"reason":"${reason}","origin":"${origin}"}"                     | "Field detail is required."                      | "400.001" | "en-US"  |
      | com detail vazio                    | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"","origin":"${origin}"}"         | "Field detail is required."                      | "400.001" | "en-US"  |
      | com detail nulo                     | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":null,"origin":"${origin}"}"       | "Field detail is required."                      | "400.001" | "en-US"  |
      | sem origin                          | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}"}"                     | "Field origin is required."                      | "400.001" | "en-US"  |
      | com origin vazio                    | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":""}"         | "Field origin is required."                      | "400.001" | "en-US"  |
      | com origin nulo                     | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":null}"       | "Field origin is required."                      | "400.001" | "en-US"  |
      | com origin inválido                 | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":"INVALID"}"  | "Field origin has an invalid format."            | "400.004" | "en-US"  |
      | com origin com apenas espaço        | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":" "}"        | "Field origin has an invalid format."            | "400.004" | "en-US"  |
      | com origin 0                        | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":0}"          | "Field origin has an invalid format."            | "400.004" | "en-US"  |
      | com origin 1                        | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":1}"          | "Field origin has an invalid format."            | "400.004" | "en-US"  |
      | sem customerSiteId pt               | "{"reason":"${reason}","detail":"${detail}","origin":"${origin}"}"                                   | "O campo customerSiteId é obrigatório."          | "400.001" | "pt-BR"  |
      | com customerSiteId vazio pt         | "{"customerSiteId":"","reason":"${reason}","detail":"${detail}","origin":"${origin}"}"               | "O campo customerSiteId é obrigatório."          | "400.001" | "pt-BR"  |
      | com customerSiteId nulo pt          | "{"customerSiteId":null,"reason":"${reason}","detail":"${detail}","origin":"${origin}"}"             | "O campo customerSiteId é obrigatório."          | "400.001" | "pt-BR"  |
      | com customerSiteId formato inválido | "{"customerSiteId":"banana","reason":"${reason}","detail":"${detail}","origin":"${origin}"}"         | "O campo customerSiteId tem o formato inválido." | "400.004" | "pt-BR"  |
      | sem reason pt                       | "{"customerSiteId":${customerSiteId},"detail":"${detail}","origin":"${origin}"}"                     | "O campo reason é obrigatório."                  | "400.001" | "pt-BR"  |
      | com reason vazio pt                 | "{"customerSiteId":${customerSiteId},"reason":"","detail":"${detail}","origin":"${origin}"}"         | "O campo reason é obrigatório."                  | "400.001" | "pt-BR"  |
      | com reason nulo pt                  | "{"customerSiteId":${customerSiteId},"reason":null,"detail":"${detail}","origin":"${origin}"}"       | "O campo reason é obrigatório."                  | "400.001" | "pt-BR"  |
      | com reason inválido pt              | "{"customerSiteId":${customerSiteId},"reason":"banana","detail":"${detail}","origin":"${origin}"}"   | "O campo reason tem o formato inválido."         | "400.004" | "pt-BR"  |
      | com reason com apenas espaço pt     | "{"customerSiteId":${customerSiteId},"reason":" ","detail":"${detail}","origin":"${origin}"}"        | "O campo reason tem o formato inválido."         | "400.004" | "pt-BR"  |
      | com reason 0 pt                     | "{"customerSiteId":${customerSiteId},"reason":0,"detail":"${detail}","origin":"${origin}"}"          | "O campo reason tem o formato inválido."         | "400.004" | "pt-BR"  |
      | com reason 9 pt                     | "{"customerSiteId":${customerSiteId},"reason":9,"detail":"${detail}","origin":"${origin}"}"          | "O campo reason tem o formato inválido."         | "400.004" | "pt-BR"  |
      | sem detail pt                       | "{"customerSiteId":${customerSiteId},"reason":"${reason}","origin":"${origin}"}"                     | "O campo detail é obrigatório."                  | "400.001" | "pt-BR"  |
      | com detail vazio pt                 | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"","origin":"${origin}"}"         | "O campo detail é obrigatório."                  | "400.001" | "pt-BR"  |
      | com detail nulo pt                  | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":null,"origin":"${origin}"}"       | "O campo detail é obrigatório."                  | "400.001" | "pt-BR"  |
      | sem origin pt                       | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}"}"                     | "O campo origin é obrigatório."                  | "400.001" | "pt-BR"  |
      | com origin vazio pt                 | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":""}"         | "O campo origin é obrigatório."                  | "400.001" | "pt-BR"  |
      | com origin nulo pt                  | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":null}"       | "O campo origin é obrigatório."                  | "400.001" | "pt-BR"  |
      | com origin inválido pt              | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":"PORTAL_Z"}" | "O campo origin tem o formato inválido."         | "400.004" | "pt-BR"  |
      | com origin com apenas espaço pt     | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":" "}"        | "O campo origin tem o formato inválido."         | "400.004" | "pt-BR"  |
      | com origin 0 pt                     | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":0}"          | "O campo origin tem o formato inválido."         | "400.004" | "pt-BR"  |
      | com origin 1 pt                     | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":1}"          | "O campo origin tem o formato inválido."         | "400.004" | "pt-BR"  |

  @Negativo
  Scenario Outline: Registrar um caso de Fale Conosco <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                      | body                                                                                                   | message                                                                    | code      | language |
      | com customerSiteId inválido    | "{"customerSiteId":${customerSiteId}1,"reason":"${reason}","detail":"${detail}","origin":"${origin}"}" | "Error on process your request. Please contact Ultragaz."                  | "422.001" | "en-US"  |
      | com customerSiteId inválido pt | "{"customerSiteId":${customerSiteId}1,"reason":"${reason}","detail":"${detail}","origin":"${origin}"}" | "Erro ao criar a solicitação. Por favor, entre em contato com a Ultragaz." | "422.001" | "pt-BR"  |

  @Positivo
  Scenario Outline: Registrar um caso de Fale Conosco <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I save response header "location" as "location"
    And I save final value of header "location" as "caseId"
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | descricao                      | body                                                                                                            |
      | com reason SUGESTAO            | "{"customerSiteId":${customerSiteId},"reason":"SUGESTAO","detail":"${detail}","origin":"${origin}"}"            |
      | com reason ELOGIO              | "{"customerSiteId":${customerSiteId},"reason":"ELOGIO","detail":"${detail}","origin":"${origin}"}"              |
      | com reason ABASTECIMENTO       | "{"customerSiteId":${customerSiteId},"reason":"ABASTECIMENTO","detail":"${detail}","origin":"${origin}"}"       |
      | com reason COMERCIAL           | "{"customerSiteId":${customerSiteId},"reason":"COMERCIAL","detail":"${detail}","origin":"${origin}"}"           |
      | com reason ASSISTENCIA_TECNICA | "{"customerSiteId":${customerSiteId},"reason":"ASSISTENCIA_TECNICA","detail":"${detail}","origin":"${origin}"}" |
      | com reason FINANCEIRO          | "{"customerSiteId":${customerSiteId},"reason":"FINANCEIRO","detail":"${detail}","origin":"${origin}"}"          |
      | com reason CADASTRO            | "{"customerSiteId":${customerSiteId},"reason":"CADASTRO","detail":"${detail}","origin":"${origin}"}"            |
      | com reason DUVIDA              | "{"customerSiteId":${customerSiteId},"reason":"DUVIDA","detail":"${detail}","origin":"${origin}"}"              |
      | com reason RECLAMACAO          | "{"customerSiteId":${customerSiteId},"reason":"RECLAMACAO","detail":"${detail}","origin":"${origin}"}"          |
      | com reason OUTROS              | "{"customerSiteId":${customerSiteId},"reason":"OUTROS","detail":"${detail}","origin":"${origin}"}"              |
      | com origin PORTAL_API_MI       | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":"PORTAL_API_MI"}"       |
