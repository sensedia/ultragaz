Feature: GET_contact-us.feature
  Operação responsável por consultar os Casos de Fale Conosco.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "/contact-us" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "331706" as "customerSiteId"

	@Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?customer-site-id=${customerSiteId}&_expand=requestedCase"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?customer-site-id=${customerSiteId}&_expand=requestedCase"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
    
  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?customer-site-id=${customerSiteId}&_expand=requestedCase"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar Casos de Fale Conosco <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                      | query                                            | message                                            | code      | language |
      | sem customerSiteId             | ?_expand=requestedCase                           | "Field customer-site-id is required."              | "400.001" | "en-US"  |
      | sem customerSiteId pt          | ?_expand=requestedCase                           | "O campo customer-site-id é obrigatório."          | "400.001" | "pt-BR"  |
      | com customerSiteId inválido    | ?customer-site-id=331706as                       | "Field customer-site-id has an invalid format."    | "400.004" | "en-US"  |
      | com customerSiteId inválido pt | ?customer-site-id=331706as                       | "O campo customer-site-id tem o formato inválido." | "400.004" | "pt-BR"  |
      | com begin-date inválido        | ?customer-site-id=331706&begin-date=2020-09-23as | "Field begin-date has an invalid format."          | "400.004" | "en-US"  |
      | com begin-date inválido pt     | ?customer-site-id=331706&begin-date=2020-09-23as | "O campo begin-date tem o formato inválido."       | "400.004" | "pt-BR"  |
      | com end-date inválido          | ?customer-site-id=331706&end-date=2020-09-23as   | "Field end-date has an invalid format."            | "400.004" | "en-US"  |
      | com end-date inválido pt       | ?customer-site-id=331706&end-date=2020-09-23as   | "O campo end-date tem o formato inválido."         | "400.004" | "pt-BR"  |
      | com status-code inválido       | ?customer-site-id=331706&status-code=SOLATEs     | "Field status-code has an invalid format."         | "400.004" | "en-US"  |
      | com status-code inválido pt    | ?customer-site-id=331706&status-code=SOLATEs     | "O campo status-code tem o formato inválido."      | "400.004" | "pt-BR"  |
      | com is-read inválido           | ?customer-site-id=331706&is-read=sas             | "Field is-read has an invalid format."             | "400.004" | "en-US"  |
      | com is-read inválido pt        | ?customer-site-id=331706&is-read=sas             | "O campo is-read tem o formato inválido."          | "400.004" | "pt-BR"  |
      | com _limit inválido            | ?customer-site-id=331706&_limit=sas              | "Field _limit has an invalid format."              | "400.004" | "en-US"  |
      | com _limit inválido pt         | ?customer-site-id=331706&_limit=sas              | "O campo _limit tem o formato inválido."           | "400.004" | "pt-BR"  |
      | com _offset inválido           | ?customer-site-id=331706&_offset=sas             | "Field _offset has an invalid format."             | "400.004" | "en-US"  |
      | com _offset inválido pt        | ?customer-site-id=331706&_offset=sas             | "O campo _offset tem o formato inválido."          | "400.004" | "pt-BR"  |

  @Positivo
  Scenario Outline: Consultar Casos de Fale Conosco <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/caseId" is not empty
    And I verify if response value "/0/caseIdSalesForce" is not empty
    And I verify if response value "/0/caseNumber" is not empty
    And I verify if response value "/0/status/code" is not empty
    And I verify if response value "/0/status/description" is not empty
    And I verify if response value "/0/subject" is not empty
    And I verify if response value "/0/reason" is not empty
    And I verify if response value "/0/detail" is not empty
    And I verify if response value "/0/origin" is not empty
    And I verify if response value "/0/requestedCase/customerSiteId" is empty
    And I verify if response value "/0/requestedCase/installBase" is empty
    And I verify if response value "/0/requestedCase/creationDate" is empty
    And I verify if response value "/0/requestedCase/isIntegrationSuccess" is empty
    And I verify if response value "/0/requestedCase/integrationResponse" is empty
    And I verify if response value "/0/isRead" is not empty
    And Response code must be 200

    Examples: 
      | descricao                   | query                                                  |
      | com customerSiteId válido   | ?customer-site-id=331706                               |
      | com beginDate válido        | ?customer-site-id=331706&begin-date=2020-11-12         |
      | com endDate válido          | ?customer-site-id=331706&end-date=2020-11-11           |
      | com caseNumber válido       | ?customer-site-id=331706&case-number=20201111/8263691  |
      | com status-code SOLATE      | ?customer-site-id=331706&status-code=AGUTRA            |
      | com isRead válido           | ?customer-site-id=331706&is-read=false                 |
      | com _limit e _offset válido | ?customer-site-id=331706&_limit=1&_offset=0            |
      | com _expand inválido        | ?customer-site-id=331706&_expand=requestedCases        |

  @Positivo
  Scenario: Consultar Casos de Fale Conosco com _xpand
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?customer-site-id=331706&_expand=requestedCase"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/caseId" is not empty
    And I verify if response value "/0/caseIdSalesForce" is not empty
    And I verify if response value "/0/caseNumber" is not empty
    And I verify if response value "/0/status/code" is not empty
    And I verify if response value "/0/status/description" is not empty
    And I verify if response value "/0/subject" is not empty
    And I verify if response value "/0/reason" is not empty
    And I verify if response value "/0/detail" is not empty
    And I verify if response value "/0/origin" is not empty
    And I verify if response value "/0/requestedCase/customerSiteId" is not empty
    And I verify if response value "/0/requestedCase/installBase" is empty
    And I verify if response value "/0/requestedCase/creationDate" is not empty
    And I verify if response value "/0/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/0/requestedCase/integrationResponse" is not empty
    And I verify if response value "/0/isRead" is not empty
    And Response code must be 200

  @Positivo
  Scenario Outline: Consultar Casos de Fale Conosco <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                      | query                                                     |
      | com customer-site-id incorreto | ?customer-site-id=1                                       |
      | com case-number vazio          | ?customer-site-id=${customerSiteId}&case-number=          |
      | com case-number inválido       | ?customer-site-id=${customerSiteId}&case-number=bla       |
      | por status-code AAPRO          | ?customer-site-id=${customerSiteId}&status-code=AAPRO     |
      | por status-code AAPRV          | ?customer-site-id=${customerSiteId}&status-code=AAPRV     |
      | por status-code ADIADO         | ?customer-site-id=${customerSiteId}&status-code=ADIADO    |
      | por status-code AGADEQ         | ?customer-site-id=${customerSiteId}&status-code=AGADEQ    |
      | por status-code AGMAT          | ?customer-site-id=${customerSiteId}&status-code=AGMAT     |
      | por status-code AGPLAN         | ?customer-site-id=${customerSiteId}&status-code=AGPLAN    |
      | por status-code AGUANA         | ?customer-site-id=${customerSiteId}&status-code=AGUANA    |
      | por status-code AGUCONLOG      | ?customer-site-id=${customerSiteId}&status-code=AGUCONLOG |
      | por status-code APROG          | ?customer-site-id=${customerSiteId}&status-code=APROG     |
      | por status-code APROVADO       | ?customer-site-id=${customerSiteId}&status-code=APROVADO  |
      | por status-code ATIVO          | ?customer-site-id=${customerSiteId}&status-code=ATIVO     |
      | por status-code CANCEL         | ?customer-site-id=${customerSiteId}&status-code=CANCEL    |
      | por status-code COMP           | ?customer-site-id=${customerSiteId}&status-code=COMP      |
      | por status-code EDITHIST       | ?customer-site-id=${customerSiteId}&status-code=EDITHIST  |
      | por status-code EMAND          | ?customer-site-id=${customerSiteId}&status-code=EMAND     |
      | por status-code FECHAR         | ?customer-site-id=${customerSiteId}&status-code=FECHAR    |
      | por status-code NA             | ?customer-site-id=${customerSiteId}&status-code=NA        |
      | por status-code NOK            | ?customer-site-id=${customerSiteId}&status-code=NOK       |
      | por status-code PEDAGE         | ?customer-site-id=${customerSiteId}&status-code=PEDAGE    |
      | por status-code REMOVIDO       | ?customer-site-id=${customerSiteId}&status-code=REMOVIDO  |
      | por status-code RENDATCLI      | ?customer-site-id=${customerSiteId}&status-code=RENDATCLI |
      | por status-code SEMEXEC        | ?customer-site-id=${customerSiteId}&status-code=SEMEXEC   |
      | por status-code SNOREC         | ?customer-site-id=${customerSiteId}&status-code=SNOREC    |
      | por status-code WMATL          | ?customer-site-id=${customerSiteId}&status-code=WMATL     |
      
 