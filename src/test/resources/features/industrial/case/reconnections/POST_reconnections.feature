Feature: POST_reconnections.feature
  Operação responsável por registrar um Caso de Religue.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "/reconnections" as "endpoint"
    And I save "01259596052" as "document"
    And I save "1937516" as "unitInstanceId"
    And I save "Solicitação de religamento" as "detail"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"document": "${document}","unitInstanceId": ${unitInstanceId}}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request header "client_id" as "${client_id}1"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"document": "${document}","unitInstanceId": ${unitInstanceId}}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"document": "${document}","unitInstanceId": ${unitInstanceId}}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}1"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"document": "${document}","unitInstanceId": ${unitInstanceId}}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"document": "${document}","unitInstanceId": ${unitInstanceId}}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario: Enviar requisição sem body
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "en-US"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Invalid request body."
    And Response code must be 400

  @Negativo
  Scenario: Enviar requisição sem body em pt
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Request body inválido."
    And Response code must be 400

  @Negativo
  Scenario Outline: Validar body <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 400

    Examples: 
      | parameter                               | body                                                                                                    | language | code      | message                                                         |
      | com document inválido                   | "{"document":"${document}2","unitInstanceId":${unitInstanceId},"detail":"${detail}","trusted":true}"    | "en-US"  | "400.004" | "Field document has an invalid format."                         |
      | com document inválido pt                | "{"document":"${document}2","unitInstanceId":${unitInstanceId},"detail":"${detail}","trusted":true}"    | "pt-BR"  | "400.004" | "O campo document tem o formato inválido."                      |
      | com document e unitInstanceId nulos     | "{"document":null,"unitInstanceId":null,"detail":"${detail}","trusted":true}"                           | "en-US"  | "400.011" | "At least one field is required on request."                    |
      | com document e unitInstanceId nulos pt  | "{"document":null,"unitInstanceId":null,"detail":"${detail}","trusted":true}"                           | "pt-BR"  | "400.011" | "É necessário informar pelo menos um dos campos na requisição." |
      | sem document e sem unitInstanceId       | "{"detail":"${detail}","trusted":true}"                                                                 | "en-US"  | "400.011" | "At least one field is required on request."                    |
      | sem document e sem unitInstanceId pt    | "{"detail":"${detail}","trusted":true}"                                                                 | "pt-BR"  | "400.011" | "É necessário informar pelo menos um dos campos na requisição." |
      | com document e unitInstanceId vazios pt | "{"document":"","unitInstanceId":"","detail":"${detail}","trusted":true}"                               | "pt-BR"  | "400.011" | "É necessário informar pelo menos um dos campos na requisição." |
      | com document e unitInstanceId vazios    | "{"document":"","unitInstanceId":"","detail":"${detail}","trusted":true}"                               | "en-US"  | "400.011" | "At least one field is required on request."                    |
      | com unitInstanceId string               | "{"document":"${document}","unitInstanceId":"banana","detail":"${detail}","trusted":true}"              | "en-US"  | "400.004" | "Field unitInstanceId has an invalid format."                   |
      | com unitInstanceId string pt            | "{"document":"${document}","unitInstanceId":"banana","detail":"${detail}","trusted":true}"              | "pt-BR"  | "400.004" | "O campo unitInstanceId tem o formato inválido."                |
      | com trusted string                      | "{"document":"${document}","unitInstanceId":${unitInstanceId},"detail":"${detail}","trusted":"banana"}" | "en-US"  | "400.004" | "Field trusted has an invalid format."                          |
      | com trusted string pt                   | "{"document":"${document}","unitInstanceId":${unitInstanceId},"detail":"${detail}","trusted":"banana"}" | "pt-BR"  | "400.004" | "O campo trusted tem o formato inválido."                       |

  @Negativo
  Scenario Outline: Criar caso de religue <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 422

    Examples: 
      | parameter                                                    | body                                                   | language | code      | message                                                                       |
      | com cliente não suspenso                                     | "{"document":"31914200888","unitInstanceId":"740106"}" | "en-US"  | "422.007" | "Customer not suspended."                                                     |
      | com cliente não suspenso pt                                  | "{"document":"31914200888","unitInstanceId":"740106"}" | "pt-BR"  | "422.007" | "O cliente não está suspenso."                                                |
      | com cliente não suspenso apenas document                     | "{"document":"31914200888"}"                           | "en-US"  | "422.011" | "Customer does not have any overdue bill. Please contact Ultragaz."           |
      | com cliente não suspenso apenas document pt                  | "{"document":"31914200888"}"                           | "pt-BR"  | "422.011" | "O cliente não possui nenhum boleto vencido, necessário verificar com a UAC." |
      | com cliente não suspenso apenas unitInstanceId               | "{"unitInstanceId":"740106"}"                          | "en-US"  | "422.007" | "Customer not suspended."                                                     |
      | com cliente não suspenso apenas unitInstanceId pt            | "{"unitInstanceId":"740106"}"                          | "pt-BR"  | "422.007" | "O cliente não está suspenso."                                                |
      | com transferencia para especialista                          | "{"document":"31914200888","unitInstanceId":"1"}"      | "en-US"  | "422.012" | "Transfer customer to Ultragaz customer service."                             |
      | com transferencia para especialista pt                       | "{"document":"31914200888","unitInstanceId":"1"}"      | "pt-BR"  | "422.012" | "Transferir o cliente para um especialista."                                  |
      | com transferencia para especialista apenas document          | "{"document":"84081634777"}"                           | "en-US"  | "422.012" | "Transfer customer to Ultragaz customer service."                             |
      | com transferencia para especialista apenas document pt       | "{"document":"84081634777"}"                           | "pt-BR"  | "422.012" | "Transferir o cliente para um especialista."                                  |
      | com transferencia para especialista apenas unitInstanceId    | "{"unitInstanceId":"1"}"                               | "en-US"  | "422.012" | "Transfer customer to Ultragaz customer service."                             |
      | com transferencia para especialista apenas unitInstanceId pt | "{"unitInstanceId":"1"}"                               | "pt-BR"  | "422.012" | "Transferir o cliente para um especialista."                                  |

  @Positivo
  Scenario Outline: Criar caso de religue <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response code must be 201

    Examples: 
      | parameter                     | body                                                                                                                                    |
      | com document e unitInstanceId | "{"document":"${document}","unitInstanceId":"${unitInstanceId}"}"                                                                       |
      | com apenas document           | "{"document":"${document}"}"                                                                                                            |
      | com apenas unitInstanceId     | "{"unitInstanceId":"${unitInstanceId}"}"                                                                                                |
      | com body completo             | "{"document":"${document}","unitInstanceId":"${unitInstanceId}","detail":"${detail}","trusted":true,"origin":"Portal Teste Automacao"}" |
