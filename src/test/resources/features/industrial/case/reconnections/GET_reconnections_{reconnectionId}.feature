Feature: GET_reconnections_{reconnectionId}.feature
  Operação responsável por gestão de casos de religue.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    #And I set request body as "{"grant_type":"password","username":"usertestlogin@sensedia.com","password":"teste123!"}"
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "/reconnections" as "endpointReconnections"
    And I save "9414a754-f25f-4aef-afe3-e4967ef1b6c6" as "caseId"

  @Positivo
  Scenario: Consultar reconnections
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointReconnections}/${caseId}"
    Then I get response body
    And I get response status code
    And I save response value "/0/caseId" as "refuelId"
    And Response code must be 200

  @Negativo
  Scenario Outline: Consultar reconnections <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointReconnections}/<caseId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao              | caseId                               |
      | com refuelId inválido  |                                    1 |
      | com refuelId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario: Consultar reconnections
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointReconnections}/${caseId}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/isRead" is not empty
    And I verify if response value "/requestedCase/document" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200
