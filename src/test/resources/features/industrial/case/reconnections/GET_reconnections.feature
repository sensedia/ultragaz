Feature: GET_reconnections.feature
  Operação responsável por consultar casos de religue do cliente

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    #And I set request body as "{"grant_type":"password","username":"usertestlogin@sensedia.com","password":"teste123!"}"
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "/reconnections" as "endpointReconnections"
    And I save "9414a754-f25f-4aef-afe3-e4967ef1b6c6" as "caseId"
    And I save "2020-07-01" as "beginDate"
    And I save "2020-07-05" as "endDate"
    And I save "01259596052" as "document"
    And I save "1937089" as "unitInstanceId"
    And I save "20200702/8257102" as "caseNumber"
    And I save "true" as "isRead"

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointReconnections}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                          | query                                                           | language | message                                          | code    |
      | com beginDate inválido             | ?_limit=1&_offset=0&begin-date=bla                              | en-US    | Field begin-date has an invalid format.          | 400.004 |
      | com beginDate ano inválido         | ?_limit=1&_offset=0&begin-date=000-12-12                        | en-US    | Field begin-date has an invalid format.          | 400.004 |
      | com beginDate mês inválido         | ?_limit=1&_offset=0&begin-date=2019-13-12                       | en-US    | Field begin-date has an invalid format.          | 400.004 |
      | com beginDate dia inválido         | ?_limit=1&_offset=0&begin-date=2019-12-35                       | en-US    | Field begin-date has an invalid format.          | 400.004 |
      | com endDate inválido               | ?_limit=1&_offset=0&end-date=bla                                | en-US    | Field end-date has an invalid format.            | 400.004 |
      | com endDate ano inválido           | ?_limit=1&_offset=0&end-date=000-12-12                          | en-US    | Field end-date has an invalid format.            | 400.004 |
      | com endDate mês inválido           | ?_limit=1&_offset=0&end-date=2019-13-12                         | en-US    | Field end-date has an invalid format.            | 400.004 |
      | com endDate dia inválido           | ?_limit=1&_offset=0&end-date=2019-12-35                         | en-US    | Field end-date has an invalid format.            | 400.004 |
      | com endDate menor que beginDate    | ?_limit=1&_offset=0&end-date=${beginDate}&begin-date=${endDate} | en-US    | Invalid date range.                              | 400.006 |
      | com limit inválido                 | ?_offset=0&end-date=${endDate}&begin-date=${beginDate}&_limit=a | en-US    | Field _limit has an invalid format.              | 400.004 |
      | com offset inválido                | ?_limit=1&end-date=${endDate}&begin-date=${beginDate}&_offset=a | en-US    | Field _offset has an invalid format.             | 400.004 |
      | com beginDate inválido pt          | ?_limit=1&_offset=0&begin-date=bla                              | pt-BR    | O campo begin-date tem o formato inválido.       | 400.004 |
      | com beginDate ano inválido pt      | ?_limit=1&_offset=0&begin-date=000-12-12                        | pt-BR    | O campo begin-date tem o formato inválido.       | 400.004 |
      | com beginDate mês inválido pt      | ?_limit=1&_offset=0&begin-date=2019-13-12                       | pt-BR    | O campo begin-date tem o formato inválido.       | 400.004 |
      | com beginDate dia inválido pt      | ?_limit=1&_offset=0&begin-date=2019-12-35                       | pt-BR    | O campo begin-date tem o formato inválido.       | 400.004 |
      | com endDate inválido pt            | ?_limit=1&_offset=0&end-date=bla                                | pt-BR    | O campo end-date tem o formato inválido.         | 400.004 |
      | com endDate ano inválido pt        | ?_limit=1&_offset=0&end-date=000-12-12                          | pt-BR    | O campo end-date tem o formato inválido.         | 400.004 |
      | com endDate mês inválido pt        | ?_limit=1&_offset=0&end-date=2019-13-12                         | pt-BR    | O campo end-date tem o formato inválido.         | 400.004 |
      | com endDate dia inválido pt        | ?_limit=1&_offset=0&end-date=2019-12-35                         | pt-BR    | O campo end-date tem o formato inválido.         | 400.004 |
      | com endDate menor que beginDate pt | ?_limit=1&_offset=0&end-date=${beginDate}&begin-date=${endDate} | pt-BR    | Intervalo de datas inválido.                     | 400.006 |
      | com limit inválido pt              | ?_offset=0&end-date=${endDate}&begin-date=${beginDate}&_limit=a | pt-BR    | O campo _limit tem o formato inválido.           | 400.004 |
      | com offset inválido pt             | ?_limit=1&end-date=2020-12-01&begin-date=${beginDate}&_offset=a | pt-BR    | O campo _offset tem o formato inválido.          | 400.004 |
      | com document incorreto             | ?document=11111111111                                           | en-US    | Field document has an invalid format.            | 400.004 |
      | com document inválido              | ?document=bla                                                   | en-US    | Field document has an invalid format.            | 400.004 |
      | com document incorreto em pt       | ?document=11111111111                                           | pt-BR    | O campo document tem o formato inválido.         | 400.004 |
      | com document inválido em pt        | ?document=bla                                                   | pt-BR    | O campo document tem o formato inválido.         | 400.004 |
      | com unitInstanceId inválido        | ?unit-instance-id=bla                                           | en-US    | Field unit-instance-id has an invalid format.    | 400.004 |
      | com unitInstanceId inválido em pt  | ?unit-instance-id=bla                                           | pt-BR    | O campo unit-instance-id tem o formato inválido. | 400.004 |
      | com isRead inválido                | ?is-read=bla                                                    | en-US    | Field is-read has an invalid format.             | 400.004 |
      | com _limit 0                       | ?_limit=0                                                       | en-US    | Field _limit value must be at least 1.           | 400.009 |
      | com _limit incorreto               | ?_limit=bla                                                     | en-US    | Field _limit has an invalid format.              | 400.004 |
      | com _offset incorreto              | ?_offset=bla                                                    | en-US    | Field _offset has an invalid format.             | 400.004 |
      | com isRead inválido em pt          | ?is-read=bla                                                    | pt-BR    | O campo is-read tem o formato inválido.          | 400.004 |
      | com _limit 0 em pt                 | ?_limit=0                                                       | pt-BR    | O valor do campo _limit deve ser pelo menos 1.   | 400.009 |
      | com _limit incorreto em pt         | ?_limit=bla                                                     | pt-BR    | O campo _limit tem o formato inválido.           | 400.004 |
      | com _offset incorreto em pt        | ?_offset=bla                                                    | pt-BR    | O campo _offset tem o formato inválido.          | 400.004 |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointReconnections}<query>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                    | query                   |
      | com caseNumber incorreto     | ?case-number=1          |
      | com caseNumber inválido      | ?case-number=bla        |
      | com caseNumber vazio         | ?case-number=           |
      | com beginDate sem registros  | ?begin-date=2021-12-30  |
      | com endDate sem registros    | ?end-date=2019-12-30    |
      | com unitInstanceId incorreto | ?unit-instance-id=11111 |
      | com isRead                   | ?is-read=${isRead}      |
      | com _offset 101              | ?_offset=101            |
      | com _offset 1                | ?_offset=1              |

  @Positivo
  Scenario Outline: Consultar manutenção <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointReconnections}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/caseId" is not empty
    And I verify if response value "/0/caseIdSalesForce" is not empty
    And I verify if response value "/0/caseNumber" is not empty
    And I verify if response value "/0/status/code" is not empty
    And I verify if response value "/0/status/description" is not empty
    And I verify if response value "/0/subject" is not empty
    And I verify if response value "/0/reason" is not empty
    And I verify if response value "/0/detail" is not empty
    And I verify if response value "/0/origin" is not empty
    And I verify if response value "/0/priority" is not empty
    And I verify if response value "/0/sendSMS" is not empty
    And I verify if response value "/0/isEscalated" is not empty
    And I verify if response value "/0/isRead" is not empty
    And I verify if response value "/0/requestedCase/customerSiteId" is empty
    And I verify if response value "/0/requestedCase/installBase" is empty
    And I verify if response value "/0/requestedCase/creationDate" is empty
    And I verify if response value "/0/requestedCase/isIntegrationSuccess" is empty
    And I verify if response value "/0/requestedCase/integrationResponse" is empty
    And I verify if response value "/0/requestedCase/document" is empty
    And I verify if response value "/0/requestedCase/unitInstanceId" is empty
    And Response code must be 200

    Examples: 
      | descricao                | query                               |
      | sem query parameter      |                                     |
      | com caseNumber           | ?case-number=${caseNumber}          |
      | com beginDate            | ?begin-date=${beginDate}            |
      | com beginDate vazio      | ?begin-date=                        |
      | com endDate              | ?end-date=${endDate}                |
      | com endDate vazio        | ?end-date=                          |
      | com document             | ?document=${document}               |
      | com document vazio       | ?document=                          |
      | com unitInstanceId       | ?unit-instance-id=${unitInstanceId} |
      | com unitInstanceId vazio | ?unit-instance-id=                  |
      | com isRead false         | ?is-read=false                      |
      | com isRead vazio         | ?is-read=                           |
      | com _expand incorreto    | ?_expand=bla                        |
      | com _expand vazio        | ?_expand=                           |
      | com _offset 0            | ?_offset=0                          |
      | com _offset vazio        | ?_offset=                           |
      | com _limit 1             | ?_limit=1                           |
      | com _limit vazio         | ?_limit=                            |

  @Positivo
  Scenario: Consultar manutenção com expand
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointReconnections}?_expand=requestedCase"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/caseId" is not empty
    And I verify if response value "/0/caseIdSalesForce" is not empty
    And I verify if response value "/0/caseNumber" is not empty
    And I verify if response value "/0/status/code" is not empty
    And I verify if response value "/0/status/description" is not empty
    And I verify if response value "/0/subject" is not empty
    And I verify if response value "/0/reason" is not empty
    And I verify if response value "/0/detail" is not empty
    And I verify if response value "/0/origin" is not empty
    And I verify if response value "/0/priority" is not empty
    And I verify if response value "/0/sendSMS" is not empty
    And I verify if response value "/0/isEscalated" is not empty
    And I verify if response value "/0/isRead" is not empty
    And I verify if response value "/6/requestedCase/customerSiteId" is not empty
    And I verify if response value "/6/requestedCase/installBase" is not empty
    And I verify if response value "/6/requestedCase/creationDate" is not empty
    And I verify if response value "/6/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/6/requestedCase/integrationResponse" is not empty
    And I verify if response value "/6/requestedCase/document" is not empty
    And I verify if response value "/6/requestedCase/unitInstanceId" is not empty
    And Response code must be 200
