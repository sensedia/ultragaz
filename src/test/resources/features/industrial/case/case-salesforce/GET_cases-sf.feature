Feature: GET_cases-sf.feature
  Operação responsável por consultar os Casos do Salesforce.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "/cases-sf" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0010M00001UwhuM" as "storeId"
    And I save "0010M00001UwhuM" as "storeIdArr"
    And I save "0010M00001SNg5K" as "storeIdAaa"
    And I save "AGUARDANDO_ANALISE_DO_ADMINISTRATIVO" as "phaseAaa"
    And I save "AGUARDANDO_RETORNO_DO_REVENDEDOR" as "phaseArr"
    And I save "REGULARIZACAO_OU_CORRECAO_DE_NF_DE_RETORNO_DE_VASILHAMES" as "type"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?store-id=${storeId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?store-id=${storeId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?store-id=${storeId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar Casos do Salesforce <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao             | query                              | message                                 | code      | language |
      | sem store-id          |                                    | "Field store-id is required."           | "400.001" | "en-US"  |
      | sem store-id pt       |                                    | "O campo store-id é obrigatório."       | "400.001" | "pt-BR"  |
      | com phase inválido    | ?store-id=${storeId}&phase=invalid | "Field phase has an invalid format."    | "400.004" | "en-US"  |
      | com phase inválido pt | ?store-id=${storeId}&phase=invalid | "O campo phase tem o formato inválido." | "400.004" | "pt-BR"  |
      | com type inválido     | ?store-id=${storeId}&type=invalid  | "Field type has an invalid format."     | "400.004" | "en-US"  |
      | com type inválido pt  | ?store-id=${storeId}&type=invalid  | "O campo type tem o formato inválido."  | "400.004" | "pt-BR"  |

  @Negativo
  Scenario Outline: Consultar Casos do Salesforce <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                              | query                                     | message                                             | code      | language |
      | com phase AAA para store-id não AAA    | ?store-id=${storeIdAaa}&phase=${phaseArr} | "Error getting cases from store ${storeIdAaa}."     | "422.016" | "en-US"  |
      | com phase AAA para store-id não AAA pt | ?store-id=${storeIdAaa}&phase=${phaseArr} | "Erro ao consultar casos da revenda ${storeIdAaa}." | "422.016" | "pt-BR"  |
      | com phase ARR para store-id não ARR    | ?store-id=${storeIdArr}&phase=${phaseAaa} | "Error getting cases from store ${storeIdArr}."     | "422.016" | "en-US"  |
      | com phase ARR para store-id não ARR pt | ?store-id=${storeIdArr}&phase=${phaseAaa} | "Erro ao consultar casos da revenda ${storeIdArr}." | "422.016" | "pt-BR"  |
      | com store-id inválido                  | ?store-id=${storeIdArr}a                  | "Error getting cases from store ${storeIdArr}a."     | "422.016" | "en-US"  |
      | com store-id inválido pt               | ?store-id=${storeIdArr}a                  | "Erro ao consultar casos da revenda ${storeIdArr}a." | "422.016" | "pt-BR"  |

  @Positivo
  Scenario Outline: Consultar Casos do Salesforce <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/number" is not empty
    And I verify if response value "/0/phase" is not empty
    And I verify if response value "/0/description" is not empty
    And I verify if response value "/0/branch" is not empty
    And I verify if response value "/0/comments/0/id" is not empty
    And I verify if response value "/0/comments/0/description" is not empty
    And Response code must be 200

    Examples: 
      | descricao                       | query                                     |
      | com phase ARR para store-id ARR | ?store-id=${storeIdArr}&phase=${phaseArr} |
      | com phase ARR e type            | ?store-id=${storeIdArr}&type=${type}      |

  @Positivo
  Scenario Outline: Consultar Casos do Salesforce <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/number" is not empty
    And I verify if response value "/0/phase" is not empty
    And I verify if response value "/0/branch" is not empty
    And I verify if response value "/0/comments/0/id" is not empty
    And I verify if response value "/0/comments/0/description" is not empty
    And Response code must be 200

    Examples: 
      | descricao                       | query                                     |
      | com phase AAA para store-id AAA | ?store-id=${storeIdAaa}&phase=${phaseAaa} |
      | com phase AAA e type            | ?store-id=${storeIdArr}&type=${type}      |

  