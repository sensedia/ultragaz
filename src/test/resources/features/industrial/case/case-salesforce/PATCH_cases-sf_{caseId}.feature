Feature: POST_contact-us.feature
  Operação responsável por Atualizar um caso na salesforce.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "/contact-us" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0010M00001UwhuM" as "storeId"
    And I save "0010M00001UwhuM" as "storeIdArr"
    And I save "0010M00001SNg5K" as "storeIdAaa"
    And I save "AGUARDANDO_ANALISE_DO_ADMINISTRATIVO" as "phaseAaa"
    And I save "AGUARDANDO_RETORNO_DO_REVENDEDOR" as "phaseArr"
    And I save "REGULARIZACAO_OU_CORRECAO_DE_NF_DE_RETORNO_DE_VASILHAMES" as "type"
    And I save "5002f000007vt8VAAQ" as "caseId"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"comment":"Testando automação"}"
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"comment":"Testando automação"}"
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"comment":"Testando automação"}"
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"comment":"Testando automação"}"
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario: Enviar requisição sem caseId
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"comment":"Testando automação"}"
    When I set PATCH api endpoint as "${endpoint}/"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 405

  @Negativo
  Scenario Outline: Atualizar um caso na salesforce <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                    | body                                                                                              | message                                                           | code      | language |
      | com body vazio               | "{}"                                                                                              | "Choose must one of comment, attachment or phase options."        | "400.014" | "en-US"  |
      | com body vazio pt            | "{}"                                                                                              | "Escolha pelo menos uma das opções comment, attachment ou phase." | "400.014" | "pt-BR"  |
      | com comment nulo             | "{"comment":null}"                                                                                | "Choose must one of comment, attachment or phase options."        | "400.014" | "en-US"  |
      | com comment nulo pt          | "{"comment":null}"                                                                                | "Escolha pelo menos uma das opções comment, attachment ou phase." | "400.014" | "pt-BR"  |
      | com phase nulo               | "{"phase":null}"                                                                                  | "Choose must one of comment, attachment or phase options."        | "400.014" | "en-US"  |
      | com phase nulo pt            | "{"phase":null}"                                                                                  | "Escolha pelo menos uma das opções comment, attachment ou phase." | "400.014" | "pt-BR"  |
      | sem attachment.name          | "{"attachment":{"description":"at ex blandit.","format":"png","body":"Iron Maiden"}}"             | "Field attachment.name is required."                              | "400.001" | "en-US"  |
      | sem attachment.name pt       | "{"attachment":{"description":"at ex blandit.","format":"png","body":"Iron Maiden"}}"             | "O campo attachment.name é obrigatório."                          | "400.001" | "pt-BR"  |
      | com attachment.name vazio    | "{"attachment":{"name":"","description":"at ex blandit.","format":"png","body":"Iron Maiden"}}"   | "Field attachment.name is required."                              | "400.001" | "en-US"  |
      | com attachment.name vazio pt | "{"attachment":{"name":"","description":"at ex blandit.","format":"png","body":"Iron Maiden"}}"   | "O campo attachment.name é obrigatório."                          | "400.001" | "pt-BR"  |
      | com attachment.name nulo     | "{"attachment":{"name":null,"description":"at ex blandit.","format":"png","body":"Iron Maiden"}}" | "Field attachment.name is required."                              | "400.001" | "en-US"  |
      | com attachment.name nulo pt  | "{"attachment":{"name":null,"description":"at ex blandit.","format":"png","body":"Iron Maiden"}}" | "O campo attachment.name é obrigatório."                          | "400.001" | "pt-BR"  |
      | sem attachment.body          | "{"attachment":{"name":"Testando","description":"at ex blandit.","format":"png"}}"                | "Field attachment.body is required."                              | "400.001" | "en-US"  |
      | sem attachment.body pt       | "{"attachment":{"name":"Testando","description":"at ex blandit.","format":"png"}}"                | "O campo attachment.body é obrigatório."                          | "400.001" | "pt-BR"  |
      | com attachment.body vazio    | "{"attachment":{"name":"Testando","description":"at ex blandit.","format":"png","body":""}}"      | "Field attachment.body is required."                              | "400.001" | "en-US"  |
      | com attachment.body vazio pt | "{"attachment":{"name":"Testando","description":"at ex blandit.","format":"png","body":""}}"      | "O campo attachment.body é obrigatório."                          | "400.001" | "pt-BR"  |
      | com attachment.body nulo     | "{"attachment":{"name":"Testando","description":"at ex blandit.","format":"png","body":null}}"    | "Field attachment.body is required."                              | "400.001" | "en-US"  |
      | com attachment.body nulo pt  | "{"attachment":{"name":"Testando","description":"at ex blandit.","format":"png","body":null}}"    | "O campo attachment.body é obrigatório."                          | "400.001" | "pt-BR"  |

  @Negativo
  Scenario Outline: Atualizar um caso na salesforce <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request body as "{"comment":"Iron Maidens"}"
    When I set PATCH api endpoint as "${endpoint}/<caseId>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao              | caseId     | message                                           | code      | language |
      | com caseId inválido    | ${caseId}a | "Error on add attachment to case ${caseId}a."     | "422.013" | "en-US"  |
      | com caseId inválido pt | ${caseId}a | "Erro ao adicionar anexo para o caso ${caseId}a." | "422.013" | "pt-BR"  |

  @Positivo
  Scenario Outline: Atualizar um caso na salesforce <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And I save response header "location" as "location"
    And I save final value of header "location" as "caseId"
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | descricao                      | body                                                                                                            |
      | com reason SUGESTAO            | "{"customerSiteId":${customerSiteId},"reason":"SUGESTAO","detail":"${detail}","origin":"${origin}"}"            |
      | com reason ELOGIO              | "{"customerSiteId":${customerSiteId},"reason":"ELOGIO","detail":"${detail}","origin":"${origin}"}"              |
      | com reason ABASTECIMENTO       | "{"customerSiteId":${customerSiteId},"reason":"ABASTECIMENTO","detail":"${detail}","origin":"${origin}"}"       |
      | com reason COMERCIAL           | "{"customerSiteId":${customerSiteId},"reason":"COMERCIAL","detail":"${detail}","origin":"${origin}"}"           |
      | com reason ASSISTENCIA_TECNICA | "{"customerSiteId":${customerSiteId},"reason":"ASSISTENCIA_TECNICA","detail":"${detail}","origin":"${origin}"}" |
      | com reason FINANCEIRO          | "{"customerSiteId":${customerSiteId},"reason":"FINANCEIRO","detail":"${detail}","origin":"${origin}"}"          |
      | com reason CADASTRO            | "{"customerSiteId":${customerSiteId},"reason":"CADASTRO","detail":"${detail}","origin":"${origin}"}"            |
      | com reason DUVIDA              | "{"customerSiteId":${customerSiteId},"reason":"DUVIDA","detail":"${detail}","origin":"${origin}"}"              |
      | com reason RECLAMACAO          | "{"customerSiteId":${customerSiteId},"reason":"RECLAMACAO","detail":"${detail}","origin":"${origin}"}"          |
      | com reason OUTROS              | "{"customerSiteId":${customerSiteId},"reason":"OUTROS","detail":"${detail}","origin":"${origin}"}"              |
      | com origin PORTAL_API_MI       | "{"customerSiteId":${customerSiteId},"reason":"${reason}","detail":"${detail}","origin":"PORTAL_API_MI"}"       |
