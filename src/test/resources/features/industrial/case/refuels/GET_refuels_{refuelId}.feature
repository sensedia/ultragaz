Feature: GET_refuels_{refuelId}.feature
  Operação responsável por consultar um abastecimento.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    #And I set request body as "{"grant_type":"password","username":"usertestlogin@sensedia.com","password":"teste123!"}"
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "/refuels" as "endpointRefuels"
    And I save "BIC022667" as "installBase"
    #And System get local date time
    And I save "2020-07-06" as "localDate"
    And I save "331706" as "customerSiteId"
    And I save "0030M000023eoLtQAI" as "contactIdSf"

  @Positivo
  Scenario: Consultar manutenção com expand
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointRefuels}?_offset=0&customer-site-id=2372988&customer-id=19&end-date=2020-12-01&begin-date=2019-09-01&_limit=1&_expand=requestedCase"
    Then I get response body
    And I get response status code
    And I save response value "/0/caseId" as "refuelId"
    And Response code must be 200

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointRefuels}/<refuelId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao              | refuelId                             |
      | com refuelId inválido  |                                    1 |
      | com refuelId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointRefuels}/${refuelId}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I compare response value "/recordType" with "<recordType>"
    And I verify if response value "/recordType" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is empty
    And I verify if response value "/agreedDate" is not empty
    And I verify if response value "/suggestedDate" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | descricao                 | recordType        | agreedDate   |
      | com parentCaseId inválido | PedidoComRotaFixa | ${localDate} |
