Feature: PATCH_refuels_{refuelId}.feature
  Operação responsável por solicitar o cancelamento de pedido de abastecimento.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "/refuels" as "endpoint"
    And I save "72ac171d-6cfc-49a0-81c5-ace214584b98" as "caseId"

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"status":"CANCELADO","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"status":"CANCELADO","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Solicitar cancelamento de pedido <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"status":"CANCELADO","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"
    When I set PATCH api endpoint as "${endpoint}/<caseId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | caseId                               |
      | com caseId inválido  |                                    1 |
      | com caseId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario Outline: Solicitar cancelamento de pedido <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao              | body                                                                                 | message                                  | code      | language |
      | sem status             | "{"origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"                       | "Field status is required."              | "400.001" | "en-US"  |
      | sem status pt          | "{"origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"                       | "O campo status é obrigatório."          | "400.001" | "pt-BR"  |
      | com status vazio       | "{"status":"","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"           | "Field status has an invalid format."    | "400.004" | "en-US"  |
      | com status vazio pt    | "{"status":"","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"           | "O campo status tem o formato inválido"  | "400.004" | "pt-BR"  |
      | com status nulo        | "{"status":null,"origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"         | "Field status is required."              | "400.001" | "en-US"  |
      | com status nulo pt     | "{"status":null,"origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}"         | "O campo status é obrigatório."          | "400.001" | "pt-BR"  |
      | com status inválido    | "{"status":"CANCELADOs","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}" | "Field status has an invalid format."    | "400.004" | "en-US"  |
      | com status inválido pt | "{"status":"CANCELADOs","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}" | "O campo status tem o formato inválido." | "400.004" | "pt-BR"  |
      | sem origin             | "{"status":"CANCELADO","reason":"Fora do Turno de Trabalho"}"                        | "Field origin is required."              | "400.001" | "en-US"  |
      | sem origin pt          | "{"status":"CANCELADO","reason":"Fora do Turno de Trabalho"}"                        | "O campo origin é obrigatório."          | "400.001" | "pt-BR"  |
      | com origin nulo        | "{"status":"CANCELADO","origin":null,"reason":"Fora do Turno de Trabalho"}"          | "Field origin is required."              | "400.001" | "en-US"  |
      | com origin nulo pt     | "{"status":"CANCELADO","origin":null,"reason":"Fora do Turno de Trabalho"}"          | "O campo origin é obrigatório."          | "400.001" | "pt-BR"  |
      | com origin vazio       | "{"status":"CANCELADO","origin":"","reason":"Fora do Turno de Trabalho"}"            | "Field origin has an invalid format."    | "400.004" | "en-US"  |
      | com origin vazio pt    | "{"status":"CANCELADO","origin":"","reason":"Fora do Turno de Trabalho"}"            | "O campo origin tem o formato inválido." | "400.004" | "pt-BR"  |
      | com origin inválido    | "{"status":"CANCELADO","origin":"PORTAL_APIs","reason":"Fora do Turno de Trabalho"}" | "Field origin has an invalid format."    | "400.004" | "en-US"  |
      | com origin inválido pt | "{"status":"CANCELADO","origin":"PORTAL_APIs","reason":"Fora do Turno de Trabalho"}" | "O campo origin tem o formato inválido." | "400.004" | "pt-BR"  |

  @Negativo
  Scenario Outline: Solicitar cancelamento de pedido <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                | body                                                                                | message                               | code      | language |
      | com caso já cancelado    | "{"status":"CANCELADO","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}" | "The case has already been canceled." | "422.019" | "en-US"  |
      | com caso já cancelado pt | "{"status":"CANCELADO","origin":"PORTAL_API","reason":"Fora do Turno de Trabalho"}" | "O caso já foi cancelado."            | "422.019" | "pt-BR"  |
