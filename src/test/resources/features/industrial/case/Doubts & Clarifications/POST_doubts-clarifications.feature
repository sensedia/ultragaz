Feature: POST_doubts-clarifications.feature
  Operação responsável por registrar um Caso de Dúvida e Esclarecimento.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "/doubts-clarifications" as "endpoint"
    And I save "BIC022667" as "installBase"
    And I save "331706" as "customerSiteId"
    And I save "Teste automação" as "detail"

  @Negativo
  Scenario: Enviar requisição sem client_id
  	Given I set request header "access_token" as "${access_token}"
  	And I set request header "Content-Type" as "application/json"
  	And I set request body as "{"customerSiteId":${customerSiteId},"installBase":"${installBase}","detail":"${detail}"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"customerSiteId":${customerSiteId},"installBase":"${installBase}","detail":"${detail}"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"customerSiteId":${customerSiteId},"installBase":"${installBase}","detail":"${detail}"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

	@Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"customerSiteId":${customerSiteId},"installBase":"${installBase}","detail":"${detail}"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415
  
  @Negativo
  Scenario Outline: Registrar um caso de dúvida e esclarecimento <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                           | body                                                                                | message                                          | code      | language |
      | sem customerSiteId                  | "{"installBase":"${installBase}","detail":"${detail}"}"                             | "Field customerSiteId is required."              | "400.001" | "en-US"  |
      | com customerSiteId vazio            | "{"customerSiteId":"","installBase":"${installBase}","detail":"${detail}"}"         | "Field customerSiteId is required."              | "400.001" | "en-US"  |
      | com customerSiteId nulo             | "{"customerSiteId":null,"installBase":"${installBase}","detail":"${detail}"}"       | "Field customerSiteId is required."              | "400.001" | "en-US"  |
      | com customerSiteId formato inválido | "{"customerSiteId":"banana","installBase":"${installBase}","detail":"${detail}"}"   | "Field customerSiteId has an invalid format."    | "400.004" | "en-US"  |
      | sem installBase                     | "{"customerSiteId":${customerSiteId},"detail":"${detail}"}"                         | "Field installBase is required."                 | "400.001" | "en-US"  |
      | com installBase vazio               | "{"customerSiteId":${customerSiteId},"installBase":"","detail":"${detail}"}"        | "Field installBase is required."                 | "400.001" | "en-US"  |
      | com installBase nulo                | "{"customerSiteId":${customerSiteId},"installBase":null,"detail":"${detail}"}"      | "Field installBase is required."                 | "400.001" | "en-US"  |
      | sem detail                          | "{"customerSiteId":${customerSiteId},"installBase":"${installBase}"}"               | "Field detail is required."                      | "400.001" | "en-US"  |
      | com detail vazio                    | "{"customerSiteId":${customerSiteId},"installBase":"${installBase}","detail":""}"   | "Field detail is required."                      | "400.001" | "en-US"  |
      | com detail nulo                     | "{"customerSiteId":${customerSiteId},"installBase":"${installBase}","detail":null}" | "Field detail is required."                      | "400.001" | "en-US"  |
      | sem customerSiteId pt               | "{"installBase":"${installBase}","detail":"${detail}"}"                             | "O campo customerSiteId é obrigatório."          | "400.001" | "pt-BR"  |
      | com customerSiteId vazio pt         | "{"customerSiteId":"","installBase":"${installBase}","detail":"${detail}"}"         | "O campo customerSiteId é obrigatório."          | "400.001" | "pt-BR"  |
      | com customerSiteId nulo pt          | "{"customerSiteId":null,"installBase":"${installBase}","detail":"${detail}"}"       | "O campo customerSiteId é obrigatório."          | "400.001" | "pt-BR"  |
      | com customerSiteId formato inválido | "{"customerSiteId":"banana","installBase":"${installBase}","detail":"${detail}"}"   | "O campo customerSiteId tem o formato inválido." | "400.004" | "pt-BR"  |
      | sem installBase pt                  | "{"customerSiteId":${customerSiteId},"detail":"${detail}"}"                         | "O campo installBase é obrigatório."             | "400.001" | "pt-BR"  |
      | com installBase vazio pt            | "{"customerSiteId":${customerSiteId},"installBase":"","detail":"${detail}"}"        | "O campo installBase é obrigatório."             | "400.001" | "pt-BR"  |
      | com installBase nulo pt             | "{"customerSiteId":${customerSiteId},"installBase":null,"detail":"${detail}"}"      | "O campo installBase é obrigatório."             | "400.001" | "pt-BR"  |
      | sem detail pt                       | "{"customerSiteId":${customerSiteId},"installBase":"${installBase}"}"               | "O campo detail é obrigatório."                  | "400.001" | "pt-BR"  |
      | com detail vazio pt                 | "{"customerSiteId":${customerSiteId},"installBase":"${installBase}","detail":""}"   | "O campo detail é obrigatório."                  | "400.001" | "pt-BR"  |
      | com detail nulo pt                  | "{"customerSiteId":${customerSiteId},"installBase":"${installBase}","detail":null}" | "O campo detail é obrigatório."                  | "400.001" | "pt-BR"  |

  @Negativo
  Scenario Outline: Registrar um caso de dúvida e esclarecimento <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                   | body                                                                                  | message                                                | code      | language |
      | com installBase inválido    | "{"customerSiteId":"${customerSiteId}","installBase":"strings","detail":"${detail}"}" | "Install base strings is invalid to customer."         | "422.004" | "en-US"  |
      | com installBase inválido pt | "{"customerSiteId":"${customerSiteId}","installBase":"strings","detail":"${detail}"}" | "Base instalada strings está inválida para o cliente." | "422.004" | "pt-BR"  |

  @Positivo
  Scenario Outline: Registrar um caso de dúvida e esclarecimento<descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I save response header "location" as "location"
    And I save final value of header "location" as "caseId"
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | descricao   | body                                                                                         |
      | com sucesso | "{"customerSiteId":"${customerSiteId}","installBase":"${installBase}","detail":"${detail}"}" |
