Feature: GET_doubts-clarifications.feature
  Operação responsável por consultar os Casos de Dúvida e Esclarecimento.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "/doubts-clarifications" as "endpoint"
    And I save "331706" as "customerSiteId"
    And I save "782c0632-49cf-4468-9f3f-6622cf24d3da" as "caseId"

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar caso de dúvida e esclarecimento <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/<caseId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404
    And I wait 2 seconds

    Examples: 
      | descricao            | assistancyId |
      | com caseId inválido  |            1 |
      | com caseId incorreto | ${caseId}c   |
      
  @Positivo
  Scenario: Consultar caso de dúvida e esclarecimento 
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${caseId}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/createdDate" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscaleted" is not empty
    And I verify if response value "/area" is not empty
    And I verify if response value "/doubtSubject" is not empty
    And I verify if response value "/doubt" is not empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And I verify if response value "/isRead" is not empty
    And Response code must be 200

    