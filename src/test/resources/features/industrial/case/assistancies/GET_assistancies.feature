Feature: GET_assistancies.feature
  Operação responsável por consultar manutenções.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "BIC022667" as "installBase"
    And I save "331706" as "customerSiteId"
    And I save "20200428/8254980" as "caseNumber"
    And I save "/assistancies" as "endpoint"

  @Negativo
  Scenario: Enviar request sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set GET api endpoint as "${endpoint}?_offset=0&customer-site-id=${customerSiteId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar request sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set GET api endpoint as "${endpoint}?_offset=0&customer-site-id=${customerSiteId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar request <description>
    Given I set request header "client_id" as <client_id>
    Given I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?_offset=0&customer-site-id=${customerSiteId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                            | query                                                                                            | language | message                                      | code      |
      | com customer-site-id vazio           | ?_limit=1&_offset=0&customer-site-id=                                                            | "en-US"  | "Field customer-site-id is required."        | "400.001" |
      | sem customer-site-id                 | ?_limit=1&_offset=0                                                                              | "en-US"  | "Field customer-site-id is required."        | "400.001" |
      | sem query parameter                  |                                                                                                  | "en-US"  | "Field customer-site-id is required."        | "400.001" |
      | com begin-date inválido              | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=bla                            | "en-US"  | "Field begin-date has an invalid format."    | "400.004" |
      | com begin-date ano inválido          | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=000-12-12                      | "en-US"  | "Field begin-date has an invalid format."    | "400.004" |
      | com begin-date mês inválido          | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=2019-13-12                     | "en-US"  | "Field begin-date has an invalid format."    | "400.004" |
      | com begin-date dia inválido          | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=2019-12-35                     | "en-US"  | "Field begin-date has an invalid format."    | "400.004" |
      | com end-date inválido                | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=bla                              | "en-US"  | "Field end-date has an invalid format."      | "400.004" |
      | com end-date ano inválido            | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=000-12-12                        | "en-US"  | "Field end-date has an invalid format."      | "400.004" |
      | com end-date mês inválido            | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=2019-13-12                       | "en-US"  | "Field end-date has an invalid format."      | "400.004" |
      | com end-date dia inválido            | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=2019-12-35                       | "en-US"  | "Field end-date has an invalid format."      | "400.004" |
      | com end-date menor que begin-date    | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=2019-01-01&begin-date=2019-12-01 | "en-US"  | "Invalid date range."                        | "400.006" |
      | com limit inválido                   | ?_offset=0&customer-site-id=${customerSiteId}&end-date=2020-01-01&begin-date=2019-12-01&_limit=a | "en-US"  | "Field _limit has an invalid format."        | "400.004" |
      | com offset inválido                  | ?_limit=1&customer-site-id=${customerSiteId}&end-date=2020-12-01&begin-date=2019-12-01&_offset=a | "en-US"  | "Field _offset has an invalid format."       | "400.004" |
      | com customer-site-id vazio pt        | ?_limit=1&_offset=0&customer-site-id=                                                            | "pt-BR"  | "O campo customer-site-id é obrigatório."    | "400.001" |
      | sem customer-site-id pt              | ?_limit=1&_offset=0                                                                              | "pt-BR"  | "O campo customer-site-id é obrigatório."    | "400.001" |
      | sem query parameter pt               |                                                                                                  | "pt-BR"  | "O campo customer-site-id é obrigatório."    | "400.001" |
      | com begin-date inválido pt           | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=bla                            | "pt-BR"  | "O campo begin-date tem o formato inválido." | "400.004" |
      | com begin-date ano inválido pt       | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=000-12-12                      | "pt-BR"  | "O campo begin-date tem o formato inválido." | "400.004" |
      | com begin-date mês inválido pt       | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=2019-13-12                     | "pt-BR"  | "O campo begin-date tem o formato inválido." | "400.004" |
      | com begin-date dia inválido pt       | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=2019-12-35                     | "pt-BR"  | "O campo begin-date tem o formato inválido." | "400.004" |
      | com end-date inválido pt             | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=bla                              | "pt-BR"  | "O campo end-date tem o formato inválido."   | "400.004" |
      | com end-date ano inválido pt         | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=000-12-12                        | "pt-BR"  | "O campo end-date tem o formato inválido."   | "400.004" |
      | com end-date mês inválido pt         | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=2019-13-12                       | "pt-BR"  | "O campo end-date tem o formato inválido."   | "400.004" |
      | com end-date dia inválido pt         | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=2019-12-35                       | "pt-BR"  | "O campo end-date tem o formato inválido."   | "400.004" |
      | com end-date menor que begin-date pt | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=2019-01-01&begin-date=2019-12-01 | "pt-BR"  | "Intervalo de datas inválido."               | "400.006" |
      | com limit inválido pt                | ?_offset=0&customer-site-id=${customerSiteId}&end-date=2020-01-01&begin-date=2019-12-01&_limit=a | "pt-BR"  | "O campo _limit tem o formato inválido."     | "400.004" |
      | com offset inválido pt               | ?_limit=1&customer-site-id=${customerSiteId}&end-date=2020-12-01&begin-date=2019-12-01&_offset=a | "pt-BR"  | "O campo _offset tem o formato inválido."    | "400.004" |
      | por status-code invalido             | ?customer-site-id=${customerSiteId}&status-code=bla                                              | "en-US"  | "Field status-code has an invalid format."   | "400.004" |
      | por status-code incorreto            | ?customer-site-id=${customerSiteId}&status-code=0                                                | "en-US"  | "Field status-code has an invalid format."   | "400.004" |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                      | query                                                                  |
      | com customer-site-id incorreto | ?_limit=1&_offset=0&customer-site-id=1                                 |
      | com case-number vazio          | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&case-number=    |
      | com case-number inválido       | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&case-number=bla |
      | por status-code AAPRO          | ?customer-site-id=${customerSiteId}&status-code=AAPRO                  |
      | por status-code AAPRV          | ?customer-site-id=${customerSiteId}&status-code=AAPRV                  |
      | por status-code ADIADO         | ?customer-site-id=${customerSiteId}&status-code=ADIADO                 |
      | por status-code AGADEQ         | ?customer-site-id=${customerSiteId}&status-code=AGADEQ                 |
      | por status-code AGMAT          | ?customer-site-id=${customerSiteId}&status-code=AGMAT                  |
      | por status-code AGPLAN         | ?customer-site-id=${customerSiteId}&status-code=AGPLAN                 |
      | por status-code AGUANA         | ?customer-site-id=${customerSiteId}&status-code=AGUANA                 |
      | por status-code AGUCONLOG      | ?customer-site-id=${customerSiteId}&status-code=AGUCONLOG              |
      | por status-code AGUTRA         | ?customer-site-id=${customerSiteId}&status-code=AGUTRA                 |
      | por status-code APROG          | ?customer-site-id=${customerSiteId}&status-code=APROG                  |
      | por status-code APROVADO       | ?customer-site-id=${customerSiteId}&status-code=APROVADO               |
      | por status-code ATIVO          | ?customer-site-id=${customerSiteId}&status-code=ATIVO                  |
      | por status-code CANCEL         | ?customer-site-id=${customerSiteId}&status-code=CANCEL                 |
      | por status-code COMP           | ?customer-site-id=${customerSiteId}&status-code=COMP                   |
      | por status-code EDITHIST       | ?customer-site-id=${customerSiteId}&status-code=EDITHIST               |
      | por status-code EMAND          | ?customer-site-id=${customerSiteId}&status-code=EMAND                  |
      | por status-code FECHAR         | ?customer-site-id=${customerSiteId}&status-code=FECHAR                 |
      | por status-code NA             | ?customer-site-id=${customerSiteId}&status-code=NA                     |
      | por status-code NOK            | ?customer-site-id=${customerSiteId}&status-code=NOK                    |
      | por status-code PEDAGE         | ?customer-site-id=${customerSiteId}&status-code=PEDAGE                 |
      | por status-code REMOVIDO       | ?customer-site-id=${customerSiteId}&status-code=REMOVIDO               |
      | por status-code RENDATCLI      | ?customer-site-id=${customerSiteId}&status-code=RENDATCLI              |
      | por status-code SEMEXEC        | ?customer-site-id=${customerSiteId}&status-code=SEMEXEC                |
      | por status-code SNOREC         | ?customer-site-id=${customerSiteId}&status-code=SNOREC                 |
      | por status-code SOLATE         | ?customer-site-id=${customerSiteId}&status-code=SOLATE                 |
      | por status-code WMATL          | ?customer-site-id=${customerSiteId}&status-code=WMATL                  |

  @Positivo
  Scenario Outline: Consultar manutenção <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/caseId" is not empty
    And I verify if response value "/0/caseIdSalesForce" is not empty
    And I verify if response value "/0/caseNumber" is not empty
    And I verify if response value "/0/status/code" is not empty
    And I verify if response value "/0/status/description" is not empty
    And I verify if response value "/0/subject" is not empty
    And I verify if response value "/0/reason" is not empty
    And I verify if response value "/0/detail" is not empty
    And I verify if response value "/0/origin" is not empty
    And I verify if response value "/0/priority" is not empty
    And I verify if response value "/0/sendSMS" is not empty
    And I verify if response value "/0/isEscalated" is not empty
    And I verify if response value "/0/research" is not empty
    And I verify if response value "/0/maximoTeam" is not empty
    And I verify if response value "/0/attendanceCodeOS" is not empty
    And I verify if response value "/0/requestedCase/customerId" is empty
    And I verify if response value "/0/requestedCase/customer-site-id" is empty
    And I verify if response value "/0/requestedCase/installBase" is empty
    And I verify if response value "/0/requestedCase/creationDate" is empty
    And I verify if response value "/0/requestedCase/isIntegrationSuccess" is empty
    And I verify if response value "/0/requestedCase/integrationResponse" is empty
    And Response code must be 200

    Examples: 
      | descricao                | query                                                                                            |
      | sem expand               | ?_offset=0&customer-site-id=${customerSiteId}&end-date=2020-12-01&begin-date=2019-09-01&_limit=1 |
      | com expand inválido      | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&_expand=bla                               |
      | com begin-date vazio     | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&begin-date=                               |
      | com end-date vazio       | ?_limit=1&_offset=0&customer-site-id=${customerSiteId}&end-date=                                 |
      | com limit vazio          | ?_offset=0&customer-site-id=${customerSiteId}&end-date=2020-12-01&begin-date=2019-12-01&_limit=  |
      | sem limit                | ?_offset=0&customer-site-id=${customerSiteId}&end-date=2020-12-01&begin-date=2019-12-01&_limit=  |
      | com offset vazio         | ?_limit=1&customer-site-id=${customerSiteId}&end-date=2020-12-01&begin-date=2019-12-01&_offset=  |
      | sem offset               | ?_limit=1&customer-site-id=${customerSiteId}&end-date=2020-12-01&begin-date=2019-12-01&_offset=  |
      | por case-number          | ?case-number=${caseNumber}&customer-site-id=${customerSiteId}                                    |
      | por status-code vazio    | ?customer-site-id=${customerSiteId}&status-code=                                                 |
      | por status-code ERRINT   | ?customer-site-id=${customerSiteId}&status-code=ERRINT                                           |
      | por status-code ERRO     | ?customer-site-id=${customerSiteId}&status-code=ERRO                                             |
      | por status-code GREOSAUT | ?customer-site-id=${customerSiteId}&status-code=GREOSAUT                                         |

	@Positivo
  Scenario Outline: Consultar manutenção <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/caseId" is not empty
    And I verify if response value "/0/caseIdSalesForce" is not empty
    And I verify if response value "/0/caseNumber" is not empty
    And I verify if response value "/0/status/code" is not empty
    And I verify if response value "/0/status/description" is not empty
    And I verify if response value "/0/subject" is not empty
    And I verify if response value "/0/reason" is not empty
    And I verify if response value "/0/detail" is not empty
    And I verify if response value "/0/origin" is not empty
    And I verify if response value "/0/priority" is not empty
    And I verify if response value "/0/sendSMS" is not empty
    And I verify if response value "/0/isEscalated" is not empty
    And I verify if response value "/0/research" is not empty
    And I verify if response value "/0/maximoTeam" is not empty
    And I verify if response value "/0/attendanceCodeOS" is not empty
    And I verify if response value "/0/requestedCase/customerId" is not empty
    And I verify if response value "/0/requestedCase/customer-site-id" is not empty
    And I verify if response value "/0/requestedCase/installBase" is not empty
    And I verify if response value "/0/requestedCase/creationDate" is not empty
    And I verify if response value "/0/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/0/requestedCase/integrationResponse" is not empty
    And I verify if response value "/0/serviceOrder/statusOs" is not empty
    And I verify if response value "/0/serviceOrder/numberOs" is not empty
    And I verify if response value "/0/serviceOrder/nameOs" is not empty
    And I verify if response value "/0/serviceOrder/responsibleAttendance" is not empty
    And I verify if response value "/0/serviceOrder/team" is not empty
    And I verify if response value "/0/serviceOrder/attendanceForecastDate" is not empty
    And I verify if response value "/0/serviceOrder/attendanceDate" is empty
    And Response code must be 200

    Examples: 
      | descricao                | query                                                                        |
      | por status-code OSGER    | ?customer-site-id=${customerSiteId}&status-code=OSGER&_expand=requestedCase  |
	
	
  @Positivo
  Scenario Outline: Consultar manutenção <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/caseId" is not empty
    And I verify if response value "/0/caseIdSalesForce" is not empty
    And I verify if response value "/0/caseNumber" is not empty
    And I verify if response value "/0/status/code" is not empty
    And I verify if response value "/0/status/description" is not empty
    And I verify if response value "/0/subject" is not empty
    And I verify if response value "/0/reason" is not empty
    And I verify if response value "/0/detail" is not empty
    And I verify if response value "/0/origin" is not empty
    And I verify if response value "/0/priority" is not empty
    And I verify if response value "/0/sendSMS" is not empty
    And I verify if response value "/0/isEscalated" is not empty
    And I verify if response value "/0/research" is empty
    And I verify if response value "/0/maximoTeam" is empty
    And I verify if response value "/0/attendanceCodeOS" is empty
    And I verify if response value "/0/requestedCase/customerId" is empty
    And I verify if response value "/0/requestedCase/customer-site-id" is empty
    And I verify if response value "/0/requestedCase/installBase" is empty
    And I verify if response value "/0/requestedCase/creationDate" is empty
    And I verify if response value "/0/requestedCase/isIntegrationSuccess" is empty
    And I verify if response value "/0/requestedCase/integrationResponse" is empty
    And Response code must be 200

    Examples: 
      | descricao                 | query                                                     |
      | por status-code GERPEDAUT | ?customer-site-id=${customerSiteId}&status-code=GERPEDAUT |

  @Positivo
  Scenario: Consultar manutenção com expand
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?_offset=0&customer-site-id=${customerSiteId}&end-date=2020-12-01&begin-date=2019-09-01&_limit=1&_expand=requestedCase"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/caseId" is not empty
    And I verify if response value "/0/caseIdSalesForce" is not empty
    And I verify if response value "/0/caseNumber" is not empty
    And I verify if response value "/0/status/code" is not empty
    And I verify if response value "/0/status/description" is not empty
    And I verify if response value "/0/subject" is not empty
    And I verify if response value "/0/reason" is not empty
    And I verify if response value "/0/detail" is not empty
    And I verify if response value "/0/origin" is not empty
    And I verify if response value "/0/priority" is not empty
    And I verify if response value "/0/sendSMS" is not empty
    And I verify if response value "/0/isEscalated" is not empty
    And I verify if response value "/0/research" is not empty
    And I verify if response value "/0/maximoTeam" is not empty
    And I verify if response value "/0/attendanceCodeOS" is not empty
    And I verify if response value "/0/requestedCase/customerId" is empty
    And I verify if response value "/0/requestedCase/customerSiteId" is not empty
    And I verify if response value "/0/requestedCase/installBase" is not empty
    And I verify if response value "/0/requestedCase/creationDate" is not empty
    And I verify if response value "/0/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/0/requestedCase/integrationResponse" is not empty
    And Response code must be 200
