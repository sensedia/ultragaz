Feature: GET_assistancies_{assistancyId}.feature
  Operação responsável por consultar uma manutenção

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/case/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "/assistancies" as "endpointAssistancies"
    And I save "BIC022667" as "installBase"
    And I save "331706" as "customerSiteId"
    And I save "0030M000023eoLtQAI" as "contactIdSf"

  @PreRequest
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAssistancies}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "assistancyId<id>"
    And Response code must be 201
    And I wait 2 seconds

    Examples: 
      | id | descricao                               | body                                                                                                                                                                                                                                                                                                                                                                                                                                            |
      |  1 | com sucesso                             | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipment":"vazando","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}     |
      |  2 | com subject aleatório                   | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"strings","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}        |
      |  3 | com reason inválido                     | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROSs","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                           |
      |  4 | com detail inválido                     | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"strings","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                      |
      |  5 | com research vazio                      | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}              |
      |  6 | com research nulo                       | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":null,"parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}            |
      |  7 | sem research                            | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                            |
      |  8 | com parentCaseId vazio                  | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}        |
      |  9 | com parentCaseId nulo                   | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":null,"subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}      |
      | 10 | com parentCaseId inválido               | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"strings","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"} |
      | 11 | sem parentCaseId                        | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                          |
      | 12 | com subreason vazio                     | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                    |
      | 13 | com subreason nulo                      | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":null,"slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                  |
      | 14 | com subreason inválido                  | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"strings","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}             |
      | 15 | sem subreason                           | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                                   |
      | 16 | com slaOS vazio                         | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}           |
      | 17 | com slaOS nulo                          | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":null,"attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}         |
      | 18 | com slaOS inválido                      | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"strings","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}    |
      | 19 | sem slaOS                               | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                      |
      | 20 | com attendanceCodeOS inválido           | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"strings","attendanceScriptEquipament":"string","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}  |
      | 21 | com attendanceScriptEquipament vazio    | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}           |
      | 22 | com attendanceScriptEquipament nulo     | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":null,"attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}         |
      | 23 | com attendanceScriptEquipament inválido | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"strings","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}    |
      | 24 | sem attendanceScriptEquipament          | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptResult":"vazando gas:sim, demais:sim, muito_muito:sim"}                                           |
      | 25 | com attendanceScriptResult vazio        | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":""}                                                 |
      | 26 | com attendanceScriptResult nulo         | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":null}                                               |
      | 27 | com attendanceScriptResult inválido     | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string","attendanceScriptResult":"strings"}                                          |
      | 28 | sem attendanceScriptResult              | {"contactIdSf":"${contactIdSf}","customerSiteId":"${customerSiteId}","installBase":"${installBase}","subject":"manutencao","reason":"OUTROS,VAZAMENTO,EQUIPAMENTOS","detail":"problemao no equipamento","research":"Realizada","parentCaseId":"123","subreason":"Mangueira zoada","slaOS":"2 dias","attendanceCodeOS":"8238","attendanceScriptEquipament":"string"}                                                                             |

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/<assistancyId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404
    And I wait 2 seconds

    Examples: 
      | descricao                  | assistancyId                         |
      | com assistancyId inválido  |                                    1 |
      | com assistancyId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is not empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao   |
      |  1 | com sucesso |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao             |
      |  2 | com subject aleatório |
      |  3 | com reason inválido   |
      |  4 | com detail inválido   |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao         |
      |  6 | com research nulo |
      |  7 | sem research      |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    #And I verify if response value "/parentCaseId" is empty
    And I verify if response value "/subreason" is empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao           |
      | 12 | com subreason vazio |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao                               |
      | 10 | com parentCaseId inválido               |
      | 14 | com subreason inválido                  |
      | 18 | com slaOS inválido                      |
      | 23 | com attendanceScriptEquipament inválido |
      | 24 | sem attendanceScriptEquipament          |
      | 27 | com attendanceScriptResult inválido     |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao                     |
      | 20 | com attendanceCodeOS inválido |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao                            |
      | 21 | com attendanceScriptEquipament vazio |
      | 22 | com attendanceScriptEquipament nulo  |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao                  |
      | 28 | sem attendanceScriptResult |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao          |
      |  5 | com research vazio |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao              |
      |  8 | com parentCaseId vazio |
      |  9 | com parentCaseId nulo  |
      | 11 | sem parentCaseId       |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    #And I verify if response value "/parentCaseId" is empty
    And I verify if response value "/subreason" is empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is not empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao          |
      | 13 | com subreason nulo |
      | 15 | sem subreason      |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointAssistancies}/${assistancyId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response value "/caseId" is not empty
    And I verify if response value "/caseIdSalesForce" is not empty
    And I verify if response value "/caseNumber" is not empty
    And I verify if response value "/status/code" is not empty
    And I verify if response value "/status/description" is not empty
    And I verify if response value "/subject" is not empty
    And I verify if response value "/reason" is not empty
    And I verify if response value "/detail" is not empty
    And I verify if response value "/origin" is not empty
    And I verify if response value "/priority" is not empty
    And I verify if response value "/sendSMS" is not empty
    And I verify if response value "/isEscalated" is not empty
    And I verify if response value "/research" is not empty
    And I verify if response value "/parentCaseId" is not empty
    And I verify if response value "/subreason" is not empty
    And I verify if response value "/maximoTeam" is not empty
    And I verify if response value "/slaOS" is empty
    And I verify if response value "/attendanceCodeOS" is not empty
    And I verify if response value "/attendanceScriptEquipment" is empty
    And I verify if response value "/attendanceScriptResult" is not empty
    And I verify if response value "/requestedCase/customerId" is empty
    And I verify if response value "/requestedCase/customerSiteId" is not empty
    And I verify if response value "/requestedCase/installBase" is not empty
    And I verify if response value "/requestedCase/creationDate" is not empty
    And I verify if response value "/requestedCase/isIntegrationSuccess" is not empty
    And I verify if response value "/requestedCase/integrationResponse" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao       |
      | 16 | com slaOS vazio |
      | 17 | com slaOS nulo  |
      | 19 | sem slaOS       |
