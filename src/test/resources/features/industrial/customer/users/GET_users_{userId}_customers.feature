@GET_users_{userId}_customers
Feature: GET_users_{userId}_customers.feature
  Operação responsável por retornar as informações do cliente associado ao usuário.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpointUsers"
    And I save "/change-profile" as "endpointChangeProfile"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "changeprofile@sensedia.com" as "login"
    And I save "Q4tu&pqk" as "password"

  @PreRequest
  Scenario Outline: Cadastrar user
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | body                                                          |
      |  1 | {"customerCode":"${customerCode}","document":"${document}"}   |
      |  2 | {"customerCode":"${CODESegundo}","document":"${CNPJSegundo}"} |

  @PreRequest
  Scenario Outline: Cadastrar contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contact":"${login}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"<lastInvoice>","penultimateInvoiceNumber":"<penultimateInvoice>"}}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId<id>"
    And Response code must be 201

    Examples: 
      | id | lastInvoice                 | penultimateInvoice                 |
      |  1 | ${lastInvoiceNumber}        | ${penultimateInvoiceNumber}        |
      |  2 | ${lastInvoiceNumberSegundo} | ${penultimateInvoiceNumberSegundo} |
      |  1 | ${lastInvoiceNumber}        | ${penultimateInvoiceNumber}        |

  @PreRequest
  Scenario Outline: Atribuir senha
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactId":"${contactId<id>}","password":"${password}","passwordConfirmation":"${password}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | id |
      |  1 |
      |  2 |

  @PreRequest
  Scenario: Validar user 1
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"REGISTER","contactId":"${contactId1}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    Given I get token on database by contact_id "${contactId1}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @PreRequest
  Scenario: Gerar token para conta 1
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"${login}","password":"${password}"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @PreRequest
  Scenario Outline: Cadastrar usuário <descricao>
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "location"
    And Response code must be 201

    Examples: 
      | id | descricao   | body                                                          |
      |  1 | com sucesso | {"customerCode":"${CODESegundo}","document":"${CNPJSegundo}"} |

  @PreRequest
  Scenario: Consultar perfil 1
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "/customers/me"
    Then I get response body
    And I get response status code
    And I compare response value "/customerCode" with "${customerCode}"
    And I compare response value "/document" with "${document}"
    And Response code must be 200

  @PreRequest
  Scenario: Consultar relacionamentos
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I save response value "/0/userCustomerRelationshipId" as "userCustomerRelationshipId"
    And Response code must be 200

  @Negativo
  Scenario: Enviar request sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "<clientId>"
    And I set request header "Accept-Language" as "<language>"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

    Examples: 
      | parameter         | clinet_id     | message                                                                       | language |
      | ClientId inválido | ${client_id}1 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | ClientId vazio    |               | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario: Enviar request sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<access_token>"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

    Examples: 
      | parameter             | clinet_id        | message                                                                       | language |
      | access_token inválido | ${access_token}1 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | access_token vazio    |                  | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/<userId>/customers?_limit=10&_offset=0"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario Outline: Consultar customers <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers<queries>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                  | queries              | message                                 | code    | language |
      | com _limit inválido        | ?_offset=0&_limit=a  | Field _limit has an invalid format.     | 400.003 | en-US    |
      | com _offset inválido       | ?_limit=10&_offset=a | Field _offset has an invalid format.    | 400.003 | en-US    |
      | com _limit inválido em pt  | ?_offset=0&_limit=a  | O campo _limit tem o formato inválido.  | 400.003 | pt-BR    |
      | com _offset inválido em pt | ?_limit=10&_offset=a | O campo _offset tem o formato inválido. | 400.003 | pt-BR    |

  @Negativo
  Scenario Outline: Consultar customers <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers<queries>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                 | queries         | message                                   |
      | com status inválido       | ?status=a       | Field status has an invalid format.       |
      | com address_type inválido | ?address_type=a | Field address_type has an invalid format. |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerCode" is not empty
    And I verify if response value "/0/userCustomerRelationshipId" is not empty
    And I verify if response value "/0/name" is not empty
    And I verify if response value "/0/document" is not empty
    And I compare response value "/0/status" with "<status>"
    And I verify if response value "/0/customerAddress/0/siteId" is not empty
    And I verify if response value "/0/customerAddress/0/code" is not empty
    And I verify if response value "/0/customerAddress/0/addressLine" is not empty
    And I verify if response value "/0/customerAddress/0/addressNumber" is not empty
    And I verify if response value "/0/customerAddress/0/complement" is not empty
    And I verify if response value "/0/customerAddress/0/country" is not empty
    And I verify if response value "/0/customerAddress/0/city" is not empty
    And I verify if response value "/0/customerAddress/0/zipCode" is not empty
    And I verify if response value "/0/customerAddress/0/addressType" is not empty
    And I verify if response value "/0/customerAddress/0/status" is not empty
    And Response code must be 200

    Examples: 
      | descricao                       | query                       | status  |
      | sem query parameter             |                             | ATIVO   |
      | com query parameter             | ?_offset=0&_limit=12        | ATIVO   |
      | sem _limit                      | ?_offset=0                  | ATIVO   |
      | com _limit vazio                | ?_offset=0&_limit=          | ATIVO   |
      | com _offset vazio               | ?_limit=10&_offset=         | ATIVO   |
      | com status ativo                | ?status=ATIVO               | ATIVO   |
      | COM  address_type SHIP_TO       | ?address_type=SHIP_TO       | ATIVO   |
      | com  address_type BILL_TO       | ?address_type=BILL_TO       | ATIVO   |
      | com  address_type CSTU_COBRANCA | ?address_type=CSTU_COBRANCA | ATIVO   |

  @Positivo
  Scenario Outline: Consultar user Teste de paginação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerCode" is not empty
    And I verify if response value "/0/userCustomerRelationshipId" is not empty
    And I verify if response value "/0/name" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/customerAddress/0/siteId" is not empty
    And I verify if response value "/0/customerAddress/0/code" is not empty
    And I verify if response value "/0/customerAddress/0/addressLine" is not empty
    And I verify if response value "/0/customerAddress/0/addressNumber" is not empty
    And I verify if response value "/0/customerAddress/0/complement" is not empty
    And I verify if response value "/0/customerAddress/0/country" is not empty
    And I verify if response value "/0/customerAddress/0/city" is not empty
    And I verify if response value "/0/customerAddress/0/zipCode" is not empty
    And I verify if response value "/0/customerAddress/0/addressType" is not empty
    And I verify if response value "/0/customerAddress/0/status" is not empty
    And I verify if response value "/1/customerId" is empty
    And I verify if response value "/1/customerCode" is empty
    And I verify if response value "/1/userCustomerRelationshipId" is empty
    And I verify if response value "/1/name" is empty
    And I verify if response value "/1/document" is empty
    And I verify if response value "/1/status" is empty
    And I verify if response value "/1/customerAddress/0/siteId" is empty
    And I verify if response value "/1/customerAddress/0/code" is empty
    And I verify if response value "/1/customerAddress/0/addressLine" is empty
    And I verify if response value "/1/customerAddress/0/address" is empty
    And I verify if response value "/1/customerAddress/0/addressNumber" is empty
    And I verify if response value "/1/customerAddress/0/complement" is empty
    And I verify if response value "/1/customerAddress/0/country" is empty
    And I verify if response value "/1/customerAddress/0/city" is empty
    And I verify if response value "/1/customerAddress/0/zipCode" is empty
    And I verify if response value "/1/customerAddress/0/addressType" is empty
    And I verify if response value "/1/customerAddress/0/status" is empty
    And Response code must be 200

    Examples: 
      | descricao   | query               |
      |           0 | ?_offset=0&_limit=1 |
      |           1 | ?_offset=1&_limit=1 |
      | sem _offset | ?_limit=1           |

  @Positivo
  Scenario Outline: Consultar user Teste de paginação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers<query>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao              | query                |
      | - Pagina sem registro  | ?_offset=1&_limit=20 |
      | com address_type vazio | ?address_type=       |
      | sem status             | ?status=0            |
      | com status vazio       | ?status=             |
      | sem address_type       | ?address_type=0      |

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId1}" and login "${login}"
    Given I remove info from userId "${userId2}" and login "${login}"
