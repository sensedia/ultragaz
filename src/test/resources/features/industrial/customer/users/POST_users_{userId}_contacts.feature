@POST_users_{userId}_contacts
Feature: POST_users_{userId}_contacts.feature
  Operação responsável por criar novo contato para usuário no portal.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "emailtest@sensedia.com" as "contact"
    And I save "EMAIL" as "contactType"
    And I save "token" as "token"
    And I save "testaddbyinvoice@sensedia.com" as "login"
    And I save "ADD_CONTACT" as "tokenType"
    And I save "${CNPJMICNPJ}" as "documentPj"
    And I save "${CODEMI}" as "customerCodeMi"
    And I save "${CPFMI}" as "documentPf"
    And I save "${CODEMICNPJ}" as "customerCodeMiCnpj"
    And I save "contactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongo@emaillongo.com" as "contactLongo"
    And I save "2126226" as "siteId"
    And I save "3506072" as "lastInvoiceNumber"
    And I save "1951556" as "penultimateInvoiceNumber"

  @PreRequest
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | descricao               | body                                                          |
      |  1 | empresarial com sucesso | "{"customerCode":"${customerCode}","document":"${document}"}" |
      |  2 | MI com sucesso          | "{"document":"${documentPf}"}"                                |

  @PreRequest
  Scenario Outline: Consultar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}/${userId<id>}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And I save response value "/0/contactId" as "contactId<id>"
    And Response code must be 200

    Examples: 
      | id | descricao               |
      |  1 | empresarial com sucesso |
      |  2 | MI com sucesso          |

  @PreRequest
  Scenario Outline: Enviar token para telefone <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"${tokenType}","siteId":"${siteId}","contactId":"${contactId<id>}"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpoint}/${userId<id>}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | id | descricao               |
      |  1 | empresarial com sucesso |
      |  2 | MI com sucesso          |

  @Definition
  Scenario: Pegar token recém enviado para tabela do banco de dados
    Given I get token on database by contact_id "${contactId1}"

#	@Negativo
  #Scenario: Enviar requisição sem client_id
   #	Given I set request header "access_token" as "${access_token}"
   #	And I set request header "Content-Type" as "application/json"
   #	And I set request body as "{"contact":"${contact}","siteId":"${sideId}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"
    #When I set POST api endpoint as "${endpoint}/${userId1}/contacts"
    #And I set request header "Content-Type" as "application/json"
    #Then I get response body
    #And I get response status code
    #And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    #And Response code must be 401

  #@Negativo
  #Scenario: Enviar requisição sem access_token
    #Given I set request header "client_id" as "${client_id}"
    #And I set request header "Content-Type" as "application/json"
    #And I set request body as "{"contact":"${contact}","siteId":"${sideId}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"
    #When I set POST api endpoint as "${endpoint}/${userId1}/contacts"
    #Then I get response body
    #And I get response status code
    #And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    #And Response code must be 401
    
  @Negativo
  Scenario: Enviar requisição sem Content-Type
  	Given I set request header "access_token" as "${access_token}"
    And I set request header "client_id" as "${client_id}"
    And I set request body as "{"contact":"${contact}","siteId":"${sideId}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"
    When I set POST api endpoint as "${endpoint}/${userId1}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

#	@Negativo
  #Scenario Outline: Enviar requisição <description>
    #Given I set request header "client_id" as <client_id>
    #And I set request header "access_token" as <access_token>
    #And I set request header "Content-Type" as "application/json"
#		And I set request body as "{"contact":"${contact}","siteId":"${sideId}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"   
    #When I set POST api endpoint as "${endpoint}/${userId1}/contacts"
    #Then I get response body
    #And I get response status code
    #And Response body must be <message>
    #And Response code must be 401
#
    #Examples: 
      #| description               | client_id       | access_token       | message                                                                                    |
      #| com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      #| com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      #| com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      #| com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |
	
  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    When I set POST api endpoint as "${endpoint}/${userId1}/contacts"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                  | language |
      | sem body       | "Invalid request body."  | "en-US"  |
      | sem body em pt | "Request body inválido." | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar contacts empresarial <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/${userId1}/contacts"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                                                  | body                                                                                                                                                                                                          | message                                                             | code      | language |
      | sem contact                                                | "{"contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                             | "Field contact is required."                                        | "400.001" | "en-US"  |
      | com contact vazio                                          | "{"contact":"","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                | "Field contact is required."                                        | "400.001" | "en-US"  |
      | com contact nulo                                           | "{"contact":null,"contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"              | "Field contact is required."                                        | "400.001" | "en-US"  |
      | com contact inválido para EMAIL                            | "{"contact":"019982722791","contactType":"EMAIL","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"             | "Field contact has an invalid format."                              | "400.003" | "en-US"  |
      | com contact EMAIL inválido                                 | "{"contact":"alinebendersensediacom","contactType":"EMAIL","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"   | "Field contact has an invalid format."                              | "400.003" | "en-US"  |
      | com contact longo                                          | "{"contact":"${contactLongo}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}" | "Field contact has an invalid format."                              | "400.003" | "en-US"  |
      | sem contactType                                            | "{"contact":"${contact}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                                     | "Field contactType has an invalid value or is empty."               | "400.007" | "en-US"  |
      | com contactType vazio                                      | "{"contact":"${contact}","contactType":"","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                    | "Field contactType has an invalid value or is empty."               | "400.007" | "en-US"  |
      | com contactType nulo                                       | "{"contact":"${contact}","contactType":null,"siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                  | "Field contactType has an invalid value or is empty."               | "400.007" | "en-US"  |
      | com contactType inválido                                   | "{"contact":"${contact}","contactType":"EMAILS","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"              | "Field contactType has an invalid value or is empty."               | "400.007" | "en-US"  |
      | sem token                                                  | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}"}"                                                                                                                                | "Field token or contactValidation is required."                     | "400.004" | "en-US"  |
      | com token vazio                                            | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","token":""}"                                                                                                                     | "Field token or contactValidation is required."                     | "400.004" | "en-US"  |
      | com token nulo                                             | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","token":null}"                                                                                                                   | "Field token or contactValidation is required."                     | "400.004" | "en-US"  |
      | sem contactValidation                                      | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}"}"                                                                                                                                | "Field token or contactValidation is required."                     | "400.004" | "en-US"  |
      | com contactValidation vazio                                | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{}}"                                                                                                         | "Field contactValidation.lastInvoiceNumber is required."            | "400.001" | "en-US"  |
      | com contactValidation nulo                                 | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":null}"                                                                                                       | "Field token or contactValidation is required."                     | "400.004" | "en-US"  |
      | sem contactValidation.lastInvoiceNumber                    | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                                                 | "Field contactValidation.lastInvoiceNumber is required."            | "400.001" | "en-US"  |
      | com contactValidation.lastInvoiceNumber vazio              | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                          | "Field contactValidation.lastInvoiceNumber is required."            | "400.001" | "en-US"  |
      | com contactValidation.lastInvoiceNumber nulo               | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":null,"penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                        | "Field contactValidation.lastInvoiceNumber is required."            | "400.001" | "en-US"  |
      | sem contactValidation.penultimateInvoiceNumber             | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}"}}"                                                               | "Field contactValidation.penultimateInvoiceNumber is required."     | "400.001" | "en-US"  |
      | com contactValidation.penultimateInvoiceNumber vazio       | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":""}}"                                 | "Field contactValidation.penultimateInvoiceNumber is required."     | "400.001" | "en-US"  |
      | com contactValidation.penultimateInvoiceNumber nulo        | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":null}}"                               | "Field contactValidation.penultimateInvoiceNumber is required."     | "400.001" | "en-US"  |
      | sem siteId                                                 | "{"contact":"${contact}","contactType":"${contactType}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}"}}"                                                                                    | "Field siteId is required."                                         | "400.001" | "en-US"  |
      | com siteId vazio                                           | "{"contact":"${contact}","contactType":"${contactType}","siteId":"","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":""}}"                                          | "Field siteId is required."                                         | "400.001" | "en-US"  |
      | com siteId nulo                                            | "{"contact":"${contact}","contactType":"${contactType}","siteId":null,"contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":null}}"                                      | "Field siteId is required."                                         | "400.001" | "en-US"  |
      | com siteId inválido caractere                              | "{"contact":"${contact}","contactType":"${contactType}","siteId":"33a3","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":null}}"                                    | "Invalid request body."                                             | "400.000" | "en-US"  |
      | com body vazio                                             | "{}"                                                                                                                                                                                                          | "Field contact is required."                                        | "400.001" | "en-US"  |
      | sem contact em pt                                          | "{"contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                             | "O campo contact é obrigatório."                                    | "400.001" | "pt-BR"  |
      | com contact vazio em pt                                    | "{"contact":"","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                | "O campo contact é obrigatório."                                    | "400.001" | "pt-BR"  |
      | com contact nulo em pt                                     | "{"contact":null,"contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"              | "O campo contact é obrigatório."                                    | "400.001" | "pt-BR"  |
      | com contact inválido para EMAIL em pt                      | "{"contact":"019982722791","contactType":"EMAIL","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"             | "O campo contact tem o formato inválido."                           | "400.003" | "pt-BR"  |
      | com contact EMAIL inválido em pt                           | "{"contact":"alinebendersensediacom","contactType":"EMAIL","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"   | "O campo contact tem o formato inválido."                           | "400.003" | "pt-BR"  |
      | com contact longo em pt                                    | "{"contact":"${contactLongo}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}" | "O campo contact tem o formato inválido."                           | "400.003" | "pt-BR"  |
      | sem contactType em pt                                      | "{"contact":"${contact}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                                     | "Campo contactType com valor inválido ou vazio."                    | "400.007" | "pt-BR"  |
      | com contactType vazio em pt                                | "{"contact":"${contact}","contactType":"","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                    | "Campo contactType com valor inválido ou vazio."                    | "400.007" | "pt-BR"  |
      | com contactType nulo em pt                                 | "{"contact":"${contact}","contactType":null,"siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                  | "Campo contactType com valor inválido ou vazio."                    | "400.007" | "pt-BR"  |
      | com contactType inválido em pt                             | "{"contact":"${contact}","contactType":"EMAILS","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"              | "Campo contactType com valor inválido ou vazio."                    | "400.007" | "pt-BR"  |
      | com token vazio em pt                                      | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","token":""}"                                                                                                                     | "O campo token ou contactValidation é obrigatório."                 | "400.004" | "pt-BR"  |
      | com token nulo em pt                                       | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","token":null}"                                                                                                                   | "O campo token ou contactValidation é obrigatório."                 | "400.004" | "pt-BR"  |
      | sem contactValidation em pt                                | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}"}"                                                                                                                                | "O campo token ou contactValidation é obrigatório."                 | "400.004" | "pt-BR"  |
      | com contactValidation vazio em pt                          | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{}}"                                                                                                         | "O campo contactValidation.lastInvoiceNumber é obrigatório."        | "400.001" | "pt-BR"  |
      | com contactValidation nulo em pt                           | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":null}"                                                                                                       | "O campo token ou contactValidation é obrigatório."                 | "400.004" | "pt-BR"  |
      | sem contactValidation.lastInvoiceNumber em pt              | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                                                 | "O campo contactValidation.lastInvoiceNumber é obrigatório."        | "400.001" | "pt-BR"  |
      | com contactValidation.lastInvoiceNumber vazio em pt        | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                          | "O campo contactValidation.lastInvoiceNumber é obrigatório."        | "400.001" | "pt-BR"  |
      | com contactValidation.lastInvoiceNumber nulo em pt         | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":null,"penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"                        | "O campo contactValidation.lastInvoiceNumber é obrigatório."        | "400.001" | "pt-BR"  |
      | sem contactValidation.penultimateInvoiceNumber em pt       | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}"}}"                                                               | "O campo contactValidation.penultimateInvoiceNumber é obrigatório." | "400.001" | "pt-BR"  |
      | com contactValidation.penultimateInvoiceNumber vazio em pt | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":""}}"                                 | "O campo contactValidation.penultimateInvoiceNumber é obrigatório." | "400.001" | "pt-BR"  |
      | com contactValidation.penultimateInvoiceNumber nulo em pt  | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":null}}"                               | "O campo contactValidation.penultimateInvoiceNumber é obrigatório." | "400.001" | "pt-BR"  |
      | sem siteId pt                                              | "{"contact":"${contact}","contactType":"${contactType}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}"}}"                                                                                    | "O campo siteId é obrigatório."                                     | "400.001" | "pt-BR"  |
      | com siteId vazio pt                                        | "{"contact":"${contact}","contactType":"${contactType}","siteId":"","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":""}}"                                          | "O campo siteId é obrigatório."                                     | "400.001" | "pt-BR"  |
      | com siteId nulo pt                                         | "{"contact":"${contact}","contactType":"${contactType}","siteId":null,"contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":null}}"                                      | "O campo siteId é obrigatório."                                     | "400.001" | "pt-BR"  |
      | com siteId inválido caractere pt                           | "{"contact":"${contact}","contactType":"${contactType}","siteId":"33a3","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":null}}"                                    | "Request body inválido."                                            | "400.000" | "pt-BR"  |
      | com body vazio em pt                                       | "{}"                                                                                                                                                                                                          | "O campo contact é obrigatório."                                    | "400.001" | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/${userId1}/contacts"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                                                         | body                                                                                                                                                            | message                                                                 | code      | language |
      | com token inválido                                                | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","token":"token"}"                                                                  | "Invalid token."                                                        | "422.002" | "en-US"  |
      | com lastInvoiceNumber e penultimateInvoiceNumber incorretos       | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"321","penultimateInvoiceNumber":"123"}}" | "Given invoices on contactValidation does not match with user history." | "422.003" | "en-US"  |
      | com siteId não existe para customer                               | "{"contact":"${contact}","contactType":"${contactType}","siteId":"333","contactValidation":{"lastInvoiceNumber":"321","penultimateInvoiceNumber":"123"}}"       | "siteId 333 does not exist to informed customer."                       | "422.027" | "en-US"  |
      | com token inválido em pt                                          | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","token":"token"}"                                                                  | "Token inválido."                                                       | "422.002" | "pt-BR"  |
      | com lastInvoiceNumber e penultimateInvoiceNumber incorretos em pt | "{"contact":"${contact}","contactType":"${contactType}","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"321","penultimateInvoiceNumber":"123"}}" | "Notas inválidas para o código do cliente selecionado."                 | "422.003" | "pt-BR"  |
      | com siteId não existe para customer pt                            | "{"contact":"${contact}","contactType":"${contactType}","siteId":"333","contactValidation":{"lastInvoiceNumber":"321","penultimateInvoiceNumber":"123"}}"       | "siteId 333 não existe para o cliente informado."                       | "422.027" | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar contact <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contact":"${contact}","siteId":"${sideId}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"
    When I set POST api endpoint as "${endpoint}/<userId>/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  | batax                                |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario Outline: Cadastrar contact <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I get token on database by contact_id "${contactId<id>}"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/${userId<id>}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | id | descricao                | body                                                                                                                                                                                          |
      |  1 | empresarial por invoices | "{"contact":"${login}","contactType":"EMAIL","siteId":"${siteId}","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}" |
      |  1 | empresarial por token    | "{"contact":"testaddbytoken@sensedia.com","contactType":"EMAIL","token":"${TOKEN}"}"                                                                                                          |
      |  2 | MI por token             | "{"contact":"testMiToken@sensedia.com","contactType":"EMAIL","token":"${TOKEN}"}"                                                                                                             |
      |  2 | MI sem token             | "{"contact":"testMiSemToken@sensedia.com","contactType":"EMAIL"}"                                                                                                                             |
      |  2 | MI com token vazio       | "{"contact":"testMiTokenVazio@sensedia.com","contactType":"EMAIL","token":""}"                                                                                                                |
      |  2 | MI com token nulo        | "{"contact":"testMiTokenNulo@sensedia.com","contactType":"EMAIL","token":null}"                                                                                                               |

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId1}" and login "${login}"
    Given I remove info from userId "${userId1}" and login "testaddbytoken@sensedia.com"
    Given I remove info from userId "${userId2}" and login "testMiToken@sensedia.com"
    Given I remove info from userId "${userId2}" and login "testMiSemToken@sensedia.com"
    Given I remove info from userId "${userId2}" and login "testMiTokenVazio@sensedia.com"
    Given I remove info from userId "${userId2}" and login "testMiTokenNulo@sensedia.com"
