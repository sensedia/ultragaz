@POST_users_{userId}_validations
Feature: POST_users_{userId}_validations.feature
  Operação responsável por validar token.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpointUsers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "${CPFMI}" as "documentPf"
    And I save "${CNPJMICNPJ}" as "documentPj"
    And I save "${CODEMI}" as "customerCodeMi"
    And I save "${CODEMICNPJ}" as "customerCodeMiCnpj"

  @PreRequest
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | descricao               | body                                                        |
      |  1 | empresarial com sucesso | {"customerCode":"${customerCode}","document":"${document}"} |
      |  2 | MI com sucesso          | {"document":"${documentPf}"}                                |

  @PreRequest
  Scenario Outline: Consultar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId<id>}/contacts?contactType=EMAIL"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And I save response value "/0/contactId" as "contactId<id>"
    And Response code must be 200

    Examples: 
      | id | descricao               |
      |  1 | empresarial com sucesso |
      |  2 | MI com sucesso          |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                | language |
      | sem body       | Invalid request body.  | en-US    |
      | sem body em pt | Request body inválido. | pt-BR    |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao             | body           | message                      | code    | language |
      | sem token             | {}             | Field token is required.     | 400.001 | en-US    |
      | com token vazio       | {"token":""}   | Field token is required.     | 400.001 | en-US    |
      | com token nulo        | {"token":null} | Field token is required.     | 400.001 | en-US    |
      | sem token em pt       | {}             | O campo token é obrigatório. | 400.001 | pt-BR    |
      | com token vazio em pt | {"token":""}   | O campo token é obrigatório. | 400.001 | pt-BR    |
      | com token nulo em pt  | {"token":null} | O campo token é obrigatório. | 400.001 | pt-BR    |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                 | body          | message         | code    | language |
      | com token incorreto       | {"token":"1"} | Invalid token.  | 422.002 | en-US    |
      | com token incorreto em pt | {"token":"1"} | Token inválido. | 422.002 | pt-BR    |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${token}"}"
    When I set POST api endpoint as "${endpointUsers}/<userId>/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |


  @Positivo
  Scenario Outline: Validar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId<id>}","url":"https://www.google.com"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    And I wait 3 seconds
    Given I get token on database by contact_id "${contactId<id>}"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | id | descricao                          | body                 | tokenType |
      |  1 | empresarial com tokenType REGISTER | {"token":"${TOKEN}"} | REGISTER  |
      |  2 | MI com tokenType REGISTER          | {"token":"${TOKEN}"} | REGISTER  |

#validar alteração cadastral

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId1}" and login "emailtest@sensedia.com"
    Given I remove info from userId "${userId2}" and login "emailtestMI@sensedia.com"
    Given I remove info from userId "${userId2}" and login "${documentPf}"
