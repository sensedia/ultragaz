@POST_users
Feature: POST_users.feature
  Operação responsável por criar um novo usuário no portal.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And System generate random CPF
    And I use api name as "/customer/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "/users" as "endpointUsers"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "${CPFMI}" as "documentPf"
    And I save "${CNPJMICNPJ}" as "documentPj"
    And I save "${CODEMI}" as "customerCodeMi"
    And I save "${CODEMICNPJ}" as "customerCodeMiCnpj"

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Cadastrar users <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                      | body                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | message                                                 | code    | language |
      | com customerCode vazio         | {"customerCode":"","document":"${document}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Field customerCode is required.                         | 400.001 | en-US    |
      | com customerCode nulo          | {"customerCode":null,"document":"${document}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | Field customerCode is required.                         | 400.001 | en-US    |
      | com customerCode longo         | {"customerCode":"customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. ","document":"${document}"} | Field customerCode must have up to 100 characters.      | 400.002 | en-US    |
      | sem customerCode               | {"document":"${document}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Field customerCode is required.                         | 400.001 | en-US    |
      | com document vazio             | {"customerCode":"string","document":""}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | Field document is required.                             | 400.001 | en-US    |
      | com document nulo              | {"customerCode":"string","document":null}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Field document is required.                             | 400.001 | en-US    |
      | com document longo             | {"customerCode":"string","document":"101010101011"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Field document has an invalid format.                   | 400.003 | en-US    |
      | com document incorreto         | {"customerCode":"string","document":"10101010101"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Field document has an invalid format.                   | 400.003 | en-US    |
      | com document com mascara       | {"customerCode":"string","document":"454.010.358-02"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Field document has an invalid format.                   | 400.003 | en-US    |
      | sem document                   | {"customerCode":"string"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Field document is required.                             | 400.001 | en-US    |
      | com customerCode vazio em pt   | {"customerCode":"","document":"${document}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | O campo customerCode é obrigatório.                     | 400.001 | pt-BR    |
      | com customerCode nulo em pt    | {"customerCode":null,"document":"${document}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | O campo customerCode é obrigatório.                     | 400.001 | pt-BR    |
      | com customerCode longo em pt   | {"customerCode":"customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. ","document":"${document}"} | O campo customerCode deve conter apenas 100 caracteres. | 400.002 | pt-BR    |
      | sem customerCode em pt         | {"document":"${document}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | O campo customerCode é obrigatório.                     | 400.001 | pt-BR    |
      | com document vazio em pt       | {"customerCode":"string","document":""}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | O campo document é obrigatório.                         | 400.001 | pt-BR    |
      | com document nulo em pt        | {"customerCode":"string","document":null}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | O campo document é obrigatório.                         | 400.001 | pt-BR    |
      | com document longo em pt       | {"customerCode":"string","document":"101010101011"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | O campo document tem o formato inválido.                | 400.003 | pt-BR    |
      | com document incorreto em pt   | {"customerCode":"string","document":"10101010101"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | O campo document tem o formato inválido.                | 400.003 | pt-BR    |
      | com document com mascara em pt | {"customerCode":"string","document":"454.010.358-02"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | O campo document tem o formato inválido.                | 400.003 | pt-BR    |
      | sem document em pt             | {"customerCode":"string"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | O campo document é obrigatório.                         | 400.001 | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar users <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                                      | body                                                          | message                                                | code    | language |  |
      | com customerCode inexistente                   | {"customerCode":"string","document":"${document}"}            | customerCode and document does not exist.              | 422.004 | en-US    |  |
      | com document incorreto para customerCode       | {"customerCode":"CODE","document":"${randomCPF}"}             | customerCode and document does not exist.              | 422.004 | en-US    |  |
      | MI com customerCode incorreto                  | {"customerCode":"${customerCode}","document":"${documentPf}"} | customerCode and document does not exist.              | 422.004 | en-US    |  |
      | com customerCode inexistente em pt             | {"customerCode":"string","document":"${document}"}            | Código do cliente e documento não são correspondentes. | 422.004 | pt-BR    |  |
      | com document incorreto para customerCode em pt | {"customerCode":"CODE","document":"${randomCPF}"}             | Código do cliente e documento não são correspondentes. | 422.004 | pt-BR    |  |
      | MI com customerCode incorreto em pt            | {"customerCode":"${customerCode}","document":"${documentPf}"} | Código do cliente e documento não são correspondentes. | 422.004 | pt-BR    |  |

  @Positivo
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | descricao                    | body                                                            |
      |  1 | empresarial com sucesso      | {"customerCode":"${customerCode}","document":"${document}"}     |
      |  2 | MI sem customerCode          | {"document":"${documentPf}"}                                    |
      |  3 | MI com customerCode vazio    | {"customerCode":"","document":"${documentPf}"}                  |
      |  4 | MI com customerCode nulo     | {"customerCode":null,"document":"${documentPf}"}                |
      |  5 | MI sem customerCode com CNPJ | {"document":"${documentPj}"}                                    |
      |  6 | MI com customerCode          | {"customerCode":"${customerCodeMi}","document":"${documentPf}"} |
      |  7 | MI com CNPJ repetido         | {"document":"${documentPj}"}                                    |
