@POST_users_change-profile
Feature: POST_users_change-profile.feature
  Operação responsável por trocar o perfil de um usuário.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpointUsers"
    And I save "/change-profile" as "endpointChangeProfile"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "changeprofile@sensedia.com" as "login"
    And I save "Q4tu&pqk" as "password"

  @PreRequest
  Scenario Outline: Cadastrar user
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | body                                                          |
      |  1 | {"customerCode":"${customerCode}","document":"${document}"}   |
      |  2 | {"customerCode":"${CODESegundo}","document":"${CNPJSegundo}"} |

  @PreRequest
  Scenario Outline: Cadastrar contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contact":"${login}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"<lastInvoice>","penultimateInvoiceNumber":"<penultimateInvoice>"}}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId<id>"
    And Response code must be 201

    Examples: 
      | id | lastInvoice                 | penultimateInvoice                 |
      |  1 | ${lastInvoiceNumber}        | ${penultimateInvoiceNumber}        |
      |  2 | ${lastInvoiceNumberSegundo} | ${penultimateInvoiceNumberSegundo} |

  @PreRequest
  Scenario Outline: Atribuir senha
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactId":"${contactId<id>}","password":"${password}","passwordConfirmation":"${password}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | id |
      |  1 |
      |  2 |

  @PreRequest
  Scenario: Validar user 1
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"REGISTER","contactId":"${contactId1}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    Given I get token on database by contact_id "${contactId1}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @PreRequest
  Scenario: Gerar token para conta 1
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"${login}","password":"${password}"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @PreRequest
  Scenario Outline: Cadastrar usuário <descricao>
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "location"
    And Response code must be 201

    Examples: 
      | id | descricao   | body                                                          |
      |  1 | com sucesso | {"customerCode":"${CODESegundo}","document":"${CNPJSegundo}"} |

  @PreRequest
  Scenario: Consultar perfil 1
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "/customers/me"
    Then I get response body
    And I get response status code
    And I compare response value "/customerCode" with "${customerCode}"
    And I compare response value "/document" with "${document}"
    And Response code must be 200

  @PreRequest
  Scenario: Consultar relacionamentos
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I save response value "/0/userCustomerRelationshipId" as "userCustomerRelationshipId"
    And Response code must be 200

  @Negativo
  Scenario: Enviar request sem client_id
    Given I set request header "Content-Type" as "application/json"
    And I set request body as "{"username":"${username}","userCustomerRelationshipId":"${userCustomerRelationshipId}"}"
    When I set POST api endpoint as "${endpointUsers}${endpointChangeProfile}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar request sem access_token
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request body as "{"username":"${username}","userCustomerRelationshipId":"${userCustomerRelationshipId}"}"
    When I set POST api endpoint as "${endpointUsers}${endpointChangeProfile}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "<client_id>"
    And I set request header "access_token" as "<access_token>"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"username":"${username}","userCustomerRelationshipId":"${userCustomerRelationshipId}"}"
    When I set POST api endpoint as "${endpointUsers}${endpointChangeProfile}"
    Then I get response body
    And I get response status code
    And Response body must be "<message>"
    And Response code must be 401

    Examples: 
      | parameter             | client_id     | access_token     | message                                                                                  | language |
      | client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            | en-US    |
      | client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            | en-US    |
      | access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token | en-US    |
      | access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token | en-US    |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"username":"${username}","userCustomerRelationshipId":"${userCustomerRelationshipId}"}"
    When I set POST api endpoint as "${endpointUsers}${endpointChangeProfile}"
    Then I get response body
    And I get response status code
    #And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}${endpointChangeProfile}"
    Then I get response body
    And I get response status code
    #And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Autenticar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}${endpointChangeProfile}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao      | body | language | message                                               | code    |
      | com body vazio | {}   | en-US    | Username and userCustomerRelationshipId are required. | 400.001 |

  #| com body vazio em pt | {}   | pt-BR    | O campo login é obrigatório.                          | 400.001 |
  @Negativo
  Scenario Outline: Autenticar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}${endpointChangeProfile}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And Response code must be <responseCode>

    Examples: 
      | descricao                                  | body                                                                                               | message                                                                             | code    | language | responseCode |
      | sem username                               | {"userCustomerRelationshipId":"${userCustomerRelationshipId}"}                                     | Username and userCustomerRelationshipId are required.                               | 400.001 | en-US    |          400 |
      | com username vazio                         | {"username":"","userCustomerRelationshipId":"${userCustomerRelationshipId}"}                       | Username and userCustomerRelationshipId are required.                               | 400.001 | en-US    |          400 |
      | com username nulo                          | {"username":null,"userCustomerRelationshipId":"${userCustomerRelationshipId}"}                     | Username and userCustomerRelationshipId are required.                               | 400.001 | en-US    |          400 |
      | com username inválido                      | {"username":"aline","userCustomerRelationshipId":"${userCustomerRelationshipId}"}                  | It was not possible to validate user's credentials or connect to the specified URL. | 400.001 | en-US    |          400 |
      | com username incorreto                     | {"username":"abender@outlook.com.br","userCustomerRelationshipId":"${userCustomerRelationshipId}"} | It was not possible to validate user's credentials or connect to the specified URL. | 400.001 | en-US    |          400 |
      | sem userCustomerRelationshipId             | {"username":"${username}"}                                                                         | Username and userCustomerRelationshipId are required.                               | 400.001 | en-US    |          400 |
      | com userCustomerRelationshipId nulo        | {"username":"${username}","userCustomerRelationshipId":null}                                       | Username and userCustomerRelationshipId are required.                               | 400.001 | en-US    |          400 |
      | com userCustomerRelationshipId vazio       | {"username":"${username}","userCustomerRelationshipId":""}                                         | Username and userCustomerRelationshipId are required.                               | 400.001 | en-US    |          400 |
      | com userCustomerRelationshipId inválido    | {"username":"${username}","userCustomerRelationshipId":"64ca9bbc-0a67-4cba-a7d8-79b4586b8fc3"}     | It was not possible to validate user's credentials or connect to the specified URL. | 400.001 | en-US    |          400 |
      | com userCustomerRelationshipId incorreto   | {"username":"${username}","userCustomerRelationshipId":"099b6d5b-7f64-48ce-9218-1eab37da92bb"}     | It was not possible to validate user's credentials or connect to the specified URL. | 400.001 | en-US    |          400 |
      | sem username em pt                         | {"userCustomerRelationshipId":"${userCustomerRelationshipId}"}                                     | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
      | com username vazio em pt                   | {"username":"","userCustomerRelationshipId":"${userCustomerRelationshipId}"}                       | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
      | com username nulo em pt                    | {"username":null,"userCustomerRelationshipId":"${userCustomerRelationshipId}"}                     | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
      #| com username inválido em pt                    | {"username":"aline","userCustomerRelationshipId":"${userCustomerRelationshipId}"}                  | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
      | com username incorreto em pt               | {"username":"abender@outlook.com.br"}                                                              | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
      #| sem userCustomerRelationshipId em pt           | {"username":"${username}","userCustomerRelationshipId":"${userCustomerRelationshipId}"}            | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
      | com userCustomerRelationshipId nulo em pt  | {"username":"${username}","userCustomerRelationshipId":null}                                       | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
      | com userCustomerRelationshipId vazio em pt | {"username":"${username}","userCustomerRelationshipId":""}                                         | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |

  #| com userCustomerRelationshipId inválido em pt  | {"username":"${username}","userCustomerRelationshipId":"64ca9bbc-0a67-4cba-a7d8-79b4586b8fc3"}     | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
  #| com userCustomerRelationshipId incorreto em pt | {"username":"${username}","userCustomerRelationshipId":"64ca9bbc-0a67-4cba-a7d8-79b4586b8fc2"}     | Username and userCustomerRelationshipId are required.                               | 400.001 | pt-BR    |          400 |
  @Positivo
  Scenario: Cadastrar mudar perfil
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"username":"${login}","userCustomerRelationshipId":"${userCustomerRelationshipId}"}"
    When I set POST api endpoint as "${endpointUsers}${endpointChangeProfile}"
    Then I get response body
    And I get response status code
    And I compare response value "/token_type" with "access_token"
    And I compare response value "/expires_in" with "3600"
    And Response code must be 201

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId1}" and login "${login}"
    And I remove info from userId "${userId2}" and login "${login}"
