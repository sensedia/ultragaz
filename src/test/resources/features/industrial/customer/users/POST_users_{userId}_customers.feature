@POST_users_{userId}_customers
Feature: POST_users_{userId}_customers.feature
  Operação responsável pela associação entre o usuário e o cliente.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token_password.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpointUsers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "Q4tu&pqk" as "password"
    And I save "postcustomertest@sensedia.com" as "login"
    And I save "customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. customerCode longo. " as "codeLongo"
    And System generate random CPF

  @PreRequest
  Scenario Outline: Cadastrar user
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | body                                                          |
      |  1 | {"customerCode":"${customerCode}","document":"${document}"}   |
      |  2 | {"customerCode":"${CODESegundo}","document":"${CNPJSegundo}"} |

  @PreRequest
  Scenario Outline: Cadastrar contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contact":"${login}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"<lastInvoice>","penultimateInvoiceNumber":"<penultimateInvoice>"}}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId<id>"
    And Response code must be 201

    Examples: 
      | id | lastInvoice                 | penultimateInvoice                 |
      |  1 | ${lastInvoiceNumber}        | ${penultimateInvoiceNumber}        |
      |  2 | ${lastInvoiceNumberSegundo} | ${penultimateInvoiceNumberSegundo} |

  @PreRequest
  Scenario Outline: Atribuir senha
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactId":"${contactId<id>}","password":"${password}","passwordConfirmation":"${password}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | id |
      |  1 |
      |  2 |

  @PreRequest
  Scenario Outline: Validar user 1
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"REGISTER","contactId":"${contactId1}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    Given I get token on database by contact_id "${contactId1}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                | language |
      | sem body       | Invalid request body.  | en-US    |
      | sem body em pt | Request body inválido. | pt-BR    |

  @Negativo
  Scenario Outline: Associar customers <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                      | body                                                                     | message                                                 | code    | language |
      | com customerCode vazio         | {"customerCode":"","document":"${document}","siteId": 66973}             | Field customerCode is required.                         | 400.001 | en-US    |
      | com customerCode nulo          | {"customerCode":null,"document":"${document}","siteId": 66973}           | Field customerCode is required.                         | 400.001 | en-US    |
      | com customerCode longo         | {"customerCode":"${codeLongo}","document":"${document}","siteId": 66973} | Field customerCode must have up to 100 characters.      | 400.002 | en-US    |
      | sem customerCode               | {"document":"${document}""siteId": 66973}                                | Field customerCode is required.                         | 400.001 | en-US    |
      | com document vazio             | {"customerCode":"string","document":"""siteId": 66973}                   | Field document is required.                             | 400.001 | en-US    |
      | com document nulo              | {"customerCode":"string","document":null"siteId": 66973}                 | Field document is required.                             | 400.001 | en-US    |
      | com document longo             | {"customerCode":"string","document":"101010101011","siteId": 66973}      | Field document has an invalid format.                   | 400.003 | en-US    |
      | com document incorreto         | {"customerCode":"string","document":"10101010101","siteId": 66973}       | Field document has an invalid format.                   | 400.003 | en-US    |
      | com document com mascara       | {"customerCode":"string","document":"454.010.358-02","siteId": 66973}    | Field document has an invalid format.                   | 400.003 | en-US    |
      | sem document                   | {"customerCode":"string","siteId": 66973}                                | Field document is required.                             | 400.001 | en-US    |
      | sem siteId                     | {"customerCode":"string","document":"${document}",}                      | Field siteId is required.                               | 400.001 | en-US    |
      | com siteId vazio               | {"customerCode":"string","document":"${document}","siteId":}             | Field siteId is required.                               | 400.001 | en-US    |
      | com siteId nulo                | {"customerCode":"string","document":"${document}","siteId":null}         | Field siteId is required.                               | 400.001 | en-US    |
      | com customerCode vazio em pt   | {"customerCode":"","document":"${document}","siteId": 66973}             | O campo customerCode é obrigatório.                     | 400.001 | pt-BR    |
      | com customerCode nulo em pt    | {"customerCode":null,"document":"${document}","siteId": 66973}           | O campo customerCode é obrigatório.                     | 400.001 | pt-BR    |
      | com customerCode longo em pt   | {"customerCode":"${codeLongo}","document":"${document}","siteId": 66973} | O campo customerCode deve conter apenas 100 caracteres. | 400.002 | pt-BR    |
      | sem customerCode em pt         | {"document":"${document}","siteId": 66973}                               | O campo customerCode é obrigatório.                     | 400.001 | pt-BR    |
      | com document vazio em pt       | {"customerCode":"string","document":"","siteId": 66973}                  | O campo document é obrigatório.                         | 400.001 | pt-BR    |
      | com document nulo em pt        | {"customerCode":"string","document":null,"siteId": 66973}                | O campo document é obrigatório.                         | 400.001 | pt-BR    |
      | com document longo em pt       | {"customerCode":"string","document":"101010101011","siteId": 66973}      | O campo document tem o formato inválido.                | 400.003 | pt-BR    |
      | com document incorreto em pt   | {"customerCode":"string","document":"10101010101","siteId": 66973}       | O campo document tem o formato inválido.                | 400.003 | pt-BR    |
      | com document com mascara em pt | {"customerCode":"string","document":"454.010.358-02","siteId": 66973}    | O campo document tem o formato inválido.                | 400.003 | pt-BR    |
      | sem document em pt             | {"customerCode":"string","siteId": 66973}                                | O campo document é obrigatório.                         | 400.001 | pt-BR    |
      | sem siteId pt                  | {"customerCode":"string","document":"${document}",}                      | O campo siteId é obrigatório.                           | 400.001 | pt-BR    |
      | com siteId vazio pt            | {"customerCode":"string","document":"${document}","siteId":}             | O campo siteId é obrigatório.                           | 400.001 | pt-BR    |
      | com siteId nulo pt             | {"customerCode":"string","document":"${document}","siteId":null}         | O campo siteId é obrigatório.                           | 400.001 | pt-BR    |

  @Negativo
  Scenario Outline: Associar customers <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                                      | body                                                            | message                                                            | code    | language |
      | com customerCode inexistente                   | {"customerCode":"string","document":"${document}"}              | customerCode and document does not exist.                          | 422.004 | en-US    |
      | com document incorreto para customerCode       | {"customerCode":"CODE","document":"${randomCPF}"}               | customerCode and document does not exist.                          | 422.004 | en-US    |
      | com customerCode inexistente em pt             | {"customerCode":"string","document":"${document}"}              | Código do cliente e documento não são correspondentes.             | 422.004 | pt-BR    |
      | com document incorreto para customerCode em pt | {"customerCode":"CODE","document":"${randomCPF}"}               | Código do cliente e documento não são correspondentes.             | 422.004 | pt-BR    |
      | com conta sem múltiplos emails                 | {"customerCode":"${CODETerceiro}","document":"${CNPJTerceiro}"} | The establishment has no contact to link with user.                | 422.014 | en-US    |
      | com conta sem múltiplos emails em pt           | {"customerCode":"${CODETerceiro}","document":"${CNPJTerceiro}"} | O estabelecimento não possui contatos para associar com o usuário. | 422.014 | pt-BR    |

  @Negativo
  Scenario Outline: Associar customers <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"customerCode":"${customerCode}","document":"${document}"}"
    When I set POST api endpoint as "${endpointUsers}/<userId>/customers"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  | batax                                |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/customers"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userCustomerId"
    And Response code must be 201

    Examples: 
      | id | descricao   | body                                                          |
      |  1 | com sucesso | {"customerCode":"${CODESegundo}","document":"${CNPJSegundo}"} |

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId1}" and login "${login}"
    Given I remove info from userId "${userId2}" and login "${login}"
