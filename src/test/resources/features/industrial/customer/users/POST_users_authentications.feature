@POST_users_authentications
Feature: POST_users_authentications.feature
  Operação responsável por autenticar um usuário.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8001"
    And I use api name as "/customer/v1"
    And I save "/users/authentications" as "endpointAuthentication"
    And I save "/users" as "endpointUsers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "Q4tu&pqk" as "password"
    And I save "testaddbyinvoice@sensedia.com" as "login"
    And I save "${CPFMI}" as "documentPf"
    And I save "${CNPJMICNPJ}" as "documentPj"
    And I save "${CODEMI}" as "customerCodeMi"
    And I save "${CODEMICNPJ}" as "customerCodeMiCnpj"

  @PreRequest
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | descricao               | body                                                        |
      |  1 | empresarial com sucesso | {"customerCode":"${customerCode}","document":"${document}"} |
      |  2 | MI com sucesso          | {"document":"${documentPf}"}                                |

  @PreRequest
  Scenario: Cadastrar contact empresarial por invoices
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contact":"${login}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId"
    And Response code must be 201
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactId":"${contactId}","password":"${password}","passwordConfirmation":"${password}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

  @PreRequest
  Scenario: Enviar token para telefone
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"REGISTER","contactId":"${contactId}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    Given I get token on database by contact_id "${contactId}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @PreRequest
  Scenario Outline: Consultar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId<id>}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And I save response value "/0/contactId" as "contactId<id>"
    And Response code must be 200

    Examples: 
      | id | descricao      |
      |  2 | MI com sucesso |

  @PreRequest
  Scenario Outline: Enviar token para telefone <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"ADD_CONTACT","contactId":"${contactId<id>}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    Given I get token on database by contact_id "${contactId<id>}"

    Examples: 
      | id | descricao      |
      |  2 | MI com sucesso |

  @PreRequest
  Scenario Outline: Cadastrar contact <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    And I get token on database by contact_id "${contactId<id>}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | id | descricao    | body                                                                     |
      |  2 | MI por token | {"contact":"testMI@sensedia.com","contactType":"EMAIL","token":${TOKEN}} |

  @PreRequest
  Scenario Outline: Enviar token e cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId<contactId>}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    And I wait 4 seconds
    And I get token on database by contact_id "${contactId<contactId>}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactId":"${contactId<contactId>}","password":"${password}","passwordConfirmation":"${password}","token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | contactId | descricao                                 | tokenType | userId |
      |         2 | para MI com tokenType REGISTER para PHONE | REGISTER  |      2 |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request body as "{"login":"${login}","password":"${password}"}"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Authenticar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageOne>"
    And I compare response value "/1/code" with "<code>"
    And I verify if response value "/2/message" is empty
    And I verify if response value "/2/code" is empty
    And Response code must be 400

    Examples: 
      | descricao            | body | language | message                      | code    | messageOne                      | messageTwo                   |
      | com body vazio       | {}   | en-US    | Field login is required.     | 400.001 | Field password is required.     | Field login is required.     |
      | com body vazio em pt | {}   | pt-BR    | O campo login é obrigatório. | 400.001 | O campo password é obrigatório. | O campo login é obrigatório. |

  @Negativo
  Scenario Outline: Autenticar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be <responseCode>

    Examples: 
      | descricao               | body                                    | message                         | code    | language | responseCode |
      | sem login               | {"password":"${password}"}              | Field login is required.        | 400.001 | en-US    |          400 |
      | com login vazio         | {"login":"","password":"${password}"}   | Field login is required.        | 400.001 | en-US    |          400 |
      | com login nulo          | {"login":null,"password":"${password}"} | Field login is required.        | 400.001 | en-US    |          400 |
      | sem password            | {"login":"${login}"}                    | Field password is required.     | 400.001 | en-US    |          400 |
      | com password nulo       | {"login":"${login}","password":null}    | Field password is required.     | 400.001 | en-US    |          400 |
      | sem login em pt         | {"password":"${password}"}              | O campo login é obrigatório.    | 400.001 | pt-BR    |          400 |
      | com login vazio em pt   | {"login":"","password":"${password}"}   | O campo login é obrigatório.    | 400.001 | pt-BR    |          400 |
      | com login nulo em pt    | {"login":null,"password":"${password}"} | O campo login é obrigatório.    | 400.001 | pt-BR    |          400 |
      | sem password em pt      | {"login":"${login}"}                    | O campo password é obrigatório. | 400.001 | pt-BR    |          400 |
      | com password nulo em pt | {"login":"${login}","password":null}    | O campo password é obrigatório. | 400.001 | pt-BR    |          400 |

  @Negativo
  Scenario Outline: Autenticar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 401

    Examples: 
      | descricao                   | body                                            | message                                                                                              | code    | language | responseCode |
      | com login inválido          | {"login":"${login}1","password":"${password}"}  | Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character.   | 400.001 | en-US    |          400 |
      | com password inválido       | {"login":"${login}1","password":"${password}1"} | Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character.   | 400.001 | en-US    |          400 |
      | com login inválido em pt    | {"login":"${login}1","password":"${password}"}  | Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial. | 400.001 | pt-BR    |          400 |
      | com password inválido em pt | {"login":"${login}","password":"${password}1"}  | Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial. | 400.001 | pt-BR    |          400 |

  @Positivo
  Scenario Outline: Realizar autenticação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    And I wait 4 seconds

    Examples: 
      | id | descricao   | body                                          |
      |  1 | empresarial | {"login":"${login}","password":"${password}"} |
      |  2 | MI          | {"login":"${CPFMI}","password":"${password}"} |

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId1}" and login "${login}"
    Given I remove info from userId "${userId2}" and login "${CPFMI}"
    Given I remove info from userId "${userId2}" and login "${documentPf}"
