@POST_users_{userId}_tokens
Feature: POST_users_{userId}_tokens.feature
  Operação responsável por enviar token.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpointUsers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "https://www.frontendtoken.com.br" as "url"
    And I save "REGISTER" as "tokenType"
    And I save "${CPFMI}" as "documentPf"
    And I save "${CNPJMICNPJ}" as "documentPj"
    And I save "${CODEMI}" as "customerCodeMi"
    And I save "${CODEMICNPJ}" as "contactId"
    And I save "${CODEMICNPJ}" as "customerCodeMiCnpj"
    And I save "66974" as "siteId"
    And I save "9fcbce73-47b3-48ae-8f9f-b2a71ecab9a6" as "userIdEMPEmail"
    And I save "35e53827-8d2b-464e-8d86-0556b68ec42d" as "userIdEMPPhone"
    And I save "1e18290e-4d1c-4ad1-9ea6-e047118664d2" as "userIdMIEmail"
    And I save "43a3ff02-5519-4063-bfe6-ea551389f079" as "userIdMIPhone"
    And I save "963A3E4F38A47FA4E053976014AC1F5D" as "contactIdEMPEmail"
    And I save "37cee1b1-8ee1-4f85-9b7d-bab4493f8dbf" as "contactIdEMPPhone"
    And I save "f23ed535-c54b-4e66-8547-3a52c692efc0" as "contactIdMIPhone"
    And I save "1b2a9c9f-a0e0-429e-b9ed-ecd4b7df9297" as "contactIdMIEmail"
    And I save "11644" as "customerCodeEMPEmail"
    And I save "300644" as "customerCodeEMPPhone"
    And I save "-999" as "customerCodeMIPhone"
    And I save "-999" as "customerCodeMIEmail"
    And I save "44248862000103" as "documentCodeEMPPhone"
    And I save "03514129000106" as "documentCodeEMPEmail"
    And I save "18561503564" as "documentCodeMIPhone"
    And I save "48793398492" as "documentCodeMIEmail"
    And I save "https://www.google.com.br" as "urlFixo"

  #@PreRequest
  #Scenario Outline: Cadastrar usuário <descricao>
  #Given I set request header "client_id" as "${client_id}"
  #And I set request header "Content-Type" as "application/json"
  #And I set request body as "<body>"
  #When I set POST api endpoint as "${endpointUsers}"
  #Then I get response body
  #And I get response status code
  #And I save response header "Location" as "location"
  #And I save final value of header "Location" as "userId<id>"
  #And Response code must be 201
  #
  #Examples:
  #| id | descricao                    | body                                                            |
  #|  1 | empresarial com sucesso      | {"customerCode":"${customerCode}","document":"${document}"}     |
  #|  2 | MI sem customerCode          | {"document":"${documentPf}"}                                    |
  #|  3 | MI com customerCode vazio    | {"customerCode":"","document":"${documentPf}"}                  |
  #|  4 | MI com customerCode nulo     | {"customerCode":null,"document":"${documentPf}"}                |
  #|  5 | MI sem customerCode com CNPJ | {"document":"${documentPj}"}                                    |
  #|  6 | MI com customerCode          | {"customerCode":"${customerCodeMi}","document":"${documentPf}"} |
  #|  7 | MI com CNPJ repetido         | {"document":"${documentPj}"}                                    |
  #
  #@PreRequest
  #Scenario Outline: Consultar contacts <descricao>
  #Given I set request header "client_id" as "${client_id}"
  #Given I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #When I set GET api endpoint as "${endpointUsers}/${userId<id>}/contacts?contactType=PHONE"
  #Then I get response body
  #And I get response status code
  #And I verify if response value "/0/contactId" is not empty
  #And I verify if response value "/0/contact" is not empty
  #And I verify if response value "/0/contactType" is not empty
  #And I save response value "/0/contactId" as "contactId<id>"
  #And Response code must be 200
  #Given I set request header "client_id" as "${client_id}"
  #And I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #When I set GET api endpoint as "${endpointUsers}/${userId<id>}/contacts?contactType=EMAIL"
  #Then I get response body
  #And I get response status code
  #And I verify if response value "/0/contactId" is not empty
  #And I verify if response value "/0/contact" is not empty
  #And I verify if response value "/0/contactType" is not empty
  #And I save response value "/0/contactId" as "contactId2-<id>"
  #And Response code must be 200
  #
  #Examples:
  #| id | descricao                    |
  #|  1 | empresarial com sucesso      |
  #|  2 | MI sem customerCode          |
  #|  3 | MI com customerCode vazio    |
  #|  4 | MI com customerCode nulo     |
  #|  5 | MI sem customerCode com CNPJ |
  #|  6 | MI com customerCode          |
  #|  7 | MI com CNPJ repetido         |
  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request body as "{"tokenType":"REGISTER","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"
    When I set POST api endpoint as "${endpointUsers}/${userIdEMPEmail}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "Content-Type" as <variable>
    When I set POST api endpoint as "${endpointUsers}/${userIdEMPEmail}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable          |
      | Content-Type inválido | "application/pdf" |
      | Content-Type vazio    | ""                |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    When I set POST api endpoint as "${endpointUsers}/${userIdEMPEmail}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                  | language |
      | sem body       | "Invalid request body."  | "en-US"  |
      | sem body em pt | "Request body inválido." | "pt-BR"  |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpointUsers}/${userIdEMPEmail}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be <responseCode>

    Examples: 
      | descricao                    | body                                                                                                                                                                                        | message                                             | code      | language | responseCode |
      | sem tokenType                | "{"contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                                  | "Field tokenType has an invalid value or is empty." | "400.007" | "en-US"  |          400 |
      | com tokenType vazio          | "{"tokenType":"","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                   | "Field tokenType has an invalid value or is empty." | "400.007" | "en-US"  |          400 |
      | com tokenType nulo           | "{"tokenType":null,"contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                 | "Field tokenType has an invalid value or is empty." | "400.007" | "en-US"  |          400 |
      | com tokenType inválido       | "{"tokenType":"TESTE","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"              | "Field tokenType has an invalid value or is empty." | "400.007" | "en-US"  |          400 |
      | com url inválido             | "{"tokenType":"REGISTER","contactId":"${contactIdEMPEmail}","url":"://www.google.com.br","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}" | "Field url has an invalid format."                  | "400.003" | "en-US"  |          400 |
      | sem contactId                | "{"tokenType":"REGISTER","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                                              | "Field contactId is required."                      | "400.001" | "en-US"  |          400 |
      | com contactId vazio          | "{"tokenType":"REGISTER","contactId":"","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                               | "Field contactId is required."                      | "400.001" | "en-US"  |          400 |
      | com contactId nulo           | "{"tokenType":"REGISTER","contactId":null,"url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                             | "Field contactId is required."                      | "400.001" | "en-US"  |          400 |
      | com body vazio em pt         | "{}"                                                                                                                                                                                        | "O campo contactId é obrigatório."                  | "400.001" | "pt-BR"  |          400 |
      | sem tokenType em pt          | "{"contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                                  | "Campo tokenType com valor inválido ou vazio."      | "400.007" | "pt-BR"  |          400 |
      | com tokenType vazio em pt    | "{"tokenType":"","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                   | "Campo tokenType com valor inválido ou vazio."      | "400.007" | "pt-BR"  |          400 |
      | com tokenType nulo em pt     | "{"tokenType":null,"contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                 | "Campo tokenType com valor inválido ou vazio."      | "400.007" | "pt-BR"  |          400 |
      | com tokenType inválido em pt | "{"tokenType":"TESTE","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"              | "Campo tokenType com valor inválido ou vazio."      | "400.007" | "pt-BR"  |          400 |
      | com url inválido em pt       | "{"tokenType":"REGISTER","contactId":"${contactIdEMPEmail}","url":"teste.com","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"            | "O campo url tem o formato inválido."               | "400.003" | "pt-BR"  |          400 |
      | sem contactId em pt          | "{"tokenType":"REGISTER","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                                              | "O campo contactId é obrigatório."                  | "400.001" | "pt-BR"  |          400 |
      | com contactId vazio em pt    | "{"tokenType":"REGISTER","contactId":"","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                               | "O campo contactId é obrigatório."                  | "400.001" | "pt-BR"  |          400 |
      | com contactId nulo em pt     | "{"tokenType":"REGISTER","contactId":null,"url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"                             | "O campo contactId é obrigatório."                  | "400.001" | "pt-BR"  |          400 |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpointUsers}/${userIdEMPEmail}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response parameter "code" contains value <code>
    And I verify if response parameter "message" contains value <message>
    And I verify if response parameter "code" contains value <code1>
    And I verify if response parameter "message" contains value <message1>

    Examples: 
      | descricao      | body | message                        | code      | language | responseCode | message1                                            | code1     |
      | com body vazio | "{}" | "Field contactId is required." | "400.001" | "en-US"  |          400 | "Field tokenType has an invalid value or is empty." | "400.007" |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpointUsers}/${userIdEMPEmail}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be <responseCode>

    Examples: 
      | descricao                                         | body                                                                                                                                                           | message                                                   | code      | language | responseCode |
      | com contactId inválido                            | "{"tokenType":"${tokenType}","url":"${urlFixo}","contactId":"1","siteId": ${siteId}}"                                                                          | "Invalid contact to user."                                | "422.008" | "en-US"  |          422 |
      | cliente EMP token type Register sem siteId        | "{"tokenType":"REGISTER","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}"}" | "Field siteId is required to EMP customer type."          | "422.052" | "en-US"  |          422 |
      | com contactId inválido em pt                      | "{"tokenType":"${tokenType}","url":"${urlFixo}","contactId":"1","siteId": ${siteId}}"                                                                          | "Contato inválido para o usuário."                        | "422.008" | "pt-BR"  |          422 |
      | cliente EMP token type Register sem siteId  em pt | "{"tokenType":"REGISTER","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}"}" | "O campo siteId é obrigatório para clientes do tipo EMP." | "422.052" | "pt-BR"  |          422 |

  @Negativo
  Scenario Outline: Enviar token <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"REGISTER","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"
    When I set POST api endpoint as "${endpointUsers}/<userId>/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario: Com userId inválido
    Given I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"REGISTER","contactId":"${contactIdEMPEmail}","url":"${urlFixo}","customerCode":"${customerCodeEMPEmail}","document":"${documentCodeEMPEmail}","siteId":${siteId}}"
    When I set POST api endpoint as "${endpointUsers}/1/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

  @Positivo
  Scenario Outline: Enviar token <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":<tokenType>,"contactId":<contactId>,"url":"${urlFixo}","customerCode":<customerCode>,"document":<document>, "siteId":<siteId> }"
    When I set POST api endpoint as "${endpointUsers}/<userId>/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | descricao                                                        | userId             | tokenType         | contactId              | customerCode              | document                  | siteId    |
      | empresarial com sucesso com tokenType REGISTER para EMAIL        | ${userIdEMPEmail}  | "REGISTER"        | "${contactIdEMPEmail}" | "${customerCodeEMPEmail}" | "${documentCodeEMPEmail}" | ${siteId} |
      | empresarial com sucesso com tokenType ADD_CONTACT para EMAIL     | ${userIdEMPEmail}  | "ADD_CONTACT"     | "${contactIdEMPEmail}" | "${customerCodeEMPEmail}" | "${documentCodeEMPEmail}" | ""        |
      | empresarial com sucesso com tokenType CHANGE_PASSWORD para EMAIL | ${userIdEMPEmail}  | "CHANGE_PASSWORD" | "${contactIdEMPEmail}" | "${customerCodeEMPEmail}" | "${documentCodeEMPEmail}" | ""        |
      | empresarial com sucesso com tokenType REGISTER para PHONE        | ${userIdEMPPhone}  | "REGISTER"        | "${contactIdEMPPhone}" | "${customerCodeEMPPhone}" | "${documentCodeEMPPhone}" |    331706 |
      | empresarial com sucesso com tokenType ADD_CONTACT para PHONE     | ${userIdEMPPhone}  | "ADD_CONTACT"     | "${contactIdEMPPhone}" | "${customerCodeEMPPhone}" | "${documentCodeEMPPhone}" | ""        |
      | empresarial com sucesso com tokenType CHANGE_PASSWORD para PHONE | ${userIdEMPPhone}  | "CHANGE_PASSWORD" | "${contactIdEMPPhone}" | "${customerCodeEMPPhone}" | "${documentCodeEMPPhone}" | ""        |
      | MI com sucesso com tokenType REGISTER para PHONE                 | ${userIdMIPhone} | "REGISTER"        | "${contactIdMIPhone}"  | "${customerCodeMIPhone}"  | "${documentCodeMIPhone}"  | ""        |
      | MI com sucesso com tokenType ADD_CONTACT para PHONE              | ${userIdMIPhone} | "ADD_CONTACT"     | "${contactIdMIPhone}"  | "${customerCodeMIPhone}"  | "${documentCodeMIPhone}"  | ""        |
      | MI com sucesso com tokenType CHANGE_PASSWORD para PHONE          | ${userIdMIPhone} | "CHANGE_PASSWORD" | "${contactIdMIPhone}"  | "${customerCodeMIPhone}"  | "${documentCodeMIPhone}"  | ""        |
      | MI com sucesso com tokenType REGISTER para EMAIL                 | ${userIdMIEmail} | "REGISTER"        | "${contactIdMIEmail}"  | "${customerCodeMIEmail}"  | "${documentCodeMIEmail}"  | ""        |
      | MI com sucesso com tokenType ADD_CONTACT para EMAIL              | ${userIdMIEmail} | "ADD_CONTACT"     | "${contactIdMIEmail}"  | "${customerCodeMIEmail}"  | "${documentCodeMIEmail}"  | ""        |
      | MI com sucesso com tokenType CHANGE_PASSWORD para EMAIL          | ${userIdMIEmail} | "CHANGE_PASSWORD" | "${contactIdMIEmail}"  | "${customerCodeMIEmail}"  | "${documentCodeMIEmail}"  | ""        |
  
  
  #@Positivo
  #Scenario Outline: Enviar token <descricao>
  #Given I set request header "Content-Type" as "application/json"
  #And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId<contactId>}"}"
  #When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/tokens"
  #Then I get response body
  #And I get response status code
  #And I verify if response body is empty
  #And Response code must be 202
  #
  #Examples:
  #| contactId | descricao                                                             | tokenType       | userId |
  #|         1 | empresarial com sucesso com tokenType REGISTER para PHONE             | REGISTER        |      1 |
  #|         1 | empresarial com sucesso com tokenType ADD_CONTACT para PHONE          | ADD_CONTACT     |      1 |
  #|         1 | empresarial com sucesso com tokenType CHANGE_PASSWORD para PHONE      | CHANGE_PASSWORD |      1 |
  #| 2-1       | empresarial com sucesso com tokenType REGISTER para EMAIL             | REGISTER        |      1 |
  #| 2-1       | empresarial com sucesso com tokenType ADD_CONTACT para EMAIL          | ADD_CONTACT     |      1 |
  #| 2-1       | empresarial com sucesso com tokenType CHANGE_PASSWORD para EMAIL      | CHANGE_PASSWORD |      1 |
  #|         2 | MI sem customerCode com tokenType REGISTER para PHONE                 | REGISTER        |      2 |
  #|         2 | MI sem customerCode com tokenType ADD_CONTACT para PHONE              | ADD_CONTACT     |      2 |
  #|         2 | MI sem customerCode com tokenType CHANGE_PASSWORD para PHONE          | CHANGE_PASSWORD |      2 |
  #| 2-2       | MI sem customerCode com tokenType REGISTER para EMAIL                 | REGISTER        |      2 |
  #| 2-2       | MI sem customerCode com tokenType ADD_CONTACT para EMAIL              | ADD_CONTACT     |      2 |
  #| 2-2       | MI sem customerCode com tokenType CHANGE_PASSWORD para EMAIL          | CHANGE_PASSWORD |      2 |
  #|         3 | MI com customerCode vazio com tokenType REGISTER para PHONE           | REGISTER        |      3 |
  #|         3 | MI com customerCode vazio com tokenType ADD_CONTACT para PHONE        | ADD_CONTACT     |      3 |
  #|         3 | MI com customerCode vazio com tokenType CHANGE_PASSWORD para PHONE    | CHANGE_PASSWORD |      3 |
  #| 2-3       | MI com customerCode vazio com tokenType REGISTER para EMAIL           | REGISTER        |      3 |
  #| 2-3       | MI com customerCode vazio com tokenType ADD_CONTACT para EMAIL        | ADD_CONTACT     |      3 |
  #| 2-3       | MI com customerCode vazio com tokenType CHANGE_PASSWORD para EMAIL    | CHANGE_PASSWORD |      3 |
  #|         4 | MI com customerCode nulo com tokenType REGISTER para PHONE            | REGISTER        |      4 |
  #|         4 | MI com customerCode nulo com tokenType ADD_CONTACT para PHONE         | ADD_CONTACT     |      4 |
  #|         4 | MI com customerCode nulo com tokenType CHANGE_PASSWORD para PHONE     | CHANGE_PASSWORD |      4 |
  #| 2-4       | MI com customerCode nulo com tokenType REGISTER para EMAIL            | REGISTER        |      4 |
  #| 2-4       | MI com customerCode nulo com tokenType ADD_CONTACT para EMAIL         | ADD_CONTACT     |      4 |
  #| 2-4       | MI com customerCode nulo com tokenType CHANGE_PASSWORD para EMAIL     | CHANGE_PASSWORD |      4 |
  #|         5 | MI sem customerCode com CNPJ com tokenType REGISTER para PHONE        | REGISTER        |      5 |
  #|         5 | MI sem customerCode com CNPJ com tokenType ADD_CONTACT para PHONE     | ADD_CONTACT     |      5 |
  #|         5 | MI sem customerCode com CNPJ com tokenType CHANGE_PASSWORD para PHONE | CHANGE_PASSWORD |      5 |
  #| 2-5       | MI sem customerCode com CNPJ com tokenType REGISTER para EMAIL        | REGISTER        |      5 |
  #| 2-5       | MI sem customerCode com CNPJ com tokenType ADD_CONTACT para EMAIL     | ADD_CONTACT     |      5 |
  #| 2-5       | MI sem customerCode com CNPJ com tokenType CHANGE_PASSWORD para EMAIL | CHANGE_PASSWORD |      5 |
  #|         6 | MI com customerCode com tokenType REGISTER para PHONE                 | REGISTER        |      6 |
  #|         6 | MI com customerCode com tokenType ADD_CONTACT para PHONE              | ADD_CONTACT     |      6 |
  #|         6 | MI com customerCode com tokenType CHANGE_PASSWORD para PHONE          | CHANGE_PASSWORD |      6 |
  #| 2-6       | MI com customerCode com tokenType REGISTER para EMAIL                 | REGISTER        |      6 |
  #| 2-6       | MI com customerCode com tokenType ADD_CONTACT para EMAIL              | ADD_CONTACT     |      6 |
  #| 2-6       | MI com customerCode com tokenType CHANGE_PASSWORD para EMAIL          | CHANGE_PASSWORD |      6 |
  #|         7 | MI com CNPJ repetido com tokenType REGISTER para PHONE                | REGISTER        |      7 |
  #|         7 | MI com CNPJ repetido com tokenType ADD_CONTACT para PHONE             | ADD_CONTACT     |      7 |
  #|         7 | MI com CNPJ repetido com tokenType CHANGE_PASSWORD para PHONE         | CHANGE_PASSWORD |      7 |
  #| 2-7       | MI com CNPJ repetido com tokenType REGISTER para EMAIL                | REGISTER        |      7 |
  #| 2-7       | MI com CNPJ repetido com tokenType ADD_CONTACT para EMAIL             | ADD_CONTACT     |      7 |
  #| 2-7       | MI com CNPJ repetido com tokenType CHANGE_PASSWORD para EMAIL         | CHANGE_PASSWORD |      7 |
