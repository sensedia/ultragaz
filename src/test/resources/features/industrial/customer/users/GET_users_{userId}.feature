@GET_users_{userId}
Feature: GET_users_{userId}.feature
  Operação responsável por consultar um usuário do portal.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpoint"
    And I save "4a539fc6-9af8-4282-983b-eb728569d1bf" as "userId"

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set GET api endpoint as "${endpoint}/<userId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario: Consultar user <descricao>
    Given I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I verify if response value "/userId" is not empty
    And I verify if response value "/customerId" is not empty
    And I verify if response value "/login" is not empty
    And I verify if response value "/document" is not empty
    And I verify if response value "/active" is not empty
    And I verify if response value "/guideNavigationTour" is not empty
    And Response code must be 200

