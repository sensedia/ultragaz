@POST_users_reset-password
Feature: POST_users_reset-password.feature
  Operação responsável por enviar link para reset da senha

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpointUsers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "${CODESegundo}" as "customerCode"
    And I save "${CNPJSegundo}" as "document"
    And I save "Q4tu&pqk" as "password"
    And I save "testresetpassword@sensedia.com" as "login"
    And I save "http://www.google.com/" as "url"

  @PreRequest
  Scenario: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"customerCode":"${customerCode}","document":"${document}"}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

  @PreRequest
  Scenario: Cadastrar contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contact":"${login}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumberSegundo}","penultimateInvoiceNumber":"${penultimateInvoiceNumberSegundo}"}}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId"
    And Response code must be 201
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactId":"${contactId}","password":"${password}","passwordConfirmation":"${password}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

  @PreRequest
  Scenario: Enviar token para telefone
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"REGISTER","contactId":"${contactId}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    Given I get token on database by contact_id "${contactId}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                | language |
      | sem body       | Invalid request body.  | en-US    |
      | sem body em pt | Request body inválido. | pt-BR    |

  @Negativo
  Scenario Outline: Enviar reset de senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be <responseCode>

    Examples: 
      | descricao             | body                            | message                                                | code    | language | responseCode |
      | sem login             | {"url":"${url}"}                | Field login is required.                               | 400.001 | en-US    |          400 |
      | com login vazio       | {"login":"","url":"${url}"}     | Field login is required.                               | 400.001 | en-US    |          400 |
      | com login nulo        | {"login": null,"url":"${url}"}  | Field login is required.                               | 400.001 | en-US    |          400 |
      | sem url               | {"login":"${login}"}            | Field url is required when login is an email.          | 400.017 | en-US    |          400 |
      | com url vazio         | {"login":"${login}","url":""}   | Field url is required when login is an email.          | 400.017 | en-US    |          400 |
      | com url nulo          | {"login":"${login}","url":null} | Field url is required when login is an email.          | 400.017 | en-US    |          400 |
      | sem login em pt       | {"url":"${url}"}                | O campo login é obrigatório.                           | 400.001 | pt-BR    |          400 |
      | com login vazio em pt | {"login":"","url":"${url}"}     | O campo login é obrigatório.                           | 400.001 | pt-BR    |          400 |
      | com login nulo em pt  | {"login": null,"url":"${url}"}  | O campo login é obrigatório.                           | 400.001 | pt-BR    |          400 |
      | sem url em pt         | {"login":"${login}"}            | O campo url é obrigatório quando o login for um email. | 400.017 | pt-BR    |          400 |
      | com url vazio em pt   | {"login":"${login}","url":""}   | O campo url é obrigatório quando o login for um email. | 400.017 | pt-BR    |          400 |
      | com url nulo em pt    | {"login":"${login}","url":null} | O campo url é obrigatório quando o login for um email. | 400.017 | pt-BR    |          400 |

  @Negativo
  Scenario Outline: Enviar reset de senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be <responseCode>

    Examples: 
      | descricao                                       | body                                        | message                             | code    | language | responseCode |
      | com url com formato inválido sem http://        | {"login":"${login}","url":"teste.com"}      | Field url has an invalid format.    | 400.018 | en-US    |          400 |
      | com url com formato inválido sem .com           | {"login":"${login}","url":"htpps:teste.br"} | Field url has an invalid format.    | 400.018 | en-US    |          400 |
      | com url com formato inválido sem http://  em pt | {"login":"${login}","url":"teste.com"}      | O campo url tem o formato inválido. | 400.018 | pt-BR    |          400 |
      | com url com formato inválido sem .com em pt     | {"login":"${login}","url":"htpps:teste.br"} | O campo url tem o formato inválido. | 400.018 | pt-BR    |          400 |

  @Negativo
  Scenario Outline: Enviar reset de senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                 | body                         | message                               | code    | language |
      | com login incorreto       | {"login":"1","url":"${url}"} | User not found for the given e-mail.  | 422.013 | en-US    |
      | com login incorreto em pt | {"login":"1","url":"${url}"} | Usuário não encontrado para o e-mail informado. | 422.013 | pt-BR    |

  @Negativo
  Scenario Outline: Enviar reset de senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao            | body | language | message                      | code    | 
      | com body vazio       | {}   | en-US    | Field login is required.     | 400.001 | 
      | com body vazio em pt | {}   | pt-BR    | O campo login é obrigatório. | 400.001 | 

  @Positivo
  Scenario: Resetar senha
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"login":"${login}","url":"${url}"}"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId}" and login "${login}"
