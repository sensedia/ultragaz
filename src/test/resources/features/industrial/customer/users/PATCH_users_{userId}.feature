@PATCH_users_{userId}
Feature: PATCH_users_{userId}.feature
  Operação responsável por atualizar um usuário.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpoint"
    And I save "4a539fc6-9af8-4282-983b-eb728569d1bf" as "userId"
    And I save "Aab45@owqefoqheohi23hohfowhto2h_vroihgoirhwoihrvqrbrqe&@rjeqprbjqojfpoqjrgoqjgpojqojpoqgerjgnpvqnrjopqe" as "passOver100"

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request body as "{"token":"081170","password":"Q4tu$pqk","passwordConfirmation":"Q4tu$pqk"}"
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                  | language |
      | sem body       | "Invalid request body."  | "en-US"  |
      | sem body em pt | "Request body inválido." | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                                                  | body                                                                                        | message                                                        | code      | language |
      | com password com menos de 8 caracteres                     | "{"token":"string","password":"Aab45@","passwordConfirmation":"Aab45@123"}"                 | "Field password size must be between 8 and 100."               | "400.009" | "en-US"  |
      | com password com mais de 100 caracteres                    | "{"token":"string","password":"${passOver100}","passwordConfirmation":"Aab45@owqefoqhe"}"   | "Field password size must be between 8 and 100."               | "400.009" | "en-US"  |
      | com passwordConfirmation com menos de 8 caracteres         | "{"token":"string","password":"Aab45@123","passwordConfirmation":"Aab45@"}"                 | "Field passwordConfirmation size must be between 8 and 100."   | "400.009" | "en-US"  |
      | com passwordConfirmation com mais de 100 caracteres        | "{"token":"string","password":"Aab45@owqpqe","passwordConfirmation":"${passOver100}"}"      | "Field passwordConfirmation size must be between 8 and 100."   | "400.009" | "en-US"  |
      | com guideNavigationTour inválido                           | "{"guideNavigationTour":"banana"}"                                                          | "Field guideNavigationTour has an invalid format."             | "400.003" | "en-US"  |
      | com password com menos de 8 caracteres em pt               | "{"token":"string","password":"Aab45@","passwordConfirmation":"Aab45@123"}"                 | "O campo password deve ter tamanho entre 8 e 100."             | "400.009" | "pt-BR"  |
      | com password com mais de 100 caracteres  em pt             | "{"token":"string","password":"${passOver100}","passwordConfirmation":"Aab45@owqefoqheoh"}" | "O campo password deve ter tamanho entre 8 e 100."             | "400.009" | "pt-BR"  |
      | com passwordConfirmation com menos de 8 caracteres em pt   | "{"token":"string","password":"Aab45@123","passwordConfirmation":"Aab45@"}"                 | "O campo passwordConfirmation deve ter tamanho entre 8 e 100." | "400.009" | "pt-BR"  |
      | com passwordConfirmation com mais de 100 caracteres  em pt | "{"token":"string","password":"Aab45@owqe","passwordConfirmation":"${passOver100}"}"        | "O campo passwordConfirmation deve ter tamanho entre 8 e 100." | "400.009" | "pt-BR"  |
      | com guideNavigationTour inválido pt                        | "{"guideNavigationTour":"banana"}"                                                          | "O campo guideNavigationTour tem o formato inválido."          | "400.003" | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with "422.012"
    And Response code must be 422

    Examples: 
      | descricao                       | body                                                                               | message                                                                                                | language |
      | com password sem numeros        | {"token":"string","password":"Aiopjpj@joh","passwordConfirmation":"Aiopjpj@joh"}   | "Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character."   | "en-US"  |
      | com password sem letras         | {"token":"string","password":"123456&7","passwordConfirmation":"123456&7"}         | "Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character."   | "en-US"  |
      | com password sem símbolos       | {"token":"string","password":"pqojr242borj","passwordConfirmation":"pqojr242borj"} | "Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character."   | "en-US"  |
      | com password com espaço         | {"token":"string","password":"qp345 rtgp","passwordConfirmation":"qp345 rtgp"}     | "Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character."   | "en-US"  |
      | com password sem numeros em pt  | {"token":"string","password":"Aiopjpj@joh","passwordConfirmation":"Aiopjpj@joh"}   | "Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial." | "pt-BR"  |
      | com password sem letras em pt   | {"token":"string","password":"123456&7","passwordConfirmation":"123456&7"}         | "Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial." | "pt-BR"  |
      | com password sem símbolos em pt | {"token":"string","password":"pqojr242borj","passwordConfirmation":"pqojr242borj"} | "Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial." | "pt-BR"  |
      | com password com espaço em pt   | {"token":"string","password":"qp345 rtgp","passwordConfirmation":"qp345 rtgp"}     | "Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial." | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                                            | body                                                                                  | message                                                                                         | code      | language |
      | com token incorreto/expirado                         | "{"token":"string","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"  | "Invalid token."                                                                                | "422.002" | "en-US"  |
      | com password diferente de passwordConfirmation       | "{"token":"string","password":"Qr5@frgr2376a","passwordConfirmation":"Qr5@frgr2376"}" | "Password and PasswordConfirmation cannot be different."                                        | "422.005" | "en-US"  |
      | com token incorreto/expirado em pt                   | "{"token":"string","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"  | "Token inválido."                                                                               | "422.002" | "pt-BR"  |
      | com password diferente de passwordConfirmation em pt | "{"token":"string","password":"Qr5@frgr2376a","passwordConfirmation":"Qr5@frgr2376"}" | "Senha e confirmação de senha não podem ser diferentes."                                        | "422.005" | "pt-BR"  |
      | sem password                                         | "{"token":"string","passwordConfirmation":"Qr5@frgr2376"}"                            | "When token is informed, password and passwordConfirmation are required."                       | "422.048" | "en-US"  |
      | com password nulo                                    | "{"token":"string","password":null,"passwordConfirmation":"Qr5@frgr2376"}"            | "When token is informed, password and passwordConfirmation are required."                       | "422.048" | "en-US"  |
      | sem passwordConfirmation                             | "{"token":"string","password":"Qr5@frgr2376"}"                                        | "When token is informed, password and passwordConfirmation are required."                       | "422.048" | "en-US"  |
      | com passwordConfirmation nulo                        | "{"token":"string","password":"Qr5@frgr2376","passwordConfirmation":null}"            | "When token is informed, password and passwordConfirmation are required."                       | "422.048" | "en-US"  |
      | sem password em pt                                   | "{"token":"string","passwordConfirmation":"Qr5@frgr2376"}"                            | "Quando o campo token é informado, os campos password e passwordConfirmation são obrigatórios." | "422.048" | "pt-BR"  |
      | com password nulo em pt                              | "{"token":"string","password":null,"passwordConfirmation":"Qr5@frgr2376"}"            | "Quando o campo token é informado, os campos password e passwordConfirmation são obrigatórios." | "422.048" | "pt-BR"  |
      | sem passwordConfirmation em pt                       | "{"token":"string","password":"Qr5@frgr2376"}"                                        | "Quando o campo token é informado, os campos password e passwordConfirmation são obrigatórios." | "422.048" | "pt-BR"  |
      | com passwordConfirmation nulo em pt                  | "{"token":"string","password":"Qr5@frgr2376","passwordConfirmation":null}"            | "Quando o campo token é informado, os campos password e passwordConfirmation são obrigatórios." | "422.048" | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"string","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"
    When I set PATCH api endpoint as "${endpoint}/<userId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404
    And I wait 2 seconds

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario: Cadastrar senha com userId inválido
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"string","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"
    When I set PATCH api endpoint as "${endpoint}/1"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

  @Positivo
  Scenario Outline: Alterar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao               | body                                                                                 |
      | sem token               | "{"password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"                  |
      | com token vazio         | "{"token":"","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"       |
      | com token nulo          | "{"token":null,"password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"     |
      | com token               | "{"token":"string","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}" |
      | com guideNavigationTour | "{"guideNavigationTour":true}"                                                       |
