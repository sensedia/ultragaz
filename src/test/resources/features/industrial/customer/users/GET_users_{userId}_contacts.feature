@GET_users_{userId}_contacts
Feature: GET_users_{userId}_contacts.feature
  Operação responsável por consultar contatos de um usuário do portal.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/customer/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "/users" as "endpointUsers"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "${CPFMI}" as "documentPf"
    And I save "${CNPJMICNPJ}" as "documentPj"
    And I save "${CODEMI}" as "customerCodeMi"
    And I save "${CODEMICNPJ}" as "customerCodeMiCnpj"

  @PreRequest
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | descricao                    | body                                                            |
      |  1 | empresarial com sucesso      | {"customerCode":"${customerCode}","document":"${document}"}     |
      |  2 | MI sem customerCode          | {"document":"${documentPf}"}                                    |
      |  3 | MI com customerCode vazio    | {"customerCode":"","document":"${documentPf}"}                  |
      |  4 | MI com customerCode nulo     | {"customerCode":null,"document":"${documentPf}"}                |
      |  5 | MI sem customerCode com CNPJ | {"document":"${documentPj}"}                                    |
      |  6 | MI com customerCode          | {"customerCode":"${customerCodeMi}","document":"${documentPf}"} |
      |  7 | MI com CNPJ repetido         | {"document":"${documentPj}"}                                    |

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/<userId>/contacts?_limit=10&_offset=0"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario Outline: Consultar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/contacts<queries>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                      | queries                                 | message                                                      | code    | language |
      | com _limit inválido            | ?_offset=0&_limit=a                     | Field _limit has an invalid format.                          | 400.003 | en-US    |
      | com _offset inválido           | ?_limit=10&_offset=a                    | Field _offset has an invalid format.                         | 400.003 | en-US    |
      | com contactType inválido       | ?_limit=10&_offset=0&contactType=bla    | Field contactType has an invalid format.                     | 400.003 | en-US    |
      | com customerCode vazio         | ?document=${document}&customerCode=     | Fields document and customerCode required.                   | 400.012 | en-US    |
      | sem customerCode               | ?document=${document}                   | Fields document and customerCode required.                   | 400.012 | en-US    |
      | com document vazio             | ?customerCode=${customerCode}&document= | Fields document and customerCode required.                   | 400.012 | en-US    |
      | sem document                   | ?customerCode=${customerCode}           | Fields document and customerCode required.                   | 400.012 | en-US    |
      | com _limit inválido em pt      | ?_offset=0&_limit=a                     | O campo _limit tem o formato inválido.                       | 400.003 | pt-BR    |
      | com _offset inválido em pt     | ?_limit=10&_offset=a                    | O campo _offset tem o formato inválido.                      | 400.003 | pt-BR    |
      | com contactType inválido em pt | ?contactType=bla                        | O campo contactType tem o formato inválido.                  | 400.003 | pt-BR    |
      | com customerCode vazio em pt   | ?document=${document}&customerCode=     | Os campos document e customerCode são obrigatórios entre si. | 400.012 | pt-BR    |
      | sem customerCode em pt         | ?document=${document}                   | Os campos document e customerCode são obrigatórios entre si. | 400.012 | pt-BR    |
      | com document vazio em pt       | ?customerCode=${customerCode}&document= | Os campos document e customerCode são obrigatórios entre si. | 400.012 | pt-BR    |
      | sem document em pt             | ?customerCode=${customerCode}           | Os campos document e customerCode são obrigatórios entre si. | 400.012 | pt-BR    |

  @Negativo
  Scenario Outline: Consultar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/contacts<queries>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                       | queries                                    | message                                                | code    | language |
      | com customerCode inválido       | ?customerCode=bla&document=${document}     | customerCode and document does not exist.              | 422.004 | en-US    |
      | com document inválido           | ?document=bla&customerCode=${customerCode} | customerCode and document does not exist.              | 422.004 | en-US    |
      | com customerCode inválido em pt | ?customerCode=bla&document=${document}     | Código do cliente e documento não são correspondentes. | 422.004 | pt-BR    |
      | com document inválido em pt     | ?document=bla&customerCode=${customerCode} | Código do cliente e documento não são correspondentes. | 422.004 | pt-BR    |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/contacts?_limit=10&_offset=0"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And Response code must be 200

    Examples: 
      | descricao        |
      | com document CPF |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId1}/contacts<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I compare response value "/0/contactType" with "<contactType>"
    And Response code must be 200

    Examples: 
      | descricao                   | query                                  | contactType |
      | com document CPF para EMAIL | ?contactType=EMAIL&_limit=10&_offset=0 | EMAIL       |
      | com document CPF para PHONE | ?contactType=PHONE&_limit=10&_offset=0 | PHONE       |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId<id>}/contacts<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And Response code must be 200

    Examples: 
      | id | descricao                    | query                              |
      |  1 | sem _limit                   | ?_offset=0                         |
      |  1 | com _limit vazio             | ?_offset=0&_limit=                 |
      |  1 | sem _offset                  | ?_limit=1                          |
      |  1 | com _offset vazio            | ?_limit=10&_offset=                |
      |  1 | sem contactType              | ?_limit=10&_offset=0               |
      |  1 | com contactType vazio        | ?_limit=10&_offset=0&contactType=  |
      |  1 | sem customerCode             | ?_limit=10&_offset=0               |
      |  1 | com customerCode vazio       | ?_limit=10&_offset=0&customerCode= |
      |  2 | MI sem customerCode          |                                    |
      |  3 | MI com customerCode vazio    |                                    |
      |  4 | MI com customerCode nulo     |                                    |
      |  5 | MI sem customerCode com CNPJ |                                    |
      |  6 | MI com customerCode          |                                    |
      |  7 | MI com CNPJ repetido         |                                    |
