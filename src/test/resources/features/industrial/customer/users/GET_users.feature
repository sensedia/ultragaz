@GET_users
Feature: GET_users.feature
  Operação responsável por consultar usuário do portal.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpoint"
    And I save "carlaaevaristo@gmail.com" as "login"
    And I save "18561503564" as "document"

  @Negativo
  Scenario Outline: Consultar usuários <descricao>
    Given I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                 | query                            | message                                     | code      | language |
      | sem query                 |                                  | "Field login is required."                  | "400.001" | "en-US"  |
      | com login vazio           | ?login=                          | "Field login is required."                  | "400.001" | "en-US"  |
      | com is-active inválido    | ?login=${login}&is-active=falsea | "Field is-active has an invalid format."    | "400.003" | "en-US"  |
      | com _limit inválido       | ?login=${login}&_limit=1a        | "Field _limit has an invalid format."       | "400.003" | "en-US"  |
      | com _offset inválido      | ?login=${login}&_offset=1a       | "Field _offset has an invalid format."      | "400.003" | "en-US"  |
      | sem query em pt           |                                  | "O campo login é obrigatório."              | "400.001" | "pt-BR"  |
      | com login vazio em pt     | ?login=                          | "O campo login é obrigatório."              | "400.001" | "pt-BR"  |
      | com is-active inválido pt | ?login=${login}&is-active=falsea | "O campo is-active tem o formato inválido." | "400.003" | "pt-BR"  |
      | com _limit inválido pt    | ?login=${login}&_limit=1a        | "O campo _limit tem o formato inválido."    | "400.003" | "pt-BR"  |
      | com _offset inválido pt   | ?login=${login}&_offset=1a       | "O campo _offset tem o formato inválido."   | "400.003" | "pt-BR"  |

  @Positivo
  Scenario: Consultar usuários com login inexistente
    Given I set GET api endpoint as "${endpoint}?login=batatinhax"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

  @Positivo
  Scenario Outline: Consultar usuário <descricao>
    Given I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/userId" is not empty
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/login" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/active" is not empty
    And I verify if response value "/0/guideNavigationTour" is not empty
    And Response code must be 200

    Examples: 
      | descricao           | query                              |
      | com document CPF    | ?login=${document}                 |
      | com email           | ?login=${login}                    |
      | com is-active true  | ?login=${login}&is-active=true     |
      | com is-active false | ?login=${document}&is-active=false |
      | com _limit 1        | ?login=${login}&_limit=1           |
      | com _offset 0       | ?login=${document}&_offset=0       |
