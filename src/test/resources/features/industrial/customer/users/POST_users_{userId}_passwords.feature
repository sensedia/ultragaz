@POST_users_{userId}_passwords
Feature: POST_users_{userId}_passwords.feature
  Operação responsável por criar nova senha para usuário no portal.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/customer/v1"
    And I save "/users" as "endpointUsers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "Q4tu&pqk" as "password"
    And I save "testaddbyinvoice@sensedia.com" as "login"
    And I save "${CODE}" as "customerCode"
    And I save "${CNPJ}" as "document"
    And I save "${CPFMI}" as "documentPf"
    And I save "${CNPJMICNPJ}" as "documentPj"
    And I save "${CODEMI}" as "customerCodeMi"
    And I save "${CODEMICNPJ}" as "customerCodeMiCnpj"

  @PreRequest
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | descricao                    | body                                                            |
      |  1 | empresarial com sucesso      | {"customerCode":"${customerCode}","document":"${document}"}     |
      |  2 | MI com sucesso               | {"document":"${documentPf}"}                                    |
      |  3 | MI com customerCode vazio    | {"customerCode":"","document":"${documentPf}"}                  |
      |  4 | MI com customerCode nulo     | {"customerCode":null,"document":"${documentPf}"}                |
      |  5 | MI sem customerCode com CNPJ | {"document":"${documentPj}"}                                    |
      |  6 | MI com customerCode          | {"customerCode":"${customerCodeMi}","document":"${documentPf}"} |
      |  7 | MI com CNPJ repetido         | {"document":"${documentPj}"}                                    |

  @PreRequest
  Scenario Outline: Cadastrar contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/<userId>/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId0"
    And Response code must be 201

    Examples: 
      | descricao    | userId     | body                                                                                                                                                                   |
      | por invoices | ${userId1} | {"contact":"${login}","contactType":"EMAIL","contactValidation":{"lastInvoiceNumber":"${lastInvoiceNumber}","penultimateInvoiceNumber":"${penultimateInvoiceNumber}"}} |

  @PreRequest
  Scenario Outline: Consultar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId<id>}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And I save response value "/0/contactId" as "contactId<id>"
    And Response code must be 200

    Examples: 
      | id | descricao                    |
      |  1 | empresarial com sucesso      |
      |  2 | MI sem customerCode          |
      |  3 | MI com customerCode vazio    |
      |  4 | MI com customerCode nulo     |
      |  5 | MI sem customerCode com CNPJ |
      |  6 | MI com customerCode          |
      |  7 | MI com CNPJ repetido         |

  @PreRequest
  Scenario Outline: Consultar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId<id>}/contacts?contactType=EMAIL"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And I save response value "/0/contactId" as "contactId2-<id>"
    And Response code must be 200

    Examples: 
      | id | descricao                    |
      |  1 | empresarial com sucesso      |
      |  2 | MI sem customerCode          |
      |  3 | MI com customerCode vazio    |
      |  4 | MI com customerCode nulo     |
      |  5 | MI sem customerCode com CNPJ |
      |  6 | MI com customerCode          |
      |  7 | MI com CNPJ repetido         |

  @Positivo
  Scenario Outline: Enviar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId<contactId>}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | contactId | descricao                                                             | tokenType       | userId |
      |         1 | empresarial com sucesso com tokenType REGISTER para PHONE             | REGISTER        |      1 |
      |         1 | empresarial com sucesso com tokenType ADD_CONTACT para PHONE          | ADD_CONTACT     |      1 |
      |         1 | empresarial com sucesso com tokenType CHANGE_PASSWORD para PHONE      | CHANGE_PASSWORD |      1 |
      | 2-1       | empresarial com sucesso com tokenType REGISTER para EMAIL             | REGISTER        |      1 |
      | 2-1       | empresarial com sucesso com tokenType ADD_CONTACT para EMAIL          | ADD_CONTACT     |      1 |
      | 2-1       | empresarial com sucesso com tokenType CHANGE_PASSWORD para EMAIL      | CHANGE_PASSWORD |      1 |
      |         2 | MI sem customerCode com tokenType REGISTER para PHONE                 | REGISTER        |      2 |
      |         2 | MI sem customerCode com tokenType ADD_CONTACT para PHONE              | ADD_CONTACT     |      2 |
      |         2 | MI sem customerCode com tokenType CHANGE_PASSWORD para PHONE          | CHANGE_PASSWORD |      2 |
      | 2-2       | MI sem customerCode com tokenType REGISTER para EMAIL                 | REGISTER        |      2 |
      | 2-2       | MI sem customerCode com tokenType ADD_CONTACT para EMAIL              | ADD_CONTACT     |      2 |
      | 2-2       | MI sem customerCode com tokenType CHANGE_PASSWORD para EMAIL          | CHANGE_PASSWORD |      2 |
      |         3 | MI com customerCode vazio com tokenType REGISTER para PHONE           | REGISTER        |      3 |
      |         3 | MI com customerCode vazio com tokenType ADD_CONTACT para PHONE        | ADD_CONTACT     |      3 |
      |         3 | MI com customerCode vazio com tokenType CHANGE_PASSWORD para PHONE    | CHANGE_PASSWORD |      3 |
      | 2-3       | MI com customerCode vazio com tokenType REGISTER para EMAIL           | REGISTER        |      3 |
      | 2-3       | MI com customerCode vazio com tokenType ADD_CONTACT para EMAIL        | ADD_CONTACT     |      3 |
      | 2-3       | MI com customerCode vazio com tokenType CHANGE_PASSWORD para EMAIL    | CHANGE_PASSWORD |      3 |
      |         4 | MI com customerCode nulo com tokenType REGISTER para PHONE            | REGISTER        |      4 |
      |         4 | MI com customerCode nulo com tokenType ADD_CONTACT para PHONE         | ADD_CONTACT     |      4 |
      |         4 | MI com customerCode nulo com tokenType CHANGE_PASSWORD para PHONE     | CHANGE_PASSWORD |      4 |
      | 2-4       | MI com customerCode nulo com tokenType REGISTER para EMAIL            | REGISTER        |      4 |
      | 2-4       | MI com customerCode nulo com tokenType ADD_CONTACT para EMAIL         | ADD_CONTACT     |      4 |
      | 2-4       | MI com customerCode nulo com tokenType CHANGE_PASSWORD para EMAIL     | CHANGE_PASSWORD |      4 |
      |         5 | MI sem customerCode com CNPJ com tokenType REGISTER para PHONE        | REGISTER        |      5 |
      |         5 | MI sem customerCode com CNPJ com tokenType ADD_CONTACT para PHONE     | ADD_CONTACT     |      5 |
      |         5 | MI sem customerCode com CNPJ com tokenType CHANGE_PASSWORD para PHONE | CHANGE_PASSWORD |      5 |
      | 2-5       | MI sem customerCode com CNPJ com tokenType REGISTER para EMAIL        | REGISTER        |      5 |
      | 2-5       | MI sem customerCode com CNPJ com tokenType ADD_CONTACT para EMAIL     | ADD_CONTACT     |      5 |
      | 2-5       | MI sem customerCode com CNPJ com tokenType CHANGE_PASSWORD para EMAIL | CHANGE_PASSWORD |      5 |
      |         6 | MI com customerCode com tokenType REGISTER para PHONE                 | REGISTER        |      6 |
      |         6 | MI com customerCode com tokenType ADD_CONTACT para PHONE              | ADD_CONTACT     |      6 |
      |         6 | MI com customerCode com tokenType CHANGE_PASSWORD para PHONE          | CHANGE_PASSWORD |      6 |
      | 2-6       | MI com customerCode com tokenType REGISTER para EMAIL                 | REGISTER        |      6 |
      | 2-6       | MI com customerCode com tokenType ADD_CONTACT para EMAIL              | ADD_CONTACT     |      6 |
      | 2-6       | MI com customerCode com tokenType CHANGE_PASSWORD para EMAIL          | CHANGE_PASSWORD |      6 |
      |         7 | MI com CNPJ repetido com tokenType REGISTER para PHONE                | REGISTER        |      7 |
      |         7 | MI com CNPJ repetido com tokenType ADD_CONTACT para PHONE             | ADD_CONTACT     |      7 |
      |         7 | MI com CNPJ repetido com tokenType CHANGE_PASSWORD para PHONE         | CHANGE_PASSWORD |      7 |
      | 2-7       | MI com CNPJ repetido com tokenType REGISTER para EMAIL                | REGISTER        |      7 |
      | 2-7       | MI com CNPJ repetido com tokenType ADD_CONTACT para EMAIL             | ADD_CONTACT     |      7 |
      | 2-7       | MI com CNPJ repetido com tokenType CHANGE_PASSWORD para EMAIL         | CHANGE_PASSWORD |      7 |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageTwo>"
    And I compare response value "/1/code" with "<code>"
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400

    Examples: 
      | descricao            | body | language | message                         | code    | messageTwo                                  |
      | com body vazio       | {}   | en-US    | Field password is required.     | 400.001 | Field passwordConfirmation is required.     |
      | com body vazio em pt | {}   | pt-BR    | O campo password é obrigatório. | 400.001 | O campo passwordConfirmation é obrigatório. |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                                | body                                                                                  | language | message                                                      | code    |
      | com contactId vazio                      | {"contactId":"","password":"${password}","passwordConfirmation":"${password}"}        | en-US    | Field contactId is required.                                 | 400.001 |
      | com contactId nulo                       | {"contactId":null,"password":"${password}","passwordConfirmation":"${password}"}      | en-US    | Field contactId is required.                                 | 400.001 |
      | sem contactId                            | {"password":"${password}","passwordConfirmation":"${password}"}                       | en-US    | Field contactId is required.                                 | 400.001 |
      | com password nulo                        | {"contactId":"${contactId0}","password":null,"passwordConfirmation":"${password}"}    | en-US    | Field password is required.                                  | 400.001 |
      | com password inválido                    | {"contactId":"${contactId0}","password":"aline","passwordConfirmation":"${password}"} | en-US    | Field password size must be between 8 and 100.               | 400.009 |
      | sem password                             | {"contactId":"${contactId0}","passwordConfirmation":"${password}"}                    | en-US    | Field password is required.                                  | 400.001 |
      | com passwordConfirmation nulo            | {"contactId":"${contactId0}","password":"${password}","passwordConfirmation":null}    | en-US    | Field passwordConfirmation is required.                      | 400.001 |
      | com passwordConfirmation incorreto       | {"contactId":"${contactId0}","password":"${password}","passwordConfirmation":"aline"} | en-US    | Field passwordConfirmation size must be between 8 and 100.   | 400.009 |
      | sem passwordConfirmation                 | {"contactId":"${contactId0}","password":"${password}"}                                | en-US    | Field passwordConfirmation is required.                      | 400.001 |
      | com contactId vazio em pt                | {"contactId":"","password":"${password}","passwordConfirmation":"${password}"}        | pt-BR    | O campo contactId é obrigatório.                             | 400.001 |
      | com contactId nulo em pt                 | {"contactId":null,"password":"${password}","passwordConfirmation":"${password}"}      | pt-BR    | O campo contactId é obrigatório.                             | 400.001 |
      | sem contactId em pt                      | {"password":"${password}","passwordConfirmation":"${password}"}                       | pt-BR    | O campo contactId é obrigatório.                             | 400.001 |
      | com password nulo em pt                  | {"contactId":"${contactId0}","password":null,"passwordConfirmation":"${password}"}    | pt-BR    | O campo password é obrigatório.                              | 400.001 |
      | com password inválido em pt              | {"contactId":"${contactId0}","password":"aline","passwordConfirmation":"${password}"} | pt-BR    | O campo password deve ter tamanho entre 8 e 100.             | 400.009 |
      | sem password em pt                       | {"contactId":"${contactId0}","passwordConfirmation":"${password}"}                    | pt-BR    | O campo password é obrigatório.                              | 400.001 |
      | com passwordConfirmation nulo em pt      | {"contactId":"${contactId0}","password":"${password}","passwordConfirmation":null}    | pt-BR    | O campo passwordConfirmation é obrigatório.                  | 400.001 |
      | com passwordConfirmation incorreto em pt | {"contactId":"${contactId0}","password":"${password}","passwordConfirmation":"aline"} | pt-BR    | O campo passwordConfirmation deve ter tamanho entre 8 e 100. | 400.009 |
      | sem passwordConfirmation em pt           | {"contactId":"${contactId0}","password":"${password}"}                                | pt-BR    | O campo passwordConfirmation é obrigatório.                  | 400.001 |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageTwo>"
    And I compare response value "/1/code" with "<codeTwo>"
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                            | body                                                                             | language | message                                     | code    | messageTwo                                                   | codeTwo |
      | com passwordConfirmation vazio em pt | {"contactId":"${contactId0}","password":"${password}","passwordConfirmation":""} | pt-BR    | O campo passwordConfirmation é obrigatório. | 400.001 | O campo passwordConfirmation deve ter tamanho entre 8 e 100. | 400.009 |
      | com password vazio                   | {"contactId":"${contactId0}","password":"","passwordConfirmation":"${password}"} | en-US    | Field password is required.                 | 400.001 | Field password size must be between 8 and 100.               | 400.009 |
      | com passwordConfirmation vazio       | {"contactId":"${contactId0}","password":"${password}","passwordConfirmation":""} | en-US    | Field passwordConfirmation is required.     | 400.001 | Field passwordConfirmation size must be between 8 and 100.   | 400.009 |
      | com password vazio em pt             | {"contactId":"${contactId0}","password":"","passwordConfirmation":"${password}"} | pt-BR    | O campo password é obrigatório.             | 400.001 | O campo password deve ter tamanho entre 8 e 100.             | 400.009 |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId1}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 422

    Examples: 
      | descricao                               | body                                                                                                               | language | message                                                                                 | code    |
      | com passwordConfirmation inválido em pt | {"contactId":"${contactId0}","password":"${password}","passwordConfirmation":"aline&A12"}                          | pt-BR    | Senha e confirmação de senha não podem ser diferentes.                                  | 422.005 |
      | com password incorreto em pt            | {"contactId":"${contactId0}","password":"aline&A12","passwordConfirmation":"${password}"}                          | pt-BR    | Senha e confirmação de senha não podem ser diferentes.                                  | 422.005 |
      | com contactId incorreto                 | {"contactId":"12ef37ec-cc9c-4a30-bb80-a03ffe868f9c","password":"${password}","passwordConfirmation":"${password}"} | en-US    | The contact 12ef37ec-cc9c-4a30-bb80-a03ffe868f9c does not exist in the user ${userId1}. | 422.009 |
      | com contactId inválido                  | {"contactId":"inválido","password":"${password}","passwordConfirmation":"${password}"}                             | en-US    | The contact inválido does not exist in the user ${userId1}.                             | 422.009 |
      | com password incorreto                  | {"contactId":"${contactId0}","password":"aline&123","passwordConfirmation":"${password}"}                          | en-US    | Password and PasswordConfirmation cannot be different.                                  | 422.005 |
      | com contactId incorreto em pt           | {"contactId":"12ef37ec-cc9c-4a30-bb80-a03ffe868f9c","password":"${password}","passwordConfirmation":"${password}"} | pt-BR    | O contato 12ef37ec-cc9c-4a30-bb80-a03ffe868f9c não pertence ao usuário ${userId1}.      | 422.009 |
      | com contactId inválido em pt            | {"contactId":"inválido","password":"${password}","passwordConfirmation":"${password}"}                             | pt-BR    | O contato inválido não pertence ao usuário ${userId1}.                                  | 422.009 |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "{"contactId":"${contactId<id>}","password":"${password}","passwordConfirmation":"${password}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<id>}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 422

    Examples: 
      | id | descricao                       | language | message                                               | code    |
      |  2 | para MI sem token no body       | en-US    | Field token is required to MI customer type.          | 422.020 |
      |  2 | para MI sem token no body em pt | pt-BR    | O Campo token é obrigatório para clientes do tipo MI. | 422.020 |

  @Negativo
  Scenario Outline: Enviar token e cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId<contactId>}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    And I get token on database by contact_id "${contactId<contactId>}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "{"contactId":"${contactId<contactId>}","password":"${password}","passwordConfirmation":"${password}","token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 422

    Examples: 
      | contactId | descricao                                              | tokenType       | userId | language | message                             | code    |
      |         2 | para MI com tokenType ADD_CONTACT para PHONE           | ADD_CONTACT     |      2 | en-US    | Invalid token.                      | 422.002 |
      |         2 | para MI com tokenType CHANGE_PASSWORD para PHONE       | CHANGE_PASSWORD |      2 | en-US    | Invalid token.                      | 422.002 |
      |         2 | para MI com tokenType ADD_CONTACT para PHONE em pt     | ADD_CONTACT     |      2 | pt-BR    | Token inválido.                     | 422.002 |
      |         2 | para MI com tokenType CHANGE_PASSWORD para PHONE em pt | CHANGE_PASSWORD |      2 | pt-BR    | Token inválido.                     | 422.002 |
      | 2-2       | para MI com tokenType ADD_CONTACT para EMAIL           | ADD_CONTACT     |      2 | en-US    | Invalid token.                      | 422.002 |
      | 2-2       | para MI com tokenType CHANGE_PASSWORD para EMAIL       | CHANGE_PASSWORD |      2 | en-US    | Invalid token.                      | 422.002 |
      | 2-2       | para MI com tokenType REGISTER para EMAIL              | REGISTER        |      2 | en-US    | Invalid token.                      | 422.002 |

  @Positivo
  Scenario Outline: Enviar token e cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId<contactId>}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | contactId | descricao                | tokenType | userId |
      |         0 | empresarial com invoices | REGISTER  |      1 |

  @Positivo
  Scenario Outline: Enviar token e cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId<contactId>}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    And I wait 4 seconds
    And I get token on database by contact_id "${contactId<contactId>}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactId":"${contactId<contactId>}","password":"${password}","passwordConfirmation":"${password}","token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId<userId>}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | contactId | descricao                                 | tokenType | userId |
      |         2 | para MI com tokenType REGISTER para PHONE | REGISTER  |      2 |

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId1}" and login "${login}"
    Given I remove info from userId "${userId2}" and login "${documentPf}"
