Feature: GET_condominiums_{code}.feature
  Operação responsável por consultar os dados do condomínio e os blocos/unidades consumidora pertencentes ao mesmo no SalesForce.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token_password.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/condominiums" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "22019" as "code"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpoint}/${code}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}/${code}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}/<autoCadastro>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be <responseCode>

    Examples: 
      | description      | autoCadastro | responseCode | code      | message                                                                  | language |
      | sem code         |              |          400 | "400.024" | "At least one filter is required [document, zip-code, name, street]."    | "en-US"  |
      | sem code pt      |              |          400 | "400.024" | "Pelo menos um filtro é obrigatório [document, zip-code, name, street]." | "pt-BR"  |
      | code inválido    | abc          |          422 | "422.023" | "Condominium does not exist."                                            | "en-US"  |
      | code inválido pt | abc          |          422 | "422.023" | "Condomínio não existe."                                                 | "pt-BR"  |

  @Positivo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${code}"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/condominiumIdSf" is not empty
    And I verify if response value "/0/name" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/organizationId" is not empty
    And I verify if response value "/0/address/fullAddress" is not empty
    And I verify if response value "/0/address/address" is not empty
    And I verify if response value "/0/address/number" is not empty
    And I verify if response value "/0/address/neighborhood" is not empty
    And I verify if response value "/0/address/city" is not empty
    And I verify if response value "/0/address/stateCode" is not empty
    And I verify if response value "/0/address/zipCode" is not empty
    And I verify if response value "/0/blocks" is not empty
    And Response code must be 200
