@GET_condominiums
Feature: GET_condominiums.feature
  Operação responsável por consultar os dados do condomínio no SalesForce.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token_password.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/condominiums" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "52262409000180" as "document"
    And I save "55045421000120" as "documentMI"
    And I save "09180080" as "zipCode"
    And I save "RUA DOUTOR HELIO DA MATA SOUZA" as "street"
    And I save "CONDOMINIO CONJUNTO RESIDENCIAL ITAPUA" as "name"

  @Negativo
  Scenario: Enviar request sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set GET api endpoint as "${endpoint}?zip-code=${zipCode}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar request sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set GET api endpoint as "${endpoint}?zip-code=${zipCode}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar request <description>
    Given I set request header "client_id" as <client_id>
    Given I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?zip-code=${zipCode}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | description                 | query                   | code      | message                                                                  | language |
      | sem query                   |                         | "400.024" | "At least one filter is required [document, zip-code, name, street]."    | "en-US"  |
      | sem query em pt             |                         | "400.024" | "Pelo menos um filtro é obrigatório [document, zip-code, name, street]." | "pt-BR"  |
      | com document vazio          | ?document=              | "400.024" | "At least one filter is required [document, zip-code, name, street]."    | "en-US"  |
      | com document vazio em pt    | ?document=              | "400.024" | "Pelo menos um filtro é obrigatório [document, zip-code, name, street]." | "pt-BR"  |
      | com zip-code vazio          | ?zip-code=              | "400.024" | "At least one filter is required [document, zip-code, name, street]."    | "en-US"  |
      | com zip-code vazio em pt    | ?zip-code=              | "400.024" | "Pelo menos um filtro é obrigatório [document, zip-code, name, street]." | "pt-BR"  |
      | com street vazio            | ?street=                | "400.024" | "At least one filter is required [document, zip-code, name, street]."    | "en-US"  |
      | com street vazio em pt      | ?street=                | "400.024" | "Pelo menos um filtro é obrigatório [document, zip-code, name, street]." | "pt-BR"  |
      | com name vazio              | ?name=                  | "400.024" | "At least one filter is required [document, zip-code, name, street]."    | "en-US"  |
      | com name vazio em pt        | ?name=                  | "400.024" | "Pelo menos um filtro é obrigatório [document, zip-code, name, street]." | "pt-BR"  |
      | com zip-code inválido       | ?zip-code=alinedeferias | "400.007" | "Field zip-code has an invalid value or is empty."                       | "en-US"  |
      | com zip-code inválido em pt | ?zip-code=alinedeferias | "400.007" | "Campo zip-code com valor inválido ou vazio."                            | "pt-BR"  |
      | com document inválido       | ?document=alinedeferias | "400.007" | "Field document has an invalid value or is empty."                       | "en-US"  |
      | com document CPF            | ?document=01234567893   | "400.007" | "Field document has an invalid value or is empty."                       | "en-US"  |
      | com document inválido em pt | ?document=alinedeferias | "400.007" | "Campo document com valor inválido ou vazio."                            | "pt-BR"  |
      | com document CPF em pt      | ?document=01234567893   | "400.007" | "Campo document com valor inválido ou vazio."                            | "pt-BR"  |

  @Negativo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | description                         | query                    | code      | message                                                          | language |
      | com zip-code incorreto              | ?zip-code=13131131       | "422.023" | "Condominium does not exist."                                    | "en-US"  |
      | com document não cadastrado         | ?document=36733451000185 | "422.023" | "Condominium does not exist."                                    | "en-US"  |
      | com street inexistente              | ?street=${street}a       | "422.023" | "Condominium does not exist."                                    | "en-US"  |
      | com name inexistente                | ?name=${name}a           | "422.023" | "Condominium does not exist."                                    | "en-US"  |
      | com zip-code indisponível portal    | ?zip-code=65099110       | "422.049" | "The new portal is not yet available for your condominium."      | "en-US"  |
      | com zip-code incorreto em pt        | ?zip-code=13131131       | "422.023" | "Condomínio não existe."                                         | "pt-BR"  |
      | com document não cadastrado em pt   | ?document=36733451000185 | "422.023" | "Condomínio não existe."                                         | "pt-BR"  |
      | com street inexistente pt           | ?street=${street}a       | "422.023" | "Condomínio não existe."                                         | "pt-BR"  |
      | com name inexistente pt             | ?name=${name}a           | "422.023" | "Condomínio não existe."                                         | "pt-BR"  |
      | com zip-code indisponível portal pt | ?zip-code=65099110       | "422.049" | "O novo portal ainda não está disponível para o seu condomínio." | "pt-BR"  |

  @Positivo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/condominiumIdSf" is not empty
    And I verify if response value "/0/code" is not empty
    And I verify if response value "/0/name" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/organizationId" is not empty
    And I verify if response value "/0/address/fullAddress" is not empty
    And I verify if response value "/0/address/address" is not empty
    And I verify if response value "/0/address/number" is not empty
    And I verify if response value "/0/address/neighborhood" is not empty
    And I verify if response value "/0/address/city" is not empty
    And I verify if response value "/0/address/stateCode" is not empty
    And I verify if response value "/0/address/zipCode" is not empty
    And Response code must be 200

    Examples: 
      | description         | query                                                                     |
      | com document válido | ?document=${documentMI}                                                   |
      | com zip-code válido | ?zip-code=${zipCode}                                                      |
      | com street válido   | ?street=${street}                                                         |
      | com name válido     | ?name=${name}                                                             |
      | com query completa  | ?document=${documentMI}&zip-code=${zipCode}&street=${street}&name=${name} |

  @Positivo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | description                      | query                 |
      | com document de customer inativo | ?document=${document} |
