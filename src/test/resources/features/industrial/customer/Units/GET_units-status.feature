Feature: GET_units-status.feature
  Operação responsável por retornar o status da unidade consumidora do cliente.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"abender@outlook.com.br","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use api name as "/dev/industrial/customer/v1"
    And I save "/units-status" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "34749072800" as "document1"
    And I save "01168014174" as "documentn"
    And I save "1051799" as "instanceId"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?document=${document1}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}?document=${document1}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <descricao>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?document=${document1}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | descricao                 | client_id       | access_token       | message                                                                                    |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar status da unidade consumidora do cliente <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | description                           | query                 | code      | message                                          | language |
      | sem query                             |                       | "400.004" | "Field document or instance-id is required."     | "en-US"  |
      | sem query pt                          |                       | "400.004" | "O campo document ou instance-id é obrigatório." | "pt-BR"  |
      | instance-id com caractere             | ?instance-id=1051799a | "400.003" | "Field instance-id has an invalid format."       | "en-US"  |
      | instance-id com caractere pt          | ?instance-id=1051799a | "400.003" | "O campo instance-id tem o formato inválido."    | "pt-BR"  |
      | instance-id com caractere especial    | ?instance-id=1051799$ | "400.003" | "Field instance-id has an invalid format."       | "en-US"  |
      | instance-id com caractere especial pt | ?instance-id=1051799$ | "400.003" | "O campo instance-id tem o formato inválido."    | "pt-BR"  |

  @Positivo
  Scenario Outline: Consultar status da unidade consumidora do cliente <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | description                      | query            |
      | com document inteiro qualquer    | ?document=323    |
      | com document banana              | ?document=banana |
      | com instance-id inteiro qualquer | ?document=232    |

  @Positivo
  Scenario Outline: Consultar status da unidade consumidora do cliente <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/instanceId" is not empty
    And I verify if response value "/0/totalValueDue" is not empty
    And I verify if response value "/0/isReconnectionInSla" is not empty
    And I verify if response value "/0/message" is not empty
    And I verify if response value "/0/isReconnectionClosed" is not empty
    And I verify if response value "/0/isActiveBilling" is not empty
    And I verify if response value "/0/closedInSla" is not empty
    And I verify if response value "/0/code" is not empty
    And I verify if response value "/0/condominiumName" is not empty
    And I verify if response value "/0/unitComplement" is not empty
    And I verify if response value "/0/unitNumber" is not empty
    And I verify if response value "/0/customerName" is not empty
    And I verify if response value "/0/address/street" is not empty
    And I verify if response value "/0/address/number" is not empty
    And I verify if response value "/0/address/neighborhood" is not empty
    And I verify if response value "/0/address/city" is not empty
    And I verify if response value "/0/address/stateCode" is not empty
    And I verify if response value "/0/address/zipCode" is not empty
    And I verify if response value "/0/address/reference" is not empty
    And Response code must be 200

    Examples: 
      | description                              | query                                            |
      | com document de customer uma unidade     | ?document=${document1}                           |
      | com document de customer varias unidades | ?document=${documentn}                           |
      | com instance-id válido                   | ?instance-id=${instanceId}                       |
      | com document e instance-id válidos       | ?document=${document1}&instance-id=${instanceId} |
