Feature: GET_questions.feature
  Operação responsável por retornar perguntas para o usuário poder validar seu cadastro.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token_password.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use api name as "/dev/industrial/customer/v1"
    And I save "/questions" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?quantity=2"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}?quantity=2"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <descricao>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?quantity=2"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | descricao                 | client_id       | access_token       | message                                                                                    |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar perguntas <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | description                  | query         | code      | message                                                              | language |
      | com quantity igual a 1       | ?quantity=1   | "400.022" | "Request parameter quantity has to have number between 2 and 8."     | "en-US"  |
      | com quantity igual a 1 pt    | ?quantity=1   | "400.022" | "O parâmetro da requisição quantity deve conter número entre 2 e 8." | "pt-BR"  |
      | com quantity igual a 0       | ?quantity=0   | "400.022" | "Request parameter quantity has to have number between 2 and 8."     | "en-US"  |
      | com quantity igual a 0 pt    | ?quantity=0   | "400.022" | "O parâmetro da requisição quantity deve conter número entre 2 e 8." | "pt-BR"  |
      | com quantity igual a -1      | ?quantity=-1  | "400.022" | "Request parameter quantity has to have number between 2 and 8."     | "en-US"  |
      | com quantity igual a -1 pt   | ?quantity=-1  | "400.022" | "O parâmetro da requisição quantity deve conter número entre 2 e 8." | "pt-BR"  |
      | com quantity igual a 9       | ?quantity=9   | "400.022" | "Request parameter quantity has to have number between 2 and 8."     | "en-US"  |
      | com quantity igual a 9 pt    | ?quantity=9   | "400.022" | "O parâmetro da requisição quantity deve conter número entre 2 e 8." | "pt-BR"  |
      | com quantity igual a 1.5     | ?quantity=1.5 | "400.003" | "Field quantity has an invalid format."                              | "en-US"  |
      | com quantity igual a 1.5 pt  | ?quantity=1.5 | "400.003" | "O campo quantity tem o formato inválido."                           | "pt-BR"  |
      | com quantity igual a qa      | ?quantity=qa  | "400.003" | "Field quantity has an invalid format."                              | "en-US"  |
      | com quantity igual a qa pt   | ?quantity=qa  | "400.003" | "O campo quantity tem o formato inválido."                           | "pt-BR"  |
      | com quantity igual a true    | ?quantity=qa  | "400.003" | "Field quantity has an invalid format."                              | "en-US"  |
      | com quantity igual a true pt | ?quantity=qa  | "400.003" | "O campo quantity tem o formato inválido."                           | "pt-BR"  |
      | com quantity igual a @       | ?quantity=qa  | "400.003" | "Field quantity has an invalid format."                              | "en-US"  |
      | com quantity igual a @ pt    | ?quantity=qa  | "400.003" | "O campo quantity tem o formato inválido."                           | "pt-BR"  |

  @Positivo
  Scenario Outline: Consultar perguntas <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/question" is not empty
    And I verify if response value "/1/id" is not empty
    And I verify if response value "/1/question" is not empty
    And I verify if response value "/2/id" is empty
    And I verify if response value "/2/question" is empty
    And Response code must be 200

    Examples: 
      | description            | query       |
      | sem quantity           |             |
      | com quantity igual a 2 | ?quantity=2 |
      
  @Positivo
  Scenario: Consultar perguntas com quantity igual a 3
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?quantity=3"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/question" is not empty
    And I verify if response value "/1/id" is not empty
    And I verify if response value "/1/question" is not empty
    And I verify if response value "/2/id" is not empty
    And I verify if response value "/2/question" is not empty
    And Response code must be 200

 @Positivo
  Scenario: Consultar perguntas com quantity igual a 8
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?quantity=8"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/question" is not empty
    And I verify if response value "/1/id" is not empty
    And I verify if response value "/1/question" is not empty
    And I verify if response value "/2/id" is not empty
    And I verify if response value "/2/question" is not empty
    And I verify if response value "/3/id" is not empty
    And I verify if response value "/3/question" is not empty
    And I verify if response value "/4/id" is not empty
    And I verify if response value "/4/question" is not empty
    And I verify if response value "/5/id" is not empty
    And I verify if response value "/5/question" is not empty
    And I verify if response value "/6/id" is not empty
    And I verify if response value "/6/question" is not empty
    And I verify if response value "/7/id" is not empty
    And I verify if response value "/7/question" is not empty
    And Response code must be 200
      
