Feature: GET_customers_me_contacts.feature 
	Operação responsável por consultar contatos de um cliente.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token_password.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial" 
	#Given I use domain as "http://localhost:8088" 
	And I use api name as "/customer/v1" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "/customers" as "endpointCustomers" 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set GET api endpoint as "${endpointCustomers}/me/contacts" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição com client_id inválido 
	Given I set request header "client_id" as "${client_id}1" 
	When I set GET api endpoint as "${endpointCustomers}/me/contacts" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Positivo 
Scenario: Consultar customer contacts 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	When I set GET api endpoint as "${endpointCustomers}/me/contacts" 
	Then I get response body 
	And I get response status code 
	And I verify if response value "/0/contactId" is not empty 
	And I verify if response value "/0/contact" is not empty 
	And I verify if response value "/0/contactType" is not empty 
	And I verify if response value "/0/status" is not empty 
	And Response code must be 200 