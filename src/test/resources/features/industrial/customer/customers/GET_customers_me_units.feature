Feature: GET_customers_me_units.feature
  Consulta para obter as unidades consumidoras do cliente.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","password": "teste123!","username": "49757773204"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/customers" as "endpointCustomers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointCustomers}/me/units"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointCustomers}/me/units"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Positivo
  Scenario: Consultar customer
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointCustomers}/me/units"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/unitId" is not empty
    And I verify if response value "/0/instanceId" is not empty
    And I verify if response value "/0/name" is not empty
    And I verify if response value "/0/dueDay" is not empty
    And I verify if response value "/0/deliveryType" is not empty
    And I verify if response value "/0/hasGasMeter" is not empty
    And I verify if response value "/0/address/fullAddress" is not empty
    And I verify if response value "/0/address/address" is not empty
    And I verify if response value "/0/address/number" is not empty
    And I verify if response value "/0/address/complement" is not empty
    And I verify if response value "/0/address/neighborhood" is not empty
    And I verify if response value "/0/address/city" is not empty
    And I verify if response value "/0/address/stateCode" is not empty
    And I verify if response value "/0/address/zipCode" is not empty
    And Response code must be 200

  @Negativo
  Scenario Outline: Consultar customer <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<access_token>"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointCustomers}/me/units"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

    Examples: 
      | descricao                  | access_token     | code      | message                                       |
      | por access_token incorreto | ${access_token}1 | "400.005" | "Field customer-code must have 8 characters." |
