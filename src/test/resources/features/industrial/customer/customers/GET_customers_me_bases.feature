Feature: GET_customers_me_bases.feature 
	Operação responsável por consultar bases instaladas.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token_password.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial" 
	And I use api name as "/customer/v1" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "/customers" as "endpointCustomers" 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set GET api endpoint as "${endpointCustomers}/me/bases" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição com client_id inválido 
	Given I set request header "client_id" as "${client_id}1" 
	When I set GET api endpoint as "${endpointCustomers}/me/bases" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario Outline: Enviar requisição <test> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}"
	And I set request header "Accept-Language" as "<language>"
	When I set GET api endpoint as "${endpointCustomers}/me/bases<queryParameter>" 
	Then I get response body 
	And I get response status code 
	And I compare response value "/0/message" with "<message>" 
	And I compare response value "/0/code" with "<code>" 
	And Response code must be 400 
	
	Examples: 
		| test                         | message                                       | code    | queryParameter          | language |
		| sem query parameter          | Field customer-site-id is required.           | 400.001 |                         | en-US    |
		| com query parameter vazio    | Field customer-site-id is required.           | 400.001 | ?customer-site-id=      | en-US    |
		| com query parameter string   | Field customer-site-id has an invalid format. | 400.003 | ?customer-site-id=aa    | en-US    |
		| sem query parameter pt       | O campo customer-site-id é obrigatório.           | 400.001 |                         | pt-BR    |
		| com query parameter vazio pt | O campo customer-site-id é obrigatório.           | 400.001 | ?customer-site-id=      | pt-BR    |
		| com query parameter string pt| O campo customer-site-id tem o formato inválido. | 400.003 | ?customer-site-id=aa    | pt-BR    |
		
		
		@Positivo 
		Scenario: Consultar customer bases com customer-site-code inexistente
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			When I set GET api endpoint as "${endpointCustomers}/me/bases?customer-site-id=231" 
			Then I get response body 
			And I get response status code 
			And Response body must be "[]"
			And Response code must be 200 
			
			
		@Positivo 
		Scenario: Consultar customer bases 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			When I set GET api endpoint as "${endpointCustomers}/me/bases?customer-site-id=2372988" 
			Then I get response body 
			And I get response status code 
			And I verify if response value "/0/siteUseId" is not empty
			And I verify if response value "/0/code" is not empty
			And I verify if response value "/0/addressLine" is not empty
			And I verify if response value "/0/addressNumber" is not empty
			And I verify if response value "/0/complement" is not empty
			And I verify if response value "/0/city" is not empty
			And I verify if response value "/0/stateCode" is not empty
			And I verify if response value "/0/cep" is not empty
			And I verify if response value "/0/country" is not empty
			And I verify if response value "/0/installBase/0/name" is not empty
			And I verify if response value "/0/installBase/0/nickname" is not empty
			And I verify if response value "/0/installBase/0/description" is not empty
			And I verify if response value "/0/installBase/0/codeHierarchy" is not empty
			And I verify if response value "/0/installBase/0/quantity" is not empty
			And I verify if response value "/0/installBase/0/status" is not empty
			And I verify if response value "/0/installBase/0/activeDateFrom" is not empty
			And I verify if response value "/0/installBase/0/creationDate" is not empty
			And I verify if response value "/0/installBase/0/updateDate" is not empty
			And Response code must be 200