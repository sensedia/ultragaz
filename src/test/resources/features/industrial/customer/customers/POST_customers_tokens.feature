@POST_customers_tokens
Feature: POST_customers_tokens.feature
  Operação responsável enviar um token de validação para o contato informado pelo cliente.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/customers" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "wellington.moura@sensedia.com" as "contact"
    And I save "19982911139" as "phone"

  @Negativo
  Scenario: Enviar request sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactType":"EMAIL","contact":"${contact}"}"
    And I set POST api endpoint as "${endpoint}/tokens"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar request sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contactType":"EMAIL","contact":"${contact}"}"
    And I set POST api endpoint as "${endpoint}/tokens"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar request <description>
    Given I set request header "client_id" as <client_id>
    Given I set request header "access_token" as <access_token>
    And I set request body as "{"contactType":"EMAIL","contact":"${contact}"}"
    When I set POST api endpoint as "${endpoint}/tokens"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"contactType":"EMAIL","contact":"${contact}"}"
    When I set POST api endpoint as "${endpoint}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição <parameter> <language>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    When I set POST api endpoint as "${endpoint}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter | message                  | language |
      | sem body  | "Invalid request body."  | "en-US"  |
      | sem body  | "Request body inválido." | "pt-BR"  |

  @Negativo
  Scenario Outline: Validar token <descricao> <language>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                  | body                                                               | message                                       | code      | language |
      | sem contactType            | "{"contact":"${contact}"}"                                         | "Field contactType is required."              | "400.001" | en-US    |
      | sem contactType            | "{"contact":"${contact}"}"                                         | "O campo contactType é obrigatório."          | "400.001" | pt-BR    |
      | sem contact                | "{"contactType":"EMAIL"}"                                          | "Field contact is required."                  | "400.001" | en-US    |
      | sem contact                | "{"contactType":"EMAIL"}"                                          | "O campo contact é obrigatório."              | "400.001" | pt-br    |
      | com contactType vazio      | "{"contactType":"","contact":"${contact}"}"                        | "Field contactType has an invalid format."    | "400.003" | en-US    |
      | com contactType vazio      | "{"contactType":"","contact":"${contact}"}"                        | "O campo contactType tem o formato inválido." | "400.003" | pt-br    |
      | com contactType nulo       | "{"contactType":null,"contact":"${contact}"}"                      | "Field contactType is required."              | "400.001" | en-US    |
      | com contactType nulo       | "{"contactType":null,"contact":"${contact}"}"                      | "O campo contactType é obrigatório."          | "400.001" | pt-br    |
      | com contact vazio          | "{"contactType":"EMAIL","contact":""}"                             | "Field contact is required."                  | "400.001" | en-US    |
      | com contact vazio          | "{"contactType":"EMAIL","contact":""}"                             | "O campo contact é obrigatório."              | "400.001" | pt-br    |
      | com contact nulo           | "{"contactType":"EMAIL","contact":null}"                           | "Field contact is required."                  | "400.001" | en-US    |
      | com contact nulo           | "{"contactType":"EMAIL","contact":null}"                           | "O campo contact é obrigatório."              | "400.001" | pt-br    |
      | com contact inválido       | "{"contactType":"EMAIL","contact":"asdasd"}"                       | "Field contact has an invalid format."        | "400.003" | en-US    |
      | com contact inválido       | "{"contactType":"EMAIL","contact":"asdasd"}"                       | "O campo contact tem o formato inválido."     | "400.003" | pt-BR    |
      | com contactType inválido   | "{"contactType":"EMAILs","contact":"${contact}"}"                  | "Field contactType has an invalid format."    | "400.003" | en-US    |
      | com contactType inválido   | "{"contactType":"EMAILs","contact":"${contact}"}"                  | "O campo contactType tem o formato inválido." | "400.003" | pt-BR    |
      | com EMAIL contact inválido | "{"contactType":"EMAIL","contact":"wellington.mourasensedia.com"}" | "Field contact has an invalid format."        | "400.003" | en-US    |
      | com EMAIL contact inválido | "{"contactType":"EMAIL","contact":"wellington.mourasensedia.com"}" | "O campo contact tem o formato inválido."     | "400.003" | pt-BR    |

  @Negativo
  Scenario Outline: Validar token <descricao> <language>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                      | body                                               | message                                      | code      | language |
      | com PHONE e contact caracteres | "{"contactType":"PHONE","contact":"asdasd"}"       | "Invalid cell phone number to user."         | "422.018" | en-US    |
      | com PHONE e contact caracteres | "{"contactType":"PHONE","contact":"asdasd"}"       | "Número de celular inválido para o usuário." | "422.018" | pt-BR    |
      | com PHONE e contact 10 dígitos | "{"contactType":"PHONE","contact":"1998291113"}"   | "Invalid cell phone number to user."         | "422.018" | en-US    |
      | com PHONE e contact 10 dígitos | "{"contactType":"PHONE","contact":"1998291113"}"   | "Número de celular inválido para o usuário." | "422.018" | pt-BR    |
      | com PHONE e contact 12 dígitos | "{"contactType":"PHONE","contact":"199829111399"}" | "Invalid cell phone number to user."         | "422.018" | en-US    |
      | com PHONE e contact 12 dígitos | "{"contactType":"PHONE","contact":"199829111399"}" | "Número de celular inválido para o usuário." | "422.018" | pt-BR    |

  @Positivo
  Scenario Outline: Enviar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | descricao | body                                             |
      | com EMAIL | "{"contactType":"EMAIL","contact":"${contact}"}" |
      | com PHONE | "{"contactType":"PHONE","contact":"${phone}"}"   |
