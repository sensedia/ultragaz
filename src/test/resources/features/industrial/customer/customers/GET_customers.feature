Feature: GET_customers.feature
  Operação responsável por retornar dados do cliente.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token_password.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/customer/v1"
    And I save "/customers" as "endpointCustomers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "44248862000103" as "document"
    And I save "61602199006587" as "documentMi"
    And I save "fcce06cb-b40f-434e-8a8e-5a2622134454" as "userIdMi"
    And I save "8ce33e48-cf24-43c3-b48e-5cce66d0fa69" as "user-id"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointCustomers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointCustomers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<access_token>"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointCustomers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

    Examples: 
      | description                | access_token     |
      | por access_token incorreto | ${access_token}1 |

  @Negativo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointCustomers}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | description                         | query                                                | code    | message                                          | language |
      | sem query                           |                                                      | 400.004 | Field document or user-id is required.           | en-US    |
      | sem query em pt                     |                                                      | 400.004 | O campo document ou user-id é obrigatório.       | pt-BR    |
      | com document vazio                  | ?document=                                           | 400.004 | Field document or user-id is required.           | en-US    |
      | com document vazio em pt            | ?document=                                           | 400.004 | O campo document ou user-id é obrigatório.       | pt-BR    |
      | com user-id vazio                   | ?user-id=                                            | 400.004 | Field document or user-id is required.           | en-US    |
      | com user-id vazio em pt             | ?user-id=                                            | 400.004 | O campo document ou user-id é obrigatório.       | pt-BR    |
      | com unit-instance-id inválido       | ?document=${document}&unit-instance-id=alinedeferias | 400.003 | Field unit-instance-id has an invalid format.    | en-US    |
      | com unit-instance-id inválido em pt | ?document=${document}&unit-instance-id=alinedeferias | 400.003 | O campo unit-instance-id tem o formato inválido. | pt-BR    |
      | com type 0                          | ?document=${document}&type=0                         | 400.003 | Field type has an invalid format.                | en-US    |
      | com type inválido                   | ?document=${document}&type=MIS                       | 400.003 | Field type has an invalid format.                | en-US    |
      | com _expand inválido                | ?document=${document}&_expand=installments           | 400.003 | Field _expand has an invalid format.             | en-US    |

  @Negativo
  Scenario Outline: Consultar customers <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointCustomers}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | description                | query                                     | message                                 | code    | language |
      | com _limit inválido        | ?document=${document}&_offset=0&_limit=a  | Field _limit has an invalid format.     | 400.003 | en-US    |
      | com _offset inválido       | ?document=${document}&_limit=10&_offset=a | Field _offset has an invalid format.    | 400.003 | en-US    |
      | com _limit inválido em pt  | ?document=${document}&_offset=0&_limit=a  | O campo _limit tem o formato inválido.  | 400.003 | pt-BR    |
      | com _offset inválido em pt | ?document=${document}&_limit=10&_offset=a | O campo _offset tem o formato inválido. | 400.003 | pt-BR    |

  @Positivo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointCustomers}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerCode" is not empty
    And I verify if response value "/0/customerSiteId" is not empty
    And I verify if response value "/0/customerType" is not empty
    And I verify if response value "/0/corporateName" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/commercialRegion" is not empty
    And I verify if response value "/0/email" is not empty
    And I verify if response value "/0/customerAddresses/0/siteId" is not empty
    And I verify if response value "/0/customerAddresses/0/code" is not empty
    And I verify if response value "/0/customerAddresses/0/addressLine" is not empty
    #And I verify if response value "/0/customerAddresses/0/address" is not empty
    And I verify if response value "/0/customerAddresses/0/addressNumber" is not empty
    And I verify if response value "/0/customerAddresses/0/complement" is not empty
    And I verify if response value "/0/customerAddresses/0/country" is not empty
    And I verify if response value "/0/customerAddresses/0/city" is not empty
    And I verify if response value "/0/customerAddresses/0/zipCode" is not empty
    And I verify if response value "/0/customerAddresses/0/addressType" is not empty
    And I verify if response value "/0/customerAddresses/0/document" is not empty
    And I verify if response value "/0/customerAddresses/0/status" is not empty
    And Response code must be 200

    Examples: 
      | description                 | query                                                                          |
      | com document válido         | ?document=${document}                                                          |
      | com user-id válido          | ?user-id=${user-id}                                                            |
      | com unit-instance-id vazio  | ?document=${document}&unit-instance-id=                                        |
      | com unit-instance-id válido | ?document=${document}&unit-instance-id=1                                       |
      | com query completa          | ?document=${document}&user-id=${user-id}&unit-instance-id=1&_offset=0&_limit=1 |

  @Positivo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointCustomers}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerCode" is not empty
    And I verify if response value "/0/customerType" is not empty
    And I verify if response value "/0/corporateName" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/customerAddresses" is empty
    And Response code must be 200

    Examples: 
      | description                    | query                                     |
      | com unit-instance-id incorreto | ?document=${document}&unit-instance-id=-1 |

  @Positivo
  Scenario Outline: Consultar customer <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointCustomers}<query>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | description            | query                                         |
      | com user-id inválido   | ?user-id=alinedeferias                        |
      | com user-id incorreto  | ?user-id=8ce33e48-cf24-43c3-b48e-5cce66d0fa61 |
      | com document inválido  | ?document=alinedeferias                       |
      | com document incorreto | ?document=01234567893                         |

  @Positivo
  Scenario Outline: Consultar customer teste de paginação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointCustomers}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerCode" is not empty
    And I verify if response value "/0/customerSiteId" is not empty
    And I verify if response value "/0/customerType" is not empty
    And I verify if response value "/0/corporateName" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/commercialRegion" is not empty
    And I verify if response value "/0/email" is not empty
    And I verify if response value "/0/customerAddresses/0/siteId" is not empty
    And I verify if response value "/0/customerAddresses/0/code" is not empty
    And I verify if response value "/0/customerAddresses/0/addressLine" is not empty
    And I verify if response value "/0/customerAddresses/0/addressNumber" is not empty
    And I verify if response value "/0/customerAddresses/0/complement" is not empty
    And I verify if response value "/0/customerAddresses/0/country" is not empty
    And I verify if response value "/0/customerAddresses/0/city" is not empty
    And I verify if response value "/0/customerAddresses/0/zipCode" is not empty
    And I verify if response value "/0/customerAddresses/0/addressType" is not empty
    And I verify if response value "/0/customerAddresses/0/document" is not empty
    And I verify if response value "/0/customerAddresses/0/status" is not empty
    And I verify if response value "/1/customerId" is empty
    And I verify if response value "/1/customerCode" is empty
    And I verify if response value "/1/customerSiteId" is empty
    And I verify if response value "/1/customerType" is empty
    And I verify if response value "/1/corporateName" is empty
    And I verify if response value "/1/document" is empty
    And I verify if response value "/1/commercialRegion" is empty
    And I verify if response value "/1/email" is empty
    And I verify if response value "/1/customerAddress/0/siteId" is empty
    And I verify if response value "/1/customerAddress/0/code" is empty
    And I verify if response value "/1/customerAddress/0/addressLine" is empty
    And I verify if response value "/1/customerAddress/0/addressNumber" is empty
    And I verify if response value "/1/customerAddress/0/complement" is empty
    And I verify if response value "/1/customerAddress/0/country" is empty
    And I verify if response value "/1/customerAddress/0/city" is empty
    And I verify if response value "/1/customerAddress/0/zipCode" is empty
    And I verify if response value "/1/customerAddress/0/addressType" is empty
    And I verify if response value "/1/customerAddresses/0/document" is empty
    And I verify if response value "/1/customerAddress/0/status" is empty
    And Response code must be 200

    Examples: 
      | descricao              | query                                           |
      |                      0 | ?document=${document}&_offset=0&_limit=1        |
      | sem _offset            | ?document=${document}&_limit=1                  |
      | com type vazio         | ?document=${document}&type=                     |
      | com type MI e document | ?document=${documentMi}&type=MI                 |
      | com type MI e user-id  | ?user-id=${userIdMi}&type=MI                    |
      | com type MI e document | ?document=${documentMi}&type=MI&_expand=CONTACT |
      | com type MI e user-id  | ?user-id=${userIdMi}&type=MI&_expand=CONTACT    |
      | com type EMP           | ?document=${document}&type=EMP                  |
      | com _expand vazio      | ?document=${document}&_expand=                  |

  @Positivo
  Scenario Outline: Consultar customer teste de paginação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointCustomers}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerCode" is not empty
    And I verify if response value "/0/customerSiteId" is not empty
    And I verify if response value "/0/customerType" is not empty
    And I verify if response value "/0/corporateName" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/commercialRegion" is not empty
    And I verify if response value "/0/email" is not empty
    And I verify if response value "/0/customerAddresses/0/siteId" is not empty
    And I verify if response value "/0/customerAddresses/0/code" is not empty
    And I verify if response value "/0/customerAddresses/0/addressLine" is not empty
    And I verify if response value "/0/customerAddresses/0/addressNumber" is not empty
    And I verify if response value "/0/customerAddresses/0/complement" is not empty
    And I verify if response value "/0/customerAddresses/0/country" is not empty
    And I verify if response value "/0/customerAddresses/0/city" is not empty
    And I verify if response value "/0/customerAddresses/0/zipCode" is not empty
    And I verify if response value "/0/customerAddresses/0/addressType" is not empty
    And I verify if response value "/0/customerAddresses/0/document" is not empty
    And I verify if response value "/0/customerAddresses/0/status" is not empty
    And I verify if response value "/0/customerAddresses/0/contacts/0/contactId" is not empty
    And I verify if response value "/0/customerAddresses/0/contacts/0/name" is not empty
    And I verify if response value "/0/customerAddresses/0/contacts/0/contact" is not empty
    And I verify if response value "/0/customerAddresses/0/contacts/0/contactType" is not empty
    And I verify if response value "/0/customerAddresses/0/contacts/0/status" is not empty
    And I verify if response value "/0/customerAddresses/0/contacts/0/isValidated" is not empty
    And I verify if response value "/0/customerAddresses/0/contacts/0/contactIdSf" is not empty
    And I verify if response value "/1/customerId" is empty
    And I verify if response value "/1/customerCode" is empty
    And I verify if response value "/1/customerSiteId" is empty
    And I verify if response value "/1/customerType" is empty
    And I verify if response value "/1/corporateName" is empty
    And I verify if response value "/1/document" is empty
    And I verify if response value "/1/commercialRegion" is empty
    And I verify if response value "/1/email" is empty
    And I verify if response value "/1/customerAddress/0/siteId" is empty
    And I verify if response value "/1/customerAddress/0/code" is empty
    And I verify if response value "/1/customerAddress/0/addressLine" is empty
    And I verify if response value "/1/customerAddress/0/addressNumber" is empty
    And I verify if response value "/1/customerAddress/0/complement" is empty
    And I verify if response value "/1/customerAddress/0/country" is empty
    And I verify if response value "/1/customerAddress/0/city" is empty
    And I verify if response value "/1/customerAddress/0/zipCode" is empty
    And I verify if response value "/1/customerAddress/0/addressType" is empty
    And I verify if response value "/1/customerAddresses/0/document" is empty
    And I verify if response value "/1/customerAddress/0/status" is empty
    And Response code must be 200

    Examples: 
      | descricao           | query                                 |
      | com _expand CONTACT | ?document=${document}&_expand=CONTACT |
