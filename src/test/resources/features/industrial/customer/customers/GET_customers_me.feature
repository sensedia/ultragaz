Feature: GET_customers_me.feature 
	Consulta para obter dados da fatura.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token_password.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial" 
	And I use api name as "/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set GET api endpoint as "${endpointCustomers}/me" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição com client_id inválido 
	Given I set request header "client_id" as "${client_id}1" 
	When I set GET api endpoint as "${endpointCustomers}/me" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Positivo 
Scenario: Consultar customer 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	When I set GET api endpoint as "${endpointCustomers}/me" 
	Then I get response body 
	And I get response status code 
	And I verify if response value "/customerId" is not empty 
	And I verify if response value "/customerCode" is not empty 
	And I verify if response value "/customerSiteId" is not empty 
	And I verify if response value "/customerType" is not empty 
	And I verify if response value "/corporateName" is not empty 
	And I verify if response value "/document" is not empty 
	And I verify if response value "/commercialRegion" is not empty 
	And I verify if response value "/email" is not empty 
	And I verify if response value "/customerAddresses/0/siteId" is not empty 
	And I verify if response value "/customerAddresses/0/code" is not empty 
	And I verify if response value "/customerAddresses/0/addressLine" is not empty 
	And I verify if response value "/customerAddresses/0/addressNumber" is not empty 
	And I verify if response value "/customerAddresses/0/complement" is not empty 
	And I verify if response value "/customerAddresses/0/country" is not empty
	And I verify if response value "/customerAddresses/0/city" is not empty 
	And I verify if response value "/customerAddresses/0/stateCode" is not empty 
	And I verify if response value "/customerAddresses/0/zipCode" is not empty 
	And I verify if response value "/customerAddresses/0/addressType" is not empty 
	And I verify if response value "/customerAddresses/0/document" is not empty 
	And I verify if response value "/customerAddresses/0/status" is not empty
	And Response code must be 200 
	
	
@Negativo 
Scenario Outline: Consultar customer <descricao> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "<access_token>" 
	And I set request header "Accept-Language" as "pt-BR" 
	When I set GET api endpoint as "${endpointCustomers}/me" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
	And Response code must be 401 
	
	Examples: 
		| descricao                        | access_token                                            | code      | message                                                  | 
		| por access_token incorreto       | ${access_token}1                                        | "400.005" | "Field customer-code must have 8 characters."            |