@GET_bills
Feature: GET_bills.feature
  Consulta para obter dados da fatura.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/invoice/v1"
    And I save "/bills" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "06985092004" as "document"
    And I save "1006780" as "unitId"
    And I save "PAGO" as "status"
    And I save "2291499" as "invoiceNumber"
    And I save "2016-11-16" as "beginDueDate"
    And I save "2016-11-16" as "endDueDate"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?unit-id=1055713"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}?unit-id=1055713"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?unit-id=1055713"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar bills <descricao> <language>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                            | query                      | code      | message                                          | language |
      | por _limit 0                         | ?_limit=0                  | "400.007" | "Field _limit has an invalid value."             | en-US    |
      | por _limit 0                         | ?_limit=0                  | "400.007" | "O campo _limit tem um valor inválido."          | pt-BR    |
      | por begin-due-date mes incorreto     | ?begin-due-date=2020-13-12 | "400.003" | "Field begin-due-date has an invalid format."    | en-US    |
      | por begin-due-date dia incorreto     | ?begin-due-date=2020-03-33 | "400.003" | "Field begin-due-date has an invalid format."    | en-US    |
      | por begin-due-date formato incorreto | ?begin-due-date=2020/13/12 | "400.003" | "Field begin-due-date has an invalid format."    | en-US    |
      | por begin-due-date mes incorreto     | ?begin-due-date=2020-13-12 | "400.003" | "O campo begin-due-date tem o formato inválido." | pt-BR    |
      | por begin-due-date dia incorreto     | ?begin-due-date=2020-03-33 | "400.003" | "O campo begin-due-date tem o formato inválido." | pt-BR    |
      | por begin-due-date formato incorreto | ?begin-due-date=2020/13/12 | "400.003" | "O campo begin-due-date tem o formato inválido." | pt-BR    |
      | por end-due-date mes incorreto       | ?end-due-date=2020-13-12   | "400.003" | "Field end-due-date has an invalid format."      | en-US    |
      | por end-due-date dia incorreto       | ?end-due-date=2020-03-33   | "400.003" | "Field end-due-date has an invalid format."      | en-US    |
      | por end-due-date formato incorreto   | ?end-due-date=2020/13/12   | "400.003" | "Field end-due-date has an invalid format."      | en-US    |
      | por end-due-date mes incorreto       | ?end-due-date=2020-13-12   | "400.003" | "O campo end-due-date tem o formato inválido."   | pt-BR    |
      | por end-due-date dia incorreto       | ?end-due-date=2020-03-33   | "400.003" | "O campo end-due-date tem o formato inválido."   | pt-BR    |
      | por end-due-date formato incorreto   | ?end-due-date=2020/13/12   | "400.003" | "O campo end-due-date tem o formato inválido."   | pt-BR    |
      | por status inválido                  | ?status=VENCIDOs           | "400.003" | "Field status has an invalid format."            | en-US    |
      | por _limit string                    | ?_limit=a                  | "400.003" | "Field _limit has an invalid format."            | en-US    |
      | por _offset string                   | ?_offset=a                 | "400.003" | "Field _offset has an invalid format."           | en-US    |
      | por status inválido                  | ?status=VENCIDOs           | "400.003" | "O campo status tem o formato inválido."         | pt-BR    |
      | por _limit string                    | ?_limit=a                  | "400.003" | "O campo _limit tem o formato inválido."         | pt-BR    |
      | por _offset string                   | ?_offset=a                 | "400.003" | "O campo _offset tem o formato inválido."        | pt-BR    |

  @Positivo
  Scenario Outline: Consultar bills <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerCode" is not empty
    And I verify if response value "/0/customerName" is not empty
    And I verify if response value "/0/customerAddress" is not empty
    And I verify if response value "/0/invoiceNumber" is not empty
    And I verify if response value "/0/emissionDate" is not empty
    And I verify if response value "/0/paymentDate" is empty
    And I verify if response value "/0/referenceMonth" is not empty
    And I verify if response value "/0/dueDate" is not empty
    And I verify if response value "/0/totalAmount" is not empty
    And I verify if response value "/0/condominiumName" is not empty
    And I verify if response value "/0/condominiumAddress" is not empty
    And I verify if response value "/0/condominiumAddressNumber" is not empty
    And I verify if response value "/0/unitComplement" is not empty
    And I verify if response value "/0/unitId" is not empty
    And I verify if response value "/0/alerts/0/message" is empty
    And I verify if response value "/0/alerts/0/order" is not empty
    And I verify if response value "/0/typeableLine" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/historicalConsumptions/0/measurementDate" is not empty
    And I verify if response value "/0/historicalConsumptions/0/initialMeasurement" is not empty
    And I verify if response value "/0/historicalConsumptions/0/finalMeasurement" is not empty
    And I verify if response value "/0/historicalConsumptions/0/m3Consumption" is not empty
    And I verify if response value "/0/historicalConsumptions/0/kgConsumption" is not empty
    And Response code must be 200

    Examples: 
      | descricao                        | queryParameter                   |
      | sem query parameter              |                                  |
      | por unit-id                      | ?unit-id=${unitId}               |
      | por document                     | ?document=${document}            |
      | por invoice-number               | ?invoice-number=${invoiceNumber} |
      | por begin-due-date               | ?begin-due-date=${beginDueDate}  |
      | por end-due-date                 | ?end-due-date=${endDueDate}      |
      | por status VENCIDO               | ?status=VENCIDO                  |
      | por _limit 1                     | ?_limit=1                        |
      | por _limit 100                   | ?_limit=100                      |
      | por _offset 0                    | ?_offset=0                       |
      | por _offset 1                    | ?_offset=1                       |
      | por _offset 100                  | ?_offset=100                     |
      | por _limit vazio                 | ?_limit=                         |
      | por status vazio                 | ?status=                         |
      | por _offset vazio                | ?_offset=                        |
      | por begin-due-date vazio         | ?begin-due-date=                 |
      | por end-due-date vazio           | ?end-due-date=                   |
      | por unit-id vazio                | ?unit-id=                        |
      | por begin-due-date ano incorreto | ?begin-due-date=0000-12-12       |

 	@Positivo
  Scenario Outline: Consultar bills <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerCode" is not empty
    And I verify if response value "/0/customerName" is not empty
    And I verify if response value "/0/customerAddress" is not empty
    And I verify if response value "/0/invoiceNumber" is not empty
    And I verify if response value "/0/emissionDate" is not empty
    And I verify if response value "/0/paymentDate" is not empty
    And I verify if response value "/0/referenceMonth" is not empty
    And I verify if response value "/0/dueDate" is not empty
    And I verify if response value "/0/totalAmount" is not empty
    And I verify if response value "/0/condominiumName" is not empty
    And I verify if response value "/0/condominiumAddress" is not empty
    And I verify if response value "/0/condominiumAddressNumber" is not empty
    And I verify if response value "/0/unitComplement" is not empty
    And I verify if response value "/0/unitId" is not empty
    And I verify if response value "/0/alerts/0/message" is empty
    And I verify if response value "/0/alerts/0/order" is not empty
    And I verify if response value "/0/typeableLine" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/historicalConsumptions/0/measurementDate" is not empty
    And I verify if response value "/0/historicalConsumptions/0/initialMeasurement" is not empty
    And I verify if response value "/0/historicalConsumptions/0/finalMeasurement" is not empty
    And I verify if response value "/0/historicalConsumptions/0/m3Consumption" is not empty
    And I verify if response value "/0/historicalConsumptions/0/kgConsumption" is not empty
    And Response code must be 200

    Examples: 
      | descricao                        | queryParameter                   |
      | por status PAGO                  | ?status=PAGO                     |

  #Filtros vazios estão retornando vazios por problema de massa de dados
  #Os 10 ultimos resultados possuem status cancelado
  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                      | queryParameter           |
      | por document incorreto         | ?document=213131         |
      | por unit-id incorreto          | ?unit-id=21312           |
      | por invoice-number incorreto   | ?invoice-number=12312    |
      | por end-due-date ano incorreto | ?end-due-date=0000-12-12 |
      | por status A_VENCER            | ?status=A_VENCER         |
      | por invoice-number vazio       | ?invoice-number=         |
      | por document vazio             | ?document=               |
