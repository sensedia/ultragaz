Feature: POST_bills_{billId}.feature
  Operação responsável por retornar o PDF da fatura.

  @PreRequest
  Scenario: Gerar token não logado
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"06985092004","password":"ultra.000"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token_passwd"

	@Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "14cb7687-901b-4e23-8752-da3696ef377f" as "questionId01"
    And I save "18650314-c8a5-4753-92ba-5cce898789aa" as "questionId02"
    And I save "62853586219" as "document"
	
	@PreRequest
  Scenario Outline: Validar respostas das perguntas com customer não logado
  	Given I use api name as "/dev/industrial/customer/v1"
    And I save "/questions/answers" as "endpointAnswers"
    And I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as <token>
    And I set request body as <body>
    When I set POST api endpoint as "${endpointAnswers}"
    Then I get response body
    And I get response status code
    And I save response header "x-token" as "xtoken<id>"
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | id | token             | body                                                                                                                                                           |
      |  1 | "${access_token}" | "{"document":"${document}","unitInstanceId":950074,"answers":[{"questionId":"${questionId01}","answer":"SP"},{"questionId":"${questionId02}","answer":"10"}]}" |
      |  2 | "${access_token}" | "{"document":"${document}","unitInstanceId":950074,"answers":[{"questionId":"${questionId01}","answer":"SP"},{"questionId":"${questionId02}","answer":"10"}]}" |

  @Definition
  Scenario: Definir configurações de ambiente
    And I use api name as "/dev/industrial/bill/v1"
    And I save "/bills" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "DOWNLOAD" as "deliveryMethod"
    And I save "wellington.moura@sensedia.com" as "email"
    And I save "979374" as "unitId"

  @PreRequest
  Scenario Outline: Consultar bills <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I save response value "/0/id" as "billId"
    And Response code must be 200

    Examples: 
      | descricao   | queryParameter     |
      | por unit-id | ?unit-id=${unitId} |

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"DOWNLOAD","isReduced":false}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"DOWNLOAD","isReduced":false}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"DOWNLOAD","isReduced":false}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token              | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token_passwd}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token_passwd}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                        | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token_passwd}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request body as "{"deliveryMethod":"DOWNLOAD","isReduced":false}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Consultar boleto <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"${deliveryMethod}","email":"${email}"}"
    When I set POST api endpoint as "${endpoint}/<id>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be <code>

    Examples: 
      | descricao           | id         | code |
      | com billId inválido | ${billId}1 |  404 |
      | com billId vazio    |            |  405 |

  @Negativo
  Scenario Outline: Consultar boleto <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                         | body                                                       | code      | message                                 | language |
      | com deliveryMethod vazio          | {"deliveryMethod":"","email":"${email}"}                   | "400.000" | "Invalid request body."                 | en-US    |
      | com deliveryMethod nulo           | {"deliveryMethod":null,"email":"${email}"}                 | "400.001" | "Field deliveryMethod is required."     | en-US    |
      | com deliveryMethod inválido       | {"deliveryMethod":"${deliveryMethod}1","email":"${email}"} | "400.000" | "Invalid request body."                 | en-US    |
      | sem deliveryMethod                | {"email":"${email}"}                                       | "400.001" | "Field deliveryMethod is required."     | en-US    |
      | com deliveryMethod vazio em pt    | {"deliveryMethod":"","email":"${email}"}                   | "400.000" | "Request body inválido."                | pt-BR    |
      | com deliveryMethod nulo em pt     | {"deliveryMethod":null,"email":"${email}"}                 | "400.001" | "O campo deliveryMethod é obrigatório." | pt-BR    |
      | com deliveryMethod inválido em pt | {"deliveryMethod":"${deliveryMethod}1","email":"${email}"} | "400.000" | "Request body inválido."                | pt-BR    |
      | sem deliveryMethod em pt          | {"email":"${email}"}                                       | "400.001" | "O campo deliveryMethod é obrigatório." | pt-BR    |

  @Negativo
  Scenario Outline: Consultar boleto para customer não logado <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 400

    Examples: 
      | descricao          | body                                                            | code      | message                        | language |
      | sem token          | "{"deliveryMethod":"DOWNLOAD","isReduced": false}"              | "400.001" | "Field token is required."     | en-US    |
      | sem token pt       | "{"deliveryMethod":"DOWNLOAD","isReduced": false}"              | "400.001" | "O campo token é obrigatório." | pt-BR    |
      | com token vazio    | "{"deliveryMethod":"DOWNLOAD","isReduced": false,"token":""}"   | "400.001" | "Field token is required."     | en-US    |
      | com token vazio pt | "{"deliveryMethod":"DOWNLOAD","isReduced": false,"token":""}"   | "400.001" | "O campo token é obrigatório." | pt-BR    |
      | com token nulo     | "{"deliveryMethod":"DOWNLOAD","isReduced": false,"token":null}" | "400.001" | "Field token is required."     | en-US    |
      | com token nulo pt  | "{"deliveryMethod":"DOWNLOAD","isReduced": false,"token":null}" | "400.001" | "O campo token é obrigatório." | pt-BR    |

  @Positivo
  Scenario: Consultar boleto customer não logado por download
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"DOWNLOAD","isReduced":false,"token":"xtoken1"}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I save response header "location" as "location"
    And I verify if response body is empty
    And Response code must be 302

  @Negativo
  Scenario Outline: Consultar boleto para customer não logado <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And I compare response value "/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao             | body                                                                    | code      | message           | language |
      | com token já usado    | "{"deliveryMethod":"DOWNLOAD","isReduced":false,"token":"${xtoken1}"}"  | "422.013" | "Invalid token."  | "en-US"  |
      | com token já usado pt | "{"deliveryMethod":"DOWNLOAD","isReduced":false,"token":"${xtoken1}"}"  | "422.013" | "Token inválido." | "pt-BR"  |
      | com token inválido    | "{"deliveryMethod":"DOWNLOAD","isReduced":false,"token":"${xtoken2}1"}" | "422.013" | "Invalid token."  | "en-US"  |
      | com token inválido pt | "{"deliveryMethod":"DOWNLOAD","isReduced":false,"token":"${xtoken2}1"}" | "422.013" | "Token inválido." | "pt-BR"  |

  @Positivo
  Scenario: Consultar boleto customer não logado por email
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"EMAIL","email":"${email}","isReduced":false,"token":"xtoken2"}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

  @Positivo
  Scenario Outline: Consultar boleto por DOWNLOAD e isReduced true <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"DOWNLOAD","isReduced":true<token>}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I save response header "location" as "location"
    And I verify if response body is empty
    And Response code must be 302

    Examples: 
      | descricao                | token              |
      | sem token                |                    |
      | com token já usado       | ,"token":"xtoken2" |
      | com outro token já usado | ,"token":"xtoken1" |
      | com token vazio          | ,"token":""        |
      | com token nulo           | ,"token":null      |

 @Positivo
  Scenario Outline: Consultar boleto por EMAIL e isReduced true <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"EMAIL","email":"${email}","isReduced":true<token>}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao                | token              |
      | sem token                |                    |
      | com token já usado       | ,"token":"xtoken2" |
      | com outro token já usado | ,"token":"xtoken1" |
      | com token vazio          | ,"token":""        |
      | com token nulo           | ,"token":null      |
      
  @Positivo
  Scenario Outline: Consultar boleto por download <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"DOWNLOAD"<isReduced>}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 302

    Examples: 
      | descricao           | isReduced          |
      | sem isReduced       |                    |
      | com isReduced vazio | ,"isReduced":""    |
      | com isReduced nulo  | ,"isReduced":null  |
      | com isReduced true  | ,"isReduced":true  |
      | com isReduced false | ,"isReduced":false |

  @Positivo
  Scenario Outline: Consultar boleto por email <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token_passwd}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"deliveryMethod":"EMAIL","email":"${email}"<isReduced>}"
    When I set POST api endpoint as "${endpoint}/${billId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao           | isReduced          |
      | sem isReduced       |                    |
      | com isReduced vazio | ,"isReduced":""    |
      | com isReduced nulo  | ,"isReduced":null  |
      | com isReduced true  | ,"isReduced":true  |
      | com isReduced false | ,"isReduced":false |
