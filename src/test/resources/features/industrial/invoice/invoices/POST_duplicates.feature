Feature: POST_duplicates.feature
  Operação responsável por retornar multiplas notas fiscais / boletos.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev"
    And I use api name as "/industrial/invoice/v1"
    And I save "/duplicates" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "A32E77886035499CE053166114AC2AAE" as "invoiceId"
    And I save "A32E79D021E24DE9E053166114ACF8A9" as "installmentId"
    And I save "A4FDFBCDE4512E92E053166114AC36D7" as "installmentId2"
    And I save "3400413" as "customerCode"
    And I save "7505917" as "customerSiteCode"
    And I save "wellington.moura@sensedia.com" as "email"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request body as "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request body as "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Retornar múltiplas NFs/boletos <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request header "Content-type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And I compare response value "/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao                                         | body                                                                                                                                                                                                                                                             | code      | message                                                                                      | language |
      | com customerCode inválido                         | "{"customerCode":"3400413A","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                    | "422.009" | "Invalid combination between invoice.ids ${invoiceId} and customerCode."                     | "en-US"  |
      | com customerSiteCode inválido                     | "{"customerCode":"3400413","customerSiteCode":"7505917A","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                    | "422.009" | "Invalid combination between invoice.ids ${invoiceId} and customerSiteCode."                 | "en-US"  |
      | com invoice.ids inválido                          | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["outubro"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                          | "422.011" | "Invalid invoice.ids: outubro."                                                              | "en-US"  |
      | com invoice.ids não pertencente ao customer       | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["A4FD77B4CDEE2A7DE053166114AC9927"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}" | "422.009" | "Invalid combination between invoice.ids A4FD77B4CDEE2A7DE053166114AC9927 and customerCode." | "en-US"  |
      | com invoiceId inválido                            | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"invalid","installmentId":"${installmentId}"}]}"                           | "422.010" | "Invalid bankslips[0].invoiceId: invalid."                                                   | "en-US"  |
      | com installmentId inválido                        | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"${invoiceId}","installmentId":"${installmentId}aa"}]}"                    | "422.012" | "Invalid combination between invoiceId and installmentId in bankslips[0]."                   | "en-US"  |
      | com installmentId não relacionado ao invoiceId    | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"${invoiceId}","installmentId":"${installmentId2}"}]}"                     | "422.012" | "Invalid combination between invoiceId and installmentId in bankslips[0]."                   | "en-US"  |
      | com customerCode inválido em pt                   | "{"customerCode":"3400413A","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                    | "422.009" | "Combinação inválida entre invoice.ids ${invoiceId} e customerCode."                         | "pt-BR"  |
      | com customerSiteCode inválido em pt               | "{"customerCode":"3400413","customerSiteCode":"7505917A","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                    | "422.009" | "Combinação inválida entre invoice.ids ${invoiceId} e customerSiteCode."                     | "pt-BR"  |
      | com invoice.ids inválido pt                       | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["outubro"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                          | "422.011" | "invoice.ids inválido: outubro."                                                             | "pt-BR"  |
      | com invoice.ids não pertencente ao customer pt    | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["A4FD77B4CDEE2A7DE053166114AC9927"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}" | "422.009" | "Combinação inválida entre invoice.ids A4FD77B4CDEE2A7DE053166114AC9927 e customerCode."     | "pt-BR"  |
      | com invoiceId inválido pt                         | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"invalido","installmentId":"${installmentId}"}]}"                          | "422.010" | "bankslips[0].invoiceId inválido: invalido."                                                 | "pt-BR"  |
      | com installmentId inválido pt                     | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"${invoiceId}","installmentId":"${installmentId}aa"}]}"                    | "422.012" | "Combinação inválida entre invoiceId e installmentId em bankslips[0]."                       | "pt-BR"  |
      | com installmentId não relacionado ao invoiceId pt | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"${invoiceId}","installmentId":"${installmentId2}"}]}"                     | "422.012" | "Combinação inválida entre invoiceId e installmentId em bankslips[0]."                       | "pt-BR"  |

  @Negativo
  Scenario Outline: Retornar múltiplas NFs/boletos <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request header "Content-type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 400

    Examples: 
      | descricao                         | body                                                                                                                                                                                                                                               | code      | message                                                   | language |
      | com customerCode vazio            | "{"customerCode":"","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"              | "400.001" | "Field customerCode is required."                         | "en-US"  |
      | com customerCode nulo             | "{"customerCode":null,"customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"            | "400.001" | "Field customerCode is required."                         | "en-US"  |
      | sem customerCode                  | "{"customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                                | "400.001" | "Field customerCode is required."                         | "en-US"  |
      | com customerSiteCode vazio        | "{"customerCode":"3400413","customerSiteCode":"","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"              | "400.001" | "Field customerSiteCode is required."                     | "en-US"  |
      | com customerSiteCode nulo         | "{"customerCode":"3400413","customerSiteCode":null,"deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"            | "400.001" | "Field customerSiteCode is required."                     | "en-US"  |
      | sem customerSiteCode              | "{"customerCode":"3400413","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                                    | "400.001" | "Field customerSiteCode is required."                     | "en-US"  |
      | com email vazio                   | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"               | "400.003" | "Field email has an invalid format."                      | "en-US"  |
      | com email nulo                    | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":null,"invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"             | "400.001" | "Field email is required."                                | "en-US"  |
      | com email inválido                | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"email.invalido","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}" | "400.003" | "Field email has an invalid format."                      | "en-US"  |
      | sem email                         | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                          | "400.001" | "Field email is required."                                | "en-US"  |
      | com deliveryMethod vazio          | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"            | "400.000" | "Invalid request body."                                   | "en-US"  |
      | com deliveryMethod nulo           | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":null,"email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"          | "400.001" | "Field deliveryMethod is required."                       | "en-US"  |
      | com deliveryMethod inválido       | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"correios","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"    | "400.000" | "Invalid request body."                                   | "en-US"  |
      | sem deliveryMethod                | "{"customerCode":"3400413","customerSiteCode":"7505917","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                                | "400.001" | "Field deliveryMethod is required."                       | "en-US"  |
      | com documentType vazio            | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"          | "400.000" | "Invalid request body."                                   | "en-US"  |
      | com documentType nulo             | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":null,"ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"        | "400.001" | "Field invoice.documentType is required."                 | "en-US"  |
      | com documentType inválido         | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XMLs","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"      | "400.000" | "Invalid request body."                                   | "en-US"  |
      | sem documentType                  | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                            | "400.001" | "Field invoice.documentType is required."                 | "en-US"  |
      | com invoice.ids vazio             | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":[]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                     | "400.009" | "Field invoice.ids must have at least one information."   | "en-US"  |
      | com invoice.ids contendo nulo     | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":[null]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                 | "400.010" | "Field invoice.ids must not have null element."           | "en-US"  |
      | com invoice.ids nulo              | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":null},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                   | "400.009" | "Field invoice.ids must have at least one information."   | "en-US"  |
      | sem invoice.ids                   | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML"},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                              | "400.001" | "Field invoice.ids is required."                          | "en-US"  |
      | com invoiceId vazio               | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "","installmentId":"${installmentId}"}]}"                   | "400.001" | "Field bankslips[0].invoiceId is required."               | "en-US"  |
      | com invoiceId nulo                | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": null,"installmentId":"${installmentId}"}]}"                 | "400.001" | "Field bankslips[0].invoiceId is required."               | "en-US"  |
      | sem invoiceId                     | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"installmentId":"${installmentId}"}]}"                                   | "400.001" | "Field bankslips[0].invoiceId is required."               | "en-US"  |
      | com installmentId vazio           | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"invalid","installmentId":""}]}"                             | "400.001" | "Field bankslips[0].installmentId is required."           | "en-US"  |
      | com installmentId nulo            | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"invalid","installmentId":null}]}"                           | "400.001" | "Field bankslips[0].installmentId is required."           | "en-US"  |
      | sem installmentId                 | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"invalid"}]}"                                                | "400.001" | "Field bankslips[0].installmentId is required."           | "en-US"  |
      | com customerCode vazio em pt      | "{"customerCode":"","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"              | "400.001" | "O campo customerCode é obrigatório."                     | "pt-BR"  |
      | com customerCode nulo em pt       | "{"customerCode":null,"customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"            | "400.001" | "O campo customerCode é obrigatório."                     | "pt-BR"  |
      | sem customerCode em pt            | "{"customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                                | "400.001" | "O campo customerCode é obrigatório."                     | "pt-BR"  |
      | com customerSiteCode vazio em pt  | "{"customerCode":"3400413","customerSiteCode":"","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"              | "400.001" | "O campo customerSiteCode é obrigatório."                 | "pt-BR"  |
      | com customerSiteCode nulo em pt   | "{"customerCode":"3400413","customerSiteCode":null,"deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"            | "400.001" | "O campo customerSiteCode é obrigatório."                 | "pt-BR"  |
      | sem customerSiteCode em pt        | "{"customerCode":"3400413","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                                    | "400.001" | "O campo customerSiteCode é obrigatório."                 | "pt-BR"  |
      | com deliveryMethod vazio em pt    | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"            | "400.000" | "Request body inválido."                                  | "pt-BR"  |
      | com deliveryMethod nulo em pt     | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":null,"email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"          | "400.001" | "O campo deliveryMethod é obrigatório."                   | "pt-BR"  |
      | com deliveryMethod inválido em pt | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"correios","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"    | "400.000" | "Request body inválido."                                  | "pt-BR"  |
      | sem deliveryMethod em pt          | "{"customerCode":"3400413","customerSiteCode":"7505917","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                                | "400.001" | "O campo deliveryMethod é obrigatório."                   | "pt-BR"  |
      | com email vazio em pt             | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"               | "400.003" | "O campo email tem o formato inválido."                   | "pt-BR"  |
      | com email nulo em pt              | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":null,"invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"             | "400.001" | "O campo email é obrigatório."                            | "pt-BR"  |
      | com email inválido em pt          | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"email.invalido","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}" | "400.003" | "O campo email tem o formato inválido."                   | "pt-BR"  |
      | sem email em pt                   | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                          | "400.001" | "O campo email é obrigatório."                            | "pt-BR"  |
      | com documentType vazio pt         | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"          | "400.000" | "Request body inválido."                                  | "pt-BR"  |
      | com documentType nulo pt          | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":null,"ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"        | "400.001" | "O campo invoice.documentType é obrigatório."             | "pt-BR"  |
      | com documentType inválido pt      | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XMLs","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"      | "400.000" | "Request body inválido."                                  | "pt-BR"  |
      | sem documentType pt               | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                            | "400.001" | "O campo invoice.documentType é obrigatório."             | "pt-BR"  |
      | com invoice.ids vazio pt          | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":[]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                     | "400.009" | "O campo invoice.ids deve ter pelo menos uma informação." | "pt-BR"  |
      | com invoice.ids contendo nulo pt  | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":[null]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                 | "400.010" | "O campo invoice.ids não pode ter elemento nulo."         | "pt-BR"  |
      | com invoice.ids nulo pt           | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":null},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                   | "400.001" | "O campo invoice.ids é obrigatório."                      | "pt-BR"  |
      | sem invoice.ids pt                | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML"},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                              | "400.001" | "O campo invoice.ids é obrigatório."                      | "pt-BR"  |
      | com invoiceId vazio               | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "","installmentId":"${installmentId}"}]}"                   | "400.001" | "O campo bankslips[0].invoiceId é obrigatório."           | "pt-BR"  |
      | com invoiceId nulo                | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": null,"installmentId":"${installmentId}"}]}"                 | "400.001" | "O campo bankslips[0].invoiceId é obrigatório."           | "pt-BR"  |
      | sem invoiceId                     | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"installmentId":"${installmentId}"}]}"                                   | "400.001" | "O campo bankslips[0].invoiceId é obrigatório."           | "pt-BR"  |
      | com installmentId vazio pt        | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"invalid","installmentId":""}]}"                             | "400.001" | "O campo bankslips[0].installmentId é obrigatório."       | "pt-BR"  |
      | com installmentId nulo pt         | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"invalid","installmentId":null}]}"                           | "400.001" | "O campo bankslips[0].installmentId é obrigatório."       | "pt-BR"  |
      | sem installmentId pt              | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId":"invalid"}]}"                                                | "400.001" | "O campo bankslips[0].installmentId é obrigatório."       | "pt-BR"  |

  @Positivo
  Scenario Outline: Retornar múltiplas NFs/boletos DOWNLOAD e <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 302

    Examples: 
      | descricao          | body                                                                                                                                                                                                                           |
      | documentType XML   | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"DOWNLOAD","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"   |
      | sem invoice        | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"DOWNLOAD","bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                                                           |
      | sem bankslips      | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"DOWNLOAD","invoice":{"documentType":"XML","ids":["${invoiceId}"]}}"                                                                                  |
      | documentType DANFE | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"DOWNLOAD","invoice":{"documentType":"DANFE","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}" |

  @Positivo
  Scenario Outline: Retornar múltiplas NFs/boletos EMAIL e <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao          | body                                                                                                                                                                                                                                           |
      | documentType XML   | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"   |
      | sem invoice        | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}"                                                           |
      | sem bankslips      | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"XML","ids":["${invoiceId}"]}}"                                                                                  |
      | documentType DANFE | "{"customerCode":"3400413","customerSiteCode":"7505917","deliveryMethod":"EMAIL","email":"${email}","invoice":{"documentType":"DANFE","ids":["${invoiceId}"]},"bankslips":[{"invoiceId": "${invoiceId}","installmentId":"${installmentId}"}]}" |
