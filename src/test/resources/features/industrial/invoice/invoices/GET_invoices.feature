Feature: GET_invoices.feature
  Consulta para obter dados da fatura.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev"
    And I use api name as "/industrial/invoice/v1"
    And I save "/invoices" as "endpointInvoices"
    And I save "16aa22fe-4aef-3900-b559-642745e09178" as "client_id"
    And I save "d24168f6-0b1e-35a3-b607-5c305e7b19f3" as "client_secret"
    And I save "300644" as "code"
    And I save "401466" as "siteCode"
    And I save "2019-01-01" as "beginDate"
    And I save "2020-12-31" as "endDate"
    And I save "1947350" as "invoiceNumber"
    And I save "100.00" as "beginAmount"
    And I save "1000.00" as "endAmount"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointInvoices}?customer-code=${code}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem client_id em Português
    Given I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}?customer-code=${code}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointInvoices}?customer-code=${code}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I compare response value "/1/code" with <codeOne>
    And I compare response value "/1/message" with <messageOne>
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao           | queryParameter | code      | message                            | codeOne   | messageOne                              |
      | sem query parameter |                | "400.001" | "Field customer-code is required." | "400.001" | "Field customer-site-code is required." |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                    | queryParameter                                 | code      | message                                 |
      | por customer-code            | ?customer-code=${code}                         | "400.001" | "Field customer-site-code is required." |
      | por customer-site-code       | ?customer-site-code=${siteCode}                | "400.001" | "Field customer-code is required."      |
      | por customer-code vazio      | ?customer-site-code=${siteCode}&customer-code= | "400.001" | "Field customer-code is required."      |
      | por customer-site-code vazio | ?customer-code=${code}&customer-site-code=     | "400.001" | "Field customer-site-code is required." |

  @Negativo
  Scenario Outline: Consultar invoice <descricao> em Português
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I compare response value "/1/code" with <codeOne>
    And I compare response value "/1/message" with <messageOne>
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao           | queryParameter | code      | message                                | codeOne   | messageOne                                  |
      | sem query parameter |                | "400.001" | "O campo customer-code é obrigatório." | "400.001" | "O campo customer-site-code é obrigatório." |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                             | queryParameter                                                                                            | code      | message                                        |
      | por customer-code                     | ?customer-code=${code}                                                                                    | "400.001" | "O campo customer-site-code é obrigatório."    |
      | por customer-site-code                | ?customer-site-code=${siteCode}                                                                           | "400.001" | "O campo customer-code é obrigatório."         |
      | por customer-code vazio               | ?customer-site-code=${siteCode}&customer-code=                                                            | "400.001" | "O campo customer-code é obrigatório."         |
      | por customer-site-code vazio          | ?customer-code=${code}&customer-site-code=                                                                | "400.001" | "O campo customer-site-code é obrigatório."    |
      | por begin-date inválido               | ?customer-site-code=${siteCode}&customer-code=${code}&begin-date=${beginDate}1                            | "400.003" | "O campo begin-date tem o formato inválido."   |
      | por begin-date incorreto              | ?customer-site-code=${siteCode}&customer-code=${code}&begin-date=01-01-2020                               | "400.003" | "O campo begin-date tem o formato inválido."   |
      | por begin-date dia inválido           | ?customer-site-code=${siteCode}&customer-code=${code}&begin-date=2020-01-33                               | "400.003" | "O campo begin-date tem o formato inválido."   |
      | por begin-date mes inválido           | ?customer-site-code=${siteCode}&customer-code=${code}&begin-date=2020-13-30                               | "400.003" | "O campo begin-date tem o formato inválido."   |
      | por begin-date ano inválido           | ?customer-site-code=${siteCode}&customer-code=${code}&begin-date=0000-01-30                               | "400.001" | "O campo end-date é obrigatório."              |
      | sem begin-date maior que end-date     | ?customer-site-code=${siteCode}&customer-code=${code}&begin-date=${endDate}&end-date=${beginDate}         | "400.005" | "Intervalo de datas inválido."                 |
      | por end-date inválido                 | ?customer-site-code=${siteCode}&customer-code=${code}&end-date=${beginDate}1                              | "400.003" | "O campo end-date tem o formato inválido."     |
      | por end-date incorreto                | ?customer-site-code=${siteCode}&customer-code=${code}&end-date=01-01-2020                                 | "400.003" | "O campo end-date tem o formato inválido."     |
      | por end-date dia inválido             | ?customer-site-code=${siteCode}&customer-code=${code}&end-date=2020-01-33                                 | "400.003" | "O campo end-date tem o formato inválido."     |
      | por end-date mes inválido             | ?customer-site-code=${siteCode}&customer-code=${code}&end-date=2020-13-30                                 | "400.003" | "O campo end-date tem o formato inválido."     |
      | por end-date ano inválido             | ?customer-site-code=${siteCode}&customer-code=${code}&end-date=0000-01-30                                 | "400.001" | "O campo begin-date é obrigatório."            |
      | por begin-amount inválido             | ?customer-site-code=${siteCode}&customer-code=${code}&begin-amount=${beginDate}1                          | "400.003" | "O campo begin-amount tem o formato inválido." |
      | por begin-amount incorreto            | ?customer-site-code=${siteCode}&customer-code=${code}&begin-amount=0,2020                                 | "400.003" | "O campo begin-amount tem o formato inválido." |
      | por end-amount inválido               | ?customer-site-code=${siteCode}&customer-code=${code}&end-amount=${beginDate}1                            | "400.003" | "O campo end-amount tem o formato inválido."   |
      | por end-amount incorreto              | ?customer-site-code=${siteCode}&customer-code=${code}&end-amount=0,2020                                   | "400.003" | "O campo end-amount tem o formato inválido."   |
      | por begin-amount maior que end-amount | ?customer-site-code=${siteCode}&customer-code=${code}&begin-amount=${endAmount}&end-amount=${beginAmount} | "400.006" | "Intervalo de valor inválido."                 |
      #| por source vazio                      | ?customer-site-code=${siteCode}&customer-code=${code}&source=                                             | "400.001" | "O campo customer-site-code é obrigatório."  |
      | por _limit inválido                   | ?customer-site-code=${siteCode}&customer-code=${code}&_limit=1a                                           | "400.003" | "O campo _limit tem o formato inválido."       |
      | por _limit 0                          | ?customer-site-code=${siteCode}&customer-code=${code}&_limit=0                                            | "400.003" | "O campo _limit tem o formato inválido."       |
      | por _offset inválido                  | ?customer-site-code=${siteCode}&customer-code=${code}&_offset=1a                                          | "400.003" | "O campo _offset tem o formato inválido."      |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/invoiceId" is not empty
    And I verify if response value "/0/organization" is not empty
    And I verify if response value "/0/invoiceNumber" is not empty
    And I verify if response value "/0/series" is not empty
    And I verify if response value "/0/emissionDate" is not empty
    And I verify if response value "/0/accessKey" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/xmlSource" is not empty
    And I verify if response value "/0/totalAmount" is not empty
    And I verify if response value "/0/creationDate" is not empty
    And I verify if response value "/0/updateDate" is not empty
    And I verify if response value "/0/customer/name" is not empty
    And I compare response value "/0/customer/code" with "${code}"
    And I compare response value "/0/customer/siteCode" with "${siteCode}"
    And I verify if response value "/0/customer/document" is not empty
    And I verify if response value "/0/customer/address" is not empty
    And I verify if response value "/0/customer/addressNumber" is not empty
    And I verify if response value "/0/customer/complement" is not empty
    And I verify if response value "/0/customer/county" is not empty
    And I verify if response value "/0/customer/city" is not empty
    And I verify if response value "/0/customer/stateCode" is not empty
    And I verify if response value "/0/customer/zipCode" is not empty
    And I verify if response value "/0/customer/country" is not empty
    And I verify if response value "/0/installments/0/installmentId" is empty
    And I verify if response value "/0/installments/0/installmentNumber" is empty
    And I verify if response value "/0/installments/0/amountOriginal" is empty
    And I verify if response value "/0/installments/0/amountRemaining" is empty
    And I verify if response value "/0/installments/0/dueDate" is empty
    And I verify if response value "/0/installments/0/paymentStatus" is empty
    And I verify if response value "/0/installments/0/typeableLine" is empty
    And Response code must be 200
    And I wait 2 seconds

    Examples: 
      | descricao                              | queryParameter                                                                    |
      | por customer-site-code e customer-code | ?customer-site-code=${siteCode}&customer-code=${code}                             |
      | por begin-date vazio                   | ?customer-site-code=${siteCode}&customer-code=${code}&begin-date=                 |
      | por end-date vazio                     | ?customer-site-code=${siteCode}&customer-code=${code}&end-date=                   |
      | por invoice-number vazio               | ?customer-site-code=${siteCode}&customer-code=${code}&invoice-number=             |
      | por begin-amount vazio                 | ?customer-site-code=${siteCode}&customer-code=${code}&begin-amount=               |
      | por begin-amount                       | ?customer-site-code=${siteCode}&customer-code=${code}&begin-amount=${beginAmount} |
      | por end-amount vazio                   | ?customer-site-code=${siteCode}&customer-code=${code}&end-amount=                 |
      | por end-amount                         | ?customer-site-code=${siteCode}&customer-code=${code}&end-amount=${endAmount}     |
      | por source NFF                         | ?customer-site-code=${siteCode}&customer-code=${code}&source=NFF                  |
      | por source COLETORES                   | ?customer-site-code=${siteCode}&customer-code=${code}&source=COLETORES            |
      | por _limit vazio                       | ?customer-site-code=${siteCode}&customer-code=${code}&_limit=                     |
      | por _limit                             | ?customer-site-code=${siteCode}&customer-code=${code}&_limit=1                    |
      | por _offset vazio                      | ?customer-site-code=${siteCode}&customer-code=${code}&_offset=                    |

  @Positivo
  Scenario Outline: Consultar invoice <descricao> com expand
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/invoiceId" is not empty
    And I verify if response value "/0/organization" is not empty
    And I verify if response value "/0/invoiceNumber" is not empty
    And I verify if response value "/0/series" is not empty
    And I verify if response value "/0/emissionDate" is not empty
    And I verify if response value "/0/accessKey" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/xmlSource" is not empty
    And I verify if response value "/0/totalAmount" is not empty
    And I verify if response value "/0/creationDate" is not empty
    And I verify if response value "/0/updateDate" is not empty
    And I verify if response value "/0/customer/name" is not empty
    And I compare response value "/0/customer/code" with "${code}"
    And I compare response value "/0/customer/siteCode" with "${siteCode}"
    And I verify if response value "/0/customer/document" is not empty
    And I verify if response value "/0/customer/address" is not empty
    And I verify if response value "/0/customer/addressNumber" is not empty
    And I verify if response value "/0/customer/complement" is not empty
    And I verify if response value "/0/customer/county" is not empty
    And I verify if response value "/0/customer/city" is not empty
    And I verify if response value "/0/customer/stateCode" is not empty
    And I verify if response value "/0/customer/zipCode" is not empty
    And I verify if response value "/0/customer/country" is not empty
    #And I verify if response value "/0/installments/0/installmentId" is not empty
    #And I verify if response value "/0/installments/0/installmentNumber" is not empty
    #And I verify if response value "/0/installments/0/amountOriginal" is not empty
    #And I verify if response value "/0/installments/0/amountRemaining" is not empty
    #And I verify if response value "/0/installments/0/dueDate" is not empty
    #And I verify if response value "/0/installments/0/paymentStatus" is not empty
    #And I verify if response value "/0/installments/0/typeableLine" is not empty
    And Response code must be 200

    Examples: 
      | descricao                              | queryParameter                                                                                    |
      | por customer-site-code e customer-code | ?customer-site-code=${siteCode}&customer-code=${code}&_expand=installments                        |
      | por invoice-number                     | ?customer-site-code=${siteCode}&customer-code=${code}&invoice-number=${invoiceNumber}             |
      | por end-date e begin-date              | ?customer-site-code=${siteCode}&customer-code=${code}&end-date=${endDate}&begin-date=${beginDate} |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                        | queryParameter                                                                         |
      | por customer-code incorreto      | ?customer-site-code=${siteCode}&customer-code=101010101                                |
      | por customer-site-code incorreto | ?customer-code=${code}&customer-site-code=213131                                       |
      | por invoice-number inválido      | ?customer-site-code=${siteCode}&customer-code=${code}&invoice-number=${invoiceNumber}1 |
      | por invoice-number incorreto     | ?customer-site-code=${siteCode}&customer-code=${code}&invoice-number=01-01-2020        |
      | por source incorreto             | ?customer-site-code=${siteCode}&customer-code=${code}&source=0.2020                    |
      | por source DEBIT_MEMO            | ?customer-site-code=${siteCode}&customer-code=${code}&source=DEBIT_MEMO                |
      | por _offset 1                    | ?customer-site-code=${siteCode}&customer-code=${code}&_offset=1                        |
