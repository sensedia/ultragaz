Feature: POST_invoices_{invoiceId}_installments_{instId}_bankslips.feature
  Operação responsável por retornar o boleto da parcela.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/industrial"
    And I use api name as "/invoice/v1"
    And I save "/invoices" as "endpointInvoices"
    And I save "16aa22fe-4aef-3900-b559-642745e09178" as "client_id"
    And I save "d24168f6-0b1e-35a3-b607-5c305e7b19f3" as "client_secret"
    And I save "A32E77886035499CE053166114AC2AAE" as "invoiceId"
    And I save "3400413" as "customerCode"
    And I save "7505917" as "customerSiteCode"
    And I save "EMAIL" as "deliveryMethod"
    And I save "wellington.moura@sensedia.com" as "email"
    And I save "A32E79D021E24DE9E053166114ACF8A9" as "installmentId"
    And I save "0030M000023eoLtQAI" as "contactIdSf"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "<variable>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

    Examples: 
      | parameter          | variable      | message                                                                       | language |
      | client_id inválido | ${client_id}1 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | client_id vazio    |               | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<variable>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

    Examples: 
      | parameter             | variable         |
      | access_token inválido | ${access_token}1 |
      | access_token vazio    |                  |

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "<variable>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointInvoices}/<invoiceId>/installments/${installmentId}/bankslips""
    Then I get response body
    And I get response status code
    And I compare response value "/status" with "404"
    And I compare response value "/error" with "Not Found"
    And I compare response value "/message" with "No message available"
    And Response code must be 404
    And I wait 2 seconds

    Examples: 
      | descricao                              | invoiceId    | message                | language |
      | por invoiceId inexistente              | 123131311231 | "No message available" | en-US    |
      | por invoiceId inexistente em portugues | 123131311231 | "No message available" | pt-BR    |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/<installmentId>/bankslips""
    Then I get response body
    And I get response status code
    And I compare response value "/status" with "404"
    And I compare response value "/error" with "Not Found"
    And I compare response value "/message" with "No message available"
    And Response code must be 404
    And I wait 2 seconds

    Examples: 
      | descricao                                  | installmentId                          | message  | language |
      | por installmentId inexistente              | "141f4d7f-d551-47b0-8968-e982a7715546" | mensagem | en-US    |
      | por installmentId inexistente em portugues | "141f4d7f-d551-47b0-8968-e982a7715546" | mensagem | pt-BR    |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                                   | body                                                                                                                                                               | message                                 | code    | language |
      | com customerCode vazio                      | {"contactIdSf":"${contactIdSf}","customerCode": "","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}               | Field customerCode is required.         | 400.001 | en-US    |
      | com customerCode nulo                       | {"contactIdSf":"${contactIdSf}","customerCode": null,"customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}             | Field customerCode is required.         | 400.001 | en-US    |
      | sem parâmetro customerCode                  | {"contactIdSf":"${contactIdSf}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}                                  | Field customerCode is required.         | 400.001 | en-US    |
      | com customerSiteCode nulo                   | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":null,"deliveryMethod":"${deliveryMethod}","email":"${email}"}                  | Field customerSiteCode is required.     | 400.001 | en-US    |
      | sem parâmetro customerSiteCode              | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}                                          | Field customerSiteCode is required.     | 400.001 | en-US    |
      | com deliveryMethod incorreto                | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"INCORRETO","email":"${email}"}         | Invalid request body.                   | 400.000 | en-US    |
      | com deliveryMethod vazio                    | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"","email":"${email}"}                  | Invalid request body.                   | 400.000 | en-US    |
      | com deliveryMethod nulo                     | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":null,"email":"${email}"}                | Field deliveryMethod is required.       | 400.001 | en-US    |
      | sem parâmetro deliveryMethod                | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","email":"${email}"}                                      | Field deliveryMethod is required.       | 400.001 | en-US    |
      | com email incorreto                         | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":${deliveryMethod},"email":"${email}99"} | Invalid request body.                   | 400.000 | en-US    |
      | com email vazio                             | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":${deliveryMethod},"email": ""}          | Invalid request body.                   | 400.000 | en-US    |
      | com email nulo                              | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":${deliveryMethod},"email":null}         | Invalid request body.                   | 400.000 | en-US    |
      | sem parâmetro email                         | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":${deliveryMethod}}                      | Invalid request body.                   | 400.000 | en-US    |
      | com email inválido                          | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"tamanho excedido","deliveryMethod":"${deliveryMethod}","email":"teste"}       | Field email has an invalid format.      | 400.003 | en-US    |
      | com customerCode vazio em portugues         | {"contactIdSf":"${contactIdSf}","customerCode": "","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}               | O campo customerCode é obrigatório.     | 400.001 | pt-BR    |
      | com customerCode nulo em portugues          | {"contactIdSf":"${contactIdSf}","customerCode": null,"customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}             | O campo customerCode é obrigatório.     | 400.001 | pt-BR    |
      | sem parâmetro customerCode em portugues     | {"contactIdSf":"${contactIdSf}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}                                  | O campo customerCode é obrigatório.     | 400.001 | pt-BR    |
      | com customerSiteCode nulo em portugues      | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":null,"deliveryMethod":"${deliveryMethod}","email":"${email}"}                  | O campo customerSiteCode é obrigatório. | 400.001 | pt-BR    |
      | sem parâmetro customerSiteCode em portugues | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}                                          | O campo customerSiteCode é obrigatório. | 400.001 | pt-BR    |
      | com deliveryMethod incorreto em portugues   | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"INCORRETO","email":"${email}"}         | Request body inválido.                  | 400.000 | pt-BR    |
      | com deliveryMethod vazio em portugues       | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"","email":"${email}"}                  | Request body inválido.                  | 400.000 | pt-BR    |
      | com deliveryMethod nulo em portugues        | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":null,"email":"${email}"}                | O campo deliveryMethod é obrigatório.   | 400.001 | pt-BR    |
      | sem parâmetro deliveryMethod em portugues   | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","email":"${email}"}                                      | O campo deliveryMethod é obrigatório.   | 400.001 | pt-BR    |
      | com email incorreto em portugues            | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":${deliveryMethod},"email":"${email}99"} | Request body inválido.                  | 400.000 | pt-BR    |
      | com email vazio em portugues                | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":${deliveryMethod},"email": ""}          | Request body inválido.                  | 400.000 | pt-BR    |
      | com email nulo em portugues                 | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":${deliveryMethod},"email":null}         | Request body inválido.                  | 400.000 | pt-BR    |
      | sem parâmetro email em portugues            | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":${deliveryMethod}}                      | Request body inválido.                  | 400.000 | pt-BR    |
      | com email inválido em portugues             | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"tamanho excedido","deliveryMethod":"${deliveryMethod}","email":"teste"}       | O campo email tem o formato inválido.   | 400.003 | pt-BR    |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422
    And I wait 2 seconds

    Examples: 
      | descricao                                   | body                                                                                                                                                                 | message                    | code    | language |
      | com customerCode inválido                   | {"contactIdSf":"${contactIdSf}","customerCode":"tamanho excedido","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}  | Invalid customerCode.      | 422.001 | en-US    |
      | com customerCode incorreto                  | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}99","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"} | Invalid customerCode.      | 422.001 | en-US    |
      | com customerSiteCode inválido               | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"tamanho excedido","deliveryMethod":"${deliveryMethod}","email":"${email}"}      | Invalid customerSiteCode.  | 422.001 | en-US    |
      | com customerSiteCode incorreto              | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}99","deliveryMethod":"${deliveryMethod}","email":"${email}"} | Invalid customerSiteCode.  | 422.001 | en-US    |
      | com customerSiteCode vazio                  | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"","deliveryMethod":"${deliveryMethod}","email":"${email}"}                      | Invalid customerSiteCode.  | 422.001 | en-US    |
      | com customerCode inválido em portugues      | {"contactIdSf":"${contactIdSf}","customerCode":"tamanho excedido","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}  | customerCode inválido.     | 422.001 | pt-BR    |
      | com customerCode incorreto em portugues     | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}99","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"} | customerCode inválido.     | 422.001 | pt-BR    |
      | com customerSiteCode inválido em portugues  | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"tamanho excedido","deliveryMethod":"${deliveryMethod}","email":"${email}"}      | customerSiteCode inválido. | 422.001 | pt-BR    |
      | com customerSiteCode incorreto em portugues | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}99","deliveryMethod":"${deliveryMethod}","email":"${email}"} | customerSiteCode inválido. | 422.001 | pt-BR    |
      | com customerSiteCode vazio em portugues     | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"","deliveryMethod":"${deliveryMethod}","email":"${email}"}                      | customerSiteCode inválido. | 422.001 | pt-BR    |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And Response code must be 204

    Examples: 
      | descricao                | body                                                                                                                                                                 |
      | por invoiceId válido     | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}   |
      | sem contactIdSf          | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}                                  |
      | com contactIdSf vazio    | {"contactIdSf":"","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}                 |
      | com contactIdSf nulo     | {"contactIdSf":null,"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"}               |
      | com contactIdSf inválido | {"contactIdSf":"${customerCode}1","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","email":"${email}"} |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceId}/installments/${installmentId}/bankslips"
    Then I get response body
    And I get response status code
    And Response code must be 302

    Examples: 
      | descricao                   | body                                                                                                                                     |
      | com deliveryMethod DOWNLOAD | {"contactIdSf":"${contactIdSf}","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"DOWNLOAD"}   |
      | sem contactIdSf             | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"DOWNLOAD"}                                  |
      | com contactIdSf vazio       | {"contactIdSf":"","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"DOWNLOAD"}                 |
      | com contactIdSf nulo        | {"contactIdSf":null,"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"DOWNLOAD"}               |
      | com contactIdSf inválido    | {"contactIdSf":"${customerCode}1","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"DOWNLOAD"} |
