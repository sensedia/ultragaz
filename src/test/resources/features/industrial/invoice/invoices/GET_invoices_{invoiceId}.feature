Feature: GET_invoices_{invoiceId}.feature
  Operação responsável por buscar dados de fatura pelo id.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev"
    And I use api name as "/industrial/invoice/v1"
    And I save "/invoices" as "endpointInvoices"
    And I save "16aa22fe-4aef-3900-b559-642745e09178" as "client_id"
    And I save "d24168f6-0b1e-35a3-b607-5c305e7b19f3" as "client_secret"
    And I save "300644" as "code"
    And I save "401466" as "siteCode"
    And I save "2019-01-01" as "beginDate"
    And I save "2020-12-31" as "endDate"
    And I save "1947350" as "invoiceNumber"
    And I save "100.00" as "beginAmount"
    And I save "1000.00" as "endAmount"
    And I save "963A3ED1FC51804AE053976014AC32AZ" as "invoiceId"
    And I save "963A3ED1FC51804AE053976014AC32AC" as "invoiceIdExpand"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

	@Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

	@Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}1"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
    
  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/<invoiceId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao                 | invoiceId                        |
      | por invoiceId inexistente | 063A3ED1FC56804AE053976014AC36YY |
      | por invoiceId boolean     | true                             |
      | por invoiceId inexistente |                     123131311231 |
      | por invoiceId string      | soundgarden                      |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/invoiceId" is not empty
    And I verify if response value "/organization" is not empty
    And I verify if response value "/invoiceNumber" is not empty
    And I verify if response value "/series" is not empty
    And I verify if response value "/emissionDate" is not empty
    And I verify if response value "/accessKey" is not empty
    And I verify if response value "/status" is not empty
    And I verify if response value "/source" is not empty
    And I verify if response value "/xmlSource" is not empty
    And I verify if response value "/totalAmount" is not empty
    And I verify if response value "/creationDate" is not empty
    And I verify if response value "/updateDate" is not empty
    And I verify if response value "/existsGeneratedInvoice" is not empty
    And I compare response value "/customer/code" with "${code}"
    And I compare response value "/customer/siteCode" with "${siteCode}"
    And I verify if response value "/customer/name" is not empty
    And I verify if response value "/customer/document" is not empty
    And I verify if response value "/customer/address" is not empty
    And I verify if response value "/customer/addressNumber" is not empty
    And I verify if response value "/customer/complement" is not empty
    And I verify if response value "/customer/county" is not empty
    And I verify if response value "/customer/city" is not empty
    And I verify if response value "/customer/stateCode" is not empty
    And I verify if response value "/customer/zipCode" is not empty
    And I verify if response value "/customer/country" is not empty
    And I verify if response value "/installments/0/installmentId" is empty
    And I verify if response value "/installments/0/installmentNumber" is empty
    And I verify if response value "/installments/0/amountOriginal" is empty
    And I verify if response value "/installments/0/amountRemaining" is empty
    And I verify if response value "/installments/0/dueDate" is empty
    And I verify if response value "/installments/0/paymentStatus" is empty
    And I verify if response value "/installments/0/typeableLine" is empty
    And I verify if response value "/installments/0/paymentTypeBoleto" is empty
    And I verify if response value "/installments/0/status" is empty
    And I verify if response value "/installments/0/invoice/invoiceId" is empty
    And I verify if response value "/installments/0/invoice/organization" is empty
    And I verify if response value "/installments/0/invoice/invoiceNumber" is empty
    And I verify if response value "/installments/0/invoice/emissionDate" is empty
    And I verify if response value "/installments/0/invoice/status" is empty
    And I verify if response value "/installments/0/invoice/totalAmount" is empty
    And Response code must be 200

    Examples: 
      | descricao                     | query                     |
      | sem expand                    |                           |
      | com expand installments vazio | ?_expand=installments     |
      | com expand inválido           | ?_expand=smashingPumpkins |

  @Positivo
  Scenario: Consultar invoice com expand installments
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceIdExpand}?_expand=installments"
    Then I get response body
    And I get response status code
    And I verify if response value "/invoiceId" is not empty
    And I verify if response value "/organization" is not empty
    And I verify if response value "/invoiceNumber" is not empty
    And I verify if response value "/series" is not empty
    And I verify if response value "/emissionDate" is not empty
    And I verify if response value "/accessKey" is not empty
    And I verify if response value "/status" is not empty
    And I verify if response value "/xmlSource" is not empty
    And I verify if response value "/totalAmount" is not empty
    And I verify if response value "/creationDate" is not empty
    And I verify if response value "/updateDate" is not empty
    And I verify if response value "/customer/name" is not empty
    And I compare response value "/customer/code" with "${code}"
    And I compare response value "/customer/siteCode" with "${siteCode}"
    And I verify if response value "/customer/document" is not empty
    And I verify if response value "/customer/address" is not empty
    And I verify if response value "/customer/addressNumber" is not empty
    And I verify if response value "/customer/complement" is not empty
    And I verify if response value "/customer/county" is not empty
    And I verify if response value "/customer/city" is not empty
    And I verify if response value "/customer/stateCode" is not empty
    And I verify if response value "/customer/zipCode" is not empty
    And I verify if response value "/customer/country" is not empty
    And I verify if response value "/installments/0/installmentId" is not empty
    And I verify if response value "/installments/0/installmentNumber" is not empty
    And I verify if response value "/installments/0/amountOriginal" is not empty
    And I verify if response value "/installments/0/amountRemaining" is not empty
    And I verify if response value "/installments/0/dueDate" is not empty
    And I verify if response value "/installments/0/paymentStatus" is not empty
    And I verify if response value "/installments/0/typeableLine" is not empty
    And I verify if response value "/installments/0/paymentTypeBoleto" is not empty
    And I verify if response value "/installments/0/status" is not empty
    And I verify if response value "/installments/0/invoice/invoiceId" is empty
    And I verify if response value "/installments/0/invoice/organization" is empty
    And I verify if response value "/installments/0/invoice/invoiceNumber" is empty
    And I verify if response value "/installments/0/invoice/emissionDate" is empty
    And I verify if response value "/installments/0/invoice/status" is empty
    And I verify if response value "/installments/0/invoice/totalAmount" is empty
    And Response code must be 200
