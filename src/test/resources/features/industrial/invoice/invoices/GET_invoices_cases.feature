Feature: GET_invoices_cases.feature
  Consulta para obter dados da fatura.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev"
    And I use api name as "/industrial/invoice/v1"
    And I save "/invoices" as "endpointInvoices"
    And I save "16aa22fe-4aef-3900-b559-642745e09178" as "client_id"
    And I save "d24168f6-0b1e-35a3-b607-5c305e7b19f3" as "client_secret"
    And I save "77b5e2ea-c079-4996-89db-7c90d14ffa36" as "invoiceId"
    And I save "300644" as "customerCode"
    And I save "401466" as "customerSiteCode"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointInvoices}/cases?customer-code=${customerCode}&customer-site-code=${customerSiteCode}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem client_id em Português
    Given I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}/cases?customer-code=${customerCode}&customer-site-code=${customerSiteCode}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointInvoices}/cases?customer-code=${customerCode}&customer-site-code=${customerSiteCode}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido em Português
    Given I set request header "client_id" as "${client_id}1"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}/cases?customer-code=${customerCode}&customer-site-code=${customerSiteCode}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/cases<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I compare response value "/1/code" with <codeOne>
    And I compare response value "/1/message" with <messageOne>
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao           | queryParameter | code      | message                            | codeOne   | messageOne                              |
      | sem query parameter |                | "400.001" | "Field customer-code is required." | "400.001" | "Field customer-site-code is required." |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/cases<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                    | queryParameter                                         | code      | message                                 |
      | por customer-code            | ?customer-code=${customerCode}                         | "400.001" | "Field customer-site-code is required." |
      | por customer-site-code       | ?customer-site-code=${customerSiteCode}                | "400.001" | "Field customer-code is required."      |
      | por customer-code vazio      | ?customer-site-code=${customerSiteCode}&customer-code= | "400.001" | "Field customer-code is required."      |
      | por customer-site-code vazio | ?customer-code=${customerCode}&customer-site-code=     | "400.001" | "Field customer-site-code is required." |

  @Negativo
  Scenario Outline: Consultar invoice <descricao> em Português
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}/cases<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I compare response value "/1/code" with <codeOne>
    And I compare response value "/1/message" with <messageOne>
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao           | queryParameter | code      | message                                | codeOne   | messageOne                                  |
      | sem query parameter |                | "400.001" | "O campo customer-code é obrigatório." | "400.001" | "O campo customer-site-code é obrigatório." |

  @Negativo
  Scenario Outline: Consultar invoice <descricao> em Português
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}/cases<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                    | queryParameter                                         | code      | message                                     |
      | por customer-code            | ?customer-code=${customerCode}                         | "400.001" | "O campo customer-site-code é obrigatório." |
      | por customer-site-code       | ?customer-site-code=${customerSiteCode}                | "400.001" | "O campo customer-code é obrigatório."      |
      | por customer-code vazio      | ?customer-site-code=${customerSiteCode}&customer-code= | "400.001" | "O campo customer-code é obrigatório."      |
      | por customer-site-code vazio | ?customer-code=${customerCode}&customer-site-code=     | "400.001" | "O campo customer-site-code é obrigatório." |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}/cases<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200
    And I wait 2 seconds

    Examples: 
      | descricao                        | queryParameter                                                  | code      | message                                                  |
      | por customer-code incorreto      | ?customer-site-code=${customerSiteCode}&customer-code=101010101 | "400.005" | "Field customer-code must have 8 characters."            |
      | por customer-site-code incorreto | ?customer-code=${customerCode}&customer-site-code=213131        | "400.004" | "Field customer-code or customer-site-code is required." |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/cases<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionId" is not empty
    And I compare response value "/0/customerCode" with "${customerCode}"
    And I compare response value "/0/customerSiteCode" with "${customerSiteCode}"
    And I verify if response value "/0/registryType" is not empty
    And I verify if response value "/0/requestError" is not empty
    And I verify if response value "/0/creationDate" is not empty
    And Response code must be 200
    And I wait 2 seconds

    Examples: 
      | descricao                              | queryParameter                                                        |
      | por customer-site-code e customer-code | ?customer-site-code=${customerSiteCode}&customer-code=${customerCode} |
