Feature: POST_invoices_{invoiceId}.feature
  Operação responsável por retornar a nota fiscal da fatura.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev"
    And I use api name as "/industrial/invoice/v1"
    And I save "/invoices" as "endpointInvoices"
    And I save "16aa22fe-4aef-3900-b559-642745e09178" as "client_id"
    And I save "d24168f6-0b1e-35a3-b607-5c305e7b19f3" as "client_secret"
    And I save "963A3ED1FC51804AE053976014AC32AZ" as "invoiceIdNFF"
    And I save "A4FD77B4CDEE2A7DE053166114AC9927" as "invoiceIdColetores"
    And I save "A32E77886035499CE053166114AC2AAE" as "invoiceIdDanfe"
    And I save "300644" as "customerCode"
    And I save "401466" as "customerSiteCode"
    And I save "3563875" as "customerCodeColetores"
    And I save "7628381" as "customerSiteCodeColetores"
    And I save "XML" as "documentType"
    And I save "DOWNLOAD" as "deliveryMethod"
    And I save "0030M000023eoLtQAI" as "contactIdSf"
    And I save "wellington.moura@sensedia.com" as "email"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request body as "{"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceIdNFF}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request body as "{"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceIdNFF}"
    Then I get response body
    And I get response status code
    And Response body must be "<message>"
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                  |
      | com client_id vazio       | ""              | "${access_token}"  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com access_token vazio    | "${client_id}"  | ""                 | Could not find a required Access Token in the request, identified by HEADER access_token |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | Could not find a required Access Token in the request, identified by HEADER access_token |

  @Negativo
  Scenario Outline: Cadastrar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    And I set request header "Content-type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceIdNFF}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And I compare response value "/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao                           | body                                                                                                                                                                              | code      | message                      | language |
      | com customerCode inválido           | {"customerCode":"${customerCode}1","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"} | "422.001" | "Invalid customerCode."      | en-US    |
      | com customerSiteCode vazio          | {"customerCode":"${customerCode}","customerSiteCode":"","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                     | "422.001" | "Invalid customerSiteCode."  | en-US    |
      | com customerSiteCode inválido       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}1","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"} | "422.001" | "Invalid customerSiteCode."  | en-US    |
      | com customerCode inválido em pt     | {"customerCode":"${customerCode}1","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"} | "422.001" | "customerCode inválido."     | pt-BR    |
      | com customerSiteCode vazio em pt    | {"customerCode":"${customerCode}","customerSiteCode":"","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                     | "422.001" | "customerSiteCode inválido." | pt-BR    |
      | com customerSiteCode inválido em pt | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}1","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"} | "422.001" | "customerSiteCode inválido." | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    And I set request header "Content-type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceIdNFF}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                         | body                                                                                                                                                                              | code      | message                                   | language |
      | com customerCode vazio            | {"customerCode":"","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                 | "400.001" | "Field customerCode is required."         | en-US    |
      | com customerCode nulo             | {"customerCode":null,"customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}               | "400.001" | "Field customerCode is required."         | en-US    |
      | sem customerCode                  | {"customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                                   | "400.001" | "Field customerCode is required."         | en-US    |
      | com customerSiteCode nulo         | {"customerCode":"${customerCode}","customerSiteCode":null,"documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                   | "400.001" | "Field customerSiteCode is required."     | en-US    |
      | sem customerSiteCode              | {"customerCode":"${customerCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                                           | "400.001" | "Field customerSiteCode is required."     | en-US    |
      | com documentType vazio            | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                 | "400.000" | "Invalid request body."                   | en-US    |
      | com documentType nulo             | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":null,"deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}               | "400.001" | "Field documentType is required."         | en-US    |
      | com documentType inválido         | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}1","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"} | "400.000" | "Invalid request body."                   | en-US    |
      | sem documentType                  | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                                   | "400.001" | "Field documentType is required."         | en-US    |
      | com email vazio                   | {"email":"","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","contactIdSf":"${contactIdSf}"}   | "400.001" | "Field email is required."                | en-US    |
      | com email nulo                    | {"email":null,"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","contactIdSf":"${contactIdSf}"} | "400.001" | "Field email is required."                | en-US    |
      | com email inválido                | {"email":"a","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","contactIdSf":"${contactIdSf}"}  | "400.003" | "Field email has an invalid format."      | en-US    |
      | sem email                         | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","contactIdSf":"${contactIdSf}"}              | "400.001" | "Field email is required."                | en-US    |
      | com deliveryMethod vazio          | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"","contactIdSf":"${contactIdSf}"}                   | "400.000" | "Invalid request body."                   | en-US    |
      | com deliveryMethod nulo           | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":null,"contactIdSf":"${contactIdSf}"}                 | "400.001" | "Field deliveryMethod is required."       | en-US    |
      | com deliveryMethod inválido       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}1","contactIdSf":"${contactIdSf}"} | "400.000" | "Invalid request body."                   | en-US    |
      | sem deliveryMethod                | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","contactIdSf":"${contactIdSf}"}                                       | "400.001" | "Field deliveryMethod is required."       | en-US    |
      | com customerCode vazio em pt      | {"customerCode":"","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                 | "400.001" | "O campo customerCode é obrigatório."     | pt-BR    |
      | com customerCode nulo em pt       | {"customerCode":null,"customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}               | "400.001" | "O campo customerCode é obrigatório."     | pt-BR    |
      | sem customerCode em pt            | {"customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                                   | "400.001" | "O campo customerCode é obrigatório."     | pt-BR    |
      | com customerSiteCode nulo em pt   | {"customerCode":"${customerCode}","customerSiteCode":null,"documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                   | "400.001" | "O campo customerSiteCode é obrigatório." | pt-BR    |
      | sem customerSiteCode em pt        | {"customerCode":"${customerCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                                           | "400.001" | "O campo customerSiteCode é obrigatório." | pt-BR    |
      | com documentType vazio em pt      | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                 | "400.000" | "Request body inválido."                  | pt-BR    |
      | com documentType nulo em pt       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":null,"deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}               | "400.001" | "O campo documentType é obrigatório."     | pt-BR    |
      | com documentType inválido em pt   | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}1","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"} | "400.000" | "Request body inválido."                  | pt-BR    |
      | sem documentType em pt            | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}                                   | "400.001" | "O campo documentType é obrigatório."     | pt-BR    |
      | com deliveryMethod vazio em pt    | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"","contactIdSf":"${contactIdSf}"}                   | "400.000" | "Request body inválido."                  | pt-BR    |
      | com deliveryMethod nulo em pt     | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":null,"contactIdSf":"${contactIdSf}"}                 | "400.001" | "O campo deliveryMethod é obrigatório."   | pt-BR    |
      | com deliveryMethod inválido em pt | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}1","contactIdSf":"${contactIdSf}"} | "400.000" | "Request body inválido."                  | pt-BR    |
      | sem deliveryMethod em pt          | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","contactIdSf":"${contactIdSf}"}                                       | "400.001" | "O campo deliveryMethod é obrigatório."   | pt-BR    |
      | com email vazio em pt             | {"email":"","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","contactIdSf":"${contactIdSf}"}   | "400.001" | "O campo email é obrigatório."            | pt-BR    |
      | com email nulo em pt              | {"email":null,"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","contactIdSf":"${contactIdSf}"} | "400.001" | "O campo email é obrigatório."            | pt-BR    |
      | com email inválido em pt          | {"email":"a","customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","contactIdSf":"${contactIdSf}"}  | "400.003" | "O campo email tem o formato inválido."   | pt-BR    |
      | sem email em pt                   | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","contactIdSf":"${contactIdSf}"}              | "400.001" | "O campo email é obrigatório."            | pt-BR    |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/${invoiceIdNFF}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao | body |

  @Positivo
  Scenario Outline: Cadastrar invoice com deliveryMethod DOWNLOAD e <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/<invoiceId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 302

    Examples: 
      | descricao            | invoiceId             | body                                                                                                                                                                              |
      | documentType XML     | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"XML","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}"}              |
      | contactIdSf inválido | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":"${contactIdSf}1"} |
      | contactIdSf vazio    | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":""}                |
      | contactIdSf nulo     | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}","contactIdSf":null}              |
      | contactIdSf          | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"${deliveryMethod}"}                                 |
      | source COLETORES     | ${invoiceIdColetores} | {"customerCode":"${customerCodeColetores}","customerSiteCode":"${customerSiteCodeColetores}","documentType":"XML","deliveryMethod":"${deliveryMethod}"}                           |

  @Positivo
  Scenario Outline: Cadastrar invoice com deliveryMethod EMAIL e <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointInvoices}/<invoiceId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao            | invoiceId             | body                                                                                                                                                                                          |
      | documentType DANFE   | ${invoiceIdDanfe}     | {"customerCode":"3400413","customerSiteCode":"7505917","documentType":"DANFE","deliveryMethod":"EMAIL","email":"${email}","contactIdSf":"${contactIdSf}"}                                     |
      | documentType XML     | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"XML","deliveryMethod":"EMAIL","email":"${email}","contactIdSf":"${contactIdSf}"}                   |
      | source COLETORES     | ${invoiceIdColetores} | {"customerCode":"${customerCodeColetores}","customerSiteCode":"${customerSiteCodeColetores}","documentType":"XML","deliveryMethod":"EMAIL","email":"${email}","contactIdSf":"${contactIdSf}"} |
      | contactIdSf inválido | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","email":"${email}","contactIdSf":"${contactIdSf}1"}      |
      | contactIdSf vazio    | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","email":"${email}","contactIdSf":""}                     |
      | contactIdSf nulo     | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","email":"${email}","contactIdSf":null}                   |
      | sem contactIdSf      | ${invoiceIdNFF}       | {"customerCode":"${customerCode}","customerSiteCode":"${customerSiteCode}","documentType":"${documentType}","deliveryMethod":"EMAIL","email":"${email}"}                                      |
