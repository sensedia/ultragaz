Feature: GET_installments.feature
  Consulta para obter dados das parcelas da fatura.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev"
    And I use api name as "/industrial/invoice/v1"
    And I save "/installments" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "2019-01-01" as "beginDate"
    And I save "2020-12-31" as "endDate"
    And I save "81047132" as "invoiceNumber"
    And I save "PAGO" as "status"
    And I save "3400413" as "code"
    And I save "7505917" as "siteCode"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401
	
	@Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
	
	@Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar installments <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                               | queryParameter                               | code      | message                                      | language |
      | por begin-date inválido                 | ?begin-date=${beginDate}1                    | "400.003" | "Field begin-date has an invalid format."    | en-US    |
      | por begin-date incorreto                | ?begin-date=01-01-2020                       | "400.003" | "Field begin-date has an invalid format."    | en-US    |
      | por begin-date dia inválido             | ?begin-date=2020-01-33                       | "400.003" | "Field begin-date has an invalid format."    | en-US    |
      | por begin-date mes inválido             | ?begin-date=2020-13-30                       | "400.003" | "Field begin-date has an invalid format."    | en-US    |
      | por begin-date ano inválido             | ?begin-date=000-01-30                        | "400.003" | "Field begin-date has an invalid format."    | en-US    |
      | sem begin-date maior que end-date       | ?begin-date=${endDate}&end-date=${beginDate} | "400.005" | "Invalid date range."                        | en-US    |
      | por end-date inválido                   | ?end-date=${beginDate}1                      | "400.003" | "Field end-date has an invalid format."      | en-US    |
      | por end-date incorreto                  | ?end-date=01-01-2020                         | "400.003" | "Field end-date has an invalid format."      | en-US    |
      | por end-date dia inválido               | ?end-date=2020-01-33                         | "400.003" | "Field end-date has an invalid format."      | en-US    |
      | por end-date mes inválido               | ?end-date=2020-13-30                         | "400.003" | "Field end-date has an invalid format."      | en-US    |
      | por end-date ano inválido               | ?end-date=000-01-30                          | "400.003" | "Field end-date has an invalid format."      | en-US    |
      | por _limit inválido                     | ?_limit=1a                                   | "400.003" | "Field _limit has an invalid format."        | en-US    |
      | por _limit 0                            | ?_limit=0                                    | "400.003" | "Field limit has an invalid format."         | en-US    |
      | por _offset inválido                    | ?_offset=1a                                  | "400.003" | "Field _offset has an invalid format."       | en-US    |
      | por status inválido                     | ?status=PAGOS                                | "400.003" | "Field status has an invalid format."        | en-US    |
      | por status 0                            | ?status=0                                    | "400.003" | "Field status has an invalid format."        | en-US    |
      | por begin-date inválido em pt           | ?begin-date=${beginDate}1                    | "400.003" | "O campo begin-date tem o formato inválido." | pt-BR    |
      | por begin-date incorreto em pt          | ?begin-date=01-01-2020                       | "400.003" | "O campo begin-date tem o formato inválido." | pt-BR    |
      | por begin-date dia inválido em pt       | ?begin-date=2020-01-33                       | "400.003" | "O campo begin-date tem o formato inválido." | pt-BR    |
      | por begin-date mes inválido em pt       | ?begin-date=2020-13-30                       | "400.003" | "O campo begin-date tem o formato inválido." | pt-BR    |
      | por begin-date ano inválido em pt       | ?begin-date=000-01-30                        | "400.003" | "O campo begin-date tem o formato inválido." | pt-BR    |
      | sem begin-date maior que end-date em pt | ?begin-date=${endDate}&end-date=${beginDate} | "400.005" | "Intervalo de datas inválido."               | pt-BR    |
      | por end-date inválido em pt             | ?end-date=${beginDate}1                      | "400.003" | "O campo end-date tem o formato inválido."   | pt-BR    |
      | por end-date incorreto em pt            | ?end-date=01-01-2020                         | "400.003" | "O campo end-date tem o formato inválido."   | pt-BR    |
      | por end-date dia inválido em pt         | ?end-date=2020-01-33                         | "400.003" | "O campo end-date tem o formato inválido."   | pt-BR    |
      | por end-date mes inválido em pt         | ?end-date=2020-13-30                         | "400.003" | "O campo end-date tem o formato inválido."   | pt-BR    |
      | por end-date ano inválido em pt         | ?end-date=000-01-30                          | "400.003" | "O campo end-date tem o formato inválido."   | pt-BR    |
      | por _limit inválido em pt               | ?_limit=1a                                   | "400.003" | "O campo _limit tem o formato inválido."     | pt-BR    |
      | por _limit 0 em pt                      | ?_limit=0                                    | "400.003" | "O campo limit tem o formato inválido."      | pt-BR    |
      | por _offset inválido em pt              | ?_offset=1a                                  | "400.003" | "O campo _offset tem o formato inválido."    | pt-BR    |
      | por status inválido em pt               | ?status=PAGOS                                | "400.003" | "O campo status tem o formato inválido."     | pt-BR    |
      | por status 0 em pt                      | ?status=0                                    | "400.003" | "O campo status tem o formato inválido."     | pt-BR    |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/installmentId" is not empty
    And I verify if response value "/0/installmentNumber" is not empty
    And I verify if response value "/0/amountOriginal" is not empty
    And I verify if response value "/0/dueDate" is not empty
    And I verify if response value "/0/paymentStatus" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/typeableLine" is not empty
    And I verify if response value "/1/installmentId" is empty
    And I verify if response value "/1/installmentNumber" is empty
    And I verify if response value "/1/amountOriginal" is empty
    And I verify if response value "/1/dueDate" is empty
    And I verify if response value "/1/paymentStatus" is empty
    And I verify if response value "/1/status" is empty
    And I verify if response value "/1/typeableLine" is empty
    And Response code must be 200

    Examples: 
      | descricao  | queryParameter |
      | por _limit | ?_limit=1      |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/installmentId" is not empty
    And I verify if response value "/0/installmentNumber" is not empty
    And I verify if response value "/0/amountOriginal" is not empty
    And I verify if response value "/0/dueDate" is not empty
    And I verify if response value "/0/paymentStatus" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/typeableLine" is not empty
    And I verify if response value "/0/invoice/invoiceId" is empty
    And I verify if response value "/0/invoice/organization" is empty
    And I verify if response value "/0/invoice/invoiceNumber" is empty
    And I verify if response value "/0/invoice/emissionDate" is empty
    And I verify if response value "/0/invoice/status" is empty
    And I verify if response value "/0/invoice/totalAmount" is empty
    And Response code must be 200

    Examples: 
      | descricao                    | queryParameter       |
      | por begin-date vazio         | ?begin-date=         |
      | por end-date vazio           | ?end-date=           |
      | por invoice-number vazio     | ?invoice-number=     |
      | por status vazio             | ?status=             |
      | por _expand vazio            | ?_expand=            |
      | por _expand 1                | ?_expand=1           |
      | por _expand inválido         | ?_expand=invoices    |
      | por _limit vazio             | ?_limit=             |
      | por _offset vazio            | ?_offset=            |
      | por _offset 1                | ?_offset=1           |
      | por customer-code vazio      | ?customer-code=      |
      | por customer-site-code vazio | ?customer-site-code= |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/installmentId" is not empty
    And I verify if response value "/0/installmentNumber" is not empty
    And I verify if response value "/0/amountOriginal" is not empty
    And I verify if response value "/0/dueDate" is not empty
    And I verify if response value "/0/paymentStatus" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/typeableLine" is not empty
    And I verify if response value "/0/invoice/invoiceId" is empty
    And I verify if response value "/0/invoice/organization" is empty
    And I verify if response value "/0/invoice/invoiceNumber" is empty
    And I verify if response value "/0/invoice/emissionDate" is empty
    And I verify if response value "/0/invoice/status" is empty
    And I verify if response value "/0/invoice/totalAmount" is empty
    And Response code must be 200

    Examples: 
      | descricao                 | queryParameter                               |
      | por status A_VENCER       | ?status=A_VENCER                             |
      | por status VENCIDO        | ?status=VENCIDO                              |
      | por end-date e begin-date | ?end-date=${endDate}&begin-date=${beginDate} |
      | sem query parameter       |                                              |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/installmentId" is not empty
    And I verify if response value "/0/installmentNumber" is not empty
    And I verify if response value "/0/amountOriginal" is not empty
    And I verify if response value "/0/dueDate" is not empty
    And I verify if response value "/0/paymentStatus" is not empty
    And I compare response value "/0/status" with "PAGO"
    And I verify if response value "/0/typeableLine" is empty
    And I verify if response value "/0/invoice/invoiceId" is empty
    And I verify if response value "/0/invoice/organization" is empty
    And I verify if response value "/0/invoice/invoiceNumber" is empty
    And I verify if response value "/0/invoice/emissionDate" is empty
    And I verify if response value "/0/invoice/status" is empty
    And I verify if response value "/0/invoice/totalAmount" is empty
    And Response code must be 200

    Examples: 
      | descricao          | queryParameter                   |
      | por invoice-number | ?invoice-number=${invoiceNumber} |
      | por status PAGO    | ?status=PAGO                     |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/installmentId" is not empty
    And I verify if response value "/0/installmentNumber" is not empty
    And I verify if response value "/0/amountOriginal" is not empty
    And I verify if response value "/0/amountRemaining" is not empty
    And I verify if response value "/0/dueDate" is not empty
    And I verify if response value "/0/paymentStatus" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/typeableLine" is not empty
    And I verify if response value "/0/paymentTypeBoleto" is not empty
    And I verify if response value "/0/invoice/invoiceId" is not empty
    And I verify if response value "/0/invoice/organization" is not empty
    And I verify if response value "/0/invoice/invoiceNumber" is not empty
    And I verify if response value "/0/invoice/emissionDate" is not empty
    And I verify if response value "/0/invoice/status" is not empty
    And I verify if response value "/0/invoice/totalAmount" is not empty
    And Response code must be 200

    Examples: 
      | descricao                              | queryParameter                                                        |
      | por _expand                            | ?_expand=invoice                                                      |
      | por customer-code                      | ?customer-code=${code}&_expand=invoice                                |
      | por customer-site-code                 | ?customer-site-code=${siteCode}&_expand=invoice                       |
      | por customer-code e customer-site-code | ?customer-code=${code}&customer-site-code=${siteCode}&_expand=invoice |
      | por invoice-number                     | ?invoice_number=81222470&_expand=invoice                                      |

  @Negativo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                       | queryParameter                    |
      | por invoice-number inválido     | ?invoice-number=${invoiceNumber}1 |
      | por invoice-number incorreto    | ?invoice-number=01-01-2020        |
      | por customer-code 1             | ?customer-code=1                  |
      | por customer-code inválido      | ?customer-code=invoices           |
      | por customer-site-code 1        | ?customer-site-code=1             |
      | por customer-site-code inválido | ?customer-site-code=invoices      |
