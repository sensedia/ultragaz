Feature: GET_ratings.feature
  Operação responsável por consultar as avaliações dos usuários.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use api name as "/dev/rating/v1"
    And I save "/ratings" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "28efb8dc-c3f1-49f7-a244-f8a2cd2fb46" as "userId"
    And I save "Gostei do aplicativo/serviço" as "justification"

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?user-id=${userId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar uma avaliação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                          | query            | code      | message                                           | language |
      | por _limit 0                       | ?_limit=0        | "400.005" | "Field _limit has an invalid value or is empty."  | "en-US"  |
      | por _limit 0 pt                    | ?_limit=0        | "400.005" | "Campo _limit tem um valor inválido ou vazio."    | "pt-BR"  |
      | por _limit -1                      | ?_limit=-1       | "400.005" | "Field _limit has an invalid value or is empty."  | "en-US"  |
      | por _limit -1 pt                   | ?_limit=-1       | "400.005" | "Campo _limit tem um valor inválido ou vazio."    | "pt-BR"  |
      | por _limit string                  | ?_limit=a        | "400.003" | "Field _limit has an invalid format."             | "en-US"  |
      | por _limit string pt               | ?_limit=a        | "400.003" | "Campo _limit tem um formato inválido."           | "pt-BR"  |
      | por _offset string                 | ?_offset=a       | "400.003" | "Field _offset has an invalid format."            | "en-US"  |
      | por _offset string pt              | ?_offset=a       | "400.003" | "Campo _offset tem um formato inválido."          | "pt-BR"  |
      | por _offset -1                     | ?_offset=-1      | "400.005" | "Field _offset has an invalid value or is empty." | "en-US"  |
      | por _offset -1 pt                  | ?_offset=-1      | "400.005" | "Campo _offset tem um valor inválido ou vazio."   | "pt-BR"  |
      | por department inválido string     | ?department=MAPS | "400.003" | "Field department has an invalid format."         | "en-US"  |
      | por department inválido string pt  | ?department=MAPS | "400.003" | "Campo department tem um formato inválido."       | "pt-BR"  |
      | por department inválido inteiro    | ?department=-1   | "400.003" | "Field department has an invalid format."         | "en-US"  |
      | por department inválido inteiro pt | ?department=-1   | "400.003" | "Campo department tem um formato inválido."       | "pt-BR"  |

  @Positivo
  Scenario Outline: Consultar uma avaliação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao            | query         |
      | por userId incorreto | ?user-id=666  |
      | por _offset 1000     | ?_offset=1000 |

  @Positivo
  Scenario Outline: Consultar uma avaliação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/userId" is not empty
    And I verify if response value "/0/rating" is not empty
    And I verify if response value "/0/justification" is not empty
    And I verify if response value "/0/department" is not empty
    And I verify if response value "/0/creationDate" is not empty
    And I verify if response value "/0/commercialConsultant/idSalesforce" is not empty
    And I verify if response value "/0/commercialConsultant/name" is not empty
    And Response code must be 200

    Examples: 
      | descricao                            | query                                                |
      | por user-id                          | ?user-id=${userId}                                   |
      | com query completa                   | ?user-id=${userId}&_limit=1&_offset=0&department=MAP |
      | por department MAP                   | ?user-id=${userId}&department=MAP                    |
      | por department COMMERCIAL_CONSULTANT | ?user-id=${userId}&department=COMMERCIAL_CONSULTANT  |

  @Positivo
  Scenario Outline: Consultar uma avaliação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/userId" is not empty
    And I verify if response value "/0/rating" is not empty
    And I verify if response value "/0/justification" is not empty
    And I verify if response value "/0/department" is not empty
    And I verify if response value "/0/creationDate" is not empty
    And I verify if response value "/0/commercialConsultant/idSalesforce" is empty
    And I verify if response value "/0/commercialConsultant/name" is empty
    And Response code must be 200

    Examples: 
      | descricao                            | query                             |
      | sem query                            |                                   |
      | por _limit 1                         | ?_limit=1                         |
      | por _limit 11                        | ?_limit=11                        |
      | por _offset 0                        | ?_offset=0                        |
      | por _offset 1                        | ?_offset=1                        |
      | por department MAP                   | ?department=MAP                   |
