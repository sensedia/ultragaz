Feature: POST_ratings.feature
  Operação responsável por registrar uma avaliação.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use api name as "/dev/rating/v1"
    And I save "/ratings" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "28efb8dc-c3f1-49f7-a244-f8a2cd2fb46" as "userId"
    And I save "Gostei do aplicativo/serviço" as "justification"
    And I save "A successful SQL injection attack can result in unauthorized access to sensitive data, such as passwords," as "strp1"
    And I save "credit card details, or personal user information. Many high-profile data breaches in recent years have been the result" as "strp2"
    And I save "of SQL injection attacks, leading to reputational damage and regulatory fines." as "strp3"
    And I save "Gostei do aplicativo/serviço" as "justification"
    And System generate random number

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"userId":"${userId}","rating":5,"justification":"${justification}","department":"MAP"}"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Registrar uma avaliação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                  | body                                                                                                                                                                  | message                                                                     | code      | language |
      | sem userId                 | "{"rating":5,"justification":"${justification}","department":"MAP"}"                                                                                                  | "Field userId is required."                                                 | "400.001" | "en-US"  |
      | com userId vazio           | "{"userId":"","rating":5,"justification":"${justification}","department":"MAP"}"                                                                                      | "Field userId is required."                                                 | "400.001" | "en-US"  |
      | com userId nulo            | "{"userId":null,"rating":5,"justification":"${justification}","department":"MAP"}"                                                                                    | "Field userId is required."                                                 | "400.001" | "en-US"  |
      | sem rating                 | "{"userId":"${userId}","justification":"${justification}","department":"MAP"}"                                                                                        | "Field rating is required."                                                 | "400.001" | "en-US"  |
      | com rating vazio           | "{"userId":"${userId}","rating":"","justification":"${justification}","department":"MAP"}"                                                                            | "Field rating is required."                                                 | "400.001" | "en-US"  |
      | com rating nulo            | "{"userId":"${userId}","rating":null,"justification":"${justification}","department":"MAP"}"                                                                          | "Field rating is required."                                                 | "400.001" | "en-US"  |
      | com rating igual a 0       | "{"userId":"${userId}","rating":0,"justification":"${justification}","department":"MAP"}"                                                                             | "Field rating has an invalid value."                                        | "400.004" | "en-US"  |
      | com rating igual a 6       | "{"userId":"${userId}","rating":6,"justification":"${justification}","department":"MAP"}"                                                                             | "Field rating has an invalid value."                                        | "400.004" | "en-US"  |
      | com rating igual a -1      | "{"userId":"${userId}","rating":-1,"justification":"${justification}","department":"MAP"}"                                                                            | "Field rating has an invalid value."                                        | "400.004" | "en-US"  |
      | com rating string          | "{"userId":"${userId}","rating":"true","justification":"${justification}","department":"MAP"}"                                                                        | "Invalid request body."                                                     | "400.000" | "en-US"  |
      | com rating boolean         | "{"userId":"${userId}","rating":true,"justification":"${justification}","department":"MAP"}"                                                                          | "Invalid request body."                                                     | "400.000" | "en-US"  |
      | com justification > 250    | "{"userId":"${userId}","rating":4,"justification":"${strp1} ${strp2} ${strp3}","department":"MAP"}"                                                                   | "Field justification must have up to 250 characters."                       | "400.002" | "en-US"  |
      | com department inválido    | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":"MAPs"}"                                                                            | "Field department has an invalid value."                                    | "400.004" | "en-US"  |
      | sem department             | "{"userId":"${userId}","rating":5,"justification":"${justification}"}"                                                                                                | "Field department has an invalid value."                                    | "400.004" | "en-US"  |
      | com department vazio       | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":""}"                                                                                | "Field department has an invalid value."                                    | "400.004" | "en-US"  |
      | com department nulo        | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":null}"                                                                              | "Field department has an invalid value."                                    | "400.004" | "en-US"  |
      | com idSalesforce > 50      | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":"MAP","commercialConsultant":{"idSalesforce":"${strp3}${random}","name":"string"}}" | "Field commercialConsultant.idSalesforce must have up to 50 characters."    | "400.002" | "en-US"  |
      | com name > 50              | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":"MAP","commercialConsultant":{"idSalesforce":"string${random}","name":"${strp3}"}}" | "Field commercialConsultant.name must have up to 50 characters."            | "400.002" | "en-US"  |
      | sem userId pt              | "{"rating":5,"justification":"${justification}","department":"MAP"}"                                                                                                  | "Campo userId é obrigatório."                                               | "400.001" | "pt-BR"  |
      | com userId vazio pt        | "{"userId":"","rating":5,"justification":"${justification}","department":"MAP"}"                                                                                      | "Campo userId é obrigatório."                                               | "400.001" | "pt-BR"  |
      | com userId nulo pt         | "{"userId":null,"rating":5,"justification":"${justification}","department":"MAP"}"                                                                                    | "Campo userId é obrigatório."                                               | "400.001" | "pt-BR"  |
      | sem rating pt              | "{"userId":"${userId}","justification":"${justification}","department":"MAP"}"                                                                                        | "Campo rating é obrigatório."                                               | "400.001" | "pt-BR"  |
      | com rating vazio pt        | "{"userId":"${userId}","rating":"","justification":"${justification}","department":"MAP"}"                                                                            | "Campo rating é obrigatório."                                               | "400.001" | "pt-BR"  |
      | com rating nulo pt         | "{"userId":"${userId}","rating":null,"justification":"${justification}","department":"MAP"}"                                                                          | "Campo rating é obrigatório."                                               | "400.001" | "pt-BR"  |
      | com rating igual a 0 pt    | "{"userId":"${userId}","rating":0,"justification":"${justification}","department":"MAP"}"                                                                             | "Campo rating tem um valor inválido."                                       | "400.004" | "pt-BR"  |
      | com rating igual a 6 pt    | "{"userId":"${userId}","rating":6,"justification":"${justification}","department":"MAP"}"                                                                             | "Campo rating tem um valor inválido."                                       | "400.004" | "pt-BR"  |
      | com rating igual a -1 pt   | "{"userId":"${userId}","rating":-1,"justification":"${justification}","department":"MAP"}"                                                                            | "Campo rating tem um valor inválido."                                       | "400.004" | "pt-BR"  |
      | com rating string pt       | "{"userId":"${userId}","rating":"true","justification":"${justification}","department":"MAP"}"                                                                        | "Requisição inválida."                                                      | "400.000" | "pt-BR"  |
      | com rating boolean pt      | "{"userId":"${userId}","rating":true,"justification":"${justification}","department":"MAP"}"                                                                          | "Requisição inválida."                                                      | "400.000" | "pt-BR"  |
      | com justification > 250 pt | "{"userId":"${userId}","rating":4,"justification":"${strp1} ${strp2} ${strp3}","department":"MAP"}"                                                                   | "Campo justification deve ter no máximo 250 caracteres."                    | "400.002" | "pt-BR"  |
      | com department inválido pt | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":"MAPs"}"                                                                            | "Campo department tem um valor inválido."                                   | "400.004" | "pt-BR"  |
      | sem department pt          | "{"userId":"${userId}","rating":5,"justification":"${justification}"}"                                                                                                | "Campo department tem um valor inválido."                                   | "400.004" | "pt-BR"  |
      | com department vazio pt    | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":""}"                                                                                | "Campo department tem um valor inválido."                                   | "400.004" | "pt-BR"  |
      | com department nulo pt     | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":null}"                                                                              | "Campo department tem um valor inválido."                                   | "400.004" | "pt-BR"  |
      | com idSalesforce > 50 pt   | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":"MAP","commercialConsultant":{"idSalesforce":"${strp3}${random}","name":"string"}}" | "Campo commercialConsultant.idSalesforce deve ter no máximo 50 caracteres." | "400.002" | "pt-BR"  |
      | com name > 50 pt           | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":"MAP","commercialConsultant":{"idSalesforce":"string${random}","name":"${strp3}"}}" | "Campo commercialConsultant.name deve ter no máximo 50 caracteres."         | "400.002" | "pt-BR"  |

  @Positivo
  Scenario Outline: Registrar uma avaliação <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I save response header "location" as "location"
    And I save final value of header "location" as "ratingId"
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | descricao               | body                                                                                      |
      | com todos os campos     | "{"userId":"${userId}","rating":5,"justification":"${justification}","department":"MAP"}" |
      | sem justification       | "{"userId":"${userId}","rating":5,"department":"MAP"}"                                    |
      | com justification vazio | "{"userId":"${userId}","rating":5,"justification":"","department":"MAP"}"                 |
      | com justification nulo  | "{"userId":"${userId}","rating":5,"justification":null,"department":"MAP"}"               |
