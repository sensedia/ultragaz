@GET_distributors
Feature: GET_distributors.feature
  Operação responsável por retornar os distribuidores de vale gás.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "83931550000232" as "document"
    And I save "I" as "status"
    And I save "20" as "_limit"
    And I save "0" as "_offset"
    And I save "596913" as "customerId"
    And I save "655881" as "customerSiteIdMatriz"
    And I save "655860" as "customerSiteIdFilial"

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I use api name as "/voucher/v1"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteIdMatriz}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use api name as "/voucher-backoffice/v1"
    And I save "/distributors" as "endpoint"

  @Negativo
  Scenario: Enviar requisição sem client_id
   	Given I set request header "access_token" as "${access_token}"
    And I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar distribuidor com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/0/message" with "<message>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                       | queryParameter             | code    | message                                                   | language |
      | customer-id inválido            | ?customer-id=inválido      | 400.004 | Field customer-id has an invalid format or value.         | en-US    |
      | customer-site-id inválido       | ?customer-site-id=inválido | 400.004 | Field customer-site-id has an invalid format or value.    | en-US    |
      | _offset inválido                | ?_offset=a                 | 400.004 | Field _offset has an invalid format or value.             | en-US    |
      | _offset incorreto               | ?_offset=-1                | 400.005 | Field _offset cannot be negative.                         | en-US    |
      | _limit inválido                 | ?_limit=a                  | 400.004 | Field _limit has an invalid format or value.              | en-US    |
      | _limit incorreto                | ?_limit=-1                 | 400.009 | Field _limit must not be empty.                           | en-US    |
      | _limit 0                        | ?_limit=0                  | 400.009 | Field _limit must not be empty.                           | en-US    |
      | customer-id inválido em pt      | ?customer-id=inválido      | 400.004 | O campo customer-id tem o formato ou valor inválido.      | pt-BR    |
      | customer-site-id inválido em pt | ?customer-site-id=inválido | 400.004 | O campo customer-site-id tem o formato ou valor inválido. | pt-BR    |
      | _offset inválido em pt          | ?_offset=a                 | 400.004 | O campo _offset tem o formato ou valor inválido.          | pt-BR    |
      | _offset incorreto em pt         | ?_offset=-1                | 400.005 | O campo _offset não pode ser negativo.                    | pt-BR    |
      | _limit inválido em pt           | ?_limit=a                  | 400.004 | O campo _limit tem o formato ou valor inválido.           | pt-BR    |
      | _limit incorreto em pt          | ?_limit=-1                 | 400.009 | Campo _limit não pode ser vazio.                          | pt-BR    |
      | _limit 0 em pt                  | ?_limit=0                  | 400.009 | Campo _limit não pode ser vazio.                          | pt-BR    |

  @Positivo
  Scenario Outline: Consultar distribuidor com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                  | queryParameter           |
      | customer-id incorreto      | ?customer-id=123456      |
      | customer-site-id incorreto | ?customer-site-id=123456 |
      | document inválido          | ?document=454.010.358-02 |
      | document incorreto         | ?document=123456         |
      | document não cadastrado    | ?document=45401035802    |
      | status inválido            | ?status=1                |
      | status incorreto           | ?status=C                |
      | com status B               | ?status=B                |
      | com status minúsculo       | ?status=b                |

  @Positivo
  Scenario Outline: Consultar distribuidor <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerSiteId" is not empty
    And I verify if response value "/0/organization" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/maxSaleVoucher" is not empty
    And I verify if response value "/0/validatePriceMethodType" is not empty
    And I verify if response value "/0/sendCompleteVoucher" is not empty
    And I verify if response value "/0/headquarterId" is empty
    And I verify if response value "/0/sendSms" is not empty
    And I verify if response value "/0/microMarket" is not empty
    And I verify if response value "/0/mainCustomerAddress" is not empty
    And I verify if response value "/0/endpointNotify" is not empty
    And I verify if response value "/0/products/0/code" is not empty
    And I verify if response value "/0/products/0/price" is not empty
    And I verify if response value "/0/products/0/discount" is not empty
    And I verify if response value "/0/products/0/validDateBegin" is not empty
    And I verify if response value "/0/products/0/validDateEnd" is not empty
    And Response code must be 200

    Examples: 
      | descricao                  | queryParameter      |
      | sem query parameter        |                     |
      | com _limit e _offset       | ?_limit=5&_offset=0 |
      | com _limit vazio           | ?_limit=            |
      | com _limit                 | ?_limit=${_limit}   |
      | com _offset válido         | ?_offset=${_offset} |
      | com _offset vazio          | ?_offset=           |
      | com _offset 0              | ?_offset=0          |
      | com customer-id vazio      | ?customer-id=       |
      | com customer-id            | ?customer-id=147524 |
      | com customer-site-id vazio | ?customer-site-id=  |
      | com document vazio         | ?document=          |
      | com status vazio           | ?status=            |
      | com status A               | ?status=A           |

  @Positivo
  Scenario Outline: Consultar distribuidor <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerSiteId" is not empty
    And I verify if response value "/0/organization" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/maxSaleVoucher" is not empty
    And I verify if response value "/0/validatePriceMethodType" is not empty
    And I verify if response value "/0/sendCompleteVoucher" is not empty
    And I verify if response value "/0/headquarterId" is not empty
    And I verify if response value "/0/sendSms" is not empty
    And I verify if response value "/0/microMarket" is not empty
    And I verify if response value "/0/mainCustomerAddress" is not empty
    And I verify if response value "/0/endpointNotify" is not empty
    And I verify if response value "/0/products/0/code" is not empty
    And I verify if response value "/0/products/0/price" is not empty
    And I verify if response value "/0/products/0/discount" is not empty
    And I verify if response value "/0/products/0/validDateBegin" is not empty
    And I verify if response value "/0/products/0/validDateEnd" is not empty
    And Response code must be 200

    Examples: 
      | descricao            | queryParameter                            |
      | com customer-site-id | ?customer-site-id=${customerSiteIdFilial} |
      | com document         | ?document=${document}                     |
      | com status I         | ?status=I                                 |

  @Positivo
  Scenario: Consultar distribuidor matriz
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}?customer-site-id=${customerSiteIdMatriz}"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/id" is not empty
    And I verify if response value "/0/customerId" is not empty
    And I verify if response value "/0/customerSiteId" is not empty
    And I verify if response value "/0/organization" is not empty
    And I verify if response value "/0/document" is not empty
    And I verify if response value "/0/status" is not empty
    And I verify if response value "/0/maxSaleVoucher" is not empty
    And I verify if response value "/0/validatePriceMethodType" is not empty
    And I verify if response value "/0/sendCompleteVoucher" is not empty
    And I verify if response value "/0/headquarterId" is empty
    And I verify if response value "/0/sendSms" is not empty
    And I verify if response value "/0/microMarket" is not empty
    And I verify if response value "/0/mainCustomerAddress" is not empty
    And I verify if response value "/0/webhook/endpointNotify" is not empty
    And I verify if response value "/0/webhook/username" is not empty
		And I verify if response value "/0/webhook/password" is not empty	
    And I verify if response value "/0/endpointNotify" is not empty
    And I verify if response value "/0/voucherValidityDays" is not empty
    And I verify if response value "/0/serviceChannel1" is not empty
    And I verify if response value "/0/serviceChannel2" is not empty
    And I verify if response value "/0/serviceChannel3" is not empty
    And I verify if response value "/0/phone1" is not empty
    And I verify if response value "/0/phone2" is not empty
    And I verify if response value "/0/territory" is not empty
    And I verify if response value "/0/generalMessage1" is not empty
    And I verify if response value "/0/generalMessage2" is not empty
    And I verify if response value "/0/generalMessage3" is not empty
    And I verify if response value "/0/generalMessage4" is not empty
    And I verify if response value "/0/generalMessage5" is not empty
    And I verify if response value "/0/products/0/code" is not empty
    And I verify if response value "/0/products/0/price" is not empty
    And I verify if response value "/0/products/0/discount" is not empty
    And I verify if response value "/0/products/0/validDateBegin" is not empty
    And I verify if response value "/0/products/0/validDateEnd" is not empty
    And Response code must be 200
