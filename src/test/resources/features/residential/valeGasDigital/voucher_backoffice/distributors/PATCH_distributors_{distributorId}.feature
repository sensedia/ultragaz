@PATCH_distributors_{distributorId}
Feature: PATCH_distributors_{distributorId}.feature
  Operação responsável por atualizar um distributor.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "2872" as "distributorId"
    And I save "2871" as "headquarterId"
    And I save "596913" as "customerId"
    And I save "655881" as "customerSiteIdMatriz"
    And I save "655860" as "customerSiteIdFilial"
    And I save "http://api-ultragaz.sensedia.com/dev/distributor-callback/v1/notify" as "url"
    And I save "2020-09-16T18:33:23.177Z" as "validDateBegin"
    And I save "2021-09-16T18:33:23.177Z" as "validDateEnd"
    And I save "jxIzjr8kBk7M6Ajxcmn2vl4MndIUWbTAdFUbpGPs4YYR0iOPWRoSlpC2pLyZPiZ2todKBeSCNOOyR2EeHtABDDBRV3j5UtpdlkP64" as "maxLength"
    And I save "jxIzjr8kBk7M6Ajxcmn2vl4MndIUWbTAdFUbpGPs4YYR0iOPWRoSlpC2pLyZPiZ2todKBeSCNOOyR2EeHtABDDBRV3j5UtpdlkP64jxIzjr8kBk7M6Ajxcmn2vl4MndIUWbTAdFUbpGPs4YYR0iOPWRoSlpC2pLyZPiZ2todKBeSCNOOyR2EeHtABDDBRV3j5UtpdlkP64jxIzjr8kBk7M6Ajxcmn2vl4MndIUWbTAdFUbpGPs4YYR0iOPWRoSlpC2pLyZPiZ2todKBeSCNOOyR2EeHtABDDBRV3j5UtpdlkP64" as "maxLength250"
    And I save "http://api-ultragaz.sensedia.com/dev/distributor-callback/v1/notify2aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" as "endpointOver200"

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I use api name as "/voucher/v1"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId":${customerId},"customerSiteId":${customerSiteIdMatriz}}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    And I use api name as "/voucher-backoffice/v1"
    And I save "/distributors" as "endpoint"

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request body as "{"status":"A"}"
    When I set PATCH api endpoint as "${endpoint}/${distributorId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"status":""}"
    When I set PATCH api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"status":""}"
    When I set PATCH api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"status":""}"
    When I set PATCH api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Atualizar <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    When I set PATCH api endpoint as "${endpoint}/${distributorId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                  | language |
      | sem body       | "Invalid request body."  | "en-US"  |
      | sem body em pt | "Request body inválido." | "pt-BR"  |

  @Negativo
  Scenario Outline: Atualizar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${distributorId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                                          | body                                                                                                      | message                                                            | code      | language |
      | com status inválido                                | "{"status":"C"}"                                                                                          | "Field status has an invalid format or value."                     | "400.004" | "en-US"  |
      | com status minúsculo inválido                      | "{"status":"a"}"                                                                                          | "Field status has an invalid format or value."                     | "400.004" | "en-US"  |
      | com maxSaleVoucher inválido                        | "{"maxSaleVoucher":"a"}"                                                                                  | "Field maxSaleVoucher has an invalid format or value."             | "400.004" | "en-US"  |
      | com maxSaleVoucher negativo                        | "{"maxSaleVoucher":-1}"                                                                                   | "Field maxSaleVoucher cannot be negative."                         | "400.005" | "en-US"  |
      | com validatePriceMethodType inválido               | "{"validatePriceMethodType":"trues"}"                                                                     | "Field validatePriceMethodType has an invalid format or value."    | "400.004" | "en-US"  |
      | com sendCompleteVoucher inválido                   | "{"sendCompleteVoucher":"truea"}"                                                                         | "Field sendCompleteVoucher has an invalid format or value."        | "400.004" | "en-US"  |
      | com isHeadquarter inválido                         | "{"isHeadquarter":"falsea"}"                                                                              | "Field isHeadquarter has an invalid format or value."              | "400.004" | "en-US"  |
      | com headquarterId inválido                         | "{"isHeadquarter":false,"headquarterId":"${headquarterId}a"}"                                             | "Field headquarterId has an invalid format or value."              | "400.004" | "en-US"  |
      | com sendSms inválido                               | "{"sendSms":"falsea"}"                                                                                    | "Field sendSms has an invalid format or value."                    | "400.004" | "en-US"  |
      | com mainCustomerAddress inválido                   | "{"mainCustomerAddress":"falsea"}"                                                                        | "Field mainCustomerAddress has an invalid format or value."        | "400.004" | "en-US"  |
      | com endpointNotify inválido                        | "{"webhook":{"endpointNotify":"Xuxa"}}"                                                                   | "Field webhook.endpointNotify has an invalid format or value."     | "400.004" | "en-US"  |
      | com endpointNotify inválido numérico               | "{"webhook":{"endpointNotify":123}}"                                                                      | "Field webhook.endpointNotify has an invalid format or value."     | "400.004" | "en-US"  |
      | com endpointNotify inválido boolean                | "{"webhook":{"endpointNotify":true}}"                                                                     | "Field webhook.endpointNotify has an invalid format or value."     | "400.004" | "en-US"  |
      | com products.price sem products.code               | "{"products":[{"price":33.00}]}"                                                                          | "Field products[0].code is required."                              | "400.001" | "en-US"  |
      | com products.discount sem products.code            | "{"products":[{"discount":10}]}"                                                                          | "Field products[0].code is required."                              | "400.001" | "en-US"  |
      | com products.validDateBegin sem products.code      | "{"products":[{"validDateBegin":"2020-09-16T18:33:23.177Z"}]}"                                            | "Field products[0].code is required."                              | "400.001" | "en-US"  |
      | com products.validDateEnd sem products.code        | "{"products":[{"validDateEnd":"2020-09-16T18:33:23.177Z"}]}"                                              | "Field products[0].code is required."                              | "400.001" | "en-US"  |
      | com products.price negativo                        | "{"products":[{"code":"0110035","price":-1}]}"                                                            | "Field products[0].price cannot be negative."                      | "400.005" | "en-US"  |
      | com products.discount negativo                     | "{"products":[{"code":"0110035","discount":-2}]}"                                                         | "Field products[0].discount has an invalid value."                 | "400.010" | "en-US"  |
      | com products.price maior que 100                   | "{"products":[{"code":"0110035","discount":101}]}"                                                        | "Field products[0].discount has an invalid value."                 | "400.010" | "en-US"  |
      | com validDateBegin > validDateEnd                  | "{"products":[{"code":"0110035","validDateBegin":"${validDateEnd}","validDateEnd":"${validDateBegin}"}]}" | "validDateBegin cannot be after validDateEnd."                     | "400.020" | "en-US"  |
      #| com validDateBegin > validDateEnd já existente  | "{"products":[{"code":"0110035","validDateBegin":"${validDateEnd}"}]}"                                    | "validDateBegin ${validDateEnd} não pode ser maior que a data de fim da vigência ${validDateEnd}."      | "400.025" | "en-US"  |
      #| com validDateEnd < data atual sem vigência      | "{"products":[{"code":"0110035","validDateEnd":"${validDateBegin}"}]}"                                    | "validDateEnd ${validDateBegin} cannot be before of begin vigency date ${validDateBegin}."              | "400.024" | "en-US"  |
      | com voucherValidityDays igual a 0                  | "{"voucherValidityDays":0}"                                                                               | "Field voucherValidityDays has an invalid format or value."        | "400.004" | "en-US"  |
      | com voucherValidityDays igual a -1                 | "{"voucherValidityDays":-1}"                                                                              | "Field voucherValidityDays has an invalid format or value."        | "400.004" | "en-US"  |
      | com serviceChannel1 com maxLength > 100            | "{"serviceChannel1":"${maxLength}"}"                                                                      | "Field serviceChannel1 must have 100 characters limit."            | "400.014" | "en-US"  |
      | com serviceChannel2 com maxLength > 100            | "{"serviceChannel2":"${maxLength}"}"                                                                      | "Field serviceChannel2 must have 100 characters limit."            | "400.014" | "en-US"  |
      | com serviceChannel3 com maxLength > 100            | "{"serviceChannel3":"${maxLength}"}"                                                                      | "Field serviceChannel3 must have 100 characters limit."            | "400.014" | "en-US"  |
      | com generalMessage1 com maxLength > 250            | "{"generalMessage1":"${maxLength250}"}"                                                                   | "Field generalMessage1 must have 250 characters limit."            | "400.014" | "en-US"  |
      | com generalMessage2 com maxLength > 250            | "{"generalMessage2":"${maxLength250}"}"                                                                   | "Field generalMessage2 must have 250 characters limit."            | "400.014" | "en-US"  |
      | com generalMessage3 com maxLength > 250            | "{"generalMessage3":"${maxLength250}"}"                                                                   | "Field generalMessage3 must have 250 characters limit."            | "400.014" | "en-US"  |
      | com generalMessage4 com maxLength > 250            | "{"generalMessage4":"${maxLength250}"}"                                                                   | "Field generalMessage4 must have 250 characters limit."            | "400.014" | "en-US"  |
      | com generalMessage5 com maxLength > 250            | "{"generalMessage5":"${maxLength250}"}"                                                                   | "Field generalMessage5 must have 250 characters limit."            | "400.014" | "en-US"  |
      | com phone1 > 30                                    | "{"phone1":"9749166775183033301349003002483"}"                                                            | "Field phone1 must have 30 characters limit."                      | "400.014" | "en-US"  |
      | com phone2 > 30                                    | "{"phone2":"9749166775183033301349003002483"}"                                                            | "Field phone2 must have 30 characters limit."                      | "400.014" | "en-US"  |
      | com territory com maxLength > 100                  | "{"territory":"${maxLength}"}"                                                                            | "Field territory must have 100 characters limit."                  | "400.014" | "en-US"  |
      | com voucherValidityDays string                     | "{"voucherValidityDays":"um"}"                                                                            | "Field voucherValidityDays has an invalid format or value."        | "400.004" | "en-US"  |
      | com webhook endpointNotify com maxLength > 200     | "{"webhook":{"endpointNotify":"${endpointOver200}"}}"                                                     | "Field webhook.endpointNotify must have 200 characters limi"       | "400.014" | "en-US"  |
      | com webhook username com maxLength > 100           | "{"webhook":{"username":"${maxLength}"}}"                                                                 | "Field webhook.username must have 100 characters limit."           | "400.014" | "en-US"  |
      | com webhook password com maxLength > 100           | "{"webhook":{"password":"${maxLength}"}}"                                                                 | "Field webhook.password must have 100 characters limit."           | "400.014" | "en-US"  |
      | com status inválido em pt                          | "{"status":"C"}"                                                                                          | "O campo status tem o formato ou valor inválido."                  | "400.004" | "pt-BR"  |
      | com status minúsculo inválido em pt                | "{"status":"a"}"                                                                                          | "O campo status tem o formato ou valor inválido."                  | "400.004" | "pt-BR"  |
      | com maxSaleVoucher inválido em pt                  | "{"maxSaleVoucher":"a"}"                                                                                  | "O campo maxSaleVoucher tem o formato ou valor inválido."          | "400.004" | "pt-BR"  |
      | com maxSaleVoucher negativo em pt                  | "{"maxSaleVoucher":-1}"                                                                                   | "O campo maxSaleVoucher não pode ser negativo."                    | "400.005" | "pt-BR"  |
      | com validatePriceMethodType inválido em pt         | "{"validatePriceMethodType":"trues"}"                                                                     | "O campo validatePriceMethodType tem o formato ou valor inválido." | "400.004" | "pt-BR"  |
      | com sendCompleteVoucher inválido em pt             | "{"sendCompleteVoucher":"truea"}"                                                                         | "O campo sendCompleteVoucher tem o formato ou valor inválido."     | "400.004" | "pt-BR"  |
      | com isHeadquarter inválido em pt                   | "{"isHeadquarter":"falsea"}"                                                                              | "O campo isHeadquarter tem o formato ou valor inválido."           | "400.004" | "pt-BR"  |
      | com headquarterId inválido em pt                   | "{"isHeadquarter":false,"headquarterId":"${headquarterId}a"}"                                             | "O campo headquarterId tem o formato ou valor inválido."           | "400.004" | "pt-BR"  |
      | com sendSms inválido em pt                         | "{"sendSms":"falsea"}"                                                                                    | "O campo sendSms tem o formato ou valor inválido."                 | "400.004" | "pt-BR"  |
      | com mainCustomerAddress inválido em pt             | "{"mainCustomerAddress":"falsea"}"                                                                        | "O campo mainCustomerAddress tem o formato ou valor inválido."     | "400.004" | "pt-BR"  |
      | com webhook endpointNotify inválido em pt          | "{"webhook":{"endpointNotify":"Xuxa"}}"                                                                   | "O campo webhook.endpointNotify tem o formato ou valor inválido."  | "400.004" | "pt-BR"  |
      | com webhook endpointNotify inválido numérico em pt | "{"webhook":{"endpointNotify":123}}"                                                                      | "O campo webhook.endpointNotify tem o formato ou valor inválido."  | "400.004" | "pt-BR"  |
      | com webhook endpointNotify inválido boolean em pt  | "{"webhook":{"endpointNotify":true}}"                                                                     | "O campo webhook.endpointNotify tem o formato ou valor inválido."  | "400.004" | "pt-BR"  |
      | com products.price sem products.code pt            | "{"products":[{"price":33.00}]}"                                                                          | "O campo products[0].code é obrigatório."                          | "400.001" | "pt-BR"  |
      | com products.discount sem products.code pt         | "{"products":[{"discount":10}]}"                                                                          | "O campo products[0].code é obrigatório."                          | "400.001" | "pt-BR"  |
      | com products.validDateBegin sem products.code pt   | "{"products":[{"validDateBegin":"2020-09-16T18:33:23.177Z"}]}"                                            | "O campo products[0].code é obrigatório."                          | "400.001" | "pt-BR"  |
      | com products.validDateEnd sem products.code pt     | "{"products":[{"validDateEnd":"2020-09-16T18:33:23.177Z"}]}"                                              | "O campo products[0].code é obrigatório."                          | "400.001" | "pt-BR"  |
      | com products.price negativo pt                     | "{"products":[{"code":"0110035","price":-1}]}"                                                            | "O campo products[0].price não pode ser negativo."                 | "400.005" | "pt-BR"  |
      | com products.discount negativo pt                  | "{"products":[{"code":"0110035","discount":-2}]}"                                                         | "Campo products[0].discount tem o valor inválido."                 | "400.010" | "pt-BR"  |
      #| com validDateBegin > validDateEnd já existente pt | "{"products":[{"code":"0110035","validDateBegin":"${validDateEnd}"}]}"                                    | "validDateBegin ${validDateEnd} não pode ser maior que a data de fim da vigência ${validDateEnd}."      | "400.025" | "pt-BR"  |
      #| com validDateEnd < data atual sem vigência pt     | "{"products":[{"code":"0110035","validDateEnd":"${validDateBegin}"}]}"                                    | "validDateEnd ${validDateBegin} não pode ser menor que a data de início da vigência ${validDateBegin}." | "400.024" | "pt-BR"  |
      | com validDateBegin > validDateEnd pt               | "{"products":[{"code":"0110035","validDateBegin":"${validDateEnd}","validDateEnd":"${validDateBegin}"}]}" | "validDateBegin não pode ser posterior a validDateEnd."            | "400.020" | "pt-BR"  |
      | com voucherValidityDays igual a 0 pt               | "{"voucherValidityDays":0}"                                                                               | "O campo voucherValidityDays tem o formato ou valor inválido."     | "400.004" | "pt-BR"  |
      | com voucherValidityDays igual a -1 pt              | "{"voucherValidityDays":-1}"                                                                              | "O campo voucherValidityDays tem o formato ou valor inválido."     | "400.004" | "pt-BR"  |
      | com serviceChannel1 com maxLength > 100 pt         | "{"serviceChannel1":"${maxLength}"}"                                                                      | "Campo serviceChannel1 tem limite de 100 caracteres."              | "400.014" | "pt-BR"  |
      | com serviceChannel2 com maxLength > 100 pt         | "{"serviceChannel2":"${maxLength}"}"                                                                      | "Campo serviceChannel2 tem limite de 100 caracteres."              | "400.014" | "pt-BR"  |
      | com serviceChannel3 com maxLength > 100 pt         | "{"serviceChannel3":"${maxLength}"}"                                                                      | "Campo serviceChannel3 tem limite de 100 caracteres."              | "400.014" | "pt-BR"  |
      | com generalMessage1 com maxLength > 250 pt         | "{"generalMessage1":"${maxLength250}"}"                                                                   | "Campo generalMessage1 tem limite de 250 caracteres."              | "400.014" | "pt-BR"  |
      | com generalMessage2 com maxLength > 250 pt         | "{"generalMessage2":"${maxLength250}"}"                                                                   | "Campo generalMessage2 tem limite de 250 caracteres."              | "400.014" | "pt-BR"  |
      | com generalMessage3 com maxLength > 250 pt         | "{"generalMessage3":"${maxLength250}"}"                                                                   | "Campo generalMessage3 tem limite de 250 caracteres."              | "400.014" | "pt-BR"  |
      | com generalMessage4 com maxLength > 250 pt         | "{"generalMessage4":"${maxLength250}"}"                                                                   | "Campo generalMessage4 tem limite de 250 caracteres."              | "400.014" | "pt-BR"  |
      | com generalMessage5 com maxLength > 250 pt         | "{"generalMessage5":"${maxLength250}"}"                                                                   | "Campo generalMessage5 tem limite de 250 caracteres."              | "400.014" | "pt-BR"  |
      | com phone1 > 30 pt                                 | "{"phone1":"9749166775183033301349003002483"}"                                                            | "Campo phone1 tem limite de 30 caracteres."                        | "400.014" | "pt-BR"  |
      | com phone2 > 30 pt                                 | "{"phone2":"9749166775183033301349003002483"}"                                                            | "Campo phone2 tem limite de 30 caracteres."                        | "400.014" | "pt-BR"  |
      | com territory com maxLength > 100 pt               | "{"territory":"${maxLength}"}"                                                                            | "Campo territory tem limite de 100 caracteres."                    | "400.014" | "pt-BR"  |
      | com voucherValidityDays string pt                  | "{"voucherValidityDays":"um"}"                                                                            | "O campo voucherValidityDays tem o formato ou valor inválido."     | "400.004" | "pt-BR"  |
      | com webhook endpointNotify com maxLength > 200 pt  | "{"webhook":{"endpointNotify":"${endpointOver200}"}}"                                                     | "Campo webhook.endpointNotify tem limite de 200 caracteres."       | "400.014" | "pt-BR"  |
      | com webhook username com maxLength > 100 pt        | "{"webhook":{"username":"${maxLength}"}}"                                                                 | "Campo webhook.username tem limite de 100 caracteres."             | "400.014" | "pt-BR"  |
      | com webhook password com maxLength > 100 pt        | "{"webhook":{"password":"${maxLength}"}}"                                                                 | "Campo webhook.password tem limite de 100 caracteres."             | "400.014" | "pt-BR"  |

  @Negativo
  Scenario Outline: Atualizar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${distributorId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.004"
    And I compare response value "/1/message" with <message>
    And I compare response value "/1/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                  | body                                                                            | message                                                   | code      | language |
      | products.price inválido    | "{"products":[{"code":"0110035","price":"acdc"}]}"                              | "Field price has an invalid format or value."             | "400.004" | "en-US"  |
      | products.price inválido    | "{"products":[{"code":"0110035","discount":"banana"}]}"                         | "Field discount has an invalid format or value."          | "400.004" | "en-US"  |
      | validDateBegin inválido    | "{"products":[{"validDateBegin":"banana","validDateEnd":"${validDateEnd}"}]}"   | "Field validDateBegin has an invalid format or value."    | "400.004" | "en-US"  |
      | validDateEnd inválido      | "{"products":[{"validDateBegin":"${validDateBegin}","validDateEnd":"banana"}]}" | "Field validDateEnd has an invalid format or value."      | "400.004" | "en-US"  |
      | products.price inválido pt | "{"products":[{"code":"0110035","price":"acdc"}]}"                              | "O campo price tem o formato ou valor inválido."          | "400.004" | "pt-BR"  |
      | products.price inválido pt | "{"products":[{"code":"0110035","discount":"banana"}]}"                         | "O campo discount tem o formato ou valor inválido."       | "400.004" | "pt-BR"  |
      | validDateBegin inválido pt | "{"products":[{"validDateBegin":"banana","validDateEnd":"${validDateEnd}"}]}"   | "O campo validDateBegin tem o formato ou valor inválido." | "400.004" | "pt-BR"  |
      | validDateEnd inválido pt   | "{"products":[{"validDateBegin":"${validDateBegin}","validDateEnd":"banana"}]}" | "O campo validDateEnd tem o formato ou valor inválido."   | "400.004" | "pt-BR"  |

  @Negativo
  Scenario Outline: Atualizar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${distributorId}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                                    | body                                              | message                                                                   | code      | language |
      | com headquarterId incorreto                  | "{"isHeadquarter":false,"headquarterId":0}"       | "It was not possible to found headquarter with identifier 0."             | "422.012" | "en-US"  |
      | com isHeadquarter true e headquarterId       | "{"isHeadquarter":true,"headquarterId":367}"      | "If isHeadquarter is true, the field headquarterId cannot be filled."     | "422.013" | "en-US"  |
      | com products.code inexistente                | "{"products":[{"code":"9990666","price":33.00}]}" | "Product 9990666 does not exists."                                        | "422.023" | "en-US"  |
      | com headquarterId incorreto em pt            | "{"isHeadquarter":false,"headquarterId":0}"       | "Não foi possivel encontrar matriz com o identificador 0."                | "422.012" | "pt-BR"  |
      | com isHeadquarter true e headquarterId em pt | "{"isHeadquarter":true,"headquarterId":367}"      | "Se isHeadquarter é true, o campo headquarterId não pode ser preenchido." | "422.013" | "pt-BR"  |
      | com products.code inexistente pt             | "{"products":[{"code":"9990666","price":33.00}]}" | "Produto 9990666 informado não existe."                                   | "422.023" | "pt-BR"  |

  @Negativo
  Scenario Outline: Atualizar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"status":"A"}"
    When I set PATCH api endpoint as "${endpoint}/<distributorId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao                   | distributorId |
      | com distributorId incorreto |             1 |

  @Negativo
  Scenario Outline: Atualizar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "{"status":"A"}"
    When I set PATCH api endpoint as "${endpoint}/<distributorId>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                        | distributorId                        | code    | message                                     | language |
      | com distributorId inválido       | b101c323-fe08-4bed-9d65-5cfc78003aac | 400.004 | Field id has an invalid format or value.    | en-US    |
      | com distributorId inválido em pt | b101c323-fe08-4bed-9d65-5cfc78003aac | 400.004 | O campo id tem o formato ou valor inválido. | pt-BR    |

  @Positivo
  Scenario Outline: Atualizar distributor <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpoint}/${distributorId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao                                       | body                                                                                                                                                |
      | com status vazio                                | {"status":""}                                                                                                                                       |
      | com status nulo                                 | {"status":null}                                                                                                                                     |
      | com status B                                    | {"status":"B"}                                                                                                                                      |
      | com status I                                    | {"status":"I"}                                                                                                                                      |
      | com status A                                    | {"status":"A"}                                                                                                                                      |
      | com maxSaleVoucher vazio                        | {"maxSaleVoucher":""}                                                                                                                               |
      | com maxSaleVoucher nulo                         | {"maxSaleVoucher":null}                                                                                                                             |
      | com maxSaleVoucher 3                            | {"maxSaleVoucher":3}                                                                                                                                |
      | com validatePriceMethodType vazio               | {"validatePriceMethodType":""}                                                                                                                      |
      | com validatePriceMethodType nulo                | {"validatePriceMethodType":null}                                                                                                                    |
      | com validatePriceMethodType BY_CNPJ             | {"validatePriceMethodType":"BY_CNPJ"}                                                                                                               |
      | com validatePriceMethodType NO                  | {"validatePriceMethodType":"NO"}                                                                                                                    |
      | com validatePriceMethodType BY_LOCATION         | {"validatePriceMethodType":"BY_LOCATION"}                                                                                                           |
      | com sendCompleteVoucher vazio                   | {"sendCompleteVoucher":""}                                                                                                                          |
      | com sendCompleteVoucher nulo                    | {"sendCompleteVoucher":null}                                                                                                                        |
      | com sendCompleteVoucher true                    | {"sendCompleteVoucher":"true"}                                                                                                                      |
      | com sendCompleteVoucher false                   | {"sendCompleteVoucher":"false"}                                                                                                                     |
      | com isHeadquarter vazio                         | {"isHeadquarter":""}                                                                                                                                |
      | com isHeadquarter nulo                          | {"isHeadquarter":null}                                                                                                                              |
      | com isHeadquarter true                          | {"isHeadquarter":true}                                                                                                                              |
      | com isHeadquarter false                         | {"isHeadquarter":false,"headquarterId":${headquarterId}}                                                                                            |
      | com headquarterId vazio                         | {"isHeadquarter":false,"headquarterId":""}                                                                                                          |
      | com headquarterId nulo                          | {"isHeadquarter":false,"headquarterId":null}                                                                                                        |
      | com headquarterId -1                            | {"isHeadquarter":false,"headquarterId":-1}                                                                                                          |
      | com headquarterId                               | {"isHeadquarter":false,"headquarterId":${headquarterId}}                                                                                            |
      | com sendSms vazio                               | {"sendSms":""}                                                                                                                                      |
      | com sendSms nulo                                | {"sendSms":null}                                                                                                                                    |
      | com sendSms true                                | {"sendSms":true}                                                                                                                                    |
      | com sendSms false                               | {"sendSms":false}                                                                                                                                   |
      | com microMarket vazio                           | {"microMarket":""}                                                                                                                                  |
      | com microMarket nulo                            | {"microMarket":null}                                                                                                                                |
      | com microMarket válido                          | {"microMarket":"G20"}                                                                                                                               |
      | com mainCustomerAddress vazio                   | {"mainCustomerAddress":""}                                                                                                                          |
      | com mainCustomerAddress nulo                    | {"mainCustomerAddress":null}                                                                                                                        |
      | com mainCustomerAddress true                    | {"mainCustomerAddress":true}                                                                                                                        |
      | com mainCustomerAddress false                   | {"mainCustomerAddress":false}                                                                                                                       |
      | com endpointNotify vazio                        | {"endpointNotify":""}                                                                                                                               |
      | com endpointNotify                              | {"endpointNotify":"${url}"}                                                                                                                         |
      | com endpointNotify nulo                         | {"endpointNotify":null}                                                                                                                             |
      | com products.price com produto vigente          | {"products":[{"code":"0110035","price":33.00}]}                                                                                                     |
      | com products.discount -1                        | {"products":[{"code":"0110035","discount":-1}]}                                                                                                     |
      | com products.discount com produto vigente       | {"products":[{"code":"0110035","discount":10}]}                                                                                                     |
      | com products.validDateBegin com produto vigente | {"products":[{"code":"0110035","validDateBegin":"${validDateBegin}"}]}                                                                              |
      | com products.validDateEnd com produto vigente   | {"products":[{"code":"0110035","validDateEnd":"${validDateEnd}"}]}                                                                                  |
      | com data não vigente                            | {"products":[{"code":"0110035","price":33.00,"discount":10,"validDateBegin":"2020-09-18T18:33:22.177Z","validDateEnd":"2020-09-19T18:33:22.177Z"}]} |
      | com voucherValidityDays vazio                   | {"voucherValidityDays":""}                                                                                                                          |
      | com voucherValidityDays                         | {"voucherValidityDays":3}                                                                                                                           |
      | com voucherValidityDays nulo                    | {"voucherValidityDays":null}                                                                                                                        |
      | com serviceChannel1 vazio                       | {"serviceChannel1":""}                                                                                                                              |
      | com serviceChannel1                             | {"serviceChannel1":"Canal de serviço 1"}                                                                                                            |
      | com serviceChannel1 nulo                        | {"serviceChannel1":null}                                                                                                                            |
      | com serviceChannel2 vazio                       | {"serviceChannel2":""}                                                                                                                              |
      | com serviceChannel2                             | {"serviceChannel2":"Canal de serviço 2"}                                                                                                            |
      | com serviceChannel2 nulo                        | {"serviceChannel2":null}                                                                                                                            |
      | com serviceChannel3 vazio                       | {"serviceChannel3":""}                                                                                                                              |
      | com serviceChannel3                             | {"serviceChannel3":"Canal de serviço 3"}                                                                                                            |
      | com serviceChannel3 nulo                        | {"serviceChannel3":null}                                                                                                                            |
      | com phone1 vazio                                | {"phone1":""}                                                                                                                                       |
      | com phone1                                      | {"phone1":"+55 19 3500-8265"}                                                                                                                       |
      | com phone1 nulo                                 | {"phone1":null}                                                                                                                                     |
      | com phone2 vazio                                | {"phone2":""}                                                                                                                                       |
      | com phone2                                      | {"phone2":"+55 19 3500-8265"}                                                                                                                       |
      | com phone2 nulo                                 | {"phone2":null}                                                                                                                                     |
      | com territory vazio                             | {"territory":""}                                                                                                                                    |
      | com territory                                   | {"territory":"Boderland"}                                                                                                                           |
      | com territory nulo                              | {"territory":null}                                                                                                                                  |
      | com generalMessage1 vazio                       | {"generalMessage1":""}                                                                                                                              |
      | com generalMessage1                             | {"generalMessage1":"Automação general 1"}                                                                                                           |
      | com generalMessage1 nulo                        | {"generalMessage1":null}                                                                                                                            |
      | com generalMessage2 vazio                       | {"generalMessage2":""}                                                                                                                              |
      | com generalMessage2                             | {"generalMessage2":"Automação general 2"}                                                                                                           |
      | com generalMessage2 nulo                        | {"generalMessage2":null}                                                                                                                            |
      | com generalMessage3 vazio                       | {"generalMessage3":""}                                                                                                                              |
      | com generalMessage3                             | {"generalMessage3":"Automação general 3"}                                                                                                           |
      | com generalMessage3 nulo                        | {"generalMessage3":null}                                                                                                                            |
      | com generalMessage4 vazio                       | {"generalMessage4":""}                                                                                                                              |
      | com generalMessage4 nulo                        | {"generalMessage4":null}                                                                                                                            |
      | com generalMessage4                             | {"generalMessage4":"Automação general 4"}                                                                                                           |
      | com generalMessage5 vazio                       | {"generalMessage5":""}                                                                                                                              |
      | com generalMessage5 nulo                        | {"generalMessage5":null}                                                                                                                            |
      | com generalMessage5                             | {"generalMessage5":"Automação general 5"}                                                                                                           |
      | com webhook endpointNotify vazio                | {"webhook":{"endpointNotify":""}}                                                                                                                   |
      | com webhook endpointNotify null                 | {"webhook":{"endpointNotify":null}}                                                                                                                 |
      | com webhook endpointNotify                      | {"webhook":{"endpointNotify":"http://api-ultragaz.sensedia.com/dev/distributor-callback/v1/notify"}}                                                |
      | com webhook username vazio                      | {"webhook":{"username":""}}                                                                                                                         |
      | com webhook username null                       | {"webhook":{"username":null}}                                                                                                                       |
      | com webhook username                            | {"webhook":{"username":"teste automação"}}                                                                                                          |
      | com webhook password vazio                      | {"webhook":{"password":""}}                                                                                                                         |
      | com webhook password null                       | {"webhook":{"password":null}}                                                                                                                       |
      | com webhook password                            | {"webhook":{"password":"teste automação"}}                                                                                                          |
      | com body vazio                                  | {}                                                                                                                                                  |
