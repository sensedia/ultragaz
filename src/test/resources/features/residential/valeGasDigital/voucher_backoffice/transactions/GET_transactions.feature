Feature: GET_transactions.feature
  Obter transações do vale gás digital via backoffice.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "VENDA" as "transactionType"
    And I save "83931550000232" as "distributorDocument"
    And I save "NAO_VENDIDO" as "voucherStatus"
    And I save "744F86UY" as "nsu"
    And I save "3257673" as "voucherAuthorizationCode"
    And I save "658880" as "consumerDocument"
    And I save "19" as "consumerPhoneDdd"
    And I save "982911139" as "consumerPhone"
    And I save "wellington.moura@sensedia.com" as "consumerEmail"
    And I save "2020-09-09 00:00:00" as "beginDate"
    And I save "2020-09-14 23:59:59" as "endDate"
    And I save "20" as "_limit"
    And I save "0" as "_offset"
    And I save "596913" as "customerId"
    And I save "655881" as "customerSiteIdMatriz"
    And I save "655860" as "customerSiteIdFilial"

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I use api name as "/voucher/v1"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteIdFilial}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I use api name as "/voucher/v1"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteIdMatriz}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "tokenMatriz"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use api name as "/voucher-backoffice/v1"
    And I save "/transactions" as "endpoint"

  @Negativo
  Scenario: Enviar requisição sem client_id
  	Given I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

	@Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token | message                                                                                    |
      | com client_id vazio       | ""              | "${token}"   | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${token}"   | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""           | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${token}1"  | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      
  @Negativo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/0/message" with "<message>"
    And Response code must be 400

    Examples: 
      | descricao                                                          | queryParameter                       | code    | message                                                                                         | language |
      | transaction-type minusculo                                         | ?transaction-type=venda              | 400.004 | Field transaction-type has an invalid format or value.                                          | en-US    |
      | transaction-type 1                                                 | ?transaction-type=1                  | 400.004 | Field transaction-type has an invalid format or value.                                          | en-US    |
      | transaction-type inválido                                          | ?transaction-type=CANCELAMENTOS      | 400.004 | Field transaction-type has an invalid format or value.                                          | en-US    |
      | voucher-status minusculo                                           | ?voucher-status=disponivel           | 400.004 | Field voucher-status has an invalid format or value.                                            | en-US    |
      | voucher-status 1                                                   | ?voucher-status=1                    | 400.004 | Field voucher-status has an invalid format or value.                                            | en-US    |
      | voucher-status inválido                                            | ?voucher-status=CANCELAMENTO         | 400.004 | Field voucher-status has an invalid format or value.                                            | en-US    |
      | begin-date 1                                                       | ?begin-date=1                        | 400.004 | Field begin-date has an invalid format or value.                                                | en-US    |
      | begin-date fora do formato                                         | ?begin-date=01/01/2020               | 400.004 | Field begin-date has an invalid format or value.                                                | en-US    |
      | begin-date dia inválido                                            | ?begin-date=2019-01-33               | 400.004 | Field begin-date has an invalid format or value.                                                | en-US    |
      | begin-date mês inválido                                            | ?begin-date=2019-21-30               | 400.004 | Field begin-date has an invalid format or value.                                                | en-US    |
      | begin-date ano inválido                                            | ?begin-date=201-01-30                | 400.004 | Field begin-date has an invalid format or value.                                                | en-US    |
      | end-date 1                                                         | ?end-date=1                          | 400.004 | Field end-date has an invalid format or value.                                                  | en-US    |
      | end-date fora do formato                                           | ?end-date=01/01/2020                 | 400.004 | Field end-date has an invalid format or value.                                                  | en-US    |
      | end-date dia inválido                                              | ?end-date=2019-01-33                 | 400.004 | Field end-date has an invalid format or value.                                                  | en-US    |
      | end-date mês inválido                                              | ?end-date=2019-21-30                 | 400.004 | Field end-date has an invalid format or value.                                                  | en-US    |
      | end-date ano inválido                                              | ?end-date=201-01-30                  | 400.004 | Field end-date has an invalid format or value.                                                  | en-US    |
      | _limit inválido                                                    | ?_limit=string                       | 400.004 | Field _limit has an invalid format or value.                                                    | en-US    |
      | _offset inválido                                                   | ?_offset=string                      | 400.004 | Field _offset has an invalid format or value.                                                   | en-US    |
      | _limit 0                                                           | ?_limit=0                            | 400.016 | Field _limit value must be at least 1.                                                          | en-US    |
      | list-transactions-from-network vazio                               | ?list-transactions-from-network=     | 400.004 | Field list-transactions-from-network has an invalid format or value.                            | en-US    |
      | list-transactions-from-network inválido                            | ?list-transactions-from-network=a    | 400.004 | Field list-transactions-from-network has an invalid format or value.                            | en-US    |
      | list-transactions-from-network true sem distributor-document       | ?list-transactions-from-network=true | 400.017 | Filter distributor-document is required when list-transactions-from-network has true value.     | en-US    |
      | success inválido                                                   | ?success=falsiane                    | 400.004 | Field success has an invalid format or value.                                                   | en-US    |
      | transaction-type minusculo em pt                                   | ?transaction-type=venda              | 400.004 | O campo transaction-type tem o formato ou valor inválido.                                       | pt-BR    |
      | transaction-type 1 em pt                                           | ?transaction-type=1                  | 400.004 | O campo transaction-type tem o formato ou valor inválido.                                       | pt-BR    |
      | transaction-type inválido em pt                                    | ?transaction-type=CANCELAMENTOS      | 400.004 | O campo transaction-type tem o formato ou valor inválido.                                       | pt-BR    |
      | voucher-status minusculo em pt                                     | ?voucher-status=disponivel           | 400.004 | O campo voucher-status tem o formato ou valor inválido.                                         | pt-BR    |
      | voucher-status 1 em pt                                             | ?voucher-status=1                    | 400.004 | O campo voucher-status tem o formato ou valor inválido.                                         | pt-BR    |
      | voucher-status inválido em pt                                      | ?voucher-status=CANCELAMENTO         | 400.004 | O campo voucher-status tem o formato ou valor inválido.                                         | pt-BR    |
      | begin-date 1 em pt                                                 | ?begin-date=1                        | 400.004 | O campo begin-date tem o formato ou valor inválido.                                             | pt-BR    |
      | begin-date fora do formato em pt                                   | ?begin-date=01/01/2020               | 400.004 | O campo begin-date tem o formato ou valor inválido.                                             | pt-BR    |
      | begin-date dia inválido em pt                                      | ?begin-date=2019-01-33               | 400.004 | O campo begin-date tem o formato ou valor inválido.                                             | pt-BR    |
      | begin-date mês inválido em pt                                      | ?begin-date=2019-21-30               | 400.004 | O campo begin-date tem o formato ou valor inválido.                                             | pt-BR    |
      | begin-date ano inválido em pt                                      | ?begin-date=201-01-30                | 400.004 | O campo begin-date tem o formato ou valor inválido.                                             | pt-BR    |
      | end-date 1 em pt                                                   | ?end-date=1                          | 400.004 | O campo end-date tem o formato ou valor inválido.                                               | pt-BR    |
      | end-date fora do formato em pt                                     | ?end-date=01/01/2020                 | 400.004 | O campo end-date tem o formato ou valor inválido.                                               | pt-BR    |
      | end-date dia inválido em pt                                        | ?end-date=2019-01-33                 | 400.004 | O campo end-date tem o formato ou valor inválido.                                               | pt-BR    |
      | end-date mês inválido em pt                                        | ?end-date=2019-21-30                 | 400.004 | O campo end-date tem o formato ou valor inválido.                                               | pt-BR    |
      | end-date ano inválido em pt                                        | ?end-date=201-01-30                  | 400.004 | O campo end-date tem o formato ou valor inválido.                                               | pt-BR    |
      | _limit inválido em pt                                              | ?_limit=string                       | 400.004 | O campo _limit tem o formato ou valor inválido.                                                 | pt-BR    |
      | _offset inválido em pt                                             | ?_offset=string                      | 400.004 | O campo _offset tem o formato ou valor inválido.                                                | pt-BR    |
      | _limit 0 em pt                                                     | ?_limit=0                            | 400.016 | O valor do campo _limit deve ser pelo menos 1.                                                  | pt-BR    |
      | list-transactions-from-network vazio em pt                         | ?list-transactions-from-network=     | 400.004 | O campo list-transactions-from-network tem o formato ou valor inválido.                         | pt-BR    |
      | list-transactions-from-network inválido em pt                      | ?list-transactions-from-network=a    | 400.004 | O campo list-transactions-from-network tem o formato ou valor inválido.                         | pt-BR    |
      | list-transactions-from-network true sem distributor-document em pt | ?list-transactions-from-network=true | 400.017 | Filtro distributor-document é obrigatório quando list-transactions-from-network tem valor true. | pt-BR    |
      | success inválido pt                                                | ?success=falsiane                    | 400.004 | O campo success tem o formato ou valor inválido.                                                | pt-BR    |

  @Positivo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionType" is not empty
    And I verify if response value "/0/time" is not empty
    And I verify if response value "/0/nsu" is not empty
    And I verify if response value "/0/latitude" is empty
    And I verify if response value "/0/longitude" is empty
    And I verify if response value "/0/microMarket" is empty
    And I verify if response value "/0/organizationId" is empty
    And I verify if response value "/0/productsVouchers/0/productCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productDescription" is not empty
    And I verify if response value "/0/productsVouchers/0/productPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherAuthorizationCode" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatusId" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatus" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherCode" is not empty
    And I verify if response value "/0/consumer/name" is not empty
    And I verify if response value "/0/consumer/documentType" is not empty
    And I verify if response value "/0/consumer/document" is not empty
    And I verify if response value "/0/consumer/email" is empty
    And I verify if response value "/0/consumer/cellphone/areaCode" is not empty
    And I verify if response value "/0/consumer/cellphone/number" is not empty
    And I verify if response value "/0/distributor/document" is not empty
    And I verify if response value "/0/distributor/headquarterId" is not empty
    And Response code must be 200

    Examples: 
      | descricao                                 | queryParameter                                          |
      | sem query parameter                       |                                                         |
      | com _limit e _offset                      | ?_limit=5&_offset=0                                     |
      | transaction-type vazio                    | ?transaction-type=                                      |
      | list-transactions-from-network false      | ?list-transactions-from-network=false                   |
      | distributor-document vazio                | ?distributor-document=                                  |
      | voucher-status vazio                      | ?voucher-status=                                        |
      | nsu vazio                                 | ?nsu=                                                   |
      | voucher-authorization-code vazio          | ?voucher-authorization-code=                            |
      | consumer-document vazio                   | ?consumer-document=                                     |
      | consumer-phone-ddd vazio                  | ?consumer-phone-ddd=                                    |
      | consumer-phone vazio                      | ?consumer-phone=                                        |
      | consumer-email vazio                      | ?consumer-email=                                        |
      | begin-date vazio                          | ?begin-date=                                            |
      | begin-date válido                         | ?begin-date=${beginDate}                                |
      | end-date vazio                            | ?end-date=                                              |
      | end-date válido                           | ?end-date=${endDate}                                    |
      | _limit vazio                              | ?_limit=                                                |
      | _offset válido                            | ?_offset=${_offset}                                     |
      | _offset vazio                             | ?_offset=                                               |
      | _offset 0                                 | ?_offset=0                                              |
      | success vazio                             | ?success=                                               |
      | voucher-authorization-code válido inteiro | ?voucher-authorization-code=${voucherAuthorizationCode} |
      | _limit 1                                  | ?_limit=1                                               |
      | _limit válido                             | ?_limit=${_limit}                                       |
      | consumer-phone-ddd válido                 | ?consumer-phone-ddd=${consumerPhoneDdd}                 |

  @Positivo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionType" is not empty
    And I verify if response value "/0/time" is not empty
    And I verify if response value "/0/nsu" is not empty
    And I verify if response value "/0/latitude" is not empty
    And I verify if response value "/0/longitude" is not empty
    And I verify if response value "/0/microMarket" is empty
    And I verify if response value "/0/organizationId" is not empty
    And I verify if response value "/0/productsVouchers/0/productCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productDescription" is not empty
    And I verify if response value "/0/productsVouchers/0/productPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/netProductPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherAuthorizationCode" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatusId" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatus" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherCode" is not empty
    And I verify if response value "/0/consumer/name" is not empty
    And I verify if response value "/0/consumer/documentType" is not empty
    And I verify if response value "/0/consumer/document" is not empty
    And I verify if response value "/0/consumer/email" is not empty
    And I verify if response value "/0/consumer/cellphone/areaCode" is not empty
    And I verify if response value "/0/consumer/cellphone/number" is not empty
    And I verify if response value "/0/distributor/document" is not empty
    And I verify if response value "/0/distributor/headquarterId" is empty
    And I verify if response value "/0/distributor/serviceChannel1" is not empty
    And I verify if response value "/0/distributor/serviceChannel2" is not empty
    And I verify if response value "/0/distributor/serviceChannel3" is not empty
    And I verify if response value "/0/distributor/phone1" is not empty
    And I verify if response value "/0/distributor/phone2" is not empty
    And I verify if response value "/0/distributor/territory" is not empty
    And I verify if response value "/0/distributor/voucherValidityDays" is not empty
    And I verify if response value "/0/distributor/generalMessage1" is not empty
    And I verify if response value "/0/distributor/generalMessage2" is not empty
    And I verify if response value "/0/distributor/generalMessage3" is not empty
    And I verify if response value "/0/distributor/generalMessage4" is not empty
    And I verify if response value "/0/distributor/generalMessage5" is not empty
    And Response code must be 200

    Examples: 
      | descricao                 | queryParameter             |
      | voucher-status DISPONIVEL | ?voucher-status=DISPONIVEL |
      | _offset 1                 | ?_offset=1                 |

  @Positivo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionType" is not empty
    And I verify if response value "/0/time" is not empty
    And I verify if response value "/0/nsu" is not empty
    And I verify if response value "/0/latitude" is not empty
    And I verify if response value "/0/longitude" is not empty
    And I verify if response value "/0/consumer/name" is not empty
    And I verify if response value "/0/consumer/documentType" is not empty
    And I verify if response value "/0/consumer/document" is not empty
    And I verify if response value "/0/consumer/email" is not empty
    And I verify if response value "/0/consumer/cellphone/areaCode" is not empty
    And I verify if response value "/0/consumer/cellphone/number" is not empty
    And I verify if response value "/0/distributor/document" is not empty
    And I verify if response value "/0/distributor/headquarterId" is empty
    And I verify if response value "/0/organizationId" is not empty
    And I verify if response value "/0/microMarket" is empty
    And I verify if response value "/0/productsVouchers/0/productCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productDescription" is not empty
    And I verify if response value "/0/productsVouchers/0/productPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/netProductPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherAuthorizationCode" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatusId" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatus" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherCode" is not empty
    And Response code must be 200

    Examples: 
      | descricao              | queryParameter                       |
      | transaction-type VENDA | ?transaction-type=${transactionType} |

  @Positivo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                                        | queryParameter                                                        |
      | end-date menor que beginDate                     | ?end-date=${beginDate}&begin-date=${endDate}                          |
      | distributor-document mascarado                   | ?distributor-document=454.010.358-02                                  |
      | distributor-document incorreto                   | ?distributor-document=45401035888                                     |
      | distributor-document inválido                    | ?distributor-document=1                                               |
      | nsu inválido                                     | ?nsu=CANCELAMENTOS                                                    |
      | voucher-authorization-code válido last twoDigits | ?voucher-authorization-code=11                                        |
      | voucher-authorization-code inválido              | ?voucher-authorization-code=CANCELAMENTOS                             |
      | consumer-email 1                                 | ?consumer-email=1                                                     |
      | consumer-email sem @                             | ?consumer-email=aline.dias.com                                        |
      | consumer-email sem .com                          | ?consumer-email=aline@sensedia                                        |
      | consumer-phone mascarado                         | ?consumer-phone=98272-2791                                            |
      | consumer-phone fixo                              | ?consumer-phone=38542714                                              |
      | consumer-phone-ddd 1                             | ?consumer-phone-ddd=1                                                 |
      | consumer-phone-ddd inválido                      | ?consumer-phone-ddd=00                                                |
      | consumer-phone inválido                          | ?consumer-phone=123                                                   |
      | voucher-status NAO_VENDIDO                       | ?voucher-status=NAO_VENDIDO                                           |
      | list-transactions-from-network true              | ?list-transactions-from-network=true&distributor-document=45401035802 |
      | voucher-status BONIFICACAO_INICIADA              | ?voucher-status=BONIFICACAO_INICIADA                                  |
      | voucher-status BONIFICADO_COM_RESTRICAO          | ?voucher-status=BONIFICADO_COM_RESTRICAO                              |
      | voucher-status BONIFICADO                        | ?voucher-status=BONIFICADO                                            |
      | voucher-status DESFEITO                          | ?voucher-status=DESFEITO                                              |

  @Positivo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionType" is not empty
    And I verify if response value "/0/time" is not empty
    And I verify if response value "/0/nsu" is not empty
    And I verify if response value "/0/latitude" is not empty
    And I verify if response value "/0/longitude" is not empty
    And I verify if response value "/0/productsVouchers/0/productCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productDescription" is not empty
    And I verify if response value "/0/productsVouchers/0/productPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/netProductPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherAuthorizationCode" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatusId" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatus" is not empty
    And I verify if response value "/0/consumer/name" is not empty
    And I verify if response value "/0/consumer/documentType" is not empty
    And I verify if response value "/0/consumer/document" is not empty
    And I verify if response value "/0/consumer/email" is not empty
    And I verify if response value "/0/consumer/cellphone/areaCode" is not empty
    And I verify if response value "/0/consumer/cellphone/number" is not empty
    And I verify if response value "/0/distributor/document" is not empty
    And I verify if response value "/0/distributor/headquarterId" is empty
    And Response code must be 200

    Examples: 
      | descricao                | queryParameter                                |
      | nsu minusculo            | ?nsu=744f86uy                                 |
      | nsu válido               | ?nsu=${nsu}                                   |
      | consumer-email maiusculo | ?consumer-email=WELLINGTON.MOURA@SENSEDIA.COM |
      | consumer-email válido    | ?consumer-email=${consumerEmail}              |

  @Positivo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionType" is not empty
    And I verify if response value "/0/time" is not empty
    And I verify if response value "/0/nsu" is not empty
    And I verify if response value "/0/latitude" is empty
    And I verify if response value "/0/longitude" is empty
    And I verify if response value "/0/organizationId" is empty
    And I verify if response value "/0/microMarket" is empty
    And I verify if response value "/0/productsVouchers/0/voucherCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productDescription" is not empty
    And I verify if response value "/0/productsVouchers/0/productPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/netProductPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherAuthorizationCode" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatusId" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatus" is not empty
    And I verify if response value "/0/consumer/name" is not empty
    And I verify if response value "/0/consumer/documentType" is not empty
    And I verify if response value "/0/consumer/document" is not empty
    And I verify if response value "/0/consumer/email" is empty
    And I verify if response value "/0/consumer/cellphone/areaCode" is not empty
    And I verify if response value "/0/consumer/cellphone/number" is not empty
    And I verify if response value "/0/distributor/document" is not empty
    And I verify if response value "/0/distributor/headquarterId" is empty
    And Response code must be 200

    Examples: 
      | descricao                     | queryParameter                   |
      | transaction-type CANCELAMENTO | ?transaction-type=CANCELAMENTO   |
      | voucher-status CANCELADO      | ?voucher-status=CANCELADO        |
      | consumer-phone válido         | ?consumer-phone=${consumerPhone} |

  @Positivo
  Scenario: Consultar com success true
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${tokenMatriz}"
    When I set GET api endpoint as "${endpoint}?success=true&transaction-type=VENDA"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionType" is not empty
    And I verify if response value "/0/time" is not empty
    And I verify if response value "/0/nsu" is not empty
    And I verify if response value "/0/latitude" is not empty
    And I verify if response value "/0/longitude" is not empty
    And I verify if response value "/0/consumer/name" is not empty
    And I verify if response value "/0/consumer/documentType" is not empty
    And I verify if response value "/0/consumer/document" is not empty
    And I verify if response value "/0/consumer/email" is not empty
    And I verify if response value "/0/consumer/cellphone/areaCode" is not empty
    And I verify if response value "/0/consumer/cellphone/number" is not empty
    And I verify if response value "/0/distributor/document" is not empty
    And I verify if response value "/0/distributor/headquarterId" is empty
    And I verify if response value "/0/organizationId" is not empty
    And I verify if response value "/0/microMarket" is empty
    And I verify if response value "/0/productsVouchers/0/voucherCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productDescription" is not empty
    And I verify if response value "/0/productsVouchers/0/productPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/netProductPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherAuthorizationCode" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatusId" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatus" is not empty
    And Response code must be 200

  @Positivo
  Scenario: Consultar com success false
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${tokenMatriz}"
    When I set GET api endpoint as "${endpoint}?success=false&begin-date=${beginDate}&end-date=${endDate}"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionType" is not empty
    And I verify if response value "/0/time" is not empty
    And I verify if response value "/0/nsu" is not empty
    And I verify if response value "/0/latitude" is not empty
    And I verify if response value "/0/longitude" is not empty
    And I verify if response value "/0/consumer/name" is not empty
    And I verify if response value "/0/consumer/documentType" is not empty
    And I verify if response value "/0/consumer/document" is not empty
    And I verify if response value "/0/consumer/email" is not empty
    And I verify if response value "/0/consumer/cellphone/areaCode" is not empty
    And I verify if response value "/0/consumer/cellphone/number" is not empty
    And I verify if response value "/0/distributor/document" is not empty
    And I verify if response value "/0/distributor/headquarterId" is empty
    And I verify if response value "/0/organizationId" is not empty
    And I verify if response value "/0/microMarket" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productDescription" is not empty
    And I verify if response value "/0/productsVouchers/0/productPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/netProductPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherAuthorizationCode" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatusId" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatus" is not empty
    And Response code must be 200

  @Positivo
  Scenario: Consultar com transactioType BLOQUEIO
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${tokenMatriz}"
    When I set GET api endpoint as "${endpoint}?transaction-type=BLOQUEIO"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/transactionType" is not empty
    And I verify if response value "/0/time" is not empty
    And I verify if response value "/0/nsu" is not empty
    And I verify if response value "/0/distributor/document" is not empty
    And I verify if response value "/0/distributor/headquarterId" is empty
    And I verify if response value "/0/productsVouchers/0/voucherCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productCode" is not empty
    And I verify if response value "/0/productsVouchers/0/productDescription" is not empty
    And I verify if response value "/0/productsVouchers/0/productPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/netProductPrice" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherAuthorizationCode" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatusId" is not empty
    And I verify if response value "/0/productsVouchers/0/voucherStatus" is not empty
    And I verify if response value "/0/orderCaseId" is not empty
    And I verify if response value "/0/channelName" is not empty
    And I verify if response value "/0/resale/document" is not empty
    And I verify if response value "/0/resale/code" is not empty
    And I verify if response value "/0/resale/siteCode" is not empty
    And Response code must be 200
