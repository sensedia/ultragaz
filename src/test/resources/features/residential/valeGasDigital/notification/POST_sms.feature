@POST_sms
Feature: POST_sms.feature
  Operação responsável por reenviar o(s) voucher(s) para o consumidor via SMS.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token_password.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev"
    #Given I use domain as "http://localhost:8001"
    And I use api name as "/notification/v1"
    And I save "/sms" as "endpointSms"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "19993120538" as "cellPhone"
    And I save "Mensagem bonitinha!" as "message"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}1"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar request sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario: Enviar request sem body
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Invalid request body."
    And Response code must be 400

  @Negativo
  Scenario: Enviar request sem body em pt
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Request body inválido."
    And Response code must be 400

  @Negativo
  Scenario Outline: Vender voucher <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<messageOne>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageTwo>"
    And I compare response value "/1/code" with "<code>"
    And I verify if response value "/2/message" is empty
    And I verify if response value "/2/code" is empty
    And Response code must be 400

    Examples: 
      | descricao            | body | language | code    | messageOne                      | messageTwo                    | 
      | com body vazio       | {}   | en-US    | 400.001 | Field message is required.     | Field cellPhone is required.     | 
      | com body vazio em pt | {}   | pt-BR    | 400.001 | O campo message é obrigatório. | O campo cellPhone é obrigatório. | 

  @Negativo
  Scenario Outline: Validar body <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <responseCode>
    And I compare response value "/0/message" with <responseDescription>
    And Response code must be <code>

    Examples: 
      | message                              | body                                                                                                                                                                                                       | language | responseCode | responseDescription                     | code |
      | com cellPhone inválido               | {"cellPhone":"string","message":"${message}"}                                                                                                                                                              | en-US    | "400.002"    | "Field cellPhone has an invalid format." |  400 |
      | com cellPhone incorreto              | {"cellPhone":"38542714","message":"${message}"}                                                                                                                                                            | en-US    | "400.002"    | "Field cellPhone has an invalid format." |  400 |
      | com cellPhone vazio                  | {"cellPhone":"","message":"${message}"}                                                                                                                                                                    | en-US    | "400.001"    | "Field cellPhone is required." |  400 |
      | com cellPhone nulo                   | {"cellPhone":null,"message":"${message}"}                                                                                                                                                                  | en-US    | "400.001"    | "Field cellPhone is required." |  400 |
      | sem cellPhone                        | {"message":"${message}"}                                                                                                                                                                                   | en-US    | "400.001"    | "Field cellPhone is required." |  400 |
      | com message inválido                 | {"cellPhone":"${cellPhone}","message":"Mensagem ultrapassando cento e sessenta caracteres. Mensagem ultrapassando cento e sessenta caracteres. Mensagem ultrapassando cento e sessenta caracteres. Um do"} | en-US    | "400.004"    | "Field message must have 160 characters limit." |  400 |  
      | com message vazio                    | {"cellPhone":"${cellPhone}","message":""}                                                                                                                                                                  | en-US    | "400.005"    | "Field message must have at least 5 characters." |  400 |
      | com message nulo                     | {"cellPhone":"${cellPhone}","message":null}                                                                                                                                                                | en-US    | "400.001"    | "Field message is required." |  400 |
      | sem message                          | {"cellPhone":"${cellPhone}"}                                                                                                                                                                               | en-US    | "400.001"    | "Field message is required." |  400 |
      | com cellPhone inválido em PT         | {"cellPhone":"string","message":"${message}"}                                                                                                                                                              | pt-BR    | "400.002"    | "O campo cellPhone tem o formato inválido." |  400 |
      | com cellPhone incorreto em PT        | {"cellPhone":"38542714","message":"${message}"}                                                                                                                                                            | pt-BR    | "400.002"    | "O campo cellPhone tem o formato inválido." |  400 |
      | com cellPhone vazio em PT            | {"cellPhone":"","message":"${message}"}                                                                                                                                                                    | pt-BR    | "400.002"    | "O campo cellPhone tem o formato inválido." |  400 |
      | com cellPhone nulo em PT             | {"cellPhone":null,"message":"${message}"}                                                                                                                                                                  | pt-BR    | "400.001"    | "O campo cellPhone é obrigatório." |  400 |
      | sem cellPhone em PT                  | {"message":"${message}"}                                                                                                                                                                                   | pt-BR    | "400.001"    | "O campo cellPhone é obrigatório." |  400 |
      | com message inválido em PT           | {"cellPhone":"${cellPhone}","message":"Mensagem ultrapassando cento e sessenta caracteres. Mensagem ultrapassando cento e sessenta caracteres. Mensagem ultrapassando cento e sessenta caracteres. Um do"} | pt-BR    | "400.004"    | "Campo message tem limite de 160 caracteres." |  400 |
      | com message vazio em PT              | {"cellPhone":"${cellPhone}","message":""}                                                                                                                                                                  | pt-BR    | "400.005"    | "Campo message deve ter pelo menos 5 caracteres." |  400 |
      | com message nulo em PT               | {"cellPhone":"${cellPhone}","message":null}                                                                                                                                                                | pt-BR    | "400.001"    | "O campo message é obrigatório." |  400 |
      | sem message em PT                    | {"cellPhone":"${cellPhone}"}                                                                                                                                                                               | pt-BR    | "400.001"    | "O campo message é obrigatório." |  400 |

  @Negativo
  Scenario Outline: Validar body <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <responseCode>
    And I compare response value "/message" with <responseDescription>
    And Response code must be 422

    Examples: 
      | message | body | language | responseCode | responseDescription | code |

  @Positivo
  Scenario Outline: Validar SMS  <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<token>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointSms}"
    Then I get response body 
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | message     | body                                                																														 | token    |
      | com sucesso | {"cellPhone":"${cellPhone}","message":"${message}"} 																														 | ${access_token} |
      | com message com carac especial       | {"cellPhone":"${cellPhone}","message":"Promoção avó à 2º instância &cia 100%. ; ? / °"} | ${access_token} |  
      | com message com carac especial em PT | {"cellPhone":"${cellPhone}","message":"Promoção avó à 2º instância &cia 100%. ; ? / °"} | ${access_token} |  