Feature: GET_vouchers-store_{cacheKey}_save.feature
  Operação responsável por retornar a planilha com os detalhes do vale gás.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/voucher/v1"
    And I save "/vouchers" as "endpointVouchers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "596913","customerSiteId":"658880"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/voucher/v1"
    And I save "/vouchers-store" as "endpointVouchers"
    And I save "5" as "organizationId"
    And I save "026053938000163" as "storeDocument"
    And I save "PENDENTE_BONIFICACAO" as "situation"
    And I save "BONIFICADO" as "situationNF"
    And I save "2020-03-01" as "beginDate"
    And I save "2020-03-07" as "endDate"
    And I save "4612758" as "customerSiteId"
    And I save "4178502" as "customerId"
    And I save "20" as "_limit"
    And I save "0" as "_offset"

  @PreRequest
  Scenario Outline: Obter xCacheKey
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And I save response header "x-cache-key" as "xCacheKey"
    And Response code must be 200

    Examples: 
      | queryParameter                                                                                                                                                               |
      | ?organization-id=5&begin-date=${beginDate}&end-date=${endDate}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation} |

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointVouchers}/${xCacheKey}/save"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpointVouchers}/${xCacheKey}/save"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointVouchers}/${xCacheKey}/save"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}1"
    When I set GET api endpoint as "${endpointVouchers}/${xCacheKey}/save"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Consultar planilha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpointVouchers}/<xCacheKey>/save"
    Then I get response body
    And I get response status code
    And Response code must be 404

    Examples: 
      | descricao              | xCacheKey       |
      | com xCacheKey vazio    | ""              |
      | com xCacheKey inválido | "${xCacheKey}1" |
      | com xCacheKey nulo     | null            |
      
  @Negativo
  Scenario Outline: Consultar planilha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpointVouchers}/<xCacheKey>/save"
    Then I get response body
    And I get response status code
    And Response code must be 400

    Examples: 
      | descricao              | xCacheKey       |
      | sem xCacheKey          |                 |
   
  @Positivo
  Scenario: Consultar planilha 
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpointVouchers}/${xCacheKey}/save"
    Then I get response body
    And I get response status code
    And I verify if response body is not empty
    And Response code must be 200
