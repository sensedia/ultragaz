@DELETE_vouchers_{voucherId}_orders
Feature: DELETE_vouchers_{voucherId}_orders.feature
  Operação responsável por desvincular o vale gás de um pedido liberando-o para ser usado. (Desbloqueio)

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/voucher/v1"
    And I save "/vouchers" as "endpointVouchers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "010598450300016" as "code"
    And I save "0688899" as "autorizationCode"
    And I save "596913" as "customerId"
    And I save "658880" as "customerSiteId"

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteId}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @PreRequest
  Scenario: Vincular vale gás digital a um pedido
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}"
    When I set POST api endpoint as "${endpointVouchers}/${code}/orders"
    Then I get response body
    And I get response status code
    And Response code must be 201

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set DELETE api endpoint as "${endpointVouchers}/${code}/orders"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set DELETE api endpoint as "${endpointVouchers}/${code}/orders"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    When I set DELETE api endpoint as "${endpointVouchers}/${code}/orders"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}1"
    When I set DELETE api endpoint as "${endpointVouchers}/${code}/orders"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set DELETE api endpoint as "${endpointVouchers}/<voucherId>/orders"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | parameter                       | voucherId        |
      | code de 16 dígitos              | 0105984503000166 |
      | code de 14 dígitos              |   01059845030001 |
      | code_authorizarion de 6 dígitos |           068889 |
      | code_authorizarion de 8 dígitos |         06888999 |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as <language>
    When I set DELETE api endpoint as "${endpointVouchers}/2290038/orders"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | parameter                         | code      | message                                   | language |
      | que não é possível desbloquear    | "422.017" | "It was not possible to unblock voucher." | "en-US"  |
      | que não é possível desbloquear pt | "422.017" | "Não foi possivel desbloquear o voucher." | "pt-BR"  |

  @Positivo
  Scenario Outline: Desvincular vale gás digital a um pedido por code
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "en-US"
    When I set DELETE api endpoint as "${endpointVouchers}/${code}orders"
    Then I get response body
    And I get response status code
    And Response code must be 201

  @Positivo
  Scenario Outline: Desvincular vale gás digital a um pedido por autorizationCode
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "en-US"
    When I set DELETE api endpoint as "${endpointVouchers}/${autorizationCode}orders"
    Then I get response body
    And I get response status code
    And Response code must be 201
