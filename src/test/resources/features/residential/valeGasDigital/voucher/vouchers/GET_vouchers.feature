Feature: GET_vouchers.feature
  Obter vouchers do vale gás digital.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/voucher/v1"
    And I save "/vouchers" as "endpointVouchers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "655881" as "customerSiteIdFilial"
    And I save "596913" as "customerIdFilial"

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerIdFilial}","customerSiteId":"${customerSiteIdFilial}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/voucher/v1"
    And I save "/vouchers" as "endpointVouchers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "13181643" as "zipCode"
    And I save "VENDA" as "transactionType"
    And I save "45401035802" as "distributorDocument"
    And I save "NAO_VENDIDO" as "voucherStatus"
    And I save "KM283V49" as "nsu"
    And I save "1130193" as "code"
    And I save "4178788" as "voucherAuthorizationCode"
    And I save "658880" as "consumerDocument"
    And I save "19" as "consumerPhoneDdd"
    And I save "982722791" as "consumerPhone"
    And I save "john.doe@provider.com" as "consumerEmail"
    And I save "2019-10-01 00:00:00" as "beginDate"
    And I save "2020-12-31 23:59:59" as "endDate"
    And I save "20" as "_limit"
    And I save "0" as "_offset"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}1"
    When I set GET api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/0/message" with "<message>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                 | queryParameter  | code    | message                                          | language |
      | nsu vazio                 | ?nsu=           | 400.018 | At least one filter is required.                 | en-US    |
      | code vazio                | ?code=          | 400.018 | At least one filter is required.                 | en-US    |
      | sem query parameter       |                 | 400.018 | At least one filter is required.                 | en-US    |
      | _limit inválido           | ?_limit=string  | 400.004 | Field _limit has an invalid format or value.     | en-US    |
      | _offset inválido          | ?_offset=string | 400.004 | Field _offset has an invalid format or value.    | en-US    |
      | _limit 0                  | ?_limit=0       | 400.016 | Field _limit value must be at least 1.           | en-US    |
      | nsu vazio em pt           | ?nsu=           | 400.018 | É necessário informar pelo menos um dos filtros. | pt-BR    |
      | code vazio em pt          | ?code=          | 400.018 | É necessário informar pelo menos um dos filtros. | pt-BR    |
      | sem query parameter em pt |                 | 400.018 | É necessário informar pelo menos um dos filtros. | pt-BR    |
      | _limit inválido em pt     | ?_limit=string  | 400.004 | O campo _limit tem o formato ou valor inválido.  | pt-BR    |
      | _offset inválido em pt    | ?_offset=string | 400.004 | O campo _offset tem o formato ou valor inválido. | pt-BR    |
      | _limit 0 em pt            | ?_limit=0       | 400.016 | O valor do campo _limit deve ser pelo menos 1.   | pt-BR    |

  @Negativo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao      | queryParameter |
      | nsu incorreto  | ?nsu=${nsu}1   |
      | code incorreto | ?code=${code}1 |

  @Positivo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/code" is not empty
    And I verify if response value "/0/saleDate" is not empty
    And I verify if response value "/0/validationDate" is empty
    And I verify if response value "/0/availability/value" is not empty
    And I verify if response value "/0/availability/description" is not empty
    And I verify if response value "/0/product/code" is not empty
    And I verify if response value "/0/product/description" is not empty
    And Response code must be 200

    Examples: 
      | descricao            | queryParameter                 |
      | com _limit e _offset | ?_limit=5&_offset=0&nsu=${nsu} |
      | _limit vazio         | ?nsu=${nsu}&_limit=            |
      | _offset válido       | ?nsu=${nsu}&_offset=${_offset} |
      | _offset vazio        | ?nsu=${nsu}&_offset=           |
      | _offset 0            | ?nsu=${nsu}&_offset=0          |
      | _limit válido        | ?nsu=${nsu}&_limit=${_limit}   |
      | _limit 1             | ?nsu=${nsu}&_limit=1           |
      | nsu                  | ?nsu=${nsu}                    |
      | code                 | ?code=${code}                  |
      | code e nsu           | ?code=${code}&nsu=${nsu}       |
      | code curto e nsu     | ?code=${code}&nsu=${nsu}       |
      | code de outro, e nsu | ?code=${code}&nsu=${nsu}       |
