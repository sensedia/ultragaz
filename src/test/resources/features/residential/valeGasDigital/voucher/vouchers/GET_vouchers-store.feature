Feature: GET_vouchers-store.feature
  Obter vouchers do vale gás digital bonificados ou pendentes de bonificação de uma loja/revenda.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/voucher/v1"
    And I save "/vouchers" as "endpointVouchers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "596913","customerSiteId":"658880"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/voucher/v1"
    And I save "/vouchers-store" as "endpointVouchers"
    And I save "5" as "organizationId"
    And I save "026053938000163" as "storeDocument"
    And I save "PENDENTE_BONIFICACAO" as "situation"
    And I save "BONIFICADO" as "situationNF"
    And I save "2020-03-01" as "beginDate"
    And I save "2020-03-07" as "endDate"
    And I save "4612758" as "customerSiteId"
    And I save "4178502" as "customerId"
    And I save "20" as "_limit"
    And I save "0" as "_offset"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}1"
    When I set GET api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/0/message" with "<message>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                           | queryParameter                                                                                                                                                                               | code    | message                                                  | language |
      | sem query parameter                 |                                                                                                                                                                                              | 400.001 | Field organization-id is required.                       | en-US    |
      | organization-id vazio               | ?organization-id=                                                                                                                                                                            | 400.004 | Field organization-id has an invalid format or value.    | en-US    |
      | organization-id inválido            | ?organization-id=1&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                 | 400.010 | Field organization-id has an invalid value.              | en-US    |
      | sem organization-id                 | ?store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                                   | 400.001 | Field organization-id is required.                       | en-US    |
      | sem store-document                  | ?organization-id=${organizationId}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                                 | 400.001 | Field store-document is required.                        | en-US    |
      | sem customer-id                     | ?organization-id=${organizationId}&store-document=${storeDocument}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                           | 400.001 | Field customer-id is required.                           | en-US    |
      | sem address-id                      | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                              | 400.001 | Field address-id is required.                            | en-US    |
      | sem situation                       | ?organization-id=${organizationId}&store-document=${storeDocument}&address-id=${customerSiteId}&customer-id=${customerId}&begin-date=${beginDate}&end-date=${endDate}                        | 400.001 | Field situation is required.                             | en-US    |
      | situation inválido                  | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&begin-date=${beginDate}&end-date=${endDate}&situation=1            | 400.004 | Field situation has an invalid format or value.          | en-US    |
      | begin-date inválido                 | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=a            | 400.004 | Field begin-date has an invalid format or value.         | en-US    |
      | begin-date formato incorreto        | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=2020/01/01   | 400.004 | Field begin-date has an invalid format or value.         | en-US    |
      | begin-date dia inválido             | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=2020-01-34   | 400.004 | Field begin-date has an invalid format or value.         | en-US    |
      | begin-date mes inválido             | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=2020-31-01   | 400.004 | Field begin-date has an invalid format or value.         | en-US    |
      | begin-date ano inválido             | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=020-01-00    | 400.004 | Field begin-date has an invalid format or value.         | en-US    |
      | end-date inválido                   | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=a          | 400.004 | Field end-date has an invalid format or value.           | en-US    |
      | end-date formato incorreto          | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=2020/01/01 | 400.004 | Field end-date has an invalid format or value.           | en-US    |
      | end-date dia inválido               | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=2020-01-34 | 400.004 | Field end-date has an invalid format or value.           | en-US    |
      | end-date mes inválido               | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=2020-31-01 | 400.004 | Field end-date has an invalid format or value.           | en-US    |
      | end-date ano inválido               | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=020-01-00  | 400.004 | Field end-date has an invalid format or value.           | en-US    |
      | end-date maior que begin-date       | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&begin-date=${endDate}&end-date=${beginDate}&situation=${situation} | 400.006 | Invalid date range.                                      | en-US    |
      | sem end-date                        | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}                     | 400.001 | Field end-date is required.                              | en-US    |
      | begin-date vazio                    | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=             | 400.009 | Field begin-date must not be empty.                      | en-US    |
      | end-date vazio                      | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=           | 400.009 | Field end-date must not be empty.                        | en-US    |
      | sem begin-date                      | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&end-date=${endDate}&situation=${situation}                         | 400.001 | Field begin-date is required.                            | en-US    |
      | sem query parameter em pt           |                                                                                                                                                                                              | 400.001 | O campo organization-id é obrigatório.                   | pt-BR    |
      | organization-id vazio em pt         | ?organization-id=                                                                                                                                                                            | 400.004 | O campo organization-id tem o formato ou valor inválido. | pt-BR    |
      | organization-id inválido em pt      | ?organization-id=1&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                 | 400.010 | Campo organization-id tem o valor inválido.              | pt-BR    |
      | sem organization-id em pt           | ?store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                                   | 400.001 | O campo organization-id é obrigatório.                   | pt-BR    |
      | sem store-document em pt            | ?organization-id=${organizationId}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                                 | 400.001 | O campo store-document é obrigatório.                    | pt-BR    |
      | sem customer-id em pt               | ?organization-id=${organizationId}&store-document=${storeDocument}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                           | 400.001 | O campo customer-id é obrigatório.                       | pt-BR    |
      | sem address-id em pt                | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}                              | 400.001 | O campo address-id é obrigatório.                        | pt-BR    |
      | sem situation em pt                 | ?organization-id=${organizationId}&store-document=${storeDocument}&address-id=${customerSiteId}&customer-id=${customerId}&begin-date=${beginDate}&end-date=${endDate}                        | 400.001 | O campo situation é obrigatório.                         | pt-BR    |
      | situation inválido em pt            | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&begin-date=${beginDate}&end-date=${endDate}&situation=1            | 400.004 | O campo situation tem o formato ou valor inválido.       | pt-BR    |
      | begin-date inválido em pt           | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=a            | 400.004 | O campo begin-date tem o formato ou valor inválido.      | pt-BR    |
      | begin-date formato incorreto em pt  | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=2020/01/01   | 400.004 | O campo begin-date tem o formato ou valor inválido.      | pt-BR    |
      | begin-date dia inválido em pt       | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=2020-01-34   | 400.004 | O campo begin-date tem o formato ou valor inválido.      | pt-BR    |
      | begin-date mes inválido em pt       | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=2020-31-01   | 400.004 | O campo begin-date tem o formato ou valor inválido.      | pt-BR    |
      | begin-date ano inválido em pt       | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=020-01-00    | 400.004 | O campo begin-date tem o formato ou valor inválido.      | pt-BR    |
      | end-date inválido em pt             | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=a          | 400.004 | O campo end-date tem o formato ou valor inválido.        | pt-BR    |
      | end-date formato incorreto em pt    | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=2020/01/01 | 400.004 | O campo end-date tem o formato ou valor inválido.        | pt-BR    |
      | end-date dia inválido em pt         | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=2020-01-34 | 400.004 | O campo end-date tem o formato ou valor inválido.        | pt-BR    |
      | end-date mes inválido em pt         | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=2020-31-01 | 400.004 | O campo end-date tem o formato ou valor inválido.        | pt-BR    |
      | end-date ano inválido em pt         | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=020-01-00  | 400.004 | O campo end-date tem o formato ou valor inválido.        | pt-BR    |
      | end-date maior que begin-date em pt | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&begin-date=${endDate}&end-date=${beginDate}&situation=${situation} | 400.006 | Intervalo de datas inválido.                             | pt-BR    |
      | sem end-date em pt                  | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}                     | 400.001 | O campo end-date é obrigatório.                          | pt-BR    |
      | begin-date vazio em pt              | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&end-date=${endDate}&begin-date=             | 400.009 | Campo begin-date não pode ser vazio.                     | pt-BR    |
      | end-date vazio em pt                | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&begin-date=${beginDate}&end-date=           | 400.009 | Campo end-date não pode ser vazio.                       | pt-BR    |
      | sem begin-date em pt                | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&end-date=${endDate}&situation=${situation}                         | 400.001 | O campo begin-date é obrigatório.                        | pt-BR    |

  @Negativo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                | queryParameter                                                                                                                                                                          |
      | store-document vazio     | ?organization-id=${organizationId}&begin-date=${beginDate}&end-date=${endDate}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&store-document=            |
      | store-document inválido  | ?organization-id=${organizationId}&begin-date=${beginDate}&end-date=${endDate}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}&store-document=1           |
      | store-document incorreto | ?organization-id=${organizationId}&begin-date=${beginDate}&end-date=${endDate}&store-document=11122211122&address-id=${customerSiteId}&customer-id=${customerId}&situation=${situation} |
      | customer-id vazio        | ?organization-id=${organizationId}&begin-date=${beginDate}&end-date=${endDate}&store-document=${storeDocument}&address-id=${customerSiteId}&situation=${situation}&customer-id=         |
      | customer-id inválido     | ?organization-id=${organizationId}&store-document=${storeDocument}&begin-date=${beginDate}&end-date=${endDate}&address-id=${customerSiteId}&situation=${situation}&customer-id=1        |
      | address-id vazio         | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&begin-date=${beginDate}&end-date=${endDate}&situation=${situation}&address-id=             |
      | address-id inválido      | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&situation=${situation}&begin-date=${beginDate}&end-date=${endDate}&address-id=1231         |
      | situation vazio          | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&begin-date=${beginDate}&end-date=${endDate}&situation=        |
      | organization-id baiana   | ?organization-id=430&begin-date=${beginDate}&end-date=${endDate}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}          |

  @Positivo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/voucherNumber" is not empty
    And I verify if response value "/0/authorizationCode" is not empty
    And I verify if response value "/0/status/value" is not empty
    And I verify if response value "/0/status/description" is empty
    And I verify if response value "/0/voucherType" is not empty
    And I verify if response value "/0/couponDate" is not empty
    And I verify if response value "/0/blockDate" is empty
    And I verify if response value "/0/bonificationDate" is empty
    And I verify if response value "/0/validationDate" is not empty
    And I verify if response value "/0/distributor/name" is not empty
    And I verify if response value "/0/distributor/address" is not empty
    And I verify if response value "/0/deliveryInvoice" is not empty
    And I verify if response value "/0/product/code" is not empty
    And I verify if response value "/0/product/description" is not empty
    And Response code must be 200

    Examples: 
      | descricao                      | queryParameter                                                                                                                                                                                       |
      | organization-id ultragaz       | ?organization-id=5&begin-date=${beginDate}&end-date=${endDate}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&situation=${situation}                         |
      | situation PENDENTE_BONIFICACAO | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&begin-date=${beginDate}&end-date=${endDate}&situation=PENDENTE_BONIFICACAO |

  @Positivo
  Scenario Outline: Consultar com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/voucherNumber" is not empty
    And I verify if response value "/0/authorizationCode" is empty
    And I verify if response value "/0/status/value" is not empty
    And I verify if response value "/0/status/description" is empty
    And I verify if response value "/0/voucherType" is not empty
    And I verify if response value "/0/couponDate" is not empty
    And I verify if response value "/0/blockDate" is empty
    And I verify if response value "/0/bonificationDate" is not empty
    And I verify if response value "/0/validationDate" is not empty
    And I verify if response value "/0/distributor/name" is not empty
    And I verify if response value "/0/distributor/address" is not empty
    And I verify if response value "/0/deliveryInvoice" is not empty
    And I verify if response value "/0/product/code" is not empty
    And I verify if response value "/0/product/description" is not empty
    And Response code must be 200

    Examples: 
      | descricao            | queryParameter                                                                                                                                                                             |
      | situation BONIFICADO | ?organization-id=${organizationId}&store-document=${storeDocument}&customer-id=${customerId}&address-id=${customerSiteId}&begin-date=${beginDate}&end-date=${endDate}&situation=${situationNF} |
