Feature: GET_vouchers_{voucherId}.feature
  Obter responsável por recuperar um vale gás específico.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/voucher/v1"
    And I save "/vouchers" as "endpointVouchers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "0103337" as "voucherId"

  @Positivo
  Scenario: Consultar vouchers
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointVouchers}/${voucherId}"
    Then I get response body
    And I get response status code
    And I compare response value "/status/value" with "2"
    And I compare response value "/status/description" with "Vendido"
    And I compare response value "/authorizationCode" with "${voucherId}"
    And I compare response value "/product/codeProduct" with "0110035"
    And I compare response value "/saleDate" with "28-05-2020"
    And I compare response value "/order/validationDate" with ""
    And Response code must be 200

  @Negativo
  Scenario: Consultar vouchers por voucherId incorreto
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointVouchers}/1"
    Then I get response body
    And I get response status code
    And I verify if response value "/status/value" is empty
    And I verify if response value "/status/description" is empty
    And I verify if response value "/authorizationCode" is empty
    And I verify if response value "/product/codeProduct" is empty
    And I verify if response value "/saleDate" is empty
    And I verify if response value "/order/validationDate" is empty
    And Response code must be 200

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointVouchers}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401
