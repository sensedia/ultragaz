@POST_vouchers_{voucherId}_orders
Feature: POST_vouchers_{voucherId}_orders.feature
  Operação responsável por vincular um vale gás a um pedido impedindo que seja utilizado novamente. (Bloqueio)

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/voucher/v1"
    And I save "/vouchers" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "string" as "caseId"
    And I save "-3" as "validationChannel"
    And I save "3497169" as "codeResale"
    And I save "7564337" as "addressValidation"
    And I save "27369839000158" as "documentResale"
    And I save "Consumidor Final" as "userType"
    And I save "0110035" as "codeProduct"
    And I save "010598450300016" as "code"
    And I save "0688899" as "autorization_code"
    And I save "596913" as "customerId"
    And I save "658880" as "customerSiteId"

  @PreRequest
  Scenario: Gerar Token de Distribuidor
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteId}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @Negativo
  Scenario: Enviar requisição sem client_id
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "${endpoint}/${code}/orders"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "${endpoint}/${code}/orders"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"status":""}"
    When I set PATCH api endpoint as "${endpoint}/${code}/orders"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set POST api endpoint as "${endpoint}/${code}/orders"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    When I set POST api endpoint as "${endpoint}/${code}/orders"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                  | language |
      | sem body       | "Invalid request body."  | "en-US"  |
      | sem body em pt | "Request body inválido." | "pt-BR"  |

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "pt-BR"
    And I set request body as "{"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}"
    When I set POST api endpoint as "${endpoint}/<voucherId>/orders"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | parameter                       | voucherId        |
      | code de 16 dígitos              | 0105984503000166 |
      | code de 14 dígitos              |   01059845030001 |
      | code_authorizarion de 6 dígitos |           068889 |
      | code_authorizarion de 8 dígitos |         06888999 |

  @Negativo
  Scenario Outline: Validar body <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpoint}/${code}/orders"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 400

    Examples: 
      | parameter                   | body                                                                                                                                                                                      | language | code      | message                                |
      | sem caseId                  | {"validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}                   | en-US    | "412.001" | "Field caseId is required."            |
      | com caseId vazio            | {"caseId":"","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}       | en-US    | "412.001" | "Field caseId is required."            |
      | com caseId nulo             | {"caseId":null,"validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}     | en-US    | "412.001" | "Field caseId is required."            |
      | sem validationChannel       | {"caseId":"string","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}                          | en-US    | "412.001" | "Field validationChannel is required." |
      | com validationChannel vazio | {"caseId":"string","validationChannel":"","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}   | en-US    | "412.001" | "Field validationChannel is required." |
      | com validationChannel nulo  | {"caseId":"string","validationChannel":null,"codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"} | en-US    | "412.001" | "Field validationChannel is required." |
      | sem codeResale              | {"caseId":"string","validationChannel":"-3","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}                        | en-US    | "412.001" | "Field codeResale is required."        |
      | com codeResale vazio        | {"caseId":"string","validationChannel":"-3","codeResale":"","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}        | en-US    | "412.001" | "Field codeResale is required."        |
      | com codeResale nulo         | {"caseId":"string","validationChannel":"-3","codeResale":null,"addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}      | en-US    | "412.001" | "Field codeResale is required."        |
      | sem addressValidation       | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}                               | en-US    | "412.001" | "Field addressValidation is required." |
      | com addressValidation vazio | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}        | en-US    | "412.001" | "Field addressValidation is required." |
      | com addressValidation nulo  | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":null,"documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"}      | en-US    | "412.001" | "Field addressValidation is required." |
      | sem documentResale          | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","userType":"Consumidor Final","codeProduct":"0110035"}                                   | en-US    | "412.001" | "Field documentResale is required."    |
      | com documentResale vazio    | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"","userType":"Consumidor Final","codeProduct":"0110035"}               | en-US    | "412.001" | "Field documentResale is required."    |
      | com documentResale nulo     | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":null,"userType":"Consumidor Final","codeProduct":"0110035"}             | en-US    | "412.001" | "Field documentResale is required."    |
      | sem userType                | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","codeProduct":"0110035"}                               | en-US    | "412.001" | "Field userType is required."          |
      | com userType vazio          | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"","codeProduct":"0110035"}                 | en-US    | "412.001" | "Field userType is required."          |
      | com userType nulo           | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":null,"codeProduct":"0110035"}               | en-US    | "412.001" | "Field userType is required."          |
      | sem codeProduct             | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final"}                         | en-US    | "412.001" | "Field codeProduct is required."       |
      | com codeProduct vazio       | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":""}        | en-US    | "412.001" | "Field codeProduct is required."       |
      | com codeProduct nulo        | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":null}      | en-US    | "412.001" | "Field codeProduct is required."       |

  @Negativo
  Scenario Outline: Bloquear voucher <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as <language>
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpoint}/<code>/orders"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <codeErr>
    And I compare response value "/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao              | body                                                                                                                                                                                      | code    | language | codeErr   | message                                 |
      | sem cadastro na AWS    | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"} | 4818058 | "en-US"  | "422.016" | "It was not possible to block voucher." |
      | sem cadastro na AWS pt | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"} | 4818058 | "pt-BR"  | "422.016" | "Não foi possivel bloquear o voucher."  |

  @Positivo
  Scenario Outline: Vincular vale gás digital a um pedido <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpoint}/<voucherId>/orders"
    Then I get response body
    And I get response status code
    And Response code must be 201

    Examples: 
      | parameter            | body                                                                                                                                                                                      | voucherId       |
      | por code             | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"} | 010598450300016 |
      | por autorizationCode | {"caseId":"string","validationChannel":"-3","codeResale":"3497169","addressValidation":"7564337","documentResale":"27369839000158","userType":"Consumidor Final","codeProduct":"0110035"} |         0688899 |
