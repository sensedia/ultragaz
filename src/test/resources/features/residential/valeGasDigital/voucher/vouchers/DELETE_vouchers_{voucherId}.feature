@DELETE_vouchers_{voucherId}.feature
Feature: DELETE_vouchers_{voucherId}.feature
  Operação responsável por cancelar voucher

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/voucher/v1"
    And I save "/vouchers" as "endpointVouchers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "13181643" as "zipCode"
    And I save "596913" as "customerId"
    And I save "655860" as "customerSiteIdFilial"
    And I save "655881" as "customerSiteIdCodeCompleto"
    And I save "DINHEIRO" as "paymentMethod"
    And I save "Nome Consumer" as "nameConsumer"
    And I save "93751450009" as "documentConsumer"
    And I save "jaqueline.souza@sensedia" as "emailConsumer"
    And I save "19" as "areaCode"
    And I save "982911139" as "number"
    And I save "codeProduct" as "codeProduct"
    And I save "50.50" as "priceProduct"
    And I save "2" as "quantity"
    And I save "-23.4810444" as "latitude"
    And I save "-46.7421504" as "longitude"

  @PreRequest
  Scenario: Gerar Token de Distribuidor com code completo
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteIdCodeCompleto}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @PreRequest
  Scenario: Gerar Token de Distribuidor com code incompleto
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteIdFilial}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "tokenIncompleto"
    And Response code must be 201

  @PreRequest
  Scenario: Criar voucher com code incompleto
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${tokenIncompleto}"
    And I set request body as "{"userType":"FILIAL","products":[{"codeProduct":"0110035","priceProduct":100.00,"netPriceProduct":90.00,"quantity":1}],"paymentMethod":"BOLETO","couponCode":"WE37DKE38","couponDiscount":15.5,"amount":100.00,"netAmount":90.00,"consumer":{"nameConsumer":"John Doe","documentTypeConsumer":"CPF","documentConsumer":83606026030,"emailConsumer":"john.doe@provider.com","cellphoneConsumer":{"areaCode":19,"number":${number}}},"location":{"latitude":${latitude},"longitude":${longitude}}}"
    When I set POST api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And I save response value "/nsu" as "nsuIncompleto"
    And I save response value "/vouchers/0/code" as "codeIncompleto"
    And Response code must be 201

  @PreRequest
  Scenario Outline: Criar voucher com code completo
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request body as "{"userType":"FILIAL","products":[{"codeProduct":"0110035","priceProduct":37,"quantity":2}],"paymentMethod":"BOLETO","couponCode":"WE37DKE38","couponDiscount":15.5,"amount":74.00,"consumer":{"nameConsumer":"John Doe","documentTypeConsumer":"CPF","documentConsumer":83606026030,"emailConsumer":"john.doe@provider.com","cellphoneConsumer":{"areaCode":19,"number":${number}}},"location":{"latitude":${latitude},"longitude":${longitude}}}"
    When I set POST api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And I save response value "/nsu" as "nsu<id>"
    And I save response value "/vouchers/0/code" as "code<id>"
    And I save response value "/vouchers/1/code" as "code2.<id>"
    And Response code must be 201

    Examples: 
      | id |
      |  1 |
      |  2 |
      |  3 |
      |  4 |
      |  5 |
      |  6 |
      |  7 |
      |  8 |
      |  9 |
      | 10 |
      | 11 |
      | 12 |
      | 13 |
      | 14 |
      | 15 |
      | 16 |
      | 17 |
      | 18 |
      | 19 |
      | 20 |
      | 21 |
      | 22 |
      | 23 |
      | 24 |
      | 25 |
      | 26 |
      | 27 |
      | 28 |

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}1"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar request sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. |

  @Negativo
  Scenario: Enviar request sem body
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Invalid request body."
    And Response code must be 400

  @Negativo
  Scenario: Enviar request sem body em pt
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Request body inválido."
    And Response code must be 400

  @Negativo
  Scenario Outline: Deletar voucher <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And I compare response value "/1/message" with <messageTwo>
    And I compare response value "/1/code" with <code>
    And I verify if response value "/2/message" is empty
    And I verify if response value "/2/code" is empty
    And Response code must be 400

    Examples: 
      | descricao            | body | code      | message                                 | messageTwo                                  | language |
      | com body vazio       | {}   | "400.001" | "Field cancellationReason is required." | "Field userType is required."               | en-US    |
      | com body vazio em pt | {}   | "400.001" | "O campo userType é obrigatório."       | "O campo cancellationReason é obrigatório." | pt-BR    |

  @Negativo
  Scenario Outline: Deletar voucher <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And I verify if response value "/1/message" is empty
    And I verify if response value "/1/code" is empty
    And Response code must be 400

    Examples: 
      | descricao                               | body                                                                                                                                                                                                                                                                                                                                                                                                               | code      | message                                                      | language |
      | com cancellationReason vazio            | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"","note":"string"}                                                                                                                                                                                                                                                                                                                      | "400.001" | "Field cancellationReason is required."                      | en-US    |
      | com cancellationReason nulo             | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":null,"note":"string"}                                                                                                                                                                                                                                                                                                                    | "400.001" | "Field cancellationReason is required."                      | en-US    |
      | com cancellationReason inválido         | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGAS","note":"string"}                                                                                                                                                                                                                                                                                                       | "400.004" | "Field cancelationReason has an invalid format or value."    | en-US    |
      | com cancellationReason minusculo        | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"demora_entrega","note":"string"}                                                                                                                                                                                                                                                                                                        | "400.004" | "Field cancelationReason has an invalid format or value."    | en-US    |
      | sem cancellationReason                  | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"note":"string"}                                                                                                                                                                                                                                                                                                                                              | "400.001" | "Field cancellationReason is required."                      | en-US    |
      | com cancellationReason 1                | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"1","note":"string"}                                                                                                                                                                                                                                                                                                                     | "400.004" | "Field cancelationReason has an invalid format or value."    | en-US    |
      | com note maior que 300 caracteres       | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 car"} | "400.014" | "Field note must have 300 characters limit."                 | en-US    |
      | com userType vazio                      | {"userType":"","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                              | "400.001" | "Field userType is required."                                | en-US    |
      | com userType nulo                       | {"userType":null,"vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                            | "400.001" | "Field userType is required."                                | en-US    |
      | com userType inválido                   | {"userType":"FILIALs","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                       | "400.004" | "Field userType has an invalid format or value."             | en-US    |
      | sem userType                            | {"vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                                            | "400.001" | "Field userType is required."                                | en-US    |
      | com userType 1                          | {"userType":"1","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                             | "400.004" | "Field userType has an invalid format or value."             | en-US    |
      | com code vazio                          | {"userType":"FILIAL","vouchers":[{"code":""}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                               | "400.015" | "Voucher list contains null or empty values."                | en-US    |
      | com code nulo                           | {"userType":"FILIAL","vouchers":[{"code":null}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                             | "400.015" | "Voucher list contains null or empty values."                | en-US    |
      | sem code                                | {"userType":"FILIAL","vouchers":[{}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                                        | "400.015" | "Voucher list contains null or empty values."                | en-US    |
      | com cancellationReason vazio em PT      | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"","note":"string"}                                                                                                                                                                                                                                                                                                                      | "400.001" | "O campo cancellationReason é obrigatório."                  | pt-BR    |
      | com cancellationReason nulo em PT       | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":null,"note":"string"}                                                                                                                                                                                                                                                                                                                    | "400.001" | "O campo cancellationReason é obrigatório."                  | pt-BR    |
      | com cancellationReason inválido em PT   | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGAS","note":"string"}                                                                                                                                                                                                                                                                                                       | "400.004" | "O campo cancelationReason tem o formato ou valor inválido." | pt-BR    |
      | com cancellationReason minusculo em PT  | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"demora_entrega","note":"string"}                                                                                                                                                                                                                                                                                                        | "400.004" | "O campo cancelationReason tem o formato ou valor inválido." | pt-BR    |
      | sem cancellationReason em PT            | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"note":"string"}                                                                                                                                                                                                                                                                                                                                              | "400.001" | "O campo cancellationReason é obrigatório."                  | pt-BR    |
      | com cancellationReason 1 em PT          | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"1","note":"string"}                                                                                                                                                                                                                                                                                                                     | "400.004" | "O campo cancelationReason tem o formato ou valor inválido." | pt-BR    |
      | com note maior que 300 caracteres em PT | {"userType":"FILIAL","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 caracteres. Note maior que 300 car"} | "400.014" | "Campo note tem limite de 300 caracteres."                   | pt-BR    |
      | com userType vazio em PT                | {"userType":"","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                              | "400.001" | "O campo userType é obrigatório."                            | pt-BR    |
      | com userType nulo em PT                 | {"userType":null,"vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                            | "400.001" | "O campo userType é obrigatório."                            | pt-BR    |
      | com userType inválido em PT             | {"userType":"FILIALs","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                       | "400.004" | "O campo userType tem o formato ou valor inválido."          | pt-BR    |
      | sem userType em PT                      | {"vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                                            | "400.001" | "O campo userType é obrigatório."                            | pt-BR    |
      | com userType 1 em PT                    | {"userType":"1","vouchers":[{"code":"${code}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                             | "400.004" | "O campo userType tem o formato ou valor inválido."          | pt-BR    |
      | com code vazio em PT                    | {"userType":"FILIAL","vouchers":[{"code":""}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                               | "400.015" | "Lista de Vouchers contém valores nulos ou vazios."          | pt-BR    |
      | com code nulo em PT                     | {"userType":"FILIAL","vouchers":[{"code":null}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                             | "400.015" | "Lista de Vouchers contém valores nulos ou vazios."          | pt-BR    |
      | sem code em PT                          | {"userType":"FILIAL","vouchers":[{}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                                                                                                                                                                                                                                                                                        | "400.015" | "Lista de Vouchers contém valores nulos ou vazios."          | pt-BR    |

  @Negativo
  Scenario Outline: Deletar voucher <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set DELETE api endpoint as "${endpointVouchers}/${nsu1}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao               | body                                                                                                         | code      | message                                                  | language |
      | com code inválido       | {"userType":"FILIAL","vouchers":[{"code":"${code}1"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"} | "422.009" | "One or more vouchers does not belong to the given NSU." | en-US    |
      | com code inválido em PT | {"userType":"FILIAL","vouchers":[{"code":"${code}1"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"} | "422.009" | "Um ou mais vouchers não pertencem ao NSU informado."    | pt-BR    |

  @Positivo
  Scenario Outline: Validar delete <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request body as "<body>"
    When I set DELETE api endpoint as "${endpointVouchers}/<type>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "nsu" as "nsu"
    And I save final value of header "nsu" as "deletedVoucher"
    And Response code must be 204

    Examples: 
      | message                                                | body                                                                                                                                  | type             |
      | por nsu                                                | {"userType":"FILIAL","vouchers":[{"code":"${code1}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                          | ${nsu1}          |
      | com vouchers vazio                                     | {"userType":"FILIAL","vouchers":[],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                             | ${nsu2}          |
      | com vouchers nulo                                      | {"userType":"FILIAL","vouchers":null,"cancellationReason":"DEMORA_ENTREGA","note":"string"}                                           | ${nsu3}          |
      | sem vouchers                                           | {"userType":"FILIAL","cancellationReason":"DEMORA_ENTREGA","note":"string"}                                                           | ${nsu4}          |
      | com cancellationReason DEMORA_ENTREGA                  | {"userType":"FILIAL","vouchers":[{"code":"${code5}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                          | ${nsu5}          |
      | com cancellationReason DESISTENCIA_COMPRA              | {"userType":"FILIAL","vouchers":[{"code":"${code6}"}],"cancellationReason":"DESISTENCIA_COMPRA","note":"string"}                      | ${nsu6}          |
      | com cancellationReason DIA_HORARIO_ATENDIMENTO         | {"userType":"FILIAL","vouchers":[{"code":"${code7}"}],"cancellationReason":"DIA_HORARIO_ATENDIMENTO","note":"string"}                 | ${nsu7}          |
      | com cancellationReason NAO_HA_COBERTURA_ENTREGA        | {"userType":"FILIAL","vouchers":[{"code":"${code8}"}],"cancellationReason":"NAO_HA_COBERTURA_ENTREGA","note":"string"}                | ${nsu8}          |
      | com cancellationReason PROBLEMAS_COM_CANAL_SOLICITACAO | {"userType":"FILIAL","vouchers":[{"code":"${code9}"}],"cancellationReason":"PROBLEMAS_COM_CANAL_SOLICITACAO","note":"string"}         | ${nsu9}          |
      | com cancellationReason TESTE_DISTRIBUIDOR              | {"userType":"FILIAL","vouchers":[{"code":"${code10}"}],"cancellationReason":"TESTE_DISTRIBUIDOR","note":"string"}                     | ${nsu10}         |
      | com cancellationReason VENDA_NAO_RECONHECIDA           | {"userType":"FILIAL","vouchers":[{"code":"${code11}"}],"cancellationReason":"VENDA_NAO_RECONHECIDA","note":"string"}                  | ${nsu11}         |
      | com note vazio                                         | {"userType":"FILIAL","vouchers":[{"code":"${code12}"}],"cancellationReason":"DEMORA_ENTREGA","note":""}                               | ${nsu12}         |
      | com note nulo                                          | {"userType":"FILIAL","vouchers":[{"code":"${code13}"}],"cancellationReason":"DEMORA_ENTREGA","note":null}                             | ${nsu13}         |
      | sem note                                               | {"userType":"FILIAL","vouchers":[{"code":"${code14}"}],"cancellationReason":"DEMORA_ENTREGA"}                                         | ${nsu14}         |
      | com userType FILIAL                                    | {"userType":"FILIAL","vouchers":[{"code":"${code15}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                         | ${nsu15}         |
      | com userType CONSUMIDOR_FINAL                          | {"userType":"CONSUMIDOR_FINAL","vouchers":[{"code":"${code16}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}               | ${nsu16}         |
      | com userType UAD                                       | {"userType":"UAD","vouchers":[{"code":"${code17}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                            | ${nsu17}         |
      | com userType REVENDEDOR                                | {"userType":"REVENDEDOR","vouchers":[{"code":"${code18}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                     | ${nsu18}         |
      | com userType DISTRIBUIDOR_POS                          | {"userType":"DISTRIBUIDOR_POS","vouchers":[{"code":"${code19}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}               | ${nsu19}         |
      | com userType DISTRIBUIDOR_PDV                          | {"userType":"DISTRIBUIDOR_PDV","vouchers":[{"code":"${code20}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}               | ${nsu20}         |
      | com userType DISTRIBUIDOR                              | {"userType":"DISTRIBUIDOR","vouchers":[{"code":"${code21}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                   | ${nsu21}         |
      | com userType BACKOFFICE                                | {"userType":"BACKOFFICE","vouchers":[{"code":"${code22}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                     | ${nsu22}         |
      | com code completo                                      | {"userType":"FILIAL","vouchers":[{"code":"${code23}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                         | ${nsu23}         |
      | com code incompleto                                    | {"userType":"FILIAL","vouchers":[{"code":"${codeIncompleto}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                 | ${nsuIncompleto} |
      | com dois codes                                         | {"userType":"FILIAL","vouchers":[{"code":"${code25}"}, {"code":"${code2.25}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"} | ${nsu25}         |
      | por code                                               | {"userType":"FILIAL","vouchers":[{"code":"${code27}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                         | ${code27}        |

  #validado manualmente| por authorization_code                                 | {"userType":"FILIAL","vouchers":[{"code":"${code28}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"}                         | ${authorization_code} |
  @Negativo
  Scenario Outline: Deletar voucher <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set DELETE api endpoint as "${endpointVouchers}/<type>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                 | body                                                                                                         | type    | code      | message                                           | language |
      | com nsu já deletado       | {"userType":"FILIAL","vouchers":[{"code":"${code1}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"} | ${nsu1} | "422.010" | "One or more vouchers do not have sold status."   | en-US    |
      | com nsu já deletado em PT | {"userType":"FILIAL","vouchers":[{"code":"${code1}"}],"cancellationReason":"DEMORA_ENTREGA","note":"string"} | ${nsu1} | "422.010" | "Um ou mais vouchers não tem a situação VENDIDO." | pt-BR    |
