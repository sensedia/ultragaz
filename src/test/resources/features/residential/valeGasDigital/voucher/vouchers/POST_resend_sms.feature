@POST_resend_sms
Feature: POST_resend_sms.feature
  Operação responsável por reenviar o(s) voucher(s) para o consumidor via SMS.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token_password.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/voucher/v1"
    And I save "/vouchers" as "endpointVouchers"
    And I save "/resend-sms" as "endpointResend-sms"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "655881" as "customerSiteIdFilial"
    And I save "596913" as "customerIdFilial"
    And I save "7SWE992X" as "nsuComVoucherUtilizado"
    And I save "8450496" as "VoucherUtilizado"

  @PreRequest
  Scenario: Gerar Token de Distribuidor sem restrição de exibição do Voucher
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerIdFilial}","customerSiteId":"${customerSiteIdFilial}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"

  @PreRequest
  Scenario: Gerar venda para distribuidor com voucher sem restrição de exibição
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request body as "{"userType":"FILIAL","products":[{"codeProduct":"0110035","priceProduct":37.0,"quantity":1},{"codeProduct":"0110035","priceProduct":37.0,"quantity":1}],"paymentMethod":"BOLETO","couponCode":"WE37DKE38","couponDiscount":15.5,"amount":74.00,"consumer":{"nameConsumer":"John Doe","emailConsumer":"aline.dias@sensedia.com","documentTypeConsumer":"CPF","documentConsumer":83606026030,"cellphoneConsumer":{"areaCode":19,"number":993120538}}}"
    When I set POST api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And I save response value "/nsu" as "nsu"
    And I save response value "/vouchers/0/code" as "voucherOne"
    And I save response value "/vouchers/1/code" as "voucherTwo"
    And I save response value "/vouchers/2/code" as "voucherThree"
    And Response code must be 201

  @PreRequest
  Scenario: Gerar Token de Distribuidor com restrição de exibição do Voucher
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerIdFilial}","customerSiteId":"${customerSiteIdFilial}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "tokenRestrito"
    And Response code must be 201

  @PreRequest
  Scenario: Gerar venda para distribuidor com voucher COM restrição de exibição
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${tokenRestrito}"
    And I set request body as "{"userType":"FILIAL","products":[{"codeProduct":"0110035","priceProduct":37.00,"quantity":2},{"codeProduct":"0110035","priceProduct":37.00,"quantity":1}],"paymentMethod":"BOLETO","couponCode":"WE37DKE38","couponDiscount":15.5,"amount":111.00,"consumer":{"nameConsumer":"John Doe","documentTypeConsumer":"CPF","documentConsumer":83606026030,"emailConsumer":"john.doe@provider.com","cellphoneConsumer":{"areaCode":11,"number":975755141}},"location":{"latitude":-23.663198055980104,"longitude":"-46.59271122100813"}}"
    When I set POST api endpoint as "${endpointVouchers}"
    Then I get response body
    And I get response status code
    And I save response value "/nsu" as "nsuRestrito"
    And I save response value "/vouchers/0/code" as "voucherFour"
    And I save response value "/vouchers/1/code" as "voucherFive"
    And I save response value "/vouchers/2/code" as "voucherSix"
    And Response code must be 201

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}1"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar request sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "Content-Type" as "<content_type>"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | content_type    |
      | Content-Type inválido | application/123 |
      | Content-Type vazio    |                 |

  @Negativo
  Scenario: Enviar request sem body
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Invalid request body."
    And Response code must be 400

  @Negativo
  Scenario: Enviar request sem body em pt
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Request body inválido."
    And Response code must be 400

  @Negativo
  Scenario Outline: Vender voucher <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<messageOne>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageTwo>"
    And I compare response value "/1/code" with "<code>"
    And I verify if response value "/2/message" is empty
    And I verify if response value "/2/code" is empty
    And Response code must be 400

    Examples: 
      | descricao            | body | language | code    | messageOne                 | messageTwo                    |
      | com body vazio       | {}   | en-US    | 400.001 | Field nsu is required.     | Field nsu must not be empty.  |
      | com body vazio em pt | {}   | pt-BR    | 400.001 | O campo nsu é obrigatório. | Campo nsu não pode ser vazio. |

  @Negativo
  Scenario Outline: Validar body <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <responseCode>
    And I compare response value "/0/message" with <responseDescription>
    And Response code must be <code>

    Examples: 
      | message                                 | body                                                      | language | responseCode | responseDescription                                                  | code |  |
      | sem nsu                                 | {"vouchers": [{"code": "${voucherOne}"}]}                 | en-US    | "400.001"    | "Field nsu is required."                                             |  400 |  |
      | com nsu vazio                           | {"nsu": "","vouchers": [{"code": "${voucherOne}"}]}       | en-US    | "400.009"    | "Field nsu must not be empty."                                       |  400 |  |
      | com nsu nulo                            | {"nsu": null,"vouchers": [{"code": "${voucherOne}"}]}     | en-US    | "400.001"    | "Field nsu is required."                                             |  400 |  |
      | com nsu com menos de 8 caracteres       | {"nsu": "123456","vouchers": [{"code": "${voucherOne}"}]} | en-US    | "422.008"    | "It was not possible to recover any vouchers through the given NSU." |  422 |  |
      | sem nsu em pt                           | {"vouchers": [{"code": "${voucherOne}"}]}                 | pt-BR    | "400.001"    | "O campo nsu é obrigatório."                                         |  400 |  |
      | com nsu vazio em pt                     | {"nsu": "","vouchers": [{"code": "${voucherOne}"}]}       | pt-BR    | "400.009"    | "Campo nsu não pode ser vazio."                                      |  400 |  |
      | com nsu nulo em pt                      | {"nsu": null,"vouchers": [{"code": "${voucherOne}"}]}     | pt-BR    | "400.001"    | "O campo nsu é obrigatório."                                         |  400 |  |
      | com nsu com menos de 8 caracteres em pt | {"nsu": "123456","vouchers": [{"code": "${voucherOne}"}]} | pt-BR    | "422.008"    | "Não foi possivel resgatar vouchers através do NSU informado."       |  422 |  |
      | com code nulo                           | {"nsu": "${nsu}","vouchers": [{"code": null}]}            | pt-BR    | "400.015"    | "Lista de Vouchers contém valores nulos ou vazios."                  |  400 |  |
      | sem code                                | {"nsu": "${nsu}","vouchers": [{}]}                        | pt-BR    | "400.015"    | "Lista de Vouchers contém valores nulos ou vazios."                  |  400 |  |

  @Negativo
  Scenario Outline: Validar reenvio <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<token>"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <responseCode>
    And I compare response value "/0/message" with <responseDescription>
    And Response code must be <code>

    Examples: 
      | message                                                                     | body                                                                                                                                         | language | responseCode | responseDescription                                                                   | code | token            |
      | NSU inválido                                                                | {"nsu": "${nsu}1","vouchers": [{"code": "${voucherOne}"}]}                                                                                   | en-US    | "422.008"    | "It was not possible to recover any vouchers through the given NSU."                  |  422 | ${token}         |
      | Voucher completo inválido                                                   | {"nsu": "${nsu}","vouchers": [{"code": "${voucherOne}1"}]}                                                                                   | en-US    | "422.009"    | "One or more vouchers does not belong to the given NSU."                              |  422 | ${token}         |
      | Voucher completo inválido com array maior que 2                             | {"nsu": "${nsu}","vouchers": [{"code": "${voucherOne}"},{"code": "${voucherTwo}1"},{"code": "${voucherThree}"}]}                             | en-US    | "400.015"    | "Voucher list contains null or empty values."                                         |  400 | ${token}         |
      | Voucher completo Restrito                                                   | {"nsu": "${nsuRestrito}","vouchers": [{"code": "${voucherfour}"}]}                                                                           | en-US    | "422.009"    | "One or more vouchers does not belong to the given NSU."                              |  422 | ${tokenRestrito} |
      | Voucher com 2 ultimos numeros inválidos                                     | {"nsu": "${nsu}","vouchers": [{"code": "string"}]}                                                                                           | en-US    | "422.009"    | "One or more vouchers does not belong to the given NSU."                              |  422 | ${token}         |
      | Voucher Restrito com 2 ultimos numeros inválidos                            | {"nsu": "${nsuRestrito}","vouchers": [{"code": "AA"}]}                                                                                       | en-US    | "422.009"    | "One or more vouchers does not belong to the given NSU."                              |  422 | ${tokenRestrito} |
      | Voucher Restrito com 2 ultimos numeros inválidos em array maior que 2       | {"nsu": "${nsuRestrito}","vouchers": [{"code": "40"},{"code": "39"},{"code": "40"}]}                                                         | en-US    | "422.009"    | "One or more vouchers does not belong to the given NSU."                              |  422 | ${tokenRestrito} |
      | telefone não informado na venda                                             | {"nsu": "EZ42E9TE","vouchers": [{"code": "1282977"}]}                                                                                        | en-US    | "422.028"    | "It is not possible to send SMS, as the cell phone was not informed during the sale." |  422 | ${token}         |
      | NSU inválido em pt                                                          | {"nsu": "${nsu}1","vouchers": [{"code": "${voucherOne}"}]}                                                                                   | pt-BR    | "422.008"    | "Não foi possivel resgatar vouchers através do NSU informado."                        |  422 | ${token}         |
      | Voucher completo inválido em pt                                             | {"nsu": "${nsu}","vouchers": [{"code": "${voucherOne}1"}]}                                                                                   | pt-BR    | "422.009"    | "Um ou mais vouchers não pertencem ao NSU informado."                                 |  422 | ${token}         |
      | Voucher completo inválido com array maior que 2 em pt                       | {"nsu": "${nsu}","vouchers": [{"code": "${voucherOne}"},{"code": "${voucherTwo}1"},{"code": "${voucherThree}"},{"code": "${voucherThree}"}]} | pt-BR    | "400.015"    | "Lista de Vouchers contém valores nulos ou vazios."                                   |  400 | ${token}         |
      | Voucher completo Restrito em pt                                             | {"nsu": "${nsuRestrito}","vouchers": [{"code": "${voucherfour}"}]}                                                                           | pt-BR    | "422.009"    | "Um ou mais vouchers não pertencem ao NSU informado."                                 |  422 | ${tokenRestrito} |
      | Voucher com 2 ultimos numeros inválidos em pt                               | {"nsu": "${nsu}","vouchers": [{"code": "string"}]}                                                                                           | pt-BR    | "422.009"    | "Um ou mais vouchers não pertencem ao NSU informado."                                 |  422 | ${token}         |
      | Voucher Restrito com 2 ultimos numeros inválidos em pt                      | {"nsu": "${nsuRestrito}","vouchers": [{"code": "12"}]}                                                                                       | pt-BR    | "422.009"    | "Um ou mais vouchers não pertencem ao NSU informado."                                 |  422 | ${tokenRestrito} |
      | Voucher Restrito com 2 ultimos numeros inválidos em array maior que 2 em pt | {"nsu": "${nsuRestrito}","vouchers": [{"code": "40"},{"code": "39"},{"code": "40"}]}                                                         | pt-BR    | "422.009"    | "Um ou mais vouchers não pertencem ao NSU informado."                                 |  422 | ${tokenRestrito} |
      | telefone não informado na venda pt                                          | {"nsu": "EZ42E9TE","vouchers": [{"code": "1282977"}]}                                                                                        | pt-BR    | "422.028"    | "Não é possível enviar SMS, pois não foi informado um celular durante a venda."       |  422 | ${token}         |

  @Positivo
  Scenario Outline: Validar Reenvio  <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<token>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointResend-sms}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | message                                    | body                                                                                                                    | token            |
      | sem vouchers                               | {"nsu": "${nsu}"}                                                                                                       | ${token}         |
      | com Voucher completo                       | {"nsu": "${nsu}","vouchers": [{"code": "${voucherOne}"}]}                                                               | ${token}         |
      | com Voucher com 2 ultimos digitos          | {"nsu": "${nsuRestrito}","vouchers": [{"code": "${voucherFour}"}]}                                                      | ${token}         |
      | com Voucher Restrito com 2 ultimos digitos | {"nsu": "${nsu}","vouchers": [{"code": "${voucherOne}"}]}                                                               | ${tokenRestrito} |
      | com Voucher já utilizado                   | {"nsu": "${nsuComVoucherUtilizado}","vouchers": [{"code": "${VoucherUtilizado}"}]}                                      | ${token}         |
      | com N vouchers                             | {"nsu": "${nsu}","vouchers": [{"code": "${voucherOne}"},{"code": "${voucherTwo}"}]}                                     | ${token}         |
      | com N vouchers restritos                   | {"nsu": "${nsuRestrito}","vouchers": [{"code": "${voucherFour}"},{"code": "${voucherFive}"},{"code": "${voucherSix}"}]} | ${tokenRestrito} |
      | com Voucher repetidos                      | {"nsu": "${nsu}","vouchers": [{"code": "${voucherOne}"},{"code": "${voucherOne}"},{"code": "${voucherOne}"}]}           | ${token}         |
