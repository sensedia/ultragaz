Feature: POST_voucher_distributor_authorization.feature
  Operação responsável gerar um novo token

  #Matriz
  #INSERT voucherdev.distributor values ('MATRIZ', '1234567', '1234567', '45401035802','A', '10', '10',null);
  #Filial
  #INSERT voucherdev.distributor values ('FILIAL', '12345678', '12345678', '31971250023', 'A', '10', '10','MATRIZ');
  #MatrizSemDocumet
  #INSERT voucherdev.distributor values ('MATRIZSEMDOCUMENT', '1111', '1111', '36757747081', 'A', '10', '10',null);
  #FilialSemDocumet
  #INSERT voucherdev.distributor values('FILIALSEMDOCUMENT', '1111', '1111', '36757747081', 'A', '10', '10',null);
  #MatrizComDocumentDiferente
  #INSERT INTO voucherdev.distributor VALUES ('MATRIZCOMDOCUMETODIFERENTE', '12225', '1223', '70445652047', 'A', '10', '10',null);
  #MatrizDesativada
  #INSERT INTO voucherdev.distributor VALUES ('MATRIZDESATIVADA', '12226', '1224', '45401035802', 'D', '10', '10',null);
  #FilialDesativada
  #INSERT INTO voucherdev.distributor VALUES ('FILIALDESATIVADA', '12227', '1225', '27409056029', 'D', '10', '10','MATRIZ');
  #FilialComMatrizDesativada
  #INSERT INTO voucherdev.distributor VALUES ('FILIALCOMMATRIZDESATIVADA', '12228', '1226', '27409056029', 'A', '10', '10','MATRIZDESATIVADA');
  #FilialComMatrizInválida
  #INSERT INTO voucherdev.distributor VALUES ('FILIALCOMMATRIZINVALIDA', '12229', '1227', '27409056029', 'A', '10', '10','MATRIZINEXISTENTE');
  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8001"
    And I use api name as "/voucher/v1"
    #And System generate Authorization with default APP information
    And I save "/distributor-authorizations" as "endpointAuthorizations"
    And I save "MWVkMjc2ZGYtZGExZS0zYWMxLTk4ZTctODVjMzRhMjI5NjA5OmI1OGQ0MTA1LTUyYTItMzgzNS1iMTkwLWQ1NjRiMGU3ZWNlYw==" as "authorization"
    And I save "MzZhNTE4YjEtYjk2Ni0zZjlmLWEzYzItYmQ4NTczODZhNWYxOjZhMDYxMzU4LTFmMzUtMzZjZC05MWZmLTE0MjE2OGJiMThjZQ==" as "authorizationSemDocument"
    And I save "1111" as "customerIdMatrizSemDocumet"
    And I save "1111" as "customerSiteIdMatrizSemDocumet"
    And I save "12224" as "customerIdFilialSemDocumet"
    And I save "1222" as "customerSiteIdFilialSemDocumet"
    And I save "12225" as "customerIdMatrizComDocumentDiferente"
    And I save "1223" as "customerSiteIdMatrizComDocumentDiferente"
    # MatrizComDocumentDiferente document 70445652047
    And I save "12226" as "customerIdMatrizDesativada"
    And I save "1224" as "customerSiteIdMatrizDesativada"
    # MATRIZ DESATIVA 45401035802
    And I save "12227" as "customerIdFilialDesativada"
    And I save "1225" as "customerSiteIdFiliarDesativada"
    # document matriz para filial desativada
    And I save "12228" as "customerIdFilialComMatrizDesativada"
    And I save "1226" as "customerSiteIdFilialComMatrizDesativada"
    # MATRIZ DESATIVA 45401035802
    And I save "12229" as "customerIdFilialComMatrizInválida"
    And I save "1227" as "customerSiteIdFilialComMatrizInválida"
    And I save "12345678" as "customerIdFilial"
    And I save "12345678" as "customerSiteIdFilial"
    And I save "1234567" as "customerIdMatriz"
    # document Matriz 45401035802
    And I save "1234567" as "customerSiteIdMatriz"

  #	And I set request header "Authorization" as "${authorization}"
  @Negativo
  Scenario: Enviar request sem Authorization
    Given I set request body as "{"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}"}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "412.001"
    And I compare response value "/0/message" with "Field Authorization is required on header."
    And Response code must be 412

  @Negativo
  Scenario: Enviar request sem Authorization em pt
    Given I set request body as "{"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}"}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "pt-BR"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "412.001"
    And I compare response value "/0/message" with "O campo Authorization é obrigatório no header."
    And Response code must be 412

  @Negativo
  Scenario: Enviar request com Authorization vazio
    Given I set request body as "{"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}"}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Authorization" as ""
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "412.001"
    And I compare response value "/0/message" with "Field Authorization is required on header."
    And Response code must be 412

  @Negativo
  Scenario: Enviar request com Authorization vazio em pt
    Given I set request body as "{"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}"}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "pt-BR"
    And I set request header "Authorization" as ""
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "412.001"
    And I compare response value "/0/message" with "O campo Authorization é obrigatório no header."
    And Response code must be 412

  @Negativo
  Scenario Outline: Enviar request com Authorization inválido
    Given I set request body as "{"customerId":"${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}"}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request header "Authorization" as "${authorization}1 "
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 403

  @Negativo
  Scenario: Enviar request sem Content-Type
    Given I set request body as "{"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}"}"
    And I set request header "Authorization" as "${authorization}"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request body as "{"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario: Enviar request sem body
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Authorization" as "${authorization}"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Invalid request body."
    And Response code must be 400

  @Negativo
  Scenario: Enviar request sem body em pt
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.000"
    And I compare if response value "/0/message" contains "Request body inválido."
    And Response code must be 400

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare if response value "/1/message" contains "<message>"
    And I compare response value "/1/code" with "<code>"
    And I compare response value "/0/message" with "<messageOne>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao            | body | language | message                           | code    | messageOne                            | messageTwo                          |
      | com body vazio       | {}   | en-US    | Field customerId is required.     | 400.001 | Field customerSiteId is required.     | Field customerIdId is required.     |
      | com body vazio em pt | {}   | pt-BR    | O campo customerId é obrigatório. | 400.001 | O campo customerSiteId é obrigatório. | O campo customerIdId é obrigatório. |

  @Negativo
  Scenario Outline: Validar body <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <responseCode>
    And I compare response value "/0/message" with <responseDescription>
    And Response code must be <code>

    Examples: 
      | message                                                     | body                                                                               | language | responseCode | responseDescription                              | code |
      | sem parâmetro customerId                                    | {"customerSiteId":"${customerSiteIdMatriz}"}                                       | en-US    | "400.001"    | "Field customerId is required."                  |  400 |
      | com parâmetro customerId vazio                              | {"customerId":"","customerSiteId":"${customerSiteIdMatriz}"}                       | en-US    | "400.001"    | "Field customerId is required."                  |  400 |
      | com parâmetro customerId nulo                               | {"customerId":null,"customerSiteId":"${customerSiteIdMatriz}"}                     | en-US    | "400.001"    | "Field customerId is required."                  |  400 |
      | com parâmetro customerId com formato inválido               | {"customerId": "${customerIdFilial}BS","customerSiteId":"${customerSiteIdMatriz}"} | en-US    | "400.004"    | "Field customerId has an invalid format."        |  400 |
      | com parâmetro customerId ultrapassando 20 digitos           | {"customerId":"1234567890123456789012","customerSiteId":"${customerSiteIdMatriz}"} | en-US    | "400.004"    | "Field customerId has an invalid format."        |  400 |
      | sem parâmetro customerSiteId                                | {"customerId": "${customerIdMatriz}"}                                              | en-US    | "400.001"    | "Field customerSiteId is required."              |  400 |
      | com parâmetro customerSiteId vazio                          | {"customerId": "${customerIdMatriz}","customerSiteId":""}                          | en-US    | "400.001"    | "Field customerSiteId is required."              |  400 |
      | com parâmetro customerSiteId nulo                           | {"customerId": "${customerIdMatriz}","customerSiteId":null}                        | en-US    | "400.001"    | "Field customerSiteId is required."              |  400 |
      | com parâmetro customerSiteId com formato inválido           | {"customerId": "${customerIdFilial}","customerSiteId":"${customerSiteIdMatriz}Bs"} | en-US    | "400.004"    | "Field customerSiteId has an invalid format."    |  400 |
      | com parâmetro customerSiteId ultrapassando 20 digitos       | {"customerId": "${customerIdMatriz}","customerSiteId":"1234567890123456789012"}    | en-US    | "400.004"    | "Field customerSiteId has an invalid format."    |  400 |
      | sem parâmetro customerId em pt                              | {"customerSiteId":"${customerSiteIdMatriz}"}                                       | pt-BR    | "400.001"    | "O campo customerId é obrigatório."              |  400 |
      | com parâmetro customerId vazio em pt                        | {"customerId":"","customerSiteId":"${customerSiteIdMatriz}"}                       | pt-BR    | "400.001"    | "O campo customerId é obrigatório."              |  400 |
      | com parâmetro customerId nulo  em pt                        | {"customerId":null,"customerSiteId":"${customerSiteIdMatriz}"}                     | pt-BR    | "400.001"    | "O campo customerId é obrigatório."              |  400 |
      | com parâmetro customerId com formato inválido em pt         | {"customerId": "${customerIdFilial}BS","customerSiteId":"${customerSiteIdMatriz}"} | pt-BR    | "400.004"    | "O campo customerId tem o formato inválido."     |  400 |
      | com parâmetro customerId ultrapassando 20 digitos em pt     | {"customerId":"1234567890123456789012","customerSiteId":"${customerSiteIdMatriz}"} | pt-BR    | "400.004"    | "O campo customerId tem o formato inválido."     |  400 |
      | sem parâmetro customerSiteId  em pt                         | {"customerId": "${customerIdMatriz}"}                                              | pt-BR    | "400.001"    | "O campo customerSiteId é obrigatório."          |  400 |
      | com parâmetro customerSiteId vazio em pt                    | {"customerId": "${customerIdMatriz}","customerSiteId":""}                          | pt-BR    | "400.001"    | "O campo customerSiteId é obrigatório."          |  400 |
      | com parâmetro customerSiteId nulo em pt                     | {"customerId": "${customerIdMatriz}","customerSiteId":null}                        | pt-BR    | "400.001"    | "O campo customerSiteId é obrigatório."          |  400 |
      | com parâmetro customerSiteId com formato inválido em pt     | {"customerId": "${customerIdFilial}","customerSiteId":"${customerSiteIdMatriz}Bs"} | pt-BR    | "400.004"    | "O campo customerSiteId tem o formato inválido." |  400 |
      | com parâmetro customerSiteId ultrapassando 20 digitos em pt | {"customerId": "${customerIdMatriz}","customerSiteId":"1234567890123456789012"}    | pt-BR    | "400.004"    | "O campo customerSiteId tem o formato inválido." |  400 |

  @Negativo
  Scenario Outline: Validar body <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <responseCode>
    And I compare response value "/message" with <responseDescription>
    And Response code must be 422

    Examples: 
      | message                                                                                                | body                                                                              | language | responseCode | responseDescription                                               | code |
      | com parâmetro customerId inválido                                                                      | {"customerId":"${customerIdMatriz}1","customerSiteId":"${customerSiteIdMatriz}"}  | en-US    | "422.002"    | "Field customerId and customerSiteId has an invalid combination." |  422 |
      | com parâmetro customerSiteId inválido                                                                  | {"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}1"} | en-US    | "422.002"    | "Field customerId and customerSiteId has an invalid combination." |  422 |
      | com parâmetro customerId e customerSiteId válidos mas não pertendentes ao mesmo representante_id       | {"customerId": "${customerIdFilial}","customerSiteId":"${customerSiteIdMatriz}"}  | en-US    | "422.002"    | "Field customerId and customerSiteId has an invalid combination." |  422 |
      | com parâmetro customerId inválido em pt                                                                | {"customerId":"${customerIdMatriz}1","customerSiteId":"${customerSiteIdMatriz}"}  | pt-BR    | "422.002"    | "Campo customerId e customerSiteId tem uma combinação inválida."  |  422 |
      | com parâmetro customerSiteId inválido em pt                                                            | {"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}1"} | pt-BR    | "422.002"    | "Campo customerId e customerSiteId tem uma combinação inválida."  |  422 |
      | com parâmetro customerId e customerSiteId válidos mas não pertendentes ao mesmo representante_id em pt | {"customerId": "${customerIdFilial}","customerSiteId":"${customerSiteIdMatriz}"}  | pt-BR    | "422.002"    | "Campo customerId e customerSiteId tem uma combinação inválida."  |  422 |

  @Negativo
  Scenario Outline: Validar Authenticação <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Authorization" as "<authorization>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 403

    Examples: 
      | message                                                          | body                                                                                                                     | authorization               |
      | Por filial com App sem document                                  | {"customerId": "${customerIdFilialSemDocumet}","customerSiteId":"${customerSiteIdFilialSemDocumet}"}                     | ${authorizationSemDocument} |
      | Por filial com Document da Matriz não batendo com a APP          | {"customerId": "${customerIdMatrizComDocumentDiferente}","customerSiteId":"${customerSiteIdMatrizComDocumentDiferente}"} | ${authorization}            |
      | Filial desativada                                                | {"customerId": "${customerIdFilialDesativada}","customerSiteId":"${customerSiteIdFiliarDesativada}"}                     | ${authorization}            |
      | Por Matriz com App sem document                                  | {"customerId": "${customerIdMatrizSemDocumet}","customerSiteId":"${customerSiteIdMatrizSemDocumet}"}                     | ${authorizationSemDocument} |
      | Com Document da Matriz não batendo com a APP                     | {"customerId": "${customerIdMatrizComDocumentDiferente}","customerSiteId":"${customerSiteIdMatrizComDocumentDiferente}"} | ${authorization}            |
      | Matriz desativada                                                | {"customerId": "${customerIdMatrizDesativada}","customerSiteId":"${customerSiteIdMatrizDesativada}"}                     | ${authorization}            |
      | Filial com headquarter_id Inválido e documento batendo com o APP | {"customerId": "${customerIdFilialComMatrizInválida}","customerSiteId":"${customerSiteIdFilialComMatrizInválida}"}       | ${authorization}            |

  @Positivo
  Scenario Outline: Authenticar com <message>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthorizations}"
    Then I get response body
    And I get response status code
    And I verify if response value "/token" is not empty
    And I verify if response value "/expiresIn" is not empty
    And Response code must be 201

    Examples: 
      | message                           | body                                                                                                                   |
      | Filial contendo Matriz desativada | {"customerId": "${customerIdFilialComMatrizDesativada}","customerSiteId":"${customerSiteIdFilialComMatrizDesativada}"} |
      | Por Filial                        | {"customerId": "${customerIdFilial}","customerSiteId":"${customerSiteIdFilial}"}                                       |
      | Por Matriz                        | {"customerId": "${customerIdMatriz}","customerSiteId":"${customerSiteIdMatriz}"}                                       |
