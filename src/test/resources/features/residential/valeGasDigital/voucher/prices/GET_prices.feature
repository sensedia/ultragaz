@GET_prices
Feature: GET_prices.feature
  Obter preços de produtos da Vale Gás Digital.

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/voucher/v1"
    And I save "/prices" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "-23.6533122" as "latitude"
    And I save "-46.614004" as "longitude"
    And I save "596913" as "customerId"
    And I save "655860" as "customerSiteIdFilial"
    And I save "655881" as "customerSiteIdMatriz"
    And I save "655881" as "customerSiteIdCNPJ"

  @PreRequest
  Scenario: Gerar Token de Distribuidor sem location
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "X-document" as "45401035802"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteIdMatriz}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "token"
    And Response code must be 201

  @PreRequest
  Scenario: Gerar Token de Distribuidor CNPJ
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteIdCNPJ}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "tokenCNPJ"
    And Response code must be 201

  @PreRequest
  Scenario: Gerar Token de Distribuidor com location
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And System generate Authorization with default APP information
    And I set request header "Authorization" as "${authorization}"
    And I set request body as "{"customerId": "${customerId}","customerSiteId":"${customerSiteIdFilial}"}"
    When I set POST api endpoint as "/distributor-authorizations"
    Then I get response body
    And I get response status code
    And I save response value "/token" as "tokenFilial"
    And Response code must be 201

  @Negativo
  Scenario: Enviar requisição sem client_id
    And I set request header "access_token" as "${token}"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "Accept-Language" as "pt-BR"
    And I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token | message                                                                                    |
      | com client_id vazio       | ""              | "${token}"   | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${token}"   | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""           | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${token}1"  | "Could not find a required Access Token in the request, identified by HEADER access_token" |

	@Negativo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                     | queryParameter                                | code      | message                                                                | language |
      | por longitude inválida em PT  | ?latitude=${latitude}&longitude=Rua           | "400.013" | "Conjunto de coordenadas inválidas."                                   | pt-BR    |
      | por latitude inválida em PT   | ?longitude=${longitude}&latitude=Rua          | "400.013" | "Conjunto de coordenadas inválidas."                                   | pt-BR    |
      | por longitude incorreta em PT | ?latitude=${latitude}&longitude=-447.0421092  | "400.013" | "Conjunto de coordenadas inválidas."                                   | pt-BR    |
      | por latitude incorreta em PT  | ?longitude=${longitude}&latitude=-447.0421092 | "400.013" | "Conjunto de coordenadas inválidas."                                   | pt-BR    |
      | por longitude inválida        | ?latitude=${latitude}&longitude=Rua           | "400.013" | "Invalid set of coordinates."                                          | en-US    |
      | por latitude inválida         | ?longitude=${longitude}&latitude=Rua          | "400.013" | "Invalid set of coordinates."                                          | en-US    |
      | por longitude incorreta       | ?latitude=${latitude}&longitude=-447.0421092  | "400.013" | "Invalid set of coordinates."                                          | en-US    |
      | por latitude incorreta        | ?longitude=${longitude}&latitude=-447.0421092 | "400.013" | "Invalid set of coordinates."                                          | en-US    |
      | por latitude em PT            | ?latitude=${latitude}                         | "400.011" | "Apenas uma das coordenadas foi recebida, a outra está faltando."      | pt-BR    |
      | por latitude vazia em PT      | ?longitude=${longitude}&latitude=             | "400.011" | "Apenas uma das coordenadas foi recebida, a outra está faltando."      | pt-BR    |
      | por longitude em PT           | ?longitude=${longitude}                       | "400.011" | "Apenas uma das coordenadas foi recebida, a outra está faltando."      | pt-BR    |
      | por longitude vazia em PT     | ?latitude=${latitude}&longitude=              | "400.011" | "Apenas uma das coordenadas foi recebida, a outra está faltando."      | pt-BR    |
      | por latitude                  | ?latitude=${latitude}                         | "400.011" | "Only one of the coordinates has been received, the other is missing." | en-US    |
      | por latitude vazia            | ?longitude=${longitude}&latitude=             | "400.011" | "Only one of the coordinates has been received, the other is missing." | en-US    |
      | por longitude                 | ?longitude=${longitude}                       | "400.011" | "Only one of the coordinates has been received, the other is missing." | en-US    |
      | por longitude vazia           | ?latitude=${latitude}&longitude=              | "400.011" | "Only one of the coordinates has been received, the other is missing." | en-US    |
      | sem longitude e longitude     | ?latitude=${latitude}&longitude=              | "400.011" | "Only one of the coordinates has been received, the other is missing." | en-US    |

  @Negativo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${tokenFilial}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And I compare response value "/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao                    | queryParameter        | code      | message                                                                                  | language |
      | sem longitude e longitude    | ?code-product=0110035 | "422.027" | "When validate price method is by location, fields latitude and longitude are required." | en-US    |
      | sem longitude e longitude pt | ?code-product=0110035 | "422.027" | "Quando preço praticado é por localidade, campos latitude e longitude são obrigatórios." | pt-BR    |
	
  @Positivo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<token>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/codeProduct" with <codeProduct>
    And I compare response value "/0/priceProduct" with <priceProduct>
    And I compare response value "/0/nameProduct" with <nameProduct>
    And I compare response value "/0/distributor/organizationId" with <organizationId>
    And I compare response value "/0/distributor/microMarket" with <microMarket>
    And Response code must be 200

    Examples: 
      | descricao                     | token          | queryParameter                                                    | codeProduct | priceProduct | nameProduct      | microMarket | organizationId |
      | por longitude e latitude CPQD | ${tokenFilial} | ?latitude=${latitude}&longitude=${longitude}                      | "0110035"   | "74.99"      | "Gás de cozinha" | "G10184"    | "5"            |
      | por code-product blueprint    | ${tokenFilial} | ?latitude=${latitude}&longitude=${longitude}&code-product=0110035 | "0110035"   | "74.99"      | "Gás de cozinha" | "G10184"    | "5"            |
      | por code-product local        | ${token}       | ?latitude=${latitude}&longitude=${longitude}&code-product=0110060 | "0110060"   | "205.5"      | "Gás de 45Kg"    | "G10184"    | "5"            |

  @Positivo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<token>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao                            | token          | queryParameter         |
      | por code-product blueprint incorreto | ${tokenFilial} | ?code-product=01100351 |
      | por code-product local incorreto     | ${token}       | ?code-product=1110a    |
      | por code-product vazio               | ${token}       | ?code-product=         |
      | sem query parameter                  | ${token}       |                        |

  @Positivo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<token>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/codeProduct" with <codeProduct>
    And I compare response value "/0/priceProduct" with <priceProduct>
    And I compare response value "/0/nameProduct" with <nameProduct>
    And I compare response value "/0/distributor/organizationId" with <organizationId>
    And I compare response value "/0/distributor/microMarket" with <microMarket>
    And I verify if response value "/0/discount" is empty
    And I verify if response value "/0/netPriceProduct" is empty
    And Response code must be 200

    Examples: 
      | descricao                                                | token    | queryParameter                               | codeProduct | priceProduct | nameProduct      | microMarket | organizationId |
      | por longitude e latitude para distribuidor sem blueprint | ${token} | ?latitude=${latitude}&longitude=${longitude} | "0110035"   | "74.99"      | "Gás de cozinha" | "G10184"    | "5"            |

  #A precisão dos campos double influencia no resultado, talvez criar um comparador adequado
  @Positivo
  Scenario Outline: Consultar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<token>"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/discount" with <discount>
    And I compare response value "/0/netPriceProduct" with <netPriceProduct>
    And I compare response value "/0/distributor/organizationId" with <organizationId>
    And I compare response value "/0/distributor/microMarket" with <microMarket>
    And I compare response value "/0/codeProduct" with <codeProduct>
    And I compare response value "/0/priceProduct" with <priceProduct>
    And I compare response value "/0/nameProduct" with <nameProduct>
    And Response code must be 200

    Examples: 
      | descricao | token        | discount | netPriceProduct | codeProduct | priceProduct | nameProduct      | microMarket  | organizationId |
      | por CNPJ  | ${tokenCNPJ} | "10"     | "33.3"          | "0110035"   | "37.0"       | "Gás de cozinha" | "MMdiscount" | "5"            |

  