Feature: GET_customers_{customerId}.feature 
	Consulta um cliente cadastrado no segmento domiciliar da Ultragaz.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set GET api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	And System generate random CPF 
	And System generate random number 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set GET api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição sem access_token 
	Given I set request header "client_id" as "${client_id}" 
	When I set GET api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario Outline: Enviar requisição <descricao> 
	Given I set request header "client_id" as "<client_id>" 
	And I set request header "access_token" as "<access_token>" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"19","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set GET api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "<message>" 
	And Response code must be 401 
	
	Examples: 
		| descricao                 | client_id     | access_token     | message                                                                          |
		| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
		| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
		| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
		| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
		
		
		@PreRequest 
		Scenario Outline: Cadastrar cliente <descricao> 
			Given System generate random number 
			And System generate random CPF 
			And I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			And I set request body as "<body>" 
			When I set POST api endpoint as "${endpointCustomers}" 
			Then I get response body 
			And I get response status code 
			And I save response header "Location" as "location" 
			And I save final value of header "Location" as "customerId<id>" 
			And I save "${random}" as "random<id>" 
			And I save "${randomCPF}" as "document<id>" 
			And I verify if response body is empty 
			And Response code must be 201 
			And I wait 2 seconds 
			
			Examples: 
				| id | descricao                                       | body                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
				| 1  | com sucesso                                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 2  | com nome repetido                               | {"firstName":"Aline","lastName":"Bender","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 3  | com businessType vazio                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 4  | com businessType null                           | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":null,"company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 5  | sem businessType                                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 6  | com company vazio                               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 7  | com company nulo                                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":null,"source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 8  | sem company                                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 9  | com source vazio                                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 10 | com source nulo                                 | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":null,"contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 11 | sem source                                      | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 12 | com contact.phones vazio                        | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 13 | com contact.phones nulo                         | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":null,"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 14 | sem contact.phones                              | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 15 | com contact.phones.ddd vazio                    | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 16 | com contact.phones.ddd nulo                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":null,"number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 17 | sem contact.phones.ddd                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 18 | com contact.phones.number vazio                 | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 19 | com contact.phones.number nulo                  | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":null,"phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 20 | sem contact.phones.number                       | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 21 | com contact.phones.phoneType vazio              | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":""}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 22 | com contact.phones.phoneType nulo               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":null}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 23 | sem contact.phones.phoneType                    | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791"}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 24 | com email vazio                                 | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":""},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 25 | com email nulo                                  | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":null},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 26 | sem email                                       | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}]},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 27 | com socialNetwork vazio                         | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 28 | com socialNetwork nulo                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":null,"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 29 | sem socialNetwork                               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 32 | com addresses.city vazio                        | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 38 | com addresses.neighborhood vazio                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 41 | com addresses.zipCode vazio                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 44 | com addresses.country vazio                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 47 | com addresses.referenceAddress vazio            | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 48 | com addresses.referenceAddress nulo             | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":null,"address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 49 | sem addresses.referenceAddress                  | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 50 | com addresses.address vazio                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 53 | com addresses.additionAddress vazio             | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 56 | com addresses.numberAddress vazio               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 60 | com addresses.latitude nulo                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":null,"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 61 | sem addresses.latitude                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 63 | com addresses.longitude nulo                    | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":null,"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 64 | sem addresses.longitude                         | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 65 | com addresses.descriptionAddress vazio          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":""}]} |
				| 69 | com addresses.latitude longo                    | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude}1132320109321903210938219382109382109830921830921830291830219830921839021830218390218309218302918302183092183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310293821,"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 70 | com addresses.longitude longo                   | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude}92381209382103801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238210938912038092183012983091283092183018032180932813821093821093821938021382901830912830912389021382193890128310829138219083091823,"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 71 | com company longo                               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. ","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 72 | com contact.phones.phoneType longo              | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 73 | com document vazio                              | {"firstName":"Aline","lastName":"${random}","document":"","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 74 | com document nulo                               | {"firstName":"Aline","lastName":"${random}","document":null,"documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 75 | sem document                                    | {"firstName":"Aline","lastName":"${random}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 76 | com documentType vazio                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 77 | com documentType nulo                           | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":null,"businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				| 78 | sem documentType                                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
				
				
				@Negativo 
				Scenario: Consultar cliente com customerId inexistente 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set GET api endpoint as "${endpointCustomers}/batatinhax" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 404 
					
					
				@Positivo 
				Scenario Outline: Consultar cliente <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/customerId" with "${customerId<id>}" 
					And I compare response value "/firstName" with "Aline" 
					And I compare response value "/lastName" with "<lastName>" 
					And I compare response value "/document" with "${document<id>}" 
					And I compare response value "/documentType" with "CPF" 
					And I compare response value "/businessType" with "IT" 
					#And I compare response value "/company" with "Sensedia" 
					And I compare response value "/source" with "QA" 
					And I compare response value "/contact/phones/0/ddd" with "19" 
					And I compare response value "/contact/phones/0/number" with "982722791" 
					And I compare response value "/contact/phones/0/phoneType" with "1" 
					And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
					And I compare response value "/socialNetwork/instagram" with "1abender" 
					And I compare response value "/socialNetwork/facebook" with "1abender" 
					And I compare response value "/socialNetwork/twitter" with "1abender" 
					And I compare response value "/addresses/0/mainAddress" with "true" 
					And I compare response value "/addresses/0/city" with "Sumaré" 
					And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
					And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
					And I compare response value "/addresses/0/zipCode" with "13181643" 
					#And I compare response value "/addresses/0/country" with "Brasilã" 
					And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
					And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
					And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
					And I compare response value "/addresses/0/numberAddress" with "528" 
					And I compare response value "/addresses/0/latitude" with "${latitude}" 
					And I compare response value "/addresses/0/longitude" with "${longitude}" 
					And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
					And Response code must be 200 
					
					Examples: 
						| id | descricao                                       | lastName    |
						| 1  | com sucesso                                     | ${random1}  |
						| 2  | com nome repetido                               | Bender      |
						
						
						@Positivo 
						Scenario Outline: Consultar cliente <descricao> 
							Given I set request header "client_id" as "${client_id}" 
							And I set request header "access_token" as "${access_token}" 
							And I set request header "Content-Type" as "application/json" 
							When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/customerId" with "${customerId<id>}" 
							And I compare response value "/firstName" with "Aline" 
							And I compare response value "/lastName" with "${random<id>}" 
							And I compare response value "/document" with "${document<id>}" 
							And I compare response value "/documentType" with "CPF" 
							And I verify if response value "/businessType" is empty 
							#And I compare response value "/company" with "Sensedia" 
							And I compare response value "/source" with "QA" 
							And I compare response value "/contact/phones/0/ddd" with "19" 
							And I compare response value "/contact/phones/0/number" with "982722791" 
							And I compare response value "/contact/phones/0/phoneType" with "1" 
							And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
							And I compare response value "/socialNetwork/instagram" with "1abender" 
							And I compare response value "/socialNetwork/facebook" with "1abender" 
							And I compare response value "/socialNetwork/twitter" with "1abender" 
							And I compare response value "/addresses/0/mainAddress" with "true" 
							And I compare response value "/addresses/0/city" with "Sumaré" 
							And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
							And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
							And I compare response value "/addresses/0/zipCode" with "13181643" 
							#And I compare response value "/addresses/0/country" with "Brasilã" 
							And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
							And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
							And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
							And I compare response value "/addresses/0/numberAddress" with "528" 
							And I compare response value "/addresses/0/latitude" with "${latitude}" 
							And I compare response value "/addresses/0/longitude" with "${longitude}" 
							And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
							And Response code must be 200 
							
							Examples: 
								| id | descricao                                       |
								| 3  | com businessType vazio                          |
								| 4  | com businessType null                           |
								| 5  | sem businessType                                |
								
								
								@Positivo 
								Scenario Outline: Consultar cliente <descricao> 
									Given I set request header "client_id" as "${client_id}" 
									And I set request header "access_token" as "${access_token}" 
									And I set request header "Content-Type" as "application/json" 
									When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
									Then I get response body 
									And I get response status code 
									And I compare response value "/customerId" with "${customerId<id>}" 
									And I compare response value "/firstName" with "Aline" 
									And I compare response value "/lastName" with "<lastName>" 
									And I compare response value "/document" with "${document<id>}" 
									And I compare response value "/documentType" with "CPF" 
									And I compare response value "/businessType" with "IT" 
									And I verify if response value "/company" is empty 
									And I compare response value "/source" with "QA" 
									And I compare response value "/contact/phones/0/ddd" with "19" 
									And I compare response value "/contact/phones/0/number" with "982722791" 
									And I compare response value "/contact/phones/0/phoneType" with "1" 
									And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
									And I compare response value "/socialNetwork/instagram" with "1abender" 
									And I compare response value "/socialNetwork/facebook" with "1abender" 
									And I compare response value "/socialNetwork/twitter" with "1abender" 
									And I compare response value "/addresses/0/mainAddress" with "true" 
									And I compare response value "/addresses/0/city" with "Sumaré" 
									And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
									And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
									And I compare response value "/addresses/0/zipCode" with "13181643" 
									#And I compare response value "/addresses/0/country" with "Brasilã" 
									And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
									And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
									And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
									And I compare response value "/addresses/0/numberAddress" with "528" 
									And I compare response value "/addresses/0/latitude" with "${latitude}" 
									And I compare response value "/addresses/0/longitude" with "${longitude}" 
									And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
									And Response code must be 200 
									
									Examples: 
										| id | descricao                                       | lastName    |
										| 6  | com company vazio                               | ${random6}  |
										| 7  | com company nulo                                | ${random7}  |
										| 8  | sem company                                     | ${random8}  |
										
										
										@Positivo 
										Scenario Outline: Consultar cliente <descricao> 
											Given I set request header "client_id" as "${client_id}" 
											And I set request header "access_token" as "${access_token}" 
											And I set request header "Content-Type" as "application/json" 
											When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
											Then I get response body 
											And I get response status code 
											And I compare response value "/customerId" with "${customerId<id>}" 
											And I compare response value "/firstName" with "Aline" 
											And I compare response value "/lastName" with "<lastName>" 
											And I compare response value "/document" with "${document<id>}" 
											And I compare response value "/documentType" with "CPF" 
											And I compare response value "/businessType" with "IT" 
											#And I compare response value "/company" with "Sensedia" 
											And I verify if response value "/source" is empty 
											And I compare response value "/contact/phones/0/ddd" with "19" 
											And I compare response value "/contact/phones/0/number" with "982722791" 
											And I compare response value "/contact/phones/0/phoneType" with "1" 
											And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
											And I compare response value "/socialNetwork/instagram" with "1abender" 
											And I compare response value "/socialNetwork/facebook" with "1abender" 
											And I compare response value "/socialNetwork/twitter" with "1abender" 
											And I compare response value "/addresses/0/mainAddress" with "true" 
											And I compare response value "/addresses/0/city" with "Sumaré" 
											And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
											And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
											And I compare response value "/addresses/0/zipCode" with "13181643" 
											#And I compare response value "/addresses/0/country" with "Brasilã" 
											And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
											And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
											And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
											And I compare response value "/addresses/0/numberAddress" with "528" 
											And I compare response value "/addresses/0/latitude" with "${latitude}" 
											And I compare response value "/addresses/0/longitude" with "${longitude}" 
											And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
											And Response code must be 200 
											
											Examples: 
												| id | descricao                                       | lastName    |
												| 9  | com source vazio                                | ${random9}  |
												| 10 | com source nulo                                 | ${random10} |
												| 11 | sem source                                      | ${random11} |
												
												
												@Positivo 
												Scenario Outline: Consultar cliente <descricao> 
													Given I set request header "client_id" as "${client_id}" 
													And I set request header "access_token" as "${access_token}" 
													And I set request header "Content-Type" as "application/json" 
													When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
													Then I get response body 
													And I get response status code 
													And I compare response value "/customerId" with "${customerId<id>}" 
													And I compare response value "/firstName" with "Aline" 
													And I compare response value "/lastName" with "<lastName>" 
													And I compare response value "/document" with "${document<id>}" 
													And I compare response value "/documentType" with "CPF" 
													And I compare response value "/businessType" with "IT" 
													#And I compare response value "/company" with "Sensedia" 
													And I compare response value "/source" with "QA" 
													And I verify if response value "/contact/phones" is empty 
													And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
													And I compare response value "/socialNetwork/instagram" with "1abender" 
													And I compare response value "/socialNetwork/facebook" with "1abender" 
													And I compare response value "/socialNetwork/twitter" with "1abender" 
													And I compare response value "/addresses/0/mainAddress" with "true" 
													And I compare response value "/addresses/0/city" with "Sumaré" 
													And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
													And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
													And I compare response value "/addresses/0/zipCode" with "13181643" 
													#And I compare response value "/addresses/0/country" with "Brasilã" 
													And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
													And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
													And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
													And I compare response value "/addresses/0/numberAddress" with "528" 
													And I compare response value "/addresses/0/latitude" with "${latitude}" 
													And I compare response value "/addresses/0/longitude" with "${longitude}" 
													And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
													And Response code must be 200 
													
													Examples: 
														| id | descricao                                       | lastName    |
														| 12 | com contact.phones vazio                        | ${random12} |
														| 13 | com contact.phones nulo                         | ${random13} |
														| 14 | sem contact.phones                              | ${random14} |
														
														
														@Positivo 
														Scenario Outline: Consultar cliente <descricao> 
															Given I set request header "client_id" as "${client_id}" 
															And I set request header "access_token" as "${access_token}" 
															And I set request header "Content-Type" as "application/json" 
															When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
															Then I get response body 
															And I get response status code 
															And I compare response value "/customerId" with "${customerId<id>}" 
															And I compare response value "/firstName" with "Aline" 
															And I compare response value "/lastName" with "<lastName>" 
															And I compare response value "/document" with "${document<id>}" 
															And I compare response value "/documentType" with "CPF" 
															And I compare response value "/businessType" with "IT" 
															#And I compare response value "/company" with "Sensedia" 
															And I compare response value "/source" with "QA" 
															And I verify if response value "/contact/phones/0/ddd" is empty 
															And I compare response value "/contact/phones/0/number" with "982722791" 
															And I compare response value "/contact/phones/0/phoneType" with "1" 
															And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
															And I compare response value "/socialNetwork/instagram" with "1abender" 
															And I compare response value "/socialNetwork/facebook" with "1abender" 
															And I compare response value "/socialNetwork/twitter" with "1abender" 
															And I compare response value "/addresses/0/mainAddress" with "true" 
															And I compare response value "/addresses/0/city" with "Sumaré" 
															And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
															And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
															And I compare response value "/addresses/0/zipCode" with "13181643" 
															#And I compare response value "/addresses/0/country" with "Brasilã" 
															And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
															And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
															And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
															And I compare response value "/addresses/0/numberAddress" with "528" 
															And I compare response value "/addresses/0/latitude" with "${latitude}" 
															And I compare response value "/addresses/0/longitude" with "${longitude}" 
															And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
															And Response code must be 200 
															
															Examples: 
																| id | descricao                                       | lastName    |
																| 15 | com contact.phones.ddd vazio                    | ${random15} |
																| 16 | com contact.phones.ddd nulo                     | ${random16} |
																| 17 | sem contact.phones.ddd                          | ${random17} |
																
																
																@Positivo 
																Scenario Outline: Consultar cliente <descricao> 
																	Given I set request header "client_id" as "${client_id}" 
																	And I set request header "access_token" as "${access_token}" 
																	And I set request header "Content-Type" as "application/json" 
																	When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																	Then I get response body 
																	And I get response status code 
																	And I compare response value "/customerId" with "${customerId<id>}" 
																	And I compare response value "/firstName" with "Aline" 
																	And I compare response value "/lastName" with "<lastName>" 
																	And I compare response value "/document" with "${document<id>}" 
																	And I compare response value "/documentType" with "CPF" 
																	And I compare response value "/businessType" with "IT" 
																	#And I compare response value "/company" with "Sensedia" 
																	And I compare response value "/source" with "QA" 
																	And I compare response value "/contact/phones/0/ddd" with "19" 
																	And I verify if response value "/contact/phones/0/number" is empty 
																	And I compare response value "/contact/phones/0/phoneType" with "1" 
																	And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																	And I compare response value "/socialNetwork/instagram" with "1abender" 
																	And I compare response value "/socialNetwork/facebook" with "1abender" 
																	And I compare response value "/socialNetwork/twitter" with "1abender" 
																	And I compare response value "/addresses/0/mainAddress" with "true" 
																	And I compare response value "/addresses/0/city" with "Sumaré" 
																	And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																	And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																	And I compare response value "/addresses/0/zipCode" with "13181643" 
																	#And I compare response value "/addresses/0/country" with "Brasilã" 
																	And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																	And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																	And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																	And I compare response value "/addresses/0/numberAddress" with "528" 
																	And I compare response value "/addresses/0/latitude" with "${latitude}" 
																	And I compare response value "/addresses/0/longitude" with "${longitude}" 
																	And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																	And Response code must be 200 
																	
																	Examples: 
																		| id | descricao                                       | lastName    |
																		| 18 | com contact.phones.number vazio                 | ${random18} |
																		| 19 | com contact.phones.number nulo                  | ${random19} |
																		| 20 | sem contact.phones.number                       | ${random20} |
																		
																		
																		
																		@Positivo 
																		Scenario Outline: Consultar cliente <descricao> 
																			Given I set request header "client_id" as "${client_id}" 
																			And I set request header "access_token" as "${access_token}" 
																			And I set request header "Content-Type" as "application/json" 
																			When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																			Then I get response body 
																			And I get response status code 
																			And I compare response value "/customerId" with "${customerId<id>}" 
																			And I compare response value "/firstName" with "Aline" 
																			And I compare response value "/lastName" with "<lastName>" 
																			And I compare response value "/document" with "${document<id>}" 
																			And I compare response value "/documentType" with "CPF" 
																			And I compare response value "/businessType" with "IT" 
																			#And I compare response value "/company" with "Sensedia" 
																			And I compare response value "/source" with "QA" 
																			And I compare response value "/contact/phones/0/ddd" with "19" 
																			And I compare response value "/contact/phones/0/number" with "982722791" 
																			And I verify if response value "/contact/phones/0/phoneType" is empty 
																			And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																			And I compare response value "/socialNetwork/instagram" with "1abender" 
																			And I compare response value "/socialNetwork/facebook" with "1abender" 
																			And I compare response value "/socialNetwork/twitter" with "1abender" 
																			And I compare response value "/addresses/0/mainAddress" with "true" 
																			And I compare response value "/addresses/0/city" with "Sumaré" 
																			And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																			And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																			And I compare response value "/addresses/0/zipCode" with "13181643" 
																			#And I compare response value "/addresses/0/country" with "Brasilã" 
																			And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																			And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																			And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																			And I compare response value "/addresses/0/numberAddress" with "528" 
																			And I compare response value "/addresses/0/latitude" with "${latitude}" 
																			And I compare response value "/addresses/0/longitude" with "${longitude}" 
																			And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																			And Response code must be 200 
																			
																			Examples: 
																				| id | descricao                                       | lastName    |
																				| 21 | com contact.phones.phoneType vazio              | ${random21} |
																				| 22 | com contact.phones.phoneType nulo               | ${random22} |
																				| 23 | sem contact.phones.phoneType                    | ${random23} |
																				
																				
																				@Positivo 
																				Scenario Outline: Consultar cliente <descricao> 
																					Given I set request header "client_id" as "${client_id}" 
																					And I set request header "access_token" as "${access_token}" 
																					And I set request header "Content-Type" as "application/json" 
																					When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																					Then I get response body 
																					And I get response status code 
																					And I compare response value "/customerId" with "${customerId<id>}" 
																					And I compare response value "/firstName" with "Aline" 
																					And I compare response value "/lastName" with "<lastName>" 
																					And I compare response value "/document" with "${document<id>}" 
																					And I compare response value "/documentType" with "CPF" 
																					And I compare response value "/businessType" with "IT" 
																					#And I compare response value "/company" with "Sensedia" 
																					And I compare response value "/source" with "QA" 
																					And I compare response value "/contact/phones/0/ddd" with "19" 
																					And I compare response value "/contact/phones/0/number" with "982722791" 
																					And I compare response value "/contact/phones/0/phoneType" with "1" 
																					And I verify if response value "/contact/email" is empty 
																					And I compare response value "/socialNetwork/instagram" with "1abender" 
																					And I compare response value "/socialNetwork/facebook" with "1abender" 
																					And I compare response value "/socialNetwork/twitter" with "1abender" 
																					And I compare response value "/addresses/0/mainAddress" with "true" 
																					And I compare response value "/addresses/0/city" with "Sumaré" 
																					And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																					And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																					And I compare response value "/addresses/0/zipCode" with "13181643" 
																					#And I compare response value "/addresses/0/country" with "Brasilã" 
																					And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																					And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																					And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																					And I compare response value "/addresses/0/numberAddress" with "528" 
																					And I compare response value "/addresses/0/latitude" with "${latitude}" 
																					And I compare response value "/addresses/0/longitude" with "${longitude}" 
																					And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																					And Response code must be 200 
																					
																					Examples: 
																						| id | descricao                                       | lastName    |
																						| 24 | com email vazio                                 | ${random24} |
																						| 25 | com email nulo                                  | ${random25} |
																						| 26 | sem email                                       | ${random26} |
																						
																						
																						@Positivo 
																						Scenario Outline: Consultar cliente <descricao> 
																							Given I set request header "client_id" as "${client_id}" 
																							And I set request header "access_token" as "${access_token}" 
																							And I set request header "Content-Type" as "application/json" 
																							When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																							Then I get response body 
																							And I get response status code 
																							And I compare response value "/customerId" with "${customerId<id>}" 
																							And I compare response value "/firstName" with "Aline" 
																							And I compare response value "/lastName" with "<lastName>" 
																							And I compare response value "/document" with "${document<id>}" 
																							And I compare response value "/documentType" with "CPF" 
																							And I compare response value "/businessType" with "IT" 
																							#And I compare response value "/company" with "Sensedia" 
																							And I compare response value "/source" with "QA" 
																							And I compare response value "/contact/phones/0/ddd" with "19" 
																							And I compare response value "/contact/phones/0/number" with "982722791" 
																							And I compare response value "/contact/phones/0/phoneType" with "1" 
																							And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																							And I verify if response value "/contact/socialNetwork" is empty 
																							And I compare response value "/addresses/0/mainAddress" with "true" 
																							And I compare response value "/addresses/0/city" with "Sumaré" 
																							And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																							And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																							And I compare response value "/addresses/0/zipCode" with "13181643" 
																							#And I compare response value "/addresses/0/country" with "Brasilã" 
																							And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																							And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																							And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																							And I compare response value "/addresses/0/numberAddress" with "528" 
																							And I compare response value "/addresses/0/latitude" with "${latitude}" 
																							And I compare response value "/addresses/0/longitude" with "${longitude}" 
																							And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																							And Response code must be 200 
																							
																							Examples: 
																								| id | descricao                                       | lastName    |
																								| 27 | com socialNetwork vazio                         | ${random27} |
																								| 28 | com socialNetwork nulo                          | ${random28} |
																								| 29 | sem socialNetwork                               | ${random29} |
																								
																								
																								@Positivo 
																								Scenario Outline: Consultar cliente <descricao> 
																									Given I set request header "client_id" as "${client_id}" 
																									And I set request header "access_token" as "${access_token}" 
																									And I set request header "Content-Type" as "application/json" 
																									When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																									Then I get response body 
																									And I get response status code 
																									And I compare response value "/customerId" with "${customerId<id>}" 
																									And I compare response value "/firstName" with "Aline" 
																									And I compare response value "/lastName" with "<lastName>" 
																									And I compare response value "/document" with "${document<id>}" 
																									And I compare response value "/documentType" with "CPF" 
																									And I compare response value "/businessType" with "IT" 
																									#And I compare response value "/company" with "Sensedia" 
																									And I compare response value "/source" with "QA" 
																									And I compare response value "/contact/phones/0/ddd" with "19" 
																									And I compare response value "/contact/phones/0/number" with "982722791" 
																									And I compare response value "/contact/phones/0/phoneType" with "1" 
																									And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																									And I compare response value "/socialNetwork/instagram" with "1abender" 
																									And I compare response value "/socialNetwork/facebook" with "1abender" 
																									And I compare response value "/socialNetwork/twitter" with "1abender" 
																									And I compare response value "/addresses/0/mainAddress" with "true" 
																									And I verify if response value "/addresses/0/city" is empty 
																									And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																									And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																									And I compare response value "/addresses/0/zipCode" with "13181643" 
																									#And I compare response value "/addresses/0/country" with "Brasilã" 
																									And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																									And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																									And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																									And I compare response value "/addresses/0/numberAddress" with "528" 
																									And I compare response value "/addresses/0/latitude" with "${latitude}" 
																									And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																									And Response code must be 200 
																									
																									Examples: 
																										| id | descricao                                       | lastName    |
																										| 32 | com addresses.city vazio                        | ${random32} |
																										
																										
																										@Positivo 
																										Scenario Outline: Consultar cliente <descricao> 
																											Given I set request header "client_id" as "${client_id}" 
																											And I set request header "access_token" as "${access_token}" 
																											And I set request header "Content-Type" as "application/json" 
																											When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																											Then I get response body 
																											And I get response status code 
																											And I compare response value "/customerId" with "${customerId<id>}" 
																											And I compare response value "/firstName" with "Aline" 
																											And I compare response value "/lastName" with "<lastName>" 
																											And I compare response value "/document" with "${document<id>}" 
																											And I compare response value "/documentType" with "CPF" 
																											And I compare response value "/businessType" with "IT" 
																											#And I compare response value "/company" with "Sensedia" 
																											And I compare response value "/source" with "QA" 
																											And I compare response value "/contact/phones/0/ddd" with "19" 
																											And I compare response value "/contact/phones/0/number" with "982722791" 
																											And I compare response value "/contact/phones/0/phoneType" with "1" 
																											And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																											And I compare response value "/socialNetwork/instagram" with "1abender" 
																											And I compare response value "/socialNetwork/facebook" with "1abender" 
																											And I compare response value "/socialNetwork/twitter" with "1abender" 
																											And I compare response value "/addresses/0/mainAddress" with "true" 
																											And I compare response value "/addresses/0/city" with "Sumaré" 
																											And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																											And I verify if response value "/addresses/0/neighborhood" is empty 
																											And I compare response value "/addresses/0/zipCode" with "13181643" 
																											#And I compare response value "/addresses/0/country" with "Brasilã" 
																											And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																											And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																											And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																											And I compare response value "/addresses/0/numberAddress" with "528" 
																											And I compare response value "/addresses/0/latitude" with "${latitude}" 
																											And I compare response value "/addresses/0/longitude" with "${longitude}" 
																											And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																											And Response code must be 200 
																											
																											Examples: 
																												| id | descricao                                       | lastName    |
																												| 38 | com addresses.neighborhood vazio                | ${random38} |
																												
																												
																												@Positivo 
																												Scenario Outline: Consultar cliente <descricao> 
																													Given I set request header "client_id" as "${client_id}" 
																													And I set request header "access_token" as "${access_token}" 
																													And I set request header "Content-Type" as "application/json" 
																													When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																													Then I get response body 
																													And I get response status code 
																													And I compare response value "/customerId" with "${customerId<id>}" 
																													And I compare response value "/firstName" with "Aline" 
																													And I compare response value "/lastName" with "<lastName>" 
																													And I compare response value "/document" with "${document<id>}" 
																													And I compare response value "/documentType" with "CPF" 
																													And I compare response value "/businessType" with "IT" 
																													#And I compare response value "/company" with "Sensedia" 
																													And I compare response value "/source" with "QA" 
																													And I compare response value "/contact/phones/0/ddd" with "19" 
																													And I compare response value "/contact/phones/0/number" with "982722791" 
																													And I compare response value "/contact/phones/0/phoneType" with "1" 
																													And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																													And I compare response value "/socialNetwork/instagram" with "1abender" 
																													And I compare response value "/socialNetwork/facebook" with "1abender" 
																													And I compare response value "/socialNetwork/twitter" with "1abender" 
																													And I compare response value "/addresses/0/mainAddress" with "true" 
																													And I compare response value "/addresses/0/city" with "Sumaré" 
																													And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																													And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																													And I verify if response value "/addresses/0/zipCode" is empty 
																													#And I compare response value "/addresses/0/country" with "Brasilã" 
																													And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																													And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																													And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																													And I compare response value "/addresses/0/numberAddress" with "528" 
																													And I compare response value "/addresses/0/latitude" with "${latitude}" 
																													And I compare response value "/addresses/0/longitude" with "${longitude}" 
																													And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																													And Response code must be 200 
																													
																													Examples: 
																														| id | descricao                                       | lastName    |
																														| 41 | com addresses.zipCode vazio                     | ${random41} |
																														
																														
																														@Positivo 
																														Scenario Outline: Consultar cliente <descricao> 
																															Given I set request header "client_id" as "${client_id}" 
																															And I set request header "access_token" as "${access_token}" 
																															And I set request header "Content-Type" as "application/json" 
																															When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																															Then I get response body 
																															And I get response status code 
																															And I compare response value "/customerId" with "${customerId<id>}" 
																															And I compare response value "/firstName" with "Aline" 
																															And I compare response value "/lastName" with "<lastName>" 
																															And I compare response value "/document" with "${document<id>}" 
																															And I compare response value "/documentType" with "CPF" 
																															And I compare response value "/businessType" with "IT" 
																															#And I compare response value "/company" with "Sensedia" 
																															And I compare response value "/source" with "QA" 
																															And I compare response value "/contact/phones/0/ddd" with "19" 
																															And I compare response value "/contact/phones/0/number" with "982722791" 
																															And I compare response value "/contact/phones/0/phoneType" with "1" 
																															And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																															And I compare response value "/socialNetwork/instagram" with "1abender" 
																															And I compare response value "/socialNetwork/facebook" with "1abender" 
																															And I compare response value "/socialNetwork/twitter" with "1abender" 
																															And I compare response value "/addresses/0/mainAddress" with "true" 
																															And I compare response value "/addresses/0/city" with "Sumaré" 
																															And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																															And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																															And I compare response value "/addresses/0/zipCode" with "13181643" 
																															And I verify if response value "/addresses/0/country" is empty 
																															And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																															And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																															And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																															And I compare response value "/addresses/0/numberAddress" with "528" 
																															And I compare response value "/addresses/0/latitude" with "${latitude}" 
																															And I compare response value "/addresses/0/longitude" with "${longitude}" 
																															And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																															And Response code must be 200 
																															
																															Examples: 
																																| id | descricao                                       | lastName    |
																																| 44 | com addresses.country vazio                     | ${random44} |
																																
																																
																																@Positivo 
																																Scenario Outline: Consultar cliente <descricao> 
																																	Given I set request header "client_id" as "${client_id}" 
																																	And I set request header "access_token" as "${access_token}" 
																																	And I set request header "Content-Type" as "application/json" 
																																	When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																	Then I get response body 
																																	And I get response status code 
																																	And I compare response value "/customerId" with "${customerId<id>}" 
																																	And I compare response value "/firstName" with "Aline" 
																																	And I compare response value "/lastName" with "<lastName>" 
																																	And I compare response value "/document" with "${document<id>}" 
																																	And I compare response value "/documentType" with "CPF" 
																																	And I compare response value "/businessType" with "IT" 
																																	#And I compare response value "/company" with "Sensedia" 
																																	And I compare response value "/source" with "QA" 
																																	And I compare response value "/contact/phones/0/ddd" with "19" 
																																	And I compare response value "/contact/phones/0/number" with "982722791" 
																																	And I compare response value "/contact/phones/0/phoneType" with "1" 
																																	And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																	And I compare response value "/socialNetwork/instagram" with "1abender" 
																																	And I compare response value "/socialNetwork/facebook" with "1abender" 
																																	And I compare response value "/socialNetwork/twitter" with "1abender" 
																																	And I compare response value "/addresses/0/mainAddress" with "true" 
																																	And I compare response value "/addresses/0/city" with "Sumaré" 
																																	And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																	And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																	And I compare response value "/addresses/0/zipCode" with "13181643" 
																																	#And I compare response value "/addresses/0/country" with "Brasilã" 
																																	And I verify if response value "/addresses/0/referenceAddress" is empty 
																																	And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																	And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																	And I compare response value "/addresses/0/numberAddress" with "528" 
																																	And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																	And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																	And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																	And Response code must be 200 
																																	
																																	Examples: 
																																		| id | descricao                                       | lastName    |
																																		| 47 | com addresses.referenceAddress vazio            | ${random47} |
																																		| 48 | com addresses.referenceAddress nulo             | ${random48} |
																																		| 49 | sem addresses.referenceAddress                  | ${random49} |
																																		
																																		
																																		@Positivo 
																																		Scenario Outline: Consultar cliente <descricao> 
																																			Given I set request header "client_id" as "${client_id}" 
																																			And I set request header "access_token" as "${access_token}" 
																																			And I set request header "Content-Type" as "application/json" 
																																			When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																			Then I get response body 
																																			And I get response status code 
																																			And I compare response value "/customerId" with "${customerId<id>}" 
																																			And I compare response value "/firstName" with "Aline" 
																																			And I compare response value "/lastName" with "<lastName>" 
																																			And I compare response value "/document" with "${document<id>}" 
																																			And I compare response value "/documentType" with "CPF" 
																																			And I compare response value "/businessType" with "IT" 
																																			#And I compare response value "/company" with "Sensedia" 
																																			And I compare response value "/source" with "QA" 
																																			And I compare response value "/contact/phones/0/ddd" with "19" 
																																			And I compare response value "/contact/phones/0/number" with "982722791" 
																																			And I compare response value "/contact/phones/0/phoneType" with "1" 
																																			And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																			And I compare response value "/socialNetwork/instagram" with "1abender" 
																																			And I compare response value "/socialNetwork/facebook" with "1abender" 
																																			And I compare response value "/socialNetwork/twitter" with "1abender" 
																																			And I compare response value "/addresses/0/mainAddress" with "true" 
																																			And I compare response value "/addresses/0/city" with "Sumaré" 
																																			And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																			And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																			And I compare response value "/addresses/0/zipCode" with "13181643" 
																																			#And I compare response value "/addresses/0/country" with "Brasilã" 
																																			And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																			And I verify if response value "/addresses/0/address" is empty 
																																			And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																			And I compare response value "/addresses/0/numberAddress" with "528" 
																																			And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																			And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																			And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																			And Response code must be 200 
																																			
																																			Examples: 
																																				| id | descricao                                       | lastName    |
																																				| 50 | com addresses.address vazio                     | ${random50} |
																																				
																																				
																																				@Positivo 
																																				Scenario Outline: Consultar cliente <descricao> 
																																					Given I set request header "client_id" as "${client_id}" 
																																					And I set request header "access_token" as "${access_token}" 
																																					And I set request header "Content-Type" as "application/json" 
																																					When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																					Then I get response body 
																																					And I get response status code 
																																					And I compare response value "/customerId" with "${customerId<id>}" 
																																					And I compare response value "/firstName" with "Aline" 
																																					And I compare response value "/lastName" with "<lastName>" 
																																					And I compare response value "/document" with "${document<id>}" 
																																					And I compare response value "/documentType" with "CPF" 
																																					And I compare response value "/businessType" with "IT" 
																																					#And I compare response value "/company" with "Sensedia" 
																																					And I compare response value "/source" with "QA" 
																																					And I compare response value "/contact/phones/0/ddd" with "19" 
																																					And I compare response value "/contact/phones/0/number" with "982722791" 
																																					And I compare response value "/contact/phones/0/phoneType" with "1" 
																																					And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																					And I compare response value "/socialNetwork/instagram" with "1abender" 
																																					And I compare response value "/socialNetwork/facebook" with "1abender" 
																																					And I compare response value "/socialNetwork/twitter" with "1abender" 
																																					And I compare response value "/addresses/0/mainAddress" with "true" 
																																					And I compare response value "/addresses/0/city" with "Sumaré" 
																																					And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																					And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																					And I compare response value "/addresses/0/zipCode" with "13181643" 
																																					#And I compare response value "/addresses/0/country" with "Brasilã" 
																																					And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																					And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																					And I verify if response value "/addresses/0/additionAddress" is empty 
																																					And I compare response value "/addresses/0/numberAddress" with "528" 
																																					And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																					And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																					And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																					And Response code must be 200 
																																					
																																					Examples: 
																																						| id | descricao                                       | lastName    |
																																						| 53 | com addresses.additionAddress vazio             | ${random53} |
																																						
																																						
																																						@Positivo 
																																						Scenario Outline: Consultar cliente <descricao> 
																																							Given I set request header "client_id" as "${client_id}" 
																																							And I set request header "access_token" as "${access_token}" 
																																							And I set request header "Content-Type" as "application/json" 
																																							When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																							Then I get response body 
																																							And I get response status code 
																																							And I compare response value "/customerId" with "${customerId<id>}" 
																																							And I compare response value "/firstName" with "Aline" 
																																							And I compare response value "/lastName" with "<lastName>" 
																																							And I compare response value "/document" with "${document<id>}" 
																																							And I compare response value "/documentType" with "CPF" 
																																							And I compare response value "/businessType" with "IT" 
																																							#And I compare response value "/company" with "Sensedia" 
																																							And I compare response value "/source" with "QA" 
																																							And I compare response value "/contact/phones/0/ddd" with "19" 
																																							And I compare response value "/contact/phones/0/number" with "982722791" 
																																							And I compare response value "/contact/phones/0/phoneType" with "1" 
																																							And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																							And I compare response value "/socialNetwork/instagram" with "1abender" 
																																							And I compare response value "/socialNetwork/facebook" with "1abender" 
																																							And I compare response value "/socialNetwork/twitter" with "1abender" 
																																							And I compare response value "/addresses/0/mainAddress" with "true" 
																																							And I compare response value "/addresses/0/city" with "Sumaré" 
																																							And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																							And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																							And I compare response value "/addresses/0/zipCode" with "13181643" 
																																							#And I compare response value "/addresses/0/country" with "Brasilã" 
																																							And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																							And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																							And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																							And I verify if response value "/addresses/0/numberAddress" is empty 
																																							And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																							And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																							And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																							And Response code must be 200 
																																							
																																							Examples: 
																																								| id | descricao                                       | lastName    |
																																								| 56 | com addresses.numberAddress vazio               | ${random56} |
																																								
																																								
																																								@Positivo 
																																								Scenario Outline: Consultar cliente <descricao> 
																																									Given I set request header "client_id" as "${client_id}" 
																																									And I set request header "access_token" as "${access_token}" 
																																									And I set request header "Content-Type" as "application/json" 
																																									When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																									Then I get response body 
																																									And I get response status code 
																																									And I compare response value "/customerId" with "${customerId<id>}" 
																																									And I compare response value "/firstName" with "Aline" 
																																									And I compare response value "/lastName" with "<lastName>" 
																																									And I compare response value "/document" with "${document<id>}" 
																																									And I compare response value "/documentType" with "CPF" 
																																									And I compare response value "/businessType" with "IT" 
																																									#And I compare response value "/company" with "Sensedia" 
																																									And I compare response value "/source" with "QA" 
																																									And I compare response value "/contact/phones/0/ddd" with "19" 
																																									And I compare response value "/contact/phones/0/number" with "982722791" 
																																									And I compare response value "/contact/phones/0/phoneType" with "1" 
																																									And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																									And I compare response value "/socialNetwork/instagram" with "1abender" 
																																									And I compare response value "/socialNetwork/facebook" with "1abender" 
																																									And I compare response value "/socialNetwork/twitter" with "1abender" 
																																									And I compare response value "/addresses/0/mainAddress" with "true" 
																																									And I compare response value "/addresses/0/city" with "Sumaré" 
																																									And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																									And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																									And I compare response value "/addresses/0/zipCode" with "13181643" 
																																									#And I compare response value "/addresses/0/country" with "Brasilã" 
																																									And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																									And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																									And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																									And I compare response value "/addresses/0/numberAddress" with "528" 
																																									And I verify if response value "/addresses/0/latitude" is empty 
																																									And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																									And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																									And Response code must be 200 
																																									
																																									Examples: 
																																										| id | descricao                                       | lastName    |
																																										| 60 | com addresses.latitude nulo                     | ${random60} |
																																										| 61 | sem addresses.latitude                          | ${random61} |
																																										
																																										
																																										@Positivo 
																																										Scenario Outline: Consultar cliente <descricao> 
																																											Given I set request header "client_id" as "${client_id}" 
																																											And I set request header "access_token" as "${access_token}" 
																																											And I set request header "Content-Type" as "application/json" 
																																											When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																											Then I get response body 
																																											And I get response status code 
																																											And I compare response value "/customerId" with "${customerId<id>}" 
																																											And I compare response value "/firstName" with "Aline" 
																																											And I compare response value "/lastName" with "<lastName>" 
																																											And I compare response value "/document" with "${document<id>}" 
																																											And I compare response value "/documentType" with "CPF" 
																																											And I compare response value "/businessType" with "IT" 
																																											#And I compare response value "/company" with "Sensedia" 
																																											And I compare response value "/source" with "QA" 
																																											And I compare response value "/contact/phones/0/ddd" with "19" 
																																											And I compare response value "/contact/phones/0/number" with "982722791" 
																																											And I compare response value "/contact/phones/0/phoneType" with "1" 
																																											And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																											And I compare response value "/socialNetwork/instagram" with "1abender" 
																																											And I compare response value "/socialNetwork/facebook" with "1abender" 
																																											And I compare response value "/socialNetwork/twitter" with "1abender" 
																																											And I compare response value "/addresses/0/mainAddress" with "true" 
																																											And I compare response value "/addresses/0/city" with "Sumaré" 
																																											And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																											And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																											And I compare response value "/addresses/0/zipCode" with "13181643" 
																																											#And I compare response value "/addresses/0/country" with "Brasilã" 
																																											And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																											And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																											And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																											And I compare response value "/addresses/0/numberAddress" with "528" 
																																											And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																											And I verify if response value "/addresses/0/longitude" is empty 
																																											And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																											And Response code must be 200 
																																											
																																											Examples: 
																																												| id | descricao                                       | lastName    |
																																												| 63 | com addresses.longitude nulo                    | ${random63} |
																																												| 64 | sem addresses.longitude                         | ${random64} |
																																												
																																												
																																												@Positivo 
																																												Scenario Outline: Consultar cliente <descricao> 
																																													Given I set request header "client_id" as "${client_id}" 
																																													And I set request header "access_token" as "${access_token}" 
																																													And I set request header "Content-Type" as "application/json" 
																																													When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																													Then I get response body 
																																													And I get response status code 
																																													And I compare response value "/customerId" with "${customerId<id>}" 
																																													And I compare response value "/firstName" with "Aline" 
																																													And I compare response value "/lastName" with "<lastName>" 
																																													And I compare response value "/document" with "${document<id>}" 
																																													And I compare response value "/documentType" with "CPF" 
																																													And I compare response value "/businessType" with "IT" 
																																													#And I compare response value "/company" with "Sensedia" 
																																													And I compare response value "/source" with "QA" 
																																													And I compare response value "/contact/phones/0/ddd" with "19" 
																																													And I compare response value "/contact/phones/0/number" with "982722791" 
																																													And I compare response value "/contact/phones/0/phoneType" with "1" 
																																													And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																													And I compare response value "/socialNetwork/instagram" with "1abender" 
																																													And I compare response value "/socialNetwork/facebook" with "1abender" 
																																													And I compare response value "/socialNetwork/twitter" with "1abender" 
																																													And I compare response value "/addresses/0/mainAddress" with "true" 
																																													And I compare response value "/addresses/0/city" with "Sumaré" 
																																													And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																													And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																													And I compare response value "/addresses/0/zipCode" with "13181643" 
																																													#And I compare response value "/addresses/0/country" with "Brasilã" 
																																													And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																													And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																													And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																													And I compare response value "/addresses/0/numberAddress" with "528" 
																																													And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																													And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																													And I verify if response value "/addresses/0/descriptionAddress" is empty 
																																													And Response code must be 200 
																																													
																																													Examples: 
																																														| id | descricao                                       | lastName    |
																																														| 65 | com addresses.descriptionAddress vazio          | ${random65} |
																																														
																																														
																																														@PreRequest 
																																														Scenario Outline: Atualizar cliente <descricao> 
																																															Given System generate random number 
																																															And System generate random CPF 
																																															And I set request header "client_id" as "${client_id}" 
																																															And I set request header "access_token" as "${access_token}" 
																																															And I set request header "Content-Type" as "application/json" 
																																															And I set request body as "<body>" 
																																															When I set PUT api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																															Then I get response body 
																																															And I get response status code 
																																															And I save "${random}" as "random<id>" 
																																															And I save "${randomCPF}" as "document<id>" 
																																															And I verify if response body is empty 
																																															And Response code must be 204 
																																															And I wait 2 seconds 
																																															
																																															Examples: 
																																																| id | descricao                                       | body                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
																																																| 1  | com sucesso                                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 2  | com nome repetido                               | {"firstName":"Aline","lastName":"Bender","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 3  | com businessType vazio                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 4  | com businessType null                           | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":null,"company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 5  | sem businessType                                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 6  | com company vazio                               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 7  | com company nulo                                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":null,"source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 8  | sem company                                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 9  | com source vazio                                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 10 | com source nulo                                 | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":null,"contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 11 | sem source                                      | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 12 | com contact.phones vazio                        | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 13 | com contact.phones nulo                         | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":null,"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 14 | sem contact.phones                              | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 15 | com contact.phones.ddd vazio                    | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 16 | com contact.phones.ddd nulo                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":null,"number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 17 | sem contact.phones.ddd                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 18 | com contact.phones.number vazio                 | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 19 | com contact.phones.number nulo                  | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":null,"phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 20 | sem contact.phones.number                       | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 21 | com contact.phones.phoneType vazio              | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":""}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 22 | com contact.phones.phoneType nulo               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":null}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 23 | sem contact.phones.phoneType                    | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791"}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 24 | com email vazio                                 | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":""},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 25 | com email nulo                                  | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":null},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 26 | sem email                                       | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}]},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 27 | com socialNetwork vazio                         | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 28 | com socialNetwork nulo                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":null,"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 29 | sem socialNetwork                               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 32 | com addresses.city vazio                        | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 38 | com addresses.neighborhood vazio                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 41 | com addresses.zipCode vazio                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 44 | com addresses.country vazio                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 47 | com addresses.referenceAddress vazio            | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 48 | com addresses.referenceAddress nulo             | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":null,"address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 49 | sem addresses.referenceAddress                  | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 50 | com addresses.address vazio                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 53 | com addresses.additionAddress vazio             | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 56 | com addresses.numberAddress vazio               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 60 | com addresses.latitude nulo                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":null,"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 61 | sem addresses.latitude                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 63 | com addresses.longitude nulo                    | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":null,"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 64 | sem addresses.longitude                         | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 65 | com addresses.descriptionAddress vazio          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":""}]} |
																																																| 69 | com addresses.latitude longo                    | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude}1132320109321903210938219382109382109830921830921830291830219830921839021830218390218309218302918302183092183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310292183091283092183092183021830219830921830921830921830921839218392103890213802198390218302918310293821,"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 70 | com addresses.longitude longo                   | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude}92381209382103801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238012983092183021830921893082193812103892183012830219830912830912830921839018092380129830921830218309218930821938121038921830128302198309128309128309218390180923801298309218302183092189308219381210389218301283021983091283091283092183901809238210938912038092183012983091283092183018032180932813821093821093821938021382901830912830912389021382193890128310829138219083091823,"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 71 | com company longo                               | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. Company too long. ","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 72 | com contact.phones.phoneType longo              | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131123132132132132131}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 73 | com document vazio                              | {"firstName":"Aline","lastName":"${random}","document":"","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 74 | com document nulo                               | {"firstName":"Aline","lastName":"${random}","document":null,"documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 75 | sem document                                    | {"firstName":"Aline","lastName":"${random}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 76 | com documentType vazio                          | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 77 | com documentType nulo                           | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":null,"businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																| 78 | sem documentType                                | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
																																																
																																																
																																																@Positivo 
				Scenario Outline: Consultar cliente <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/customerId" with "${customerId<id>}" 
					And I compare response value "/firstName" with "Aline" 
					And I compare response value "/lastName" with "<lastName>" 
					And I compare response value "/document" with "${document<id>}" 
					And I compare response value "/documentType" with "CPF" 
					And I compare response value "/businessType" with "IT" 
					#And I compare response value "/company" with "Sensedia" 
					And I compare response value "/source" with "QA" 
					And I compare response value "/contact/phones/0/ddd" with "19" 
					And I compare response value "/contact/phones/0/number" with "982722791" 
					And I compare response value "/contact/phones/0/phoneType" with "1" 
					And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
					And I compare response value "/socialNetwork/instagram" with "1abender" 
					And I compare response value "/socialNetwork/facebook" with "1abender" 
					And I compare response value "/socialNetwork/twitter" with "1abender" 
					And I compare response value "/addresses/0/mainAddress" with "true" 
					And I compare response value "/addresses/0/city" with "Sumaré" 
					And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
					And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
					And I compare response value "/addresses/0/zipCode" with "13181643" 
					#And I compare response value "/addresses/0/country" with "Brasilã" 
					And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
					And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
					And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
					And I compare response value "/addresses/0/numberAddress" with "528" 
					And I compare response value "/addresses/0/latitude" with "${latitude}" 
					And I compare response value "/addresses/0/longitude" with "${longitude}" 
					And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
					And Response code must be 200 
					
					Examples: 
						| id | descricao                                       | lastName    |
						| 1  | com sucesso                                     | ${random1}  |
						| 2  | com nome repetido                               | Bender      |
						
						
						@Positivo 
						Scenario Outline: Consultar cliente <descricao> 
							Given I set request header "client_id" as "${client_id}" 
							And I set request header "access_token" as "${access_token}" 
							And I set request header "Content-Type" as "application/json" 
							When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/customerId" with "${customerId<id>}" 
							And I compare response value "/firstName" with "Aline" 
							And I compare response value "/lastName" with "${random<id>}" 
							And I compare response value "/document" with "${document<id>}" 
							And I compare response value "/documentType" with "CPF" 
							And I verify if response value "/businessType" is empty 
							#And I compare response value "/company" with "Sensedia" 
							And I compare response value "/source" with "QA" 
							And I compare response value "/contact/phones/0/ddd" with "19" 
							And I compare response value "/contact/phones/0/number" with "982722791" 
							And I compare response value "/contact/phones/0/phoneType" with "1" 
							And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
							And I compare response value "/socialNetwork/instagram" with "1abender" 
							And I compare response value "/socialNetwork/facebook" with "1abender" 
							And I compare response value "/socialNetwork/twitter" with "1abender" 
							And I compare response value "/addresses/0/mainAddress" with "true" 
							And I compare response value "/addresses/0/city" with "Sumaré" 
							And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
							And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
							And I compare response value "/addresses/0/zipCode" with "13181643" 
							#And I compare response value "/addresses/0/country" with "Brasilã" 
							And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
							And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
							And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
							And I compare response value "/addresses/0/numberAddress" with "528" 
							And I compare response value "/addresses/0/latitude" with "${latitude}" 
							And I compare response value "/addresses/0/longitude" with "${longitude}" 
							And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
							And Response code must be 200 
							
							Examples: 
								| id | descricao                                       |
								| 3  | com businessType vazio                          |
								| 4  | com businessType null                           |
								| 5  | sem businessType                                |
								
								
								@Positivo 
								Scenario Outline: Consultar cliente <descricao> 
									Given I set request header "client_id" as "${client_id}" 
									And I set request header "access_token" as "${access_token}" 
									And I set request header "Content-Type" as "application/json" 
									When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
									Then I get response body 
									And I get response status code 
									And I compare response value "/customerId" with "${customerId<id>}" 
									And I compare response value "/firstName" with "Aline" 
									And I compare response value "/lastName" with "<lastName>" 
									And I compare response value "/document" with "${document<id>}" 
									And I compare response value "/documentType" with "CPF" 
									And I compare response value "/businessType" with "IT" 
									And I verify if response value "/company" is empty 
									And I compare response value "/source" with "QA" 
									And I compare response value "/contact/phones/0/ddd" with "19" 
									And I compare response value "/contact/phones/0/number" with "982722791" 
									And I compare response value "/contact/phones/0/phoneType" with "1" 
									And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
									And I compare response value "/socialNetwork/instagram" with "1abender" 
									And I compare response value "/socialNetwork/facebook" with "1abender" 
									And I compare response value "/socialNetwork/twitter" with "1abender" 
									And I compare response value "/addresses/0/mainAddress" with "true" 
									And I compare response value "/addresses/0/city" with "Sumaré" 
									And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
									And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
									And I compare response value "/addresses/0/zipCode" with "13181643" 
									#And I compare response value "/addresses/0/country" with "Brasilã" 
									And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
									And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
									And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
									And I compare response value "/addresses/0/numberAddress" with "528" 
									And I compare response value "/addresses/0/latitude" with "${latitude}" 
									And I compare response value "/addresses/0/longitude" with "${longitude}" 
									And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
									And Response code must be 200 
									
									Examples: 
										| id | descricao                                       | lastName    |
										| 6  | com company vazio                               | ${random6}  |
										| 7  | com company nulo                                | ${random7}  |
										| 8  | sem company                                     | ${random8}  |
										
										
										@Positivo 
										Scenario Outline: Consultar cliente <descricao> 
											Given I set request header "client_id" as "${client_id}" 
											And I set request header "access_token" as "${access_token}" 
											And I set request header "Content-Type" as "application/json" 
											When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
											Then I get response body 
											And I get response status code 
											And I compare response value "/customerId" with "${customerId<id>}" 
											And I compare response value "/firstName" with "Aline" 
											And I compare response value "/lastName" with "<lastName>" 
											And I compare response value "/document" with "${document<id>}" 
											And I compare response value "/documentType" with "CPF" 
											And I compare response value "/businessType" with "IT" 
											#And I compare response value "/company" with "Sensedia" 
											And I verify if response value "/source" is empty 
											And I compare response value "/contact/phones/0/ddd" with "19" 
											And I compare response value "/contact/phones/0/number" with "982722791" 
											And I compare response value "/contact/phones/0/phoneType" with "1" 
											And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
											And I compare response value "/socialNetwork/instagram" with "1abender" 
											And I compare response value "/socialNetwork/facebook" with "1abender" 
											And I compare response value "/socialNetwork/twitter" with "1abender" 
											And I compare response value "/addresses/0/mainAddress" with "true" 
											And I compare response value "/addresses/0/city" with "Sumaré" 
											And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
											And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
											And I compare response value "/addresses/0/zipCode" with "13181643" 
											#And I compare response value "/addresses/0/country" with "Brasilã" 
											And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
											And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
											And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
											And I compare response value "/addresses/0/numberAddress" with "528" 
											And I compare response value "/addresses/0/latitude" with "${latitude}" 
											And I compare response value "/addresses/0/longitude" with "${longitude}" 
											And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
											And Response code must be 200 
											
											Examples: 
												| id | descricao                                       | lastName    |
												| 9  | com source vazio                                | ${random9}  |
												| 10 | com source nulo                                 | ${random10} |
												| 11 | sem source                                      | ${random11} |
												
												
												@Positivo 
												Scenario Outline: Consultar cliente <descricao> 
													Given I set request header "client_id" as "${client_id}" 
													And I set request header "access_token" as "${access_token}" 
													And I set request header "Content-Type" as "application/json" 
													When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
													Then I get response body 
													And I get response status code 
													And I compare response value "/customerId" with "${customerId<id>}" 
													And I compare response value "/firstName" with "Aline" 
													And I compare response value "/lastName" with "<lastName>" 
													And I compare response value "/document" with "${document<id>}" 
													And I compare response value "/documentType" with "CPF" 
													And I compare response value "/businessType" with "IT" 
													#And I compare response value "/company" with "Sensedia" 
													And I compare response value "/source" with "QA" 
													And I verify if response value "/contact/phones" is empty 
													And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
													And I compare response value "/socialNetwork/instagram" with "1abender" 
													And I compare response value "/socialNetwork/facebook" with "1abender" 
													And I compare response value "/socialNetwork/twitter" with "1abender" 
													And I compare response value "/addresses/0/mainAddress" with "true" 
													And I compare response value "/addresses/0/city" with "Sumaré" 
													And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
													And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
													And I compare response value "/addresses/0/zipCode" with "13181643" 
													#And I compare response value "/addresses/0/country" with "Brasilã" 
													And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
													And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
													And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
													And I compare response value "/addresses/0/numberAddress" with "528" 
													And I compare response value "/addresses/0/latitude" with "${latitude}" 
													And I compare response value "/addresses/0/longitude" with "${longitude}" 
													And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
													And Response code must be 200 
													
													Examples: 
														| id | descricao                                       | lastName    |
														| 12 | com contact.phones vazio                        | ${random12} |
														| 13 | com contact.phones nulo                         | ${random13} |
														| 14 | sem contact.phones                              | ${random14} |
														
														
														@Positivo 
														Scenario Outline: Consultar cliente <descricao> 
															Given I set request header "client_id" as "${client_id}" 
															And I set request header "access_token" as "${access_token}" 
															And I set request header "Content-Type" as "application/json" 
															When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
															Then I get response body 
															And I get response status code 
															And I compare response value "/customerId" with "${customerId<id>}" 
															And I compare response value "/firstName" with "Aline" 
															And I compare response value "/lastName" with "<lastName>" 
															And I compare response value "/document" with "${document<id>}" 
															And I compare response value "/documentType" with "CPF" 
															And I compare response value "/businessType" with "IT" 
															#And I compare response value "/company" with "Sensedia" 
															And I compare response value "/source" with "QA" 
															And I verify if response value "/contact/phones/0/ddd" is empty 
															And I compare response value "/contact/phones/0/number" with "982722791" 
															And I compare response value "/contact/phones/0/phoneType" with "1" 
															And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
															And I compare response value "/socialNetwork/instagram" with "1abender" 
															And I compare response value "/socialNetwork/facebook" with "1abender" 
															And I compare response value "/socialNetwork/twitter" with "1abender" 
															And I compare response value "/addresses/0/mainAddress" with "true" 
															And I compare response value "/addresses/0/city" with "Sumaré" 
															And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
															And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
															And I compare response value "/addresses/0/zipCode" with "13181643" 
															#And I compare response value "/addresses/0/country" with "Brasilã" 
															And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
															And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
															And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
															And I compare response value "/addresses/0/numberAddress" with "528" 
															And I compare response value "/addresses/0/latitude" with "${latitude}" 
															And I compare response value "/addresses/0/longitude" with "${longitude}" 
															And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
															And Response code must be 200 
															
															Examples: 
																| id | descricao                                       | lastName    |
																| 15 | com contact.phones.ddd vazio                    | ${random15} |
																| 16 | com contact.phones.ddd nulo                     | ${random16} |
																| 17 | sem contact.phones.ddd                          | ${random17} |
																
																
																@Positivo 
																Scenario Outline: Consultar cliente <descricao> 
																	Given I set request header "client_id" as "${client_id}" 
																	And I set request header "access_token" as "${access_token}" 
																	And I set request header "Content-Type" as "application/json" 
																	When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																	Then I get response body 
																	And I get response status code 
																	And I compare response value "/customerId" with "${customerId<id>}" 
																	And I compare response value "/firstName" with "Aline" 
																	And I compare response value "/lastName" with "<lastName>" 
																	And I compare response value "/document" with "${document<id>}" 
																	And I compare response value "/documentType" with "CPF" 
																	And I compare response value "/businessType" with "IT" 
																	#And I compare response value "/company" with "Sensedia" 
																	And I compare response value "/source" with "QA" 
																	And I compare response value "/contact/phones/0/ddd" with "19" 
																	And I verify if response value "/contact/phones/0/number" is empty 
																	And I compare response value "/contact/phones/0/phoneType" with "1" 
																	And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																	And I compare response value "/socialNetwork/instagram" with "1abender" 
																	And I compare response value "/socialNetwork/facebook" with "1abender" 
																	And I compare response value "/socialNetwork/twitter" with "1abender" 
																	And I compare response value "/addresses/0/mainAddress" with "true" 
																	And I compare response value "/addresses/0/city" with "Sumaré" 
																	And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																	And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																	And I compare response value "/addresses/0/zipCode" with "13181643" 
																	#And I compare response value "/addresses/0/country" with "Brasilã" 
																	And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																	And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																	And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																	And I compare response value "/addresses/0/numberAddress" with "528" 
																	And I compare response value "/addresses/0/latitude" with "${latitude}" 
																	And I compare response value "/addresses/0/longitude" with "${longitude}" 
																	And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																	And Response code must be 200 
																	
																	Examples: 
																		| id | descricao                                       | lastName    |
																		| 18 | com contact.phones.number vazio                 | ${random18} |
																		| 19 | com contact.phones.number nulo                  | ${random19} |
																		| 20 | sem contact.phones.number                       | ${random20} |
																		
																		
																		
																		@Positivo 
																		Scenario Outline: Consultar cliente <descricao> 
																			Given I set request header "client_id" as "${client_id}" 
																			And I set request header "access_token" as "${access_token}" 
																			And I set request header "Content-Type" as "application/json" 
																			When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																			Then I get response body 
																			And I get response status code 
																			And I compare response value "/customerId" with "${customerId<id>}" 
																			And I compare response value "/firstName" with "Aline" 
																			And I compare response value "/lastName" with "<lastName>" 
																			And I compare response value "/document" with "${document<id>}" 
																			And I compare response value "/documentType" with "CPF" 
																			And I compare response value "/businessType" with "IT" 
																			#And I compare response value "/company" with "Sensedia" 
																			And I compare response value "/source" with "QA" 
																			And I compare response value "/contact/phones/0/ddd" with "19" 
																			And I compare response value "/contact/phones/0/number" with "982722791" 
																			And I verify if response value "/contact/phones/0/phoneType" is empty 
																			And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																			And I compare response value "/socialNetwork/instagram" with "1abender" 
																			And I compare response value "/socialNetwork/facebook" with "1abender" 
																			And I compare response value "/socialNetwork/twitter" with "1abender" 
																			And I compare response value "/addresses/0/mainAddress" with "true" 
																			And I compare response value "/addresses/0/city" with "Sumaré" 
																			And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																			And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																			And I compare response value "/addresses/0/zipCode" with "13181643" 
																			#And I compare response value "/addresses/0/country" with "Brasilã" 
																			And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																			And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																			And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																			And I compare response value "/addresses/0/numberAddress" with "528" 
																			And I compare response value "/addresses/0/latitude" with "${latitude}" 
																			And I compare response value "/addresses/0/longitude" with "${longitude}" 
																			And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																			And Response code must be 200 
																			
																			Examples: 
																				| id | descricao                                       | lastName    |
																				| 21 | com contact.phones.phoneType vazio              | ${random21} |
																				| 22 | com contact.phones.phoneType nulo               | ${random22} |
																				| 23 | sem contact.phones.phoneType                    | ${random23} |
																				
																				
																				@Positivo 
																				Scenario Outline: Consultar cliente <descricao> 
																					Given I set request header "client_id" as "${client_id}" 
																					And I set request header "access_token" as "${access_token}" 
																					And I set request header "Content-Type" as "application/json" 
																					When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																					Then I get response body 
																					And I get response status code 
																					And I compare response value "/customerId" with "${customerId<id>}" 
																					And I compare response value "/firstName" with "Aline" 
																					And I compare response value "/lastName" with "<lastName>" 
																					And I compare response value "/document" with "${document<id>}" 
																					And I compare response value "/documentType" with "CPF" 
																					And I compare response value "/businessType" with "IT" 
																					#And I compare response value "/company" with "Sensedia" 
																					And I compare response value "/source" with "QA" 
																					And I compare response value "/contact/phones/0/ddd" with "19" 
																					And I compare response value "/contact/phones/0/number" with "982722791" 
																					And I compare response value "/contact/phones/0/phoneType" with "1" 
																					And I verify if response value "/contact/email" is empty 
																					And I compare response value "/socialNetwork/instagram" with "1abender" 
																					And I compare response value "/socialNetwork/facebook" with "1abender" 
																					And I compare response value "/socialNetwork/twitter" with "1abender" 
																					And I compare response value "/addresses/0/mainAddress" with "true" 
																					And I compare response value "/addresses/0/city" with "Sumaré" 
																					And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																					And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																					And I compare response value "/addresses/0/zipCode" with "13181643" 
																					#And I compare response value "/addresses/0/country" with "Brasilã" 
																					And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																					And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																					And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																					And I compare response value "/addresses/0/numberAddress" with "528" 
																					And I compare response value "/addresses/0/latitude" with "${latitude}" 
																					And I compare response value "/addresses/0/longitude" with "${longitude}" 
																					And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																					And Response code must be 200 
																					
																					Examples: 
																						| id | descricao                                       | lastName    |
																						| 24 | com email vazio                                 | ${random24} |
																						| 25 | com email nulo                                  | ${random25} |
																						| 26 | sem email                                       | ${random26} |
																						
																						
																						@Positivo 
																						Scenario Outline: Consultar cliente <descricao> 
																							Given I set request header "client_id" as "${client_id}" 
																							And I set request header "access_token" as "${access_token}" 
																							And I set request header "Content-Type" as "application/json" 
																							When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																							Then I get response body 
																							And I get response status code 
																							And I compare response value "/customerId" with "${customerId<id>}" 
																							And I compare response value "/firstName" with "Aline" 
																							And I compare response value "/lastName" with "<lastName>" 
																							And I compare response value "/document" with "${document<id>}" 
																							And I compare response value "/documentType" with "CPF" 
																							And I compare response value "/businessType" with "IT" 
																							#And I compare response value "/company" with "Sensedia" 
																							And I compare response value "/source" with "QA" 
																							And I compare response value "/contact/phones/0/ddd" with "19" 
																							And I compare response value "/contact/phones/0/number" with "982722791" 
																							And I compare response value "/contact/phones/0/phoneType" with "1" 
																							And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																							And I verify if response value "/contact/socialNetwork" is empty 
																							And I compare response value "/addresses/0/mainAddress" with "true" 
																							And I compare response value "/addresses/0/city" with "Sumaré" 
																							And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																							And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																							And I compare response value "/addresses/0/zipCode" with "13181643" 
																							#And I compare response value "/addresses/0/country" with "Brasilã" 
																							And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																							And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																							And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																							And I compare response value "/addresses/0/numberAddress" with "528" 
																							And I compare response value "/addresses/0/latitude" with "${latitude}" 
																							And I compare response value "/addresses/0/longitude" with "${longitude}" 
																							And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																							And Response code must be 200 
																							
																							Examples: 
																								| id | descricao                                       | lastName    |
																								| 27 | com socialNetwork vazio                         | ${random27} |
																								| 28 | com socialNetwork nulo                          | ${random28} |
																								| 29 | sem socialNetwork                               | ${random29} |
																								
																								
																								@Positivo 
																								Scenario Outline: Consultar cliente <descricao> 
																									Given I set request header "client_id" as "${client_id}" 
																									And I set request header "access_token" as "${access_token}" 
																									And I set request header "Content-Type" as "application/json" 
																									When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																									Then I get response body 
																									And I get response status code 
																									And I compare response value "/customerId" with "${customerId<id>}" 
																									And I compare response value "/firstName" with "Aline" 
																									And I compare response value "/lastName" with "<lastName>" 
																									And I compare response value "/document" with "${document<id>}" 
																									And I compare response value "/documentType" with "CPF" 
																									And I compare response value "/businessType" with "IT" 
																									#And I compare response value "/company" with "Sensedia" 
																									And I compare response value "/source" with "QA" 
																									And I compare response value "/contact/phones/0/ddd" with "19" 
																									And I compare response value "/contact/phones/0/number" with "982722791" 
																									And I compare response value "/contact/phones/0/phoneType" with "1" 
																									And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																									And I compare response value "/socialNetwork/instagram" with "1abender" 
																									And I compare response value "/socialNetwork/facebook" with "1abender" 
																									And I compare response value "/socialNetwork/twitter" with "1abender" 
																									And I compare response value "/addresses/0/mainAddress" with "true" 
																									And I verify if response value "/addresses/0/city" is empty 
																									And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																									And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																									And I compare response value "/addresses/0/zipCode" with "13181643" 
																									#And I compare response value "/addresses/0/country" with "Brasilã" 
																									And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																									And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																									And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																									And I compare response value "/addresses/0/numberAddress" with "528" 
																									And I compare response value "/addresses/0/latitude" with "${latitude}" 
																									And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																									And Response code must be 200 
																									
																									Examples: 
																										| id | descricao                                       | lastName    |
																										| 32 | com addresses.city vazio                        | ${random32} |
																										
																										
																										@Positivo 
																										Scenario Outline: Consultar cliente <descricao> 
																											Given I set request header "client_id" as "${client_id}" 
																											And I set request header "access_token" as "${access_token}" 
																											And I set request header "Content-Type" as "application/json" 
																											When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																											Then I get response body 
																											And I get response status code 
																											And I compare response value "/customerId" with "${customerId<id>}" 
																											And I compare response value "/firstName" with "Aline" 
																											And I compare response value "/lastName" with "<lastName>" 
																											And I compare response value "/document" with "${document<id>}" 
																											And I compare response value "/documentType" with "CPF" 
																											And I compare response value "/businessType" with "IT" 
																											#And I compare response value "/company" with "Sensedia" 
																											And I compare response value "/source" with "QA" 
																											And I compare response value "/contact/phones/0/ddd" with "19" 
																											And I compare response value "/contact/phones/0/number" with "982722791" 
																											And I compare response value "/contact/phones/0/phoneType" with "1" 
																											And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																											And I compare response value "/socialNetwork/instagram" with "1abender" 
																											And I compare response value "/socialNetwork/facebook" with "1abender" 
																											And I compare response value "/socialNetwork/twitter" with "1abender" 
																											And I compare response value "/addresses/0/mainAddress" with "true" 
																											And I compare response value "/addresses/0/city" with "Sumaré" 
																											And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																											And I verify if response value "/addresses/0/neighborhood" is empty 
																											And I compare response value "/addresses/0/zipCode" with "13181643" 
																											#And I compare response value "/addresses/0/country" with "Brasilã" 
																											And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																											And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																											And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																											And I compare response value "/addresses/0/numberAddress" with "528" 
																											And I compare response value "/addresses/0/latitude" with "${latitude}" 
																											And I compare response value "/addresses/0/longitude" with "${longitude}" 
																											And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																											And Response code must be 200 
																											
																											Examples: 
																												| id | descricao                                       | lastName    |
																												| 38 | com addresses.neighborhood vazio                | ${random38} |
																												
																												
																												@Positivo 
																												Scenario Outline: Consultar cliente <descricao> 
																													Given I set request header "client_id" as "${client_id}" 
																													And I set request header "access_token" as "${access_token}" 
																													And I set request header "Content-Type" as "application/json" 
																													When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																													Then I get response body 
																													And I get response status code 
																													And I compare response value "/customerId" with "${customerId<id>}" 
																													And I compare response value "/firstName" with "Aline" 
																													And I compare response value "/lastName" with "<lastName>" 
																													And I compare response value "/document" with "${document<id>}" 
																													And I compare response value "/documentType" with "CPF" 
																													And I compare response value "/businessType" with "IT" 
																													#And I compare response value "/company" with "Sensedia" 
																													And I compare response value "/source" with "QA" 
																													And I compare response value "/contact/phones/0/ddd" with "19" 
																													And I compare response value "/contact/phones/0/number" with "982722791" 
																													And I compare response value "/contact/phones/0/phoneType" with "1" 
																													And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																													And I compare response value "/socialNetwork/instagram" with "1abender" 
																													And I compare response value "/socialNetwork/facebook" with "1abender" 
																													And I compare response value "/socialNetwork/twitter" with "1abender" 
																													And I compare response value "/addresses/0/mainAddress" with "true" 
																													And I compare response value "/addresses/0/city" with "Sumaré" 
																													And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																													And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																													And I verify if response value "/addresses/0/zipCode" is empty 
																													#And I compare response value "/addresses/0/country" with "Brasilã" 
																													And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																													And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																													And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																													And I compare response value "/addresses/0/numberAddress" with "528" 
																													And I compare response value "/addresses/0/latitude" with "${latitude}" 
																													And I compare response value "/addresses/0/longitude" with "${longitude}" 
																													And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																													And Response code must be 200 
																													
																													Examples: 
																														| id | descricao                                       | lastName    |
																														| 41 | com addresses.zipCode vazio                     | ${random41} |
																														
																														
																														@Positivo 
																														Scenario Outline: Consultar cliente <descricao> 
																															Given I set request header "client_id" as "${client_id}" 
																															And I set request header "access_token" as "${access_token}" 
																															And I set request header "Content-Type" as "application/json" 
																															When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																															Then I get response body 
																															And I get response status code 
																															And I compare response value "/customerId" with "${customerId<id>}" 
																															And I compare response value "/firstName" with "Aline" 
																															And I compare response value "/lastName" with "<lastName>" 
																															And I compare response value "/document" with "${document<id>}" 
																															And I compare response value "/documentType" with "CPF" 
																															And I compare response value "/businessType" with "IT" 
																															#And I compare response value "/company" with "Sensedia" 
																															And I compare response value "/source" with "QA" 
																															And I compare response value "/contact/phones/0/ddd" with "19" 
																															And I compare response value "/contact/phones/0/number" with "982722791" 
																															And I compare response value "/contact/phones/0/phoneType" with "1" 
																															And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																															And I compare response value "/socialNetwork/instagram" with "1abender" 
																															And I compare response value "/socialNetwork/facebook" with "1abender" 
																															And I compare response value "/socialNetwork/twitter" with "1abender" 
																															And I compare response value "/addresses/0/mainAddress" with "true" 
																															And I compare response value "/addresses/0/city" with "Sumaré" 
																															And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																															And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																															And I compare response value "/addresses/0/zipCode" with "13181643" 
																															And I verify if response value "/addresses/0/country" is empty 
																															And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																															And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																															And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																															And I compare response value "/addresses/0/numberAddress" with "528" 
																															And I compare response value "/addresses/0/latitude" with "${latitude}" 
																															And I compare response value "/addresses/0/longitude" with "${longitude}" 
																															And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																															And Response code must be 200 
																															
																															Examples: 
																																| id | descricao                                       | lastName    |
																																| 44 | com addresses.country vazio                     | ${random44} |
																																
																																
																																@Positivo 
																																Scenario Outline: Consultar cliente <descricao> 
																																	Given I set request header "client_id" as "${client_id}" 
																																	And I set request header "access_token" as "${access_token}" 
																																	And I set request header "Content-Type" as "application/json" 
																																	When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																	Then I get response body 
																																	And I get response status code 
																																	And I compare response value "/customerId" with "${customerId<id>}" 
																																	And I compare response value "/firstName" with "Aline" 
																																	And I compare response value "/lastName" with "<lastName>" 
																																	And I compare response value "/document" with "${document<id>}" 
																																	And I compare response value "/documentType" with "CPF" 
																																	And I compare response value "/businessType" with "IT" 
																																	#And I compare response value "/company" with "Sensedia" 
																																	And I compare response value "/source" with "QA" 
																																	And I compare response value "/contact/phones/0/ddd" with "19" 
																																	And I compare response value "/contact/phones/0/number" with "982722791" 
																																	And I compare response value "/contact/phones/0/phoneType" with "1" 
																																	And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																	And I compare response value "/socialNetwork/instagram" with "1abender" 
																																	And I compare response value "/socialNetwork/facebook" with "1abender" 
																																	And I compare response value "/socialNetwork/twitter" with "1abender" 
																																	And I compare response value "/addresses/0/mainAddress" with "true" 
																																	And I compare response value "/addresses/0/city" with "Sumaré" 
																																	And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																	And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																	And I compare response value "/addresses/0/zipCode" with "13181643" 
																																	#And I compare response value "/addresses/0/country" with "Brasilã" 
																																	And I verify if response value "/addresses/0/referenceAddress" is empty 
																																	And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																	And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																	And I compare response value "/addresses/0/numberAddress" with "528" 
																																	And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																	And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																	And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																	And Response code must be 200 
																																	
																																	Examples: 
																																		| id | descricao                                       | lastName    |
																																		| 47 | com addresses.referenceAddress vazio            | ${random47} |
																																		| 48 | com addresses.referenceAddress nulo             | ${random48} |
																																		| 49 | sem addresses.referenceAddress                  | ${random49} |
																																		
																																		
																																		@Positivo 
																																		Scenario Outline: Consultar cliente <descricao> 
																																			Given I set request header "client_id" as "${client_id}" 
																																			And I set request header "access_token" as "${access_token}" 
																																			And I set request header "Content-Type" as "application/json" 
																																			When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																			Then I get response body 
																																			And I get response status code 
																																			And I compare response value "/customerId" with "${customerId<id>}" 
																																			And I compare response value "/firstName" with "Aline" 
																																			And I compare response value "/lastName" with "<lastName>" 
																																			And I compare response value "/document" with "${document<id>}" 
																																			And I compare response value "/documentType" with "CPF" 
																																			And I compare response value "/businessType" with "IT" 
																																			#And I compare response value "/company" with "Sensedia" 
																																			And I compare response value "/source" with "QA" 
																																			And I compare response value "/contact/phones/0/ddd" with "19" 
																																			And I compare response value "/contact/phones/0/number" with "982722791" 
																																			And I compare response value "/contact/phones/0/phoneType" with "1" 
																																			And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																			And I compare response value "/socialNetwork/instagram" with "1abender" 
																																			And I compare response value "/socialNetwork/facebook" with "1abender" 
																																			And I compare response value "/socialNetwork/twitter" with "1abender" 
																																			And I compare response value "/addresses/0/mainAddress" with "true" 
																																			And I compare response value "/addresses/0/city" with "Sumaré" 
																																			And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																			And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																			And I compare response value "/addresses/0/zipCode" with "13181643" 
																																			#And I compare response value "/addresses/0/country" with "Brasilã" 
																																			And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																			And I verify if response value "/addresses/0/address" is empty 
																																			And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																			And I compare response value "/addresses/0/numberAddress" with "528" 
																																			And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																			And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																			And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																			And Response code must be 200 
																																			
																																			Examples: 
																																				| id | descricao                                       | lastName    |
																																				| 50 | com addresses.address vazio                     | ${random50} |
																																				
																																				
																																				@Positivo 
																																				Scenario Outline: Consultar cliente <descricao> 
																																					Given I set request header "client_id" as "${client_id}" 
																																					And I set request header "access_token" as "${access_token}" 
																																					And I set request header "Content-Type" as "application/json" 
																																					When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																					Then I get response body 
																																					And I get response status code 
																																					And I compare response value "/customerId" with "${customerId<id>}" 
																																					And I compare response value "/firstName" with "Aline" 
																																					And I compare response value "/lastName" with "<lastName>" 
																																					And I compare response value "/document" with "${document<id>}" 
																																					And I compare response value "/documentType" with "CPF" 
																																					And I compare response value "/businessType" with "IT" 
																																					#And I compare response value "/company" with "Sensedia" 
																																					And I compare response value "/source" with "QA" 
																																					And I compare response value "/contact/phones/0/ddd" with "19" 
																																					And I compare response value "/contact/phones/0/number" with "982722791" 
																																					And I compare response value "/contact/phones/0/phoneType" with "1" 
																																					And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																					And I compare response value "/socialNetwork/instagram" with "1abender" 
																																					And I compare response value "/socialNetwork/facebook" with "1abender" 
																																					And I compare response value "/socialNetwork/twitter" with "1abender" 
																																					And I compare response value "/addresses/0/mainAddress" with "true" 
																																					And I compare response value "/addresses/0/city" with "Sumaré" 
																																					And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																					And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																					And I compare response value "/addresses/0/zipCode" with "13181643" 
																																					#And I compare response value "/addresses/0/country" with "Brasilã" 
																																					And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																					And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																					And I verify if response value "/addresses/0/additionAddress" is empty 
																																					And I compare response value "/addresses/0/numberAddress" with "528" 
																																					And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																					And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																					And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																					And Response code must be 200 
																																					
																																					Examples: 
																																						| id | descricao                                       | lastName    |
																																						| 53 | com addresses.additionAddress vazio             | ${random53} |
																																						
																																						
																																						@Positivo 
																																						Scenario Outline: Consultar cliente <descricao> 
																																							Given I set request header "client_id" as "${client_id}" 
																																							And I set request header "access_token" as "${access_token}" 
																																							And I set request header "Content-Type" as "application/json" 
																																							When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																							Then I get response body 
																																							And I get response status code 
																																							And I compare response value "/customerId" with "${customerId<id>}" 
																																							And I compare response value "/firstName" with "Aline" 
																																							And I compare response value "/lastName" with "<lastName>" 
																																							And I compare response value "/document" with "${document<id>}" 
																																							And I compare response value "/documentType" with "CPF" 
																																							And I compare response value "/businessType" with "IT" 
																																							#And I compare response value "/company" with "Sensedia" 
																																							And I compare response value "/source" with "QA" 
																																							And I compare response value "/contact/phones/0/ddd" with "19" 
																																							And I compare response value "/contact/phones/0/number" with "982722791" 
																																							And I compare response value "/contact/phones/0/phoneType" with "1" 
																																							And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																							And I compare response value "/socialNetwork/instagram" with "1abender" 
																																							And I compare response value "/socialNetwork/facebook" with "1abender" 
																																							And I compare response value "/socialNetwork/twitter" with "1abender" 
																																							And I compare response value "/addresses/0/mainAddress" with "true" 
																																							And I compare response value "/addresses/0/city" with "Sumaré" 
																																							And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																							And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																							And I compare response value "/addresses/0/zipCode" with "13181643" 
																																							#And I compare response value "/addresses/0/country" with "Brasilã" 
																																							And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																							And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																							And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																							And I verify if response value "/addresses/0/numberAddress" is empty 
																																							And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																							And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																							And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																							And Response code must be 200 
																																							
																																							Examples: 
																																								| id | descricao                                       | lastName    |
																																								| 56 | com addresses.numberAddress vazio               | ${random56} |
																																								
																																								
																																								@Positivo 
																																								Scenario Outline: Consultar cliente <descricao> 
																																									Given I set request header "client_id" as "${client_id}" 
																																									And I set request header "access_token" as "${access_token}" 
																																									And I set request header "Content-Type" as "application/json" 
																																									When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																									Then I get response body 
																																									And I get response status code 
																																									And I compare response value "/customerId" with "${customerId<id>}" 
																																									And I compare response value "/firstName" with "Aline" 
																																									And I compare response value "/lastName" with "<lastName>" 
																																									And I compare response value "/document" with "${document<id>}" 
																																									And I compare response value "/documentType" with "CPF" 
																																									And I compare response value "/businessType" with "IT" 
																																									#And I compare response value "/company" with "Sensedia" 
																																									And I compare response value "/source" with "QA" 
																																									And I compare response value "/contact/phones/0/ddd" with "19" 
																																									And I compare response value "/contact/phones/0/number" with "982722791" 
																																									And I compare response value "/contact/phones/0/phoneType" with "1" 
																																									And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																									And I compare response value "/socialNetwork/instagram" with "1abender" 
																																									And I compare response value "/socialNetwork/facebook" with "1abender" 
																																									And I compare response value "/socialNetwork/twitter" with "1abender" 
																																									And I compare response value "/addresses/0/mainAddress" with "true" 
																																									And I compare response value "/addresses/0/city" with "Sumaré" 
																																									And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																									And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																									And I compare response value "/addresses/0/zipCode" with "13181643" 
																																									#And I compare response value "/addresses/0/country" with "Brasilã" 
																																									And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																									And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																									And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																									And I compare response value "/addresses/0/numberAddress" with "528" 
																																									And I verify if response value "/addresses/0/latitude" is empty 
																																									And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																									And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																									And Response code must be 200 
																																									
																																									Examples: 
																																										| id | descricao                                       | lastName    |
																																										| 60 | com addresses.latitude nulo                     | ${random60} |
																																										| 61 | sem addresses.latitude                          | ${random61} |
																																										
																																										
																																										@Positivo 
																																										Scenario Outline: Consultar cliente <descricao> 
																																											Given I set request header "client_id" as "${client_id}" 
																																											And I set request header "access_token" as "${access_token}" 
																																											And I set request header "Content-Type" as "application/json" 
																																											When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																											Then I get response body 
																																											And I get response status code 
																																											And I compare response value "/customerId" with "${customerId<id>}" 
																																											And I compare response value "/firstName" with "Aline" 
																																											And I compare response value "/lastName" with "<lastName>" 
																																											And I compare response value "/document" with "${document<id>}" 
																																											And I compare response value "/documentType" with "CPF" 
																																											And I compare response value "/businessType" with "IT" 
																																											#And I compare response value "/company" with "Sensedia" 
																																											And I compare response value "/source" with "QA" 
																																											And I compare response value "/contact/phones/0/ddd" with "19" 
																																											And I compare response value "/contact/phones/0/number" with "982722791" 
																																											And I compare response value "/contact/phones/0/phoneType" with "1" 
																																											And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																											And I compare response value "/socialNetwork/instagram" with "1abender" 
																																											And I compare response value "/socialNetwork/facebook" with "1abender" 
																																											And I compare response value "/socialNetwork/twitter" with "1abender" 
																																											And I compare response value "/addresses/0/mainAddress" with "true" 
																																											And I compare response value "/addresses/0/city" with "Sumaré" 
																																											And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																											And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																											And I compare response value "/addresses/0/zipCode" with "13181643" 
																																											#And I compare response value "/addresses/0/country" with "Brasilã" 
																																											And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																											And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																											And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																											And I compare response value "/addresses/0/numberAddress" with "528" 
																																											And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																											And I verify if response value "/addresses/0/longitude" is empty 
																																											And I compare response value "/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
																																											And Response code must be 200 
																																											
																																											Examples: 
																																												| id | descricao                                       | lastName    |
																																												| 63 | com addresses.longitude nulo                    | ${random63} |
																																												| 64 | sem addresses.longitude                         | ${random64} |
																																												
																																												
																																												@Positivo 
																																												Scenario Outline: Consultar cliente <descricao> 
																																													Given I set request header "client_id" as "${client_id}" 
																																													And I set request header "access_token" as "${access_token}" 
																																													And I set request header "Content-Type" as "application/json" 
																																													When I set GET api endpoint as "${endpointCustomers}/${customerId<id>}" 
																																													Then I get response body 
																																													And I get response status code 
																																													And I compare response value "/customerId" with "${customerId<id>}" 
																																													And I compare response value "/firstName" with "Aline" 
																																													And I compare response value "/lastName" with "<lastName>" 
																																													And I compare response value "/document" with "${document<id>}" 
																																													And I compare response value "/documentType" with "CPF" 
																																													And I compare response value "/businessType" with "IT" 
																																													#And I compare response value "/company" with "Sensedia" 
																																													And I compare response value "/source" with "QA" 
																																													And I compare response value "/contact/phones/0/ddd" with "19" 
																																													And I compare response value "/contact/phones/0/number" with "982722791" 
																																													And I compare response value "/contact/phones/0/phoneType" with "1" 
																																													And I compare response value "/contact/email" with "aline.dias@sensedia.com" 
																																													And I compare response value "/socialNetwork/instagram" with "1abender" 
																																													And I compare response value "/socialNetwork/facebook" with "1abender" 
																																													And I compare response value "/socialNetwork/twitter" with "1abender" 
																																													And I compare response value "/addresses/0/mainAddress" with "true" 
																																													And I compare response value "/addresses/0/city" with "Sumaré" 
																																													And I compare response value "/addresses/0/stateAbbreviation" with "SP" 
																																													And I compare response value "/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
																																													And I compare response value "/addresses/0/zipCode" with "13181643" 
																																													#And I compare response value "/addresses/0/country" with "Brasilã" 
																																													And I compare response value "/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
																																													And I compare response value "/addresses/0/address" with "Rua Domingõs Conradô" 
																																													And I compare response value "/addresses/0/additionAddress" with "Cãsa" 
																																													And I compare response value "/addresses/0/numberAddress" with "528" 
																																													And I compare response value "/addresses/0/latitude" with "${latitude}" 
																																													And I compare response value "/addresses/0/longitude" with "${longitude}" 
																																													And I verify if response value "/addresses/0/descriptionAddress" is empty 
																																													And Response code must be 200 
																																													
																																													Examples: 
																																														| id | descricao                                       | lastName    |
																																														| 65 | com addresses.descriptionAddress vazio          | ${random65} |
																																																														