Feature: GET_customers.feature 
	Consulta clientes cadastrados no segmento domiciliar da Ultragaz.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set GET api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	And System generate random CPF 
	And System generate random number 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set GET api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição sem access_token 
	Given I set request header "client_id" as "${client_id}" 
	When I set GET api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario Outline: Enviar requisição <descricao> 
	Given I set request header "client_id" as "<client_id>" 
	And I set request header "access_token" as "<access_token>" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"19","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set GET api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "<message>" 
	And Response code must be 401 
	
	Examples: 
		| descricao                 | client_id     | access_token     | message                                                                          |
		| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
		| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
		| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
		| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
		
		
		@PreRequest 
		Scenario Outline: Cadastrar cliente <descricao> 
			Given System generate random number 
			And System generate random CPF 
			And I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			And I set request body as "<body>" 
			When I set POST api endpoint as "${endpointCustomers}" 
			Then I get response body 
			And I get response status code 
			And I save response header "Location" as "location" 
			And I save final value of header "Location" as "customerId<id>" 
			And I save "${random}" as "random<id>" 
			And I save "${randomCPF}" as "document<id>" 
			And I verify if response body is empty 
			And Response code must be 201 
			And I wait 2 seconds 
			
			Examples: 
				| id | descricao                                       | body                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
				| 1  | com sucesso                                     | {"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]} |
								
					
				@Positivo 
				Scenario Outline: Consultar cliente <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointCustomers}?<query>" 
					Then I get response body 
					And I get response status code 
					And I verify if response value "/0/customerId" is not empty 
					And I compare response value "/0/firstName" with "Aline" 
					And I verify if response value "/0/lastName" is not empty
					And I verify if response value "/0/document" is not empty 
					And I compare response value "/0/documentType" with "CPF" 
					And I compare response value "/0/businessType" with "IT" 
					#And I compare response value "/0/company" with "Sensedia" 
					And I compare response value "/0/source" with "QA" 
					And I compare response value "/0/contact/phones/0/ddd" with "19" 
					And I compare response value "/0/contact/phones/0/number" with "982722791" 
					And I compare response value "/0/contact/phones/0/phoneType" with "1" 
					And I compare response value "/0/socialNetwork/instagram" with "1abender" 
					And I compare response value "/0/socialNetwork/facebook" with "1abender" 
					And I compare response value "/0/socialNetwork/twitter" with "1abender" 
					And I compare response value "/0/addresses/0/mainAddress" with "true" 
					And I compare response value "/0/addresses/0/city" with "Sumaré" 
					And I compare response value "/0/addresses/0/stateAbbreviation" with "SP" 
					And I compare response value "/0/addresses/0/neighborhood" with "Jardim Bom Retirõ" 
					And I compare response value "/0/addresses/0/zipCode" with "13181643" 
					#And I compare response value "/0/addresses/0/country" with "Brasilã" 
					And I compare response value "/0/addresses/0/referenceAddress" with "Praça dõ Bom Retiro" 
					And I compare response value "/0/addresses/0/address" with "Rua Domingõs Conradô" 
					And I compare response value "/0/addresses/0/additionAddress" with "Cãsa" 
					And I compare response value "/0/addresses/0/numberAddress" with "528" 
					And I compare response value "/0/addresses/0/latitude" with "${latitude}" 
					And I compare response value "/0/addresses/0/longitude" with "${longitude}" 
					And I compare response value "/0/addresses/0/descriptionAddress" with "Minha casa maravilhosa" 
					And Response code must be 200 
					
					Examples: 
						| id | descricao              | query                                     |
						| 1  | com document           | document=${document1}                     |
						| 2  | com phone              | phone=19982722791                         |
						| 3  | com phone e document   | phone=19982722791&document=${document1}   |
						
						
			@Positivo
				Scenario Outline: Consultar cliente <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointCustomers}?<query>" 
					Then I get response body 
					And I get response status code 
					And Response body must be "[]" 
					And Response code must be 200 
					
					Examples: 
						| id | descricao               | query                                     |
						| 1  | com document incorreto  | document=1                                |
						| 2  | com phone    incorreto  | phone=1                                   |			
																																																														