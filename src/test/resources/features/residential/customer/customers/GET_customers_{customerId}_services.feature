Feature: GET_customers_{customerId}_services.feature 
	Consulta atendimentos de cliente cadastrado no segmento domiciliar da Ultragaz.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set GET api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	And I save "001q0000015D9Se" as "customerId" 
	And System generate random CPF 
	And System generate random number 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set GET api endpoint as "${endpointCustomers}/${customerId}/services" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição sem access_token 
	Given I set request header "client_id" as "${client_id}" 
	When I set GET api endpoint as "${endpointCustomers}/${customerId}/services" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario Outline: Enviar requisição <descricao> 
	Given I set request header "client_id" as "<client_id>" 
	And I set request header "access_token" as "<access_token>" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"19","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set GET api endpoint as "${endpointCustomers}/${customerId}/services" 
	Then I get response body 
	And I get response status code 
	And Response body must be "<message>" 
	And Response code must be 401 
	
	Examples: 
		| descricao                 | client_id     | access_token     | message                                                                          |
		| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
		| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
		| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
		| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
		
		
		@Negativo 
		Scenario: Consultar serviço com customerId inexistente 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			When I set GET api endpoint as "${endpointCustomers}/batatinhax/services" 
			Then I get response body 
			And I get response status code 
			And I verify if response body is empty 
			And Response code must be 404
			
			
		@Positivo 
		Scenario: Consultar serviço 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			When I set GET api endpoint as "${endpointCustomers}/${customerId}/services" 
			Then I get response body 
			And I get response status code 
			And I verify if response value "/0/statusService" is not empty
			And I verify if response value "/0/responsible" is not empty
			And I verify if response value "/0/orderId" is not empty
			And I verify if response value "/0/source" is not empty
			And I verify if response value "/0/notes" is not empty
			And I verify if response value "/0/cancellationReason" is not empty
			And I verify if response value "/0/reasonService" is not empty
			And I verify if response value "/0/serviceId" is not empty
			And I verify if response value "/0/phase" is not empty
			And I verify if response value "/0/requestDate" is not empty
			And I verify if response value "/0/expectedDate" is not empty
			And I verify if response value "/0/relatedServiceCode" is not empty
			And I verify if response value "/0/codeService" is not empty
			And I verify if response value "/0/customerId" is not empty
			And Response code must be 200
			
			
		@Positivo
		Scenario: Consultar cliente que não possui serviço
			Given I set request header "client_id" as "${client_id}"
			And I set request header "access_token" as "${access_token}"
			And I set request header "Content-Type" as "application/json"
			When I set GET api endpoint as "${endpointCustomers}/001q0000019mYcQAAU/services" 
			Then I get response body
			And I get response status code 
			And I verify if response body is empty 
			And Response code must be 404