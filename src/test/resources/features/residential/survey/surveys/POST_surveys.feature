Feature: POST_surveys.feature 
	Operação responsável por cadastrar uma pesquisa.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/survey/v1" 
	And I save "/surveys" as "endpointSurveys" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "10" as "rating" 
	And I save "Anotação bonitinha pra chuchu!" as "note" 
	And I save "pedido" as "serviceType" 
	And I save "801q000000060IYAAY" as "identifier" 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set POST api endpoint as "${endpointSurveys}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição sem access_token 
	Given I set request header "client_id" as "${client_id}" 
	When I set POST api endpoint as "${endpointSurveys}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
	And Response code must be 401 
	
	
@Negativo 
Scenario Outline: Enviar requisição <descricao> 
	Given I set request header "client_id" as "<client_id>" 
	And I set request header "access_token" as "<access_token>" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"numberOrder":"string","source":"string","notes":"string","phase":"string","reasonService":"string","customerId":"string","relatedServiceCode":"string","serviceType":"string","expectedDate":"string"}" 
	When I set POST api endpoint as "${endpointSurveys}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "<message>" 
	And Response code must be 401 
	
	Examples: 
		| descricao                 | client_id     | access_token     | message                                                                                  |
		| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
		| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
		| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
		| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
		
		
		@Positivo 
		Scenario Outline: Cadastrar atendimento <descricao> 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			And I set request body as "<body>" 
			When I set POST api endpoint as "${endpointSurveys}" 
			Then I get response body 
			And I get response status code 
			And I verify if response body is empty 
			And Response code must be 201 
			And I wait 2 seconds 
			
			Examples: 
				| id | descricao                                  | body                                                                                                                                                                                                                                         |
				| 2  | com rating 101                             | {"rating":"101","note":"${note}","serviceType":"${serviceType}","identifier":"${identifier}"}  |
				| 3  | com rating 0                               | {"rating":"0","note":"${note}","serviceType":"${serviceType}","identifier":"${identifier}"}  |
				| 4  | com note vazio                             | {"rating":${rating},"note":"","serviceType":"${serviceType}","identifier":"${identifier}"}  |
				| 5  | com note nulo                              | {"rating":${rating},"note":null,"serviceType":"${serviceType}","identifier":"${identifier}"}  |
				| 6  | com note longo                             | {"rating":${rating},"note":"Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.Note longo.","serviceType":"${serviceType}","identifier":"${identifier}"}  |
				| 7  | sem note                                   | {"rating":${rating},"serviceType":"${serviceType}","identifier":"${identifier}"}  |
				| 1  | com sucesso                                | {"rating":${rating},"note":"${note}","serviceType":"${serviceType}","identifier":"${identifier}"}  |
						
				
				@Negativo 
				Scenario: Cadastrar pedido sem body 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set POST api endpoint as "${endpointSurveys}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/code" with "400.000" 
					And I compare response value "/message" with "Body Inválido" 
					And Response code must be 400 
					
					
				@Negativo 
				Scenario: Cadastrar pedido sem Content-Type 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "{"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}" 
					When I set POST api endpoint as "${endpointSurveys}" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 415 
					
					
				@Negativo 
				Scenario Outline: Cadastrar atendimento <descricao> 
					Given I set request header "Content-Type" as "application/json" 
					And I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "<body>" 
					When I set POST api endpoint as "${endpointSurveys}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/code" with "<code>" 
					And I compare response value "/message" with "<message>" 
					And Response code must be <responseCode> 
					
					Examples: 
						| descricao                                  | body                                                                                               | message                                                    | code    | responseCode |
						| com rating vazio                           | {"rating":"","note":"${note}","serviceType":"${serviceType}","identifier":"${identifier}"}         | Valor inválido informado.                                  | 400.001 | 400          |
						| com rating nulo                            | {"rating":null,"note":"${note}","serviceType":"${serviceType}","identifier":"${identifier}"}       | Valor inválido informado.                                  | 400.001 | 400          |
						| com rating inválido                        | {"rating":"S","note":"${note}","serviceType":"${serviceType}","identifier":"${identifier}"}        | Valor inválido informado.                                  | 400.001 | 400          |
						| sem rating                                 | {"note":"${note}","serviceType":"${serviceType}","identifier":"${identifier}"}                     | Valor inválido informado.                                  | 400.001 | 400          |
						| com serviceType vazio                      | {"rating":${rating},"note":"${note}","serviceType":"","identifier":"${identifier}"}                | Valor inválido informado no campo serviceType.             | 400.002 | 400          |
						| com serviceType nulo                       | {"rating":${rating},"note":"${note}","serviceType":null,"identifier":"${identifier}"}              | Campo obrigatório não informado.                           | 400.002 | 400          |
						| com serviceType inválido                   | {"rating":${rating},"note":"${note}","serviceType":"Inválido","identifier":"${identifier}"}        | Valor inválido informado no campo serviceType.             | 400.002 | 400          |
						| sem serviceType                            | {"rating":${rating},"note":"${note}","identifier":"${identifier}"}                                 | Campo obrigatório não informado.                           | 400.002 | 400          |
						| com identifier vazio                       | {"rating":${rating},"note":"${note}","serviceType":"${serviceType}","identifier":""}               | Campo obrigatório não informado.                           | 400.002 | 400          |
						| com identifier nulo                        | {"rating":${rating},"note":"${note}","serviceType":"${serviceType}","identifier":null}             | Campo obrigatório não informado.                           | 400.002 | 400          |
						| com identifier inválido                    | {"rating":${rating},"note":"${note}","serviceType":"${serviceType}","identifier":"Inválido"}       | Não foi encontado um pedido com o identifier especificado. | 422.001 | 422          |
						| sem identifier                             | {"rating":${rating},"note":"${note}","serviceType":"${serviceType}"}                               | Campo obrigatório não informado.                           | 400.002 | 400          |