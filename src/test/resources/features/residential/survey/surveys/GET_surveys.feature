Feature: GET_surveys.feature 
	Obter pesquisas do segmento domiciliar da Ultragaz.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/survey/v1" 
	And I save "/surveys" as "endpointSurveys" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "801q000000060IYAAY" as "orderId" 
	And I save "500q000000JkwlDAAR" as "serviceId" 
	
	
@Positivo 
Scenario Outline: Consultar surveys <descricao> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	When I set GET api endpoint as "${endpointSurveys}<queryParameter>" 
	Then I get response body 
	And I get response status code 
	And I compare response value "/0/rating" with <rating> 
	And I compare response value "/0/note" with <note> 
	And I compare response value "/0/serviceType" with <serviceType> 
	And Response code must be 200 
	And I wait 2 seconds 
	
	Examples: 
		| descricao                        | queryParameter                                                                 | rating  | note                                                                     | serviceType |
		| por order-id                     | ?order-id=801q000000060IYAAY                                                   | "10"    | "Anotação bonitinha pra chuchu!" | "ORDER"     |
		
		
		@Positivo 
		Scenario: Consultar surveys por service-id 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			When I set GET api endpoint as "${endpointSurveys}?service-id=500q000000JkwlDAAR" 
			Then I get response body 
			And I get response status code 
			And I compare response value "/0/rating" with "0" 
			And I compare response value "/0/serviceType" with "SERVICE" 
			And Response code must be 200 
			And I wait 2 seconds 
			
			
		@Negativo 
		Scenario Outline: Consultar surveys <descricao> 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			When I set GET api endpoint as "${endpointSurveys}<queryParameter>" 
			Then I get response body 
			And I get response status code 
			And I compare response value "/code" with <code> 
			And I compare response value "/message" with <message> 
			And Response code must be <responseCode> 
			
			Examples: 
				| descricao                    | queryParameter                                                           | code      | message                                                 | responseCode |
				| por order-id vazio           | ?order-id=                                                               | "422.000" | "Formato invalido de parâmetro informado."      | 422 |
				| por service-id vazio         | ?service-id=                                                             | "422.000" | "Formato invalido de parâmetro informado."      | 422 |
				| por service-id e order-id    | ?service-id=500q000000JkwlDAAR&order-id=801q000000060IYAAY               | "400.000" | "Apenas um parâmetro (service-id ou order-id) é permitido por vez." | 400 |				
				
				
				@Negativo 
				Scenario Outline: Consultar surveys <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set GET api endpoint as "${endpointSurveys}<queryParameter>" 
					Then I get response body 
					And I get response status code 
					And Response body must be "[]" 
					And Response code must be 200 
					
					Examples: 
						| descricao                    | queryParameter                                                           |
						| por order-id incorreto       | ?order-id=S                                                              |
						| por service-id incorreto     | ?service-id=S                                                            |
						
						
						@Negativo 
						Scenario: Enviar requisição sem client_id 
							When I set GET api endpoint as "${endpointSurveys}?service-id=S" 
							Then I get response body 
							And I get response status code 
							And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
							And Response code must be 401 
							
							
						@Negativo 
						Scenario: Enviar requisição com client_id inválido 
							Given I set request header "client_id" as "${client_id}1" 
							When I set GET api endpoint as "${endpointSurveys}?service-id=S" 
							Then I get response body 
							And I get response status code 
							And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
							And Response code must be 401																			