@GET_privacy-policies
Feature: GET_privacy-policies.feature
  Operação responsável por retornar a última politica de privacidade vigente.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"client_credentials"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/privacy-policies" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"

  @Negativo
  Scenario: Enviar requisição sem client_id
  	Given I set request header "access_token" as "${access_token}"	
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401
	
	@Negativo
  Scenario: Enviar requisição sem access_token
  	Given I set request header "client_id" as "${client_id}"	
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
    
  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Positivo
  Scenario: Retornar a última politica de privacidade vigente
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I verify if response value "/file" is not empty
    And I verify if response value "/version" is not empty
    And Response code must be 200

