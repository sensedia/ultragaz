@PATCH_users_{userId}_employees_{employeeId}
Feature: PATCH_users_{userId}_employees_{employeeId}.feature
  Operação responsável por atualizar os dados cadastrais e permissões de um funcionário (usuário).

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/store/v1"
    And I save "/users" as "endpointUsers"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "cf51fa1e-aa25-4d53-91e8-0caa317ada4e" as "userId"
    And I save "50002291-42fc-4839-9543-288320e673a2" as "userIdSameGroup"
    And I save "af311a8f-95bd-4701-b18a-45fba0be9a45" as "userIdDiffGroup"
    And I save "498033d0-f591-49cc-99b4-470ffeb98916" as "employeeId"
    And I save "0024531d-4379-46d1-af45-eb1ffc0406b3" as "employeeId2"
    And I save "2051e9b5-a3ec-497d-a5ff-b5b398b57cb6" as "employeeIdSameGroup"
    And I save "4d215c42-6051-4b88-8007-af0f31cf896c" as "employeeIdDiffGroup"
    And I save "89f960c4-f11f-4770-80d0-8c4a62655696" as "notOwnerEmployeeId"
    And I save "Wellington Employee" as "name"
    And I save "19982911139" as "phone"
    And I save "0010M00001MqKwSQAV" as "storeId"
    And I save "0013000001FnYmbAAF" as "storeIdDiffGroup"

  @Negativo
  Scenario Outline: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"name":"${name}","phone":${phone},"active":true,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}"}]}"
    When I set PATCH api endpoint as "${endpointUsers}/${userId}/employees/${employeeId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415
 
 @Negativo
  Scenario: Enviar requisição sem client_id
  	Given I set request header "access_token" as "${access_token}"	
    When I set PATCH api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401
	
	@Negativo
  Scenario: Enviar requisição sem access_token
  	Given I set request header "client_id" as "${client_id}"	
    When I set PATCH api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
    
  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set PATCH api endpoint as "${endpointUsers}/${userId}/employees/${employeeId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message               | language |
      | sem body       | Invalid request body. | en-US    |
      | sem body em pt | Requisição inválida.  | pt-BR    |

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"name":"${name}","phone":${phone},"active":true,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}"}]}"
    When I set PATCH api endpoint as "${endpointUsers}/${userId}/employees/${employeeId}"
    Then I get response body
    And I get response status code
    And Response body must be "<message>"
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                  |
      | com client_id vazio       | ""              | "${access_token}"  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com access_token vazio    | "${client_id}"  | ""                 | Could not find a required Access Token in the request, identified by HEADER access_token |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | Could not find a required Access Token in the request, identified by HEADER access_token |

  @Negativo
  Scenario Outline: Atualizar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointUsers}/${userId}/employees/${employeeId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                      | body                               | message                                               | code    | language |
      | com phone com mais dígitos     | {"phone":${phone}9}                | Field phone has an invalid format.                    | 400.003 | en-US    |
      | com phone com menos dígitos    | {"phone":1999999999}               | Field phone has an invalid format.                    | 400.003 | en-US    |
      | com phone com caracteres       | {"phone":"199B2911139"}            | Field phone has an invalid format.                    | 400.003 | en-US    |
      | com name roles inválido        | {"roles":[{"name":"IRON_MAIDEN"}]} | Field roles[0].name has an invalid value or is empty. | 400.007 | en-US    |
      | com phone com mais dígitos pt  | {"phone":${phone}9}                | Campo phone tem um formato inválido.                  | 400.003 | pt-BR    |
      | com phone com menos dígitos pt | {"phone":1999999999}               | Campo phone tem um formato inválido.                  | 400.003 | pt-BR    |
      | com phone com caracteres pt    | {"phone":"199B2911139"}            | Campo phone tem um formato inválido.                  | 400.003 | pt-BR    |
      | com name roles inválido pt     | {"roles":[{"name":"IRON_MAIDEN"}]} | Campo roles[0].name tem um valor inválido ou vazio.   | 400.007 | pt-BR    |

  @Negativo
  Scenario Outline: Atualizar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointUsers}/${userId}/employees/${employeeId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                                         | body                                           | message                                                   | code    | language |
      | com storeId inválido                              | {"stores":[{"storeId":"batatadoce"}]}          | Store [batatadoce] not found for the given user.          | 422.020 | en-US    |
      | com storeId inválido pt                           | {"stores":[{"storeId":"batatadoce"}]}          | Loja [batatadoce] não encontrada para o usuário.          | 422.020 | pt-BR    |
      | com storeId não pertencente ao grupo econômico    | {"stores":[{"storeId":"${storeIdDiffGroup}"}]} | Store [${storeIdDiffGroup}] not found for the given user. | 422.020 | en-US    |
      | com storeId não pertencente ao grupo econômico pt | {"stores":[{"storeId":"${storeIdDiffGroup}"}]} | Loja [${storeIdDiffGroup}] não encontrada para o usuário. | 422.020 | pt-BR    |

  @Negativo
  Scenario Outline: Atualizar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointUsers}/<userId>/employees/<employeeId>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                                        | userId        | employeeId            | body                               | message                                                                      | code    | language |
      | não pertencente ao grupo econômico               | ${userId}     | ${notOwnerEmployeeId} | {"name":"Fora do grupo econômico"} | Signatory does not have permission to update user ${notOwnerEmployeeId}.     | 422.022 | en-US    |
      | não pertencente ao grupo econômico pt            | ${userId}     | ${notOwnerEmployeeId} | {"name":"Fora do grupo econômico"} | Signatário não tem permissão para atualizar o usuário ${notOwnerEmployeeId}. | 422.022 | pt-BR    |
      | com employeeId trocado com userId                | ${employeeId} | ${userId}             | {"name":"IDs trocados"}            | User is not a signatory.                                                     | 422.021 | en-US    |
      | com employeeId trocado com userId pt             | ${employeeId} | ${userId}             | {"name":"IDs trocados"}            | Usuário não é um signatário.                                                 | 422.021 | pt-BR    |
      | com employee alterando employee                  | ${employeeId} | ${employeeId2}        | {"name":"IDs trocados"}            | User is not a signatory.                                                     | 422.021 | en-US    |
      | com employee alterando employee pt               | ${employeeId} | ${employeeId2}        | {"name":"IDs de employees"}        | Usuário não é um signatário.                                                 | 422.021 | pt-BR    |
      | com IDs de dois singatários                      | ${userId}     | ${userIdSameGroup}    | {"name":"IDs de singatários"}      | Signatory does not have permission to update user ${userIdSameGroup}.        | 422.022 | en-US    |
      | com IDs de dois singatários pt                   | ${userId}     | ${userIdSameGroup}    | {"name":"IDs de singatários"}      | Signatário não tem permissão para atualizar o usuário ${userIdSameGroup}.    | 422.022 | pt-BR    |
      | com IDs de dois singatários grupos diferentes    | ${userId}     | ${userIdDiffGroup}    | {"name":"IDs de singatários"}      | Signatory does not have permission to update user ${userIdDiffGroup}.        | 422.022 | en-US    |
      | com IDs de dois singatários grupos diferentes pt | ${userId}     | ${userIdDiffGroup}    | {"name":"IDs de singatários"}      | Signatário não tem permissão para atualizar o usuário ${userIdDiffGroup}.    | 422.022 | pt-BR    |

  @Negativo
  Scenario Outline: Editar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"name":"Teste edit employee 404"}"
    When I set PATCH api endpoint as "${endpointUsers}/<userId>/employees/<employeeId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao                | userId                               | employeeId                           |
      | com userId inválido      | batax                                | ${employeeId}                        |
      | com userId incorreto     | b101c323-fe08-4bed-9d65-5cfc78003aac | ${employeeId}                        |
      | com employeeId inválido  | ${userId}                            | batax                                |
      | com employeeId incorreto | ${userId}                            | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario Outline: Atualizar employee com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointUsers}/${userId}/employees/${employeeId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao                          | body                                                                                                                          |
      | name                               | {"name":"${name}"}                                                                                                            |
      | phone                              | {"phone":19999999999}                                                                                                         |
      | active false                       | {"active":false}                                                                                                              |
      | active true                        | {"active":true}                                                                                                               |
      | roles.name boletos                 | {"roles":[{"name":"BOLETOS"}]}                                                                                                |
      | roles.name contratos               | {"roles":[{"name":"CONTRATOS"}]}                                                                                              |
      | roles.name desafio lapidar         | {"roles":[{"name":"DESAFIO_LAPIDAR"}]}                                                                                        |
      | roles.name hub                     | {"roles":[{"name":"HUB"}]}                                                                                                    |
      | roles.name marketing               | {"roles":[{"name":"MARKETING"}]}                                                                                              |
      | roles.name notas fiscais           | {"roles":[{"name":"NOTAS_FISCAIS"}]}                                                                                          |
      | roles.name status do pedido        | {"roles":[{"name":"STATUS_DO_PEDIDO"}]}                                                                                       |
      | roles.name pedido de abastecimento | {"roles":[{"name":"PEDIDO_DE_ABASTECIMENTO"}]}                                                                                |
      | roles.name treinamentos            | {"roles":[{"name":"TREINAMENTOS"}]}                                                                                           |
      | mais de um roles.name              | {"roles":[{"name":"TREINAMENTOS"},{"name":"BOLETOS"}]}                                                                        |
      | storeId                            | {"stores":[{"storeId":"0010M00001T0jyUQAR"}]}                                                                                 |
      | mais de um storeId                 | {"stores":[{"storeId":"0010M00001T0jyUQAR"},{"storeId": "0010M00001T0k0LQAR"}]}                                               |
      | body completo                      | {"name":"${name} Atualizado","phone":${phone},"active":true,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}"}]} |

  @Positivo
  Scenario Outline: Atualizar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointUsers}/${userId}/employees/${employeeIdSameGroup}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao                              | body                             |
      | de mesmo grupo econômico               | {"name":"Marcelo Sato Edited"}   |
      | de mesmo grupo econômico estado incial | {"name":"Marcelo Sato Employee"} |
