@POST_users
Feature: POST_users.feature
  Operação responsável por criar um novo usuário no portal.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8088"
    And System generate random CPF
    And I use api name as "/store/v1"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "/users" as "endpointUsers"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Cadastrar users <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                           | body                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | message                                            | code    | language |
      | com storeCode vazio                 | {"storeCode":"","storeDocument":"${storeDocument}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Field storeCode is required.                       | 400.001 | en-US    |
      | com storeCode nulo                  | {"storeCode":null,"storeDocument":"${storeDocument}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Field storeCode is required.                       | 400.001 | en-US    |
      | com storeCode longo                 | {"storeCode":"storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. ","storeDocument":"${storeDocument}"} | Field storeCode must have up to 100 characters.    | 400.002 | en-US    |
      | sem storeCode                       | {"storeDocument":"${storeDocument}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Field storeCode is required.                       | 400.001 | en-US    |
      | com storeDocument vazio             | {"storeCode":"string","storeDocument":""}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Field storeDocument is required.                   | 400.001 | en-US    |
      | com storeDocument nulo              | {"storeCode":"string","storeDocument":null}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Field storeDocument is required.                   | 400.001 | en-US    |
      | com storeDocument longo             | {"storeCode":"string","storeDocument":"101010101011"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Field storeDocument has an invalid format.         | 400.003 | en-US    |
      | com storeDocument incorreto         | {"storeCode":"string","storeDocument":"10101010101"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Field storeDocument has an invalid format.         | 400.003 | en-US    |
      | com storeDocument com mascara       | {"storeCode":"string","storeDocument":"454.010.358-02"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | Field storeDocument has an invalid format.         | 400.003 | en-US    |
      | sem storeDocument                   | {"storeCode":"string"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Field storeDocument is required.                   | 400.001 | en-US    |
      | com storeCode vazio em pt           | {"storeCode":"","storeDocument":"${storeDocument}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Campo storeCode é obrigatório.                     | 400.001 | pt-BR    |
      | com storeCode nulo em pt            | {"storeCode":null,"storeDocument":"${storeDocument}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Campo storeCode é obrigatório.                     | 400.001 | pt-BR    |
      | com storeCode longo em pt           | {"storeCode":"storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. storeCode longo. ","storeDocument":"${storeDocument}"} | Campo storeCode deve ter no máximo 100 caracteres. | 400.002 | pt-BR    |
      | sem storeCode em pt                 | {"storeDocument":"${storeDocument}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Campo storeCode é obrigatório.                     | 400.001 | pt-BR    |
      | com storeDocument vazio em pt       | {"storeCode":"string","storeDocument":""}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Campo storeDocument é obrigatório.                 | 400.001 | pt-BR    |
      | com storeDocument nulo em pt        | {"storeCode":"string","storeDocument":null}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Campo storeDocument é obrigatório.                 | 400.001 | pt-BR    |
      | com storeDocument longo em pt       | {"storeCode":"string","storeDocument":"101010101011"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Campo storeDocument tem um formato inválido.       | 400.003 | pt-BR    |
      | com storeDocument incorreto em pt   | {"storeCode":"string","storeDocument":"10101010101"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Campo storeDocument tem um formato inválido.       | 400.003 | pt-BR    |
      | com storeDocument com mascara em pt | {"storeCode":"string","storeDocument":"454.010.358-02"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | Campo storeDocument tem um formato inválido.       | 400.003 | pt-BR    |
      | sem storeDocument em pt             | {"storeCode":"string"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Campo storeDocument é obrigatório.                 | 400.001 | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar users <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                                        | body                                                      | message                                                            | code    | language |
      | com storeCode inexistente                        | {"storeCode":"string","storeDocument":"${storeDocument}"} | Invalid storeCode and storeDocument field value combination.       | 422.001 | en-US    |
      | com storeCode inválido                           | {"storeCode":"1","storeDocument":"${storeDocument}"}      | Invalid storeCode and storeDocument field value combination.       | 422.001 | en-US    |
      | com storeDocument incorreto para storeCode       | {"storeCode":"CODE","storeDocument":"${randomCPF}"}       | Invalid storeCode and storeDocument field value combination.       | 422.001 | en-US    |
      | com storeCode inexistente em pt                  | {"storeCode":"string","storeDocument":"${storeDocument}"} | Campos storeCode e storeDocument tem combinação de valor inválida. | 422.001 | pt-BR    |
      | com storeDocument incorreto para storeCode em pt | {"storeCode":"CODE","storeDocument":"${randomCPF}"}       | Campos storeCode e storeDocument tem combinação de valor inválida. | 422.001 | pt-BR    |
      | com storeCode inválido em pt                     | {"storeCode":"1","storeDocument":"${storeDocument}"}      | Campos storeCode e storeDocument tem combinação de valor inválida. | 422.001 | pt-BR    |

  @Positivo
  Scenario Outline: Cadastrar usuário <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId<id>"
    And Response code must be 201

    Examples: 
      | id | descricao   | body                                                            |
      |  1 | com sucesso | {"storeCode":"${storeCode}","storeDocument":"${storeDocument}"} |
