@GET_users_{userId}_contacts
Feature: GET_users_{userId}_contacts.feature
  Operação responsável por consultar contatos de um usuário do portal.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/store/v1"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "/users" as "endpointUsers"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"
    And System generate random number
    And I save "testauth${random}@sensedia.com" as "login"

  @PreRequest
  Scenario: Cadastrar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"storeCode":"${storeCode}","storeDocument":"${storeDocument}"}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

  @PreRequest
  Scenario: Consultar contacts
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I save response value "/0/contactId" as "contactId"
    And Response code must be 200

  @PreRequest
  Scenario Outline: Enviar token para telefone
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"ADD_CONTACT","contactId":"${contactId}"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    Given I get token on database by contact_id "${contactId}"
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/<userId>/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId<id>"
    And Response code must be 201

    Examples: 
      | id | descricao   | userId    | body                                                                    |
      |  1 | para ativar | ${userId} | {"contact":"${login}","contactType":"EMAIL","token":${TOKEN}} |

  @PreRequest
  Scenario Outline: Cadastrar senha
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | body                                                                                        |
      | {"contactId":"${contactId1}","password":"${password}","passwordConfirmation":"${password}"} |

  @PreRequest
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId1}","url":"https://www.google.com"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    And I wait 3 seconds
    Given I get token on database by contact_id "${contactId1}"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | descricao              | body                 | tokenType |
      | com tokenType REGISTER | {"token":"${TOKEN}"} | REGISTER  |

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/<userId>/contacts?_limit=10&_offset=0"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario Outline: Consultar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts<queries>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                               | queries                               | message                                             | code    | language |
      | com _limit inválido                     | ?_offset=0&_limit=a                   | Field _limit has an invalid format.                 | 400.003 | en-US    |
      | com _offset inválido                    | ?_limit=10&_offset=a                  | Field _offset has an invalid format.                | 400.003 | en-US    |
      | com contact-type inválido               | ?_limit=10&_offset=0&contact-type=bla | Field contact-type has an invalid format.           | 400.003 | en-US    |
      | com is-contact-validated inválido       | ?is-contact-validated=true1           | Field is-contact-validated has an invalid format.   | 400.003 | en-US    |
      | com _limit inválido em pt               | ?_offset=0&_limit=a                   | Campo _limit tem um formato inválido.               | 400.003 | pt-BR    |
      | com _offset inválido em pt              | ?_limit=10&_offset=a                  | Campo _offset tem um formato inválido.              | 400.003 | pt-BR    |
      | com contact-type inválido em pt         | ?_limit=10&_offset=0&contact-type=bla | Campo contact-type tem um formato inválido.         | 400.003 | pt-BR    |
      | com is-contact-validated inválido em pt | ?is-contact-validated=true1           | Campo is-contact-validated tem um formato inválido. | 400.003 | pt-BR    |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts<queries>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao            | queries              |
      | com contact vazio    | ?contact=            |
      | com contact inválido | ?contact=${login}1 |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?_limit=10&_offset=0"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And Response code must be 200

    Examples: 
      | descricao              |
      | com store-document CPF |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I compare response value "/0/contactType" with "<contact-type>"
    And I verify if response value "/0/isContactValidated" is not empty
    And I verify if response value "/0/contactIdSf" is not empty
    And Response code must be 200

    Examples: 
      | descricao              | query                                   | contact-type |
      | com contact-type EMAIL | ?contact-type=EMAIL&_limit=10&_offset=0 | EMAIL        |
      | com contact-type PHONE | ?contact-type=PHONE&_limit=10&_offset=0 | PHONE        |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And I verify if response value "/0/isContactValidated" is not empty
    And I verify if response value "/0/contactIdSf" is not empty
    And Response code must be 200

    Examples: 
      | descricao                      | query                              |
      | sem _limit                     | ?_offset=0                         |
      | com _limit vazio               | ?_offset=0&_limit=                 |
      | sem _offset                    | ?_limit=1                          |
      | com _offset vazio              | ?_limit=10&_offset=                |
      | sem contact-type               | ?_limit=10&_offset=0               |
      | com contact-type vazio         | ?_limit=10&_offset=0&contact-type= |
      | com is-contact-validated true  | ?is-contact-validated=true         |
      | com is-contact-validated false | ?is-contact-validated=false        |
      | com is-contact-validated vazio | ?is-contact-validated=             |
      | com contact                    | ?contact=${login}                |
