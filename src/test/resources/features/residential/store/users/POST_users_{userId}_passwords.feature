@POST_users_{userId}_passwords
Feature: POST_users_{userId}_passwords.feature
  Operação responsável por criar nova senha para usuário no portal.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/store/v1"
    And I save "/users" as "endpointUsers"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "teste123!" as "password"
    And I save "testaddbyinvoice@sensedia.com" as "login"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"

  @PreRequest
  Scenario: Cadastrar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"storeCode":"${storeCode}","storeDocument":"${storeDocument}"}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

  @PreRequest
  Scenario: Consultar contacts
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I save response value "/0/contactId" as "contactId"
    And Response code must be 200

  @PreRequest
  Scenario: Enviar token para telefone
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"ADD_CONTACT","contactId":"${contactId}"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Definition
  Scenario: Pegar token recém enviado para tabela do banco de dados
    Given I get token on database by contact_id "${contactId}"

  @PreRequest
  Scenario Outline: Cadastrar contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/<userId>/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId"
    And Response code must be 201

    Examples: 
      | descricao    | userId    | body                                                            |
      | por invoices | ${userId} | {"contact":"${login}","contactType":"EMAIL","token":"${TOKEN}"} |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageTwo>"
    And I compare response value "/1/code" with "<code>"
    And I compare response value "/2/message" with "<messageThree>"
    And I compare response value "/2/code" with "<code>"
    And I verify if response value "/3/code" is empty
    And I verify if response value "/3/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao            | body | language | message                       | code    | messageTwo                     | messageThree                              |
      | com body vazio       | {}   | en-US    | Field password is required.   | 400.001 | Field contactId is required.   | Field passwordConfirmation is required.   |
      | com body vazio em pt | {}   | pt-BR    | Campo password é obrigatório. | 400.001 | Campo contactId é obrigatório. | Campo passwordConfirmation é obrigatório. |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                                | body                                                                                 | language | message                                                      | code    |
      | com contactId vazio                      | {"contactId":"","password":"${password}","passwordConfirmation":"${password}"}       | en-US    | Field contactId is required.                                 | 400.001 |
      | com contactId nulo                       | {"contactId":null,"password":"${password}","passwordConfirmation":"${password}"}     | en-US    | Field contactId is required.                                 | 400.001 |
      | sem contactId                            | {"password":"${password}","passwordConfirmation":"${password}"}                      | en-US    | Field contactId is required.                                 | 400.001 |
      | com password nulo                        | {"contactId":"${contactId}","password":null,"passwordConfirmation":"${password}"}    | en-US    | Field password is required.                                  | 400.001 |
      | com password inválido                    | {"contactId":"${contactId}","password":"aline","passwordConfirmation":"${password}"} | en-US    | Field password size must be between 8 and 100.               | 400.009 |
      | sem password                             | {"contactId":"${contactId}","passwordConfirmation":"${password}"}                    | en-US    | Field password is required.                                  | 400.001 |
      | com passwordConfirmation nulo            | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":null}    | en-US    | Field passwordConfirmation is required.                      | 400.001 |
      | com passwordConfirmation incorreto       | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":"aline"} | en-US    | Field passwordConfirmation size must be between 8 and 100.   | 400.009 |
      | sem passwordConfirmation                 | {"contactId":"${contactId}","password":"${password}"}                                | en-US    | Field passwordConfirmation is required.                      | 400.001 |
      | com contactId vazio em pt                | {"contactId":"","password":"${password}","passwordConfirmation":"${password}"}       | pt-BR    | Campo contactId é obrigatório.                               | 400.001 |
      | com contactId nulo em pt                 | {"contactId":null,"password":"${password}","passwordConfirmation":"${password}"}     | pt-BR    | Campo contactId é obrigatório.                               | 400.001 |
      | sem contactId em pt                      | {"password":"${password}","passwordConfirmation":"${password}"}                      | pt-BR    | Campo contactId é obrigatório.                               | 400.001 |
      | com password nulo em pt                  | {"contactId":"${contactId}","password":null,"passwordConfirmation":"${password}"}    | pt-BR    | Campo password é obrigatório.                                | 400.001 |
      | com password inválido em pt              | {"contactId":"${contactId}","password":"aline","passwordConfirmation":"${password}"} | pt-BR    | O campo password deve ter tamanho entre 8 e 100.             | 400.009 |
      | sem password em pt                       | {"contactId":"${contactId}","passwordConfirmation":"${password}"}                    | pt-BR    | Campo password é obrigatório.                                | 400.001 |
      | com passwordConfirmation nulo em pt      | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":null}    | pt-BR    | Campo passwordConfirmation é obrigatório.                    | 400.001 |
      | com passwordConfirmation incorreto em pt | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":"aline"} | pt-BR    | O campo passwordConfirmation deve ter tamanho entre 8 e 100. | 400.009 |
      | sem passwordConfirmation em pt           | {"contactId":"${contactId}","password":"${password}"}                                | pt-BR    | Campo passwordConfirmation é obrigatório.                    | 400.001 |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageTwo>"
    And I compare response value "/1/code" with "<codeTwo>"
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400

    Examples: 
      | descricao                            | body                                                                            | language | message                                   | code    | messageTwo                                                   | codeTwo |
      | com passwordConfirmation vazio em pt | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":""} | pt-BR    | Campo passwordConfirmation é obrigatório. | 400.001 | O campo passwordConfirmation deve ter tamanho entre 8 e 100. | 400.009 |
      | com password vazio                   | {"contactId":"${contactId}","password":"","passwordConfirmation":"${password}"} | en-US    | Field password is required.               | 400.001 | Field password size must be between 8 and 100.               | 400.009 |
      | com passwordConfirmation vazio       | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":""} | en-US    | Field passwordConfirmation is required.   | 400.001 | Field passwordConfirmation size must be between 8 and 100.   | 400.009 |
      | com password vazio em pt             | {"contactId":"${contactId}","password":"","passwordConfirmation":"${password}"} | pt-BR    | Campo password é obrigatório.             | 400.001 | O campo password deve ter tamanho entre 8 e 100.             | 400.009 |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 422

    Examples: 
      | descricao                               | body                                                                                                               | language | message                                                                                | code    |
      | com passwordConfirmation inválido em pt | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":"aline&A12"}                           | pt-BR    | Senha e confirmação de senha não podem ser diferentes.                                 | 422.005 |
      | com password incorreto em pt            | {"contactId":"${contactId}","password":"aline&A12","passwordConfirmation":"${password}"}                           | pt-BR    | Senha e confirmação de senha não podem ser diferentes.                                 | 422.005 |
      | com contactId incorreto                 | {"contactId":"12ef37ec-cc9c-4a30-bb80-a03ffe868f9c","password":"${password}","passwordConfirmation":"${password}"} | en-US    | The contact 12ef37ec-cc9c-4a30-bb80-a03ffe868f9c does not exist in the user ${userId}. | 422.009 |
      | com contactId inválido                  | {"contactId":"inválido","password":"${password}","passwordConfirmation":"${password}"}                             | en-US    | The contact inválido does not exist in the user ${userId}.                             | 422.009 |
      | com password incorreto                  | {"contactId":"${contactId}","password":"aline&123","passwordConfirmation":"${password}"}                           | en-US    | Password and PasswordConfirmation cannot be different.                                 | 422.005 |
      | com contactId incorreto em pt           | {"contactId":"12ef37ec-cc9c-4a30-bb80-a03ffe868f9c","password":"${password}","passwordConfirmation":"${password}"} | pt-BR    | O contato 12ef37ec-cc9c-4a30-bb80-a03ffe868f9c não pertence ao usuário ${userId}.      | 422.009 |
      | com contactId inválido em pt            | {"contactId":"inválido","password":"${password}","passwordConfirmation":"${password}"}                             | pt-BR    | O contato inválido não pertence ao usuário ${userId}.                                  | 422.009 |

  @Positivo
  Scenario Outline: Cadastrar senha
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | body                                                                                       |
      | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":"${password}"} |

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId}" and login "${login}"
