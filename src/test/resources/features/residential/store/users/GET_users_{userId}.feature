@GET_users_{userId}
Feature: GET_users_{userId}.feature
  Operação responsável por retornar o usuário por id.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/store/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "/users" as "endpoint"
    And I save "f66cb524-7a48-4cc6-9e08-85f42b8c9265" as "userId"

	@Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
    
	@Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}/<userId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404
    And I wait 2 seconds

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario: Consultar user por id
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/userId" with "${userId}"
    And I verify if response value "/ownerId" is empty
    And I verify if response value "/storeId" is not empty
    And I verify if response value "/storeCode" is not empty
    And I verify if response value "/storeDocument" is not empty
    And I verify if response value "/login" is not empty
    And I verify if response value "/createdDate" is not empty
    And I verify if response value "/roles/0/name" is not empty
    And I verify if response value "/roles/0/description" is not empty
    And I verify if response value "/storesAccess/0/storeId" is not empty
    And I verify if response value "/storesAccess/0/name" is not empty
    And I verify if response value "/storesAccess/0/document" is empty
    And I verify if response value "/contacts/0/contactId" is not empty
    And I verify if response value "/contacts/0/contact" is not empty
    And I verify if response value "/contacts/0/contactType" is not empty
    And I verify if response value "/contacts/0/isContactValidated" is not empty
    And I verify if response value "/contacts/0/name" is not empty
    And I verify if response value "/contacts/0/contactIdSf" is not empty
    And I verify if response value "/privacyPolicy/version" is not empty
    And I verify if response value "/privacyPolicy/accepted" is not empty
    And I verify if response value "/privacyPolicy/acceptedDate" is not empty
    And I verify if response value "/active" is not empty
    And Response code must be 200
    
