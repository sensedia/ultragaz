@POST_users_authentications
Feature: POST_users_authentications.feature
  Operação responsável por autenticar um usuário.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8001"
    And I use api name as "/store/v1"
    And I save "/users/authentications" as "endpointAuthentication"
    And I save "/users" as "endpointUsers"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "teste123!" as "password"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"
    And System generate random number
    And I save "testauth${random}@sensedia.com" as "login"

  @PreRequest
  Scenario: Cadastrar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"storeCode":"${storeCode}","storeDocument":"${storeDocument}"}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

  @PreRequest
  Scenario: Consultar contacts
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I save response value "/0/contactId" as "contactId"
    And Response code must be 200

  @PreRequest
  Scenario: Enviar token para telefone
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"ADD_CONTACT","contactId":"${contactId}"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Definition
  Scenario: Pegar token recém enviado para tabela do banco de dados
    Given I get token on database by contact_id "${contactId}"

  @PreRequest
  Scenario Outline: Cadastrar contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/<userId>/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And I save response header "Location" as "contacts"
    And I save final value of header "Location" as "contactId"
    And Response code must be 201

    Examples: 
      | descricao    | userId    | body                                                            |
      | por invoices | ${userId} | {"contact":"${login}","contactType":"EMAIL","token":"${TOKEN}"} |

  @PreRequest
  Scenario Outline: Cadastrar senha
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | body                                                                                       |
      | {"contactId":"${contactId}","password":"${password}","passwordConfirmation":"${password}"} |

  @PreRequest
  Scenario: Enviar token para telefone
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"REGISTER","contactId":"${contactId}","url":"https://www.google.com"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Definition
  Scenario: Pegar token recém enviado para tabela do banco de dados
    Given I get token on database by contact_id "${contactId}"

  @PreRequest
  Scenario: Validar token
    Given I get token on database by contact_id "${contactId}"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "{"token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"login":"${login}","password":"${password}"}"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Authenticar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageOne>"
    And I compare response value "/1/code" with "<code>"
    And I verify if response value "/2/message" is empty
    And I verify if response value "/2/code" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao            | body | language | message                    | code    | messageOne                    | messageTwo                 |
      | com body vazio       | {}   | en-US    | Field login is required.   | 400.001 | Field password is required.   | Field login is required.   |
      | com body vazio em pt | {}   | pt-BR    | Campo login é obrigatório. | 400.001 | Campo password é obrigatório. | Campo login é obrigatório. |

  @Negativo
  Scenario Outline: Autenticar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be <responseCode>

    Examples: 
      | descricao               | body                                    | message                       | code    | language | responseCode |
      | sem login               | {"password":"${password}"}              | Field login is required.      | 400.001 | en-US    |          400 |
      | com login vazio         | {"login":"","password":"${password}"}   | Field login is required.      | 400.001 | en-US    |          400 |
      | com login nulo          | {"login":null,"password":"${password}"} | Field login is required.      | 400.001 | en-US    |          400 |
      | sem password            | {"login":"${login}"}                    | Field password is required.   | 400.001 | en-US    |          400 |
      | com password nulo       | {"login":"${login}","password":null}    | Field password is required.   | 400.001 | en-US    |          400 |
      | sem login em pt         | {"password":"${password}"}              | Campo login é obrigatório.    | 400.001 | pt-BR    |          400 |
      | com login vazio em pt   | {"login":"","password":"${password}"}   | Campo login é obrigatório.    | 400.001 | pt-BR    |          400 |
      | com login nulo em pt    | {"login":null,"password":"${password}"} | Campo login é obrigatório.    | 400.001 | pt-BR    |          400 |
      | sem password em pt      | {"login":"${login}"}                    | Campo password é obrigatório. | 400.001 | pt-BR    |          400 |
      | com password nulo em pt | {"login":"${login}","password":null}    | Campo password é obrigatório. | 400.001 | pt-BR    |          400 |

  @Negativo
  Scenario Outline: Autenticar <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 401

    Examples: 
      | descricao                   | body                                            | message                                                                                              | code    | language | responseCode |
      | com login inválido          | {"login":"${login}1","password":"${password}"}  | Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character.   | 400.001 | en-US    |          400 |
      | com password inválido       | {"login":"${login}1","password":"${password}1"} | Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character.   | 400.001 | en-US    |          400 |
      | com login inválido em pt    | {"login":"${login}1","password":"${password}"}  | Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial. | 400.001 | pt-BR    |          400 |
      | com password inválido em pt | {"login":"${login}","password":"${password}1"}  | Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial. | 400.001 | pt-BR    |          400 |

  @Positivo
  Scenario: Realizar autenticação
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"login":"diretor@beguetto.com.br","password":"${password}"}"
    When I set POST api endpoint as "${endpointAuthentication}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId}" and login "${login}"
