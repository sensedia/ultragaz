@PATCH_users_{userId}
Feature: PATCH_users_{userId}.feature
  Operação responsável por criar uma nova senha para o Cliente

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/store/v1"
    And I save "/users" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "310708" as "storeCode"
    And I save "23539964000108" as "storeDocument"
    And I save "CHANGE_PASSWORD" as "tokenType"
    And I save "Aab45@owqefoqheohi23hohfowhto2h_vroihgoirhwoihrvqrbrqe&@rjeqprbjqojfpoqjrgoqjgpojqojpoqgerjgnpvqnrjopqe" as "passwordConfirmation101"
    And I save "f66cb524-7a48-4cc6-9e08-85f42b8c9265" as "userId"
    And I save "https://www.google.com" as "url"

  @Ignore
  Scenario Outline: Cadastrar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

    Examples: 
      | id | descricao | body                                                            |
      |  1 | com cpf   | {"storeCode":"${storeCode}","storeDocument":"${storeDocument}"} |

  @PreRequest
  Scenario: Consultar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/userId" with "${userId}"
    And I compare response value "/storeDocument" with "${storeDocument}"
    And I verify if response value "/active" is not empty
    And I verify if response value "/storeId" is not empty
    And Response code must be 200

  @PreRequest
  Scenario: Consultar contacts
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}/${userId}/contacts?contactType=EMAIL"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And I save response value "/0/contactId" as "contactId"
    And Response code must be 200

  @PreRequest
  Scenario: Enviar token para email
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"${tokenType}","contactId":"${contactId}","url":"${url}"}"
    When I set POST api endpoint as "${endpoint}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Definition
  Scenario: Pegar token recém enviado para tabela do banco de dados
    Given I get token on database by contact_id "${contactId}"

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request body as "{"token":"081170","password":"Q4tu$pqk","passwordConfirmation":"Q4tu$pqk"}"
    When I set GET api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"token":"081170","password":"Q4tu$pqk","passwordConfirmation":"Q4tu$pqk"}"
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message               | language |
      | sem body       | Invalid request body. | en-US    |
      | sem body em pt | Requisição inválida.  | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                                                  | body                                                                                                                        | message                                                        | code      | language |
      | com password com menos de 8 caracteres                     | "{"token":"${token}","password":"Aab45@","passwordConfirmation":"Aab45@123"}"                                               | "Field password size must be between 8 and 100."               | "400.009" | "en-US"  |
      | com password com mais de 100 caracteres                    | "{"token":"${token}","password":"${passwordConfirmation101}","passwordConfirmation":"Aab45@owqefoqhe"}"                     | "Field password size must be between 8 and 100."               | "400.009" | "en-US"  |
      | com passwordConfirmation com menos de 8 caracteres         | "{"token":"${token}","password":"Aab45@123","passwordConfirmation":"Aab45@"}"                                               | "Field passwordConfirmation size must be between 8 and 100."   | "400.009" | "en-US"  |
      | com passwordConfirmation com mais de 100 caracteres        | "{"token":"${token}","password":"Aab45@owqpqe","passwordConfirmation":"${passwordConfirmation101}"}"                        | "Field passwordConfirmation size must be between 8 and 100."   | "400.009" | "en-US"  |
      | com privacyPoliciesAccepted inválido                       | "{"token":"${token}","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376","privacyPoliciesAccepted":"falseane"}" | "Invalid request body."                                        | "400.000" | "en-US"  |
      | com password com menos de 8 caracteres em pt               | "{"token":"${token}","password":"Aab45@","passwordConfirmation":"Aab45@123"}"                                               | "O campo password deve ter tamanho entre 8 e 100."             | "400.009" | "pt-BR"  |
      | com password com mais de 100 caracteres  em pt             | "{"token":"${token}","password":"${passwordConfirmation101}","passwordConfirmation":"Aab45@owqefoqheoh"}"                   | "O campo password deve ter tamanho entre 8 e 100."             | "400.009" | "pt-BR"  |
      | com passwordConfirmation com menos de 8 caracteres em pt   | "{"token":"${token}","password":"Aab45@123","passwordConfirmation":"Aab45@"}"                                               | "O campo passwordConfirmation deve ter tamanho entre 8 e 100." | "400.009" | "pt-BR"  |
      | com passwordConfirmation com mais de 100 caracteres  em pt | "{"token":"${token}","password":"Aab45@owqe","passwordConfirmation":"${passwordConfirmation101}"}"                          | "O campo passwordConfirmation deve ter tamanho entre 8 e 100." | "400.009" | "pt-BR"  |
      | com privacyPoliciesAccepted inválido pt                    | "{"token":"${token}","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376","privacyPoliciesAccepted":"falseane"}" | "Requisição inválida."                                         | "400.000" | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                                            | body                                                                                           | message                                                                                                | code      | language |
      | com password sem numeros                             | "{"token":"${token}","password":"Aiopjpj@joh","passwordConfirmation":"Aiopjpj@joh"}"           | "Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character."   | "422.012" | "en-US"  |
      | com password sem letras                              | "{"token":"${token}","password":"123456&7","passwordConfirmation":"123456&7"}"                 | "Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character."   | "422.012" | "en-US"  |
      | com password sem símbolos                            | "{"token":"${token}","password":"pqojr242borj","passwordConfirmation":"pqojr242borj"}"         | "Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character."   | "422.012" | "en-US"  |
      | com password com espaço                              | "{"token":"${token}","password":"qp345 rtgp","passwordConfirmation":"qp345 rtgp"}"             | "Password must have at least 8 characters with at least 1 letter, 1 number and 1 special character."   | "422.012" | "en-US"  |
      | sem password                                         | "{"token":"${token}","passwordConfirmation":"Qr5@frgr2376"}"                                   | "When token is informed, password and passwordConfirmation are required."                              | "422.026" | "en-US"  |
      | com password nulo                                    | "{"token":"${token}","password":null,"passwordConfirmation":"Qr5@frgr2376"}"                   | "When token is informed, password and passwordConfirmation are required."                              | "422.026" | "en-US"  |
      | sem passwordConfirmation                             | "{"token":"${token}","password":"Qr5@frgr2376"}"                                               | "When token is informed, password and passwordConfirmation are required."                              | "422.026" | "en-US"  |
      | com passwordConfirmation nulo                        | "{"token":"${token}","password":"Qr5@frgr2376","passwordConfirmation":null}"                   | "When token is informed, password and passwordConfirmation are required."                              | "422.026" | "en-US"  |
      | com token incorreto/expirado                         | "{"token":"${tokenExpirado}","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}" | "Invalid token."                                                                                       | "422.002" | "en-US"  |
      | com password diferente de passwordConfirmation       | "{"token":"${token}","password":"Qr5@frgr2376a","passwordConfirmation":"Qr5@frgr2376"}"        | "Password and PasswordConfirmation cannot be different."                                               | "422.005" | "en-US"  |
      | com password sem numeros em pt                       | "{"token":"${token}","password":"Aiopjpj@joh","passwordConfirmation":"Aiopjpj@joh"}"           | "Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial." | "422.012" | "pt-BR"  |
      | com password sem letras em pt                        | "{"token":"${token}","password":"123456&7","passwordConfirmation":"123456&7"}"                 | "Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial." | "422.012" | "pt-BR"  |
      | com password sem símbolos em pt                      | "{"token":"${token}","password":"pqojr242borj","passwordConfirmation":"pqojr242borj"}"         | "Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial." | "422.012" | "pt-BR"  |
      | com password com espaço em pt                        | "{"token":"${token}","password":"qp345 rtgp","passwordConfirmation":"qp345 rtgp"}"             | "Senha deve ter no mínimo 8 caracteres combinando pelo menos 1 letra, 1 número e 1 caracter especial." | "422.012" | "pt-BR"  |
      | sem password em pt                                   | "{"token":"${token}","passwordConfirmation":"Qr5@frgr2376"}"                                   | "Quando o campo token é informado, os campos password e passwordConfirmation são obrigatórios."        | "422.026" | "pt-BR"  |
      | com password nulo em pt                              | "{"token":"${token}","password":null,"passwordConfirmation":"Qr5@frgr2376"}"                   | "Quando o campo token é informado, os campos password e passwordConfirmation são obrigatórios."        | "422.026" | "pt-BR"  |
      | sem passwordConfirmation em pt                       | "{"token":"${token}","password":"Qr5@frgr2376"}"                                               | "Quando o campo token é informado, os campos password e passwordConfirmation são obrigatórios."        | "422.026" | "pt-BR"  |
      | com passwordConfirmation nulo em pt                  | "{"token":"${token}","password":"Qr5@frgr2376","passwordConfirmation":null}"                   | "Quando o campo token é informado, os campos password e passwordConfirmation são obrigatórios."        | "422.026" | "pt-BR"  |
      | com token incorreto/expirado em pt                   | "{"token":"${tokenExpirado}","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}" | "Token inválido."                                                                                      | "422.002" | "pt-BR"  |
      | com password diferente de passwordConfirmation em pt | "{"token":"${token}","password":"Qr5@frgr2376a","passwordConfirmation":"Qr5@frgr2376"}"        | "Senha e confirmação de senha não podem ser diferentes."                                               | "422.005" | "pt-BR"  |

  @Negativo
  Scenario Outline: Cadastrar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${token}","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"
    When I set PATCH api endpoint as "${endpoint}/<userId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404
    And I wait 2 seconds

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario: Cadastrar senha com userId inválido
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${token}","password":"Qr5@frgr2376","passwordConfirmation":"Qr5@frgr2376"}"
    When I set PATCH api endpoint as "${endpoint}/1"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

  @Positivo
  Scenario Outline: Alterar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao                           | body                                                                                                            |
      | com body completo                   | "{"token":"${TOKEN}","password":"teste123!","passwordConfirmation":"teste123!","privacyPoliciesAccepted":true}" |
      
  @Ignore
  Scenario Outline: Alterar senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpoint}/${userId}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | descricao                           | body                                                                                                            |
      | com body vazio                      | "{}"                                                                                                            |
      | sem token                           | "{"password":"teste123!","passwordConfirmation":"teste123!","privacyPoliciesAccepted":true}"                    |
      | sem privacyPoliciesAccepted         | "{"token":"${TOKEN}","password":"teste123!","passwordConfirmation":"teste123!"}"                                |
      | sem token e privacyPoliciesAccepted | "{"password":"teste123!","passwordConfirmation":"teste123!"}"                                                   |
      | com apenas password                 | "{"password":"teste123!"}"                                                                                      |
      | com apenas passwordConfirmation     | "{"passwordConfirmation":"teste123!"}"                                                                          |
   
