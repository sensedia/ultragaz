@POST_users_{userId}_tokens
Feature: POST_users_{userId}_tokens.feature
  Operação responsável por enviar token.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/store/v1"
    And I save "/users" as "endpointUsers"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "https://www.frontendtoken.com.br" as "url"
    And I save "REGISTER" as "tokenType"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"

  @PreRequest
  Scenario: Cadastrar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"storeCode":"${storeCode}","storeDocument":"${storeDocument}"}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

  @PreRequest
  Scenario: Consultar contacts
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I save response value "/0/contactId" as "contactId"
    And Response code must be 200
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=EMAIL"
    Then I get response body
    And I get response status code
    And I save response value "/0/contactId" as "contactId2"
    And Response code must be 200

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message               | language |
      | sem body       | Invalid request body. | en-US    |
      | sem body em pt | Requisição inválida.  | pt-BR    |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be <responseCode>

    Examples: 
      | descricao                    | body                                                              | message                                           | code    | language | responseCode |
      | com body vazio               | {}                                                                | Field contactId is required.                      | 400.001 | en-US    |          400 |
      | sem tokenType                | {"url":"${url}","contactId":"${contactId}"}                       | Field tokenType has an invalid value or is empty. | 400.007 | en-US    |          400 |
      | com tokenType vazio          | {"tokenType":"","url":"${url}","contactId":"${contactId}"}        | Field tokenType has an invalid value or is empty. | 400.007 | en-US    |          400 |
      | com tokenType nulo           | {"tokenType":null,"url":"${url}","contactId":"${contactId}"}      | Field tokenType has an invalid value or is empty. | 400.007 | en-US    |          400 |
      | com tokenType inválido       | {"tokenType":"1","url":"${url}","contactId":"${contactId}"}       | Field tokenType has an invalid value or is empty. | 400.007 | en-US    |          400 |
      | com url inválido             | {"tokenType":"${tokenType}","url":"a","contactId":"${contactId}"} | Field url has an invalid format.                  | 400.003 | en-US    |          400 |
      | sem contactId                | {"tokenType":"${tokenType}","url":"${url}"}                       | Field contactId is required.                      | 400.001 | en-US    |          400 |
      | com contactId vazio          | {"tokenType":"${tokenType}","url":"${url}","contactId":""}        | Field contactId is required.                      | 400.001 | en-US    |          400 |
      | com contactId nulo           | {"tokenType":"${tokenType}","url":"${url}","contactId":null}      | Field contactId is required.                      | 400.001 | en-US    |          400 |
      | com body vazio em pt         | {}                                                                | Campo contactId é obrigatório.                    | 400.001 | pt-BR    |          400 |
      | sem tokenType em pt          | {"url":"${url}","contactId":"${contactId}"}                       | Campo tokenType tem um valor inválido ou vazio.   | 400.007 | pt-BR    |          400 |
      | com tokenType vazio em pt    | {"tokenType":"","url":"${url}","contactId":"${contactId}"}        | Campo tokenType tem um valor inválido ou vazio.   | 400.007 | pt-BR    |          400 |
      | com tokenType nulo em pt     | {"tokenType":null,"url":"${url}","contactId":"${contactId}"}      | Campo tokenType tem um valor inválido ou vazio.   | 400.007 | pt-BR    |          400 |
      | com tokenType inválido em pt | {"tokenType":"1","url":"${url}","contactId":"${contactId}"}       | Campo tokenType tem um valor inválido ou vazio.   | 400.007 | pt-BR    |          400 |
      | com url inválido em pt       | {"tokenType":"${tokenType}","url":"1","contactId":"${contactId}"} | Campo url tem um formato inválido.                | 400.003 | pt-BR    |          400 |
      | sem contactId em pt          | {"tokenType":"${tokenType}","url":"${url}"}                       | Campo contactId é obrigatório.                    | 400.001 | pt-BR    |          400 |
      | com contactId vazio em pt    | {"tokenType":"${tokenType}","url":"${url}","contactId":""}        | Campo contactId é obrigatório.                    | 400.001 | pt-BR    |          400 |
      | com contactId nulo em pt     | {"tokenType":"${tokenType}","url":"${url}","contactId":null}      | Campo contactId é obrigatório.                    | 400.001 | pt-BR    |          400 |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be <responseCode>

    Examples: 
      | descricao                    | body                                                        | message                          | code    | language | responseCode |
      | com contactId inválido       | {"tokenType":"${tokenType}","url":"${url}","contactId":"1"} | Invalid contact to user.         | 422.008 | en-US    |          422 |
      | com contactId inválido em pt | {"tokenType":"${tokenType}","url":"${url}","contactId":"1"} | Contato inválido para o usuário. | 422.008 | pt-BR    |          422 |

  @Negativo
  Scenario Outline: Enviar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"${tokenType}","url":"${url}","contactId":"${contactId}"}"
    When I set POST api endpoint as "${endpointUsers}/<userId>/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario: Com userId inválido
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"${tokenType}","url":"${url}","contactId":"${contactId}"}"
    When I set POST api endpoint as "${endpointUsers}/1/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

  @Positivo
  Scenario Outline: Enviar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"<contactId>"}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | descricao                                | tokenType       | contactId     |
      | com tokenType REGISTER para PHONE        | REGISTER        | ${contactId}  |
      | com tokenType ADD_CONTACT para PHONE     | ADD_CONTACT     | ${contactId}  |
      | com tokenType CHANGE_PASSWORD para PHONE | CHANGE_PASSWORD | ${contactId}  |
      | com tokenType REGISTER para EMAIL        | REGISTER        | ${contactId2} |
      | com tokenType ADD_CONTACT para EMAIL     | ADD_CONTACT     | ${contactId2} |
      | com tokenType CHANGE_PASSWORD para EMAIL | CHANGE_PASSWORD | ${contactId2} |
