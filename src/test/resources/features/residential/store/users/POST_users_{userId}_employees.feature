@POST_users_{userId}_employees
Feature: POST_users_{userId}_employees.feature
  Operação responsável por criar um funcionário (usuário) atrelado ao usuário owner (signatário).

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/store/v1"
    And I save "/users" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "cf51fa1e-aa25-4d53-91e8-0caa317ada4e" as "userId"
    And I save "Wellington Employee" as "name"
    And I save "19982911139" as "phone"
    And I save "0010M00001MqKwSQAV" as "storeId"
    And I save "FFF COMERCIO VAREJISTA DE GLP EIRELI" as "storeName"
    And I save "wellington.moura+employee@sensedia.com" as "login"

  @Negativo
  Scenario Outline: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"
    When I set POST api endpoint as "${endpoint}/${userId}/employees"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"
    When I set POST api endpoint as "${endpoint}/${userId}/employees"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"
    When I set POST api endpoint as "${endpoint}/${userId}/employees"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"
    When I set POST api endpoint as "${endpoint}/${userId}/employees"
    Then I get response body
    And I get response status code
    And Response body must be "<message>"
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                  |
      | com client_id vazio       | ""              | "${access_token}"  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com access_token vazio    | "${client_id}"  | ""                 | Could not find a required Access Token in the request, identified by HEADER access_token |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | Could not find a required Access Token in the request, identified by HEADER access_token |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpoint}/${userId}/employees"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message               | language |
      | sem body       | Invalid request body. | en-US    |
      | sem body em pt | Requisição inválida.  | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpoint}/${userId}/employees"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                      | body                                                                                                                                               | message                                               | code    | language |
      | sem name                       | {"login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                       | Field name is required.                               | 400.001 | en-US    |
      | com name vazio                 | {"name":"","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}             | Field name is required.                               | 400.001 | en-US    |
      | com name nulo                  | {"name":null,"login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}           | Field name is required.                               | 400.001 | en-US    |
      | sem login                      | {"name":"${name}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                         | Field login is required.                              | 400.001 | en-US    |
      | com login vazio                | {"name":"${name}","login":"","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}              | Field login is required.                              | 400.001 | en-US    |
      | com login nulo                 | {"name":"${name}","login":null,"phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}            | Field login is required.                              | 400.001 | en-US    |
      | com login inválido             | {"name":"${name}","login":"teste.com","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}     | Field login has an invalid format.                    | 400.003 | en-US    |
      | sem phone                      | {"name":"${name}","login":"${login}","roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                       | Field phone is required.                              | 400.001 | en-US    |
      | com phone vazio                | {"name":"${name}","login":"${login}","phone":"","roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}            | Field phone is required.                              | 400.001 | en-US    |
      | com phone nulo                 | {"name":"${name}","login":"${login}","phone":null,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}          | Field phone is required.                              | 400.001 | en-US    |
      | com phone com mais dígitos     | {"name":"${name}","login":"${login}","phone":${phone}98989,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]} | Field phone has an invalid format.                    | 400.003 | en-US    |
      | com phone com menos dígitos    | {"name":"${name}","login":"${login}","phone":199999999,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}     | Field phone has an invalid format.                    | 400.003 | en-US    |
      | com phone inválido             | {"name":"${name}","login":"${login}","phone":"199B2911139","roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]} | Field phone has an invalid format.                    | 400.003 | en-US    |
      | sem roles                      | {"name":"${name}","login":"${login}","phone":${phone},"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                                   | Field roles is required.                              | 400.001 | en-US    |
      | com roles vazio                | {"name":"${name}","login":"${login}","phone":${phone},"roles":[],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                        | Field roles must have at least one information.       | 400.012 | en-US    |
      | com name roles vazio           | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":""}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}             | Field roles[0].name has an invalid value or is empty. | 400.007 | en-US    |
      | com name roles nulo            | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":null}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}           | Field roles[0].name has an invalid value or is empty. | 400.007 | en-US    |
      | com name roles inválido        | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"IRONMAIDEN"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}   | Field roles[0].name has an invalid value or is empty. | 400.007 | en-US    |
      | sem store                      | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}]}                                                                | Field stores is required.                             | 400.001 | en-US    |
      | com store vazio                | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[]}                                                    | Field stores must have at least one information.      | 400.012 | en-US    |
      | sem storeId                    | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"name":"${storeName}"}]}                             | Field stores[0].storeId is required.                  | 400.001 | en-US    |
      | com storeId vazio              | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"","name":"${storeName}"}]}                | Field stores[0].storeId is required.                  | 400.001 | en-US    |
      | com storeId nulo               | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":null,"name":"${storeName}"}]}              | Field stores[0].storeId is required.                  | 400.001 | en-US    |
      | sem store name                 | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}"}]}                            | Field stores[0].name is required.                     | 400.001 | en-US    |
      | com store name vazio           | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":""}]}                  | Field stores[0].name is required.                     | 400.001 | en-US    |
      | com store name nulo            | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":null}]}                | Field stores[0].name is required.                     | 400.001 | en-US    |
      | sem name pt                    | {"login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                       | Campo name é obrigatório.                             | 400.001 | pt-BR    |
      | com name vazio pt              | {"name":"","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}             | Campo name é obrigatório.                             | 400.001 | pt-BR    |
      | com name nulo  pt              | {"name":null,"login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}           | Campo name é obrigatório.                             | 400.001 | pt-BR    |
      | sem login pt                   | {"name":"${name}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                         | Campo login é obrigatório.                            | 400.001 | pt-BR    |
      | com login vazio pt             | {"name":"${name}","login":"","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}              | Campo login é obrigatório.                            | 400.001 | pt-BR    |
      | com login nulo pt              | {"name":"${name}","login":null,"phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}            | Campo login é obrigatório.                            | 400.001 | pt-BR    |
      | com login inválido pt          | {"name":"${name}","login":"teste.com","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}     | Campo login tem um formato inválido.                  | 400.003 | pt-BR    |
      | sem phone pt                   | {"name":"${name}","login":"${login}","roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                       | Campo phone é obrigatório.                            | 400.001 | pt-BR    |
      | com phone vazio pt             | {"name":"${name}","login":"${login}","phone":"","roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}            | Campo phone é obrigatório.                            | 400.001 | pt-BR    |
      | com phone nulo pt              | {"name":"${name}","login":"${login}","phone":null,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}          | Campo phone é obrigatório.                            | 400.001 | pt-BR    |
      | com phone com mais dígitos pt  | {"name":"${name}","login":"${login}","phone":${phone}98989,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]} | Campo phone tem um formato inválido.                  | 400.003 | pt-BR    |
      | com phone com menos dígitos pt | {"name":"${name}","login":"${login}","phone":199999999,"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}     | Campo phone tem um formato inválido.                  | 400.003 | pt-BR    |
      | com phone inválido pt          | {"name":"${name}","login":"${login}","phone":"199B2911139","roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]} | Campo phone tem um formato inválido.                  | 400.003 | pt-BR    |
      | sem roles pt                   | {"name":"${name}","login":"${login}","phone":${phone},"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                                   | Campo roles é obrigatório.                            | 400.001 | pt-BR    |
      | com roles vazio pt             | {"name":"${name}","login":"${login}","phone":${phone},"roles":[],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}                        | O campo roles deve ter pelo menos uma informação.     | 400.012 | pt-BR    |
      | com name roles vazio pt        | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":""}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}             | Campo roles[0].name tem um valor inválido ou vazio.   | 400.007 | pt-BR    |
      | com name roles nulo pt         | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":null}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}           | Campo roles[0].name tem um valor inválido ou vazio.   | 400.007 | pt-BR    |
      | com name roles inválido pt     | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"IRONMAIDEN"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}   | Campo roles[0].name tem um valor inválido ou vazio.   | 400.007 | pt-BR    |
      | sem store pt                   | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}]}                                                                | Campo stores é obrigatório.                           | 400.001 | pt-BR    |
      | com store vazio pt             | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[]}                                                    | O campo stores deve ter pelo menos uma informação.    | 400.012 | pt-BR    |
      | sem storeId pt                 | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"name":"${storeName}"}]}                             | Campo stores[0].storeId é obrigatório.                | 400.001 | pt-BR    |
      | com storeId vazio pt           | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"","name":"${storeName}"}]}                | Campo stores[0].storeId é obrigatório.                | 400.001 | pt-BR    |
      | com storeId nulo pt            | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":null,"name":"${storeName}"}]}              | Campo stores[0].storeId é obrigatório.                | 400.001 | pt-BR    |
      | sem store name pt              | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}"}]}                            | Campo stores[0].name é obrigatório.                   | 400.001 | pt-BR    |
      | com store name vazio pt        | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":""}]}                  | Campo stores[0].name é obrigatório.                   | 400.001 | pt-BR    |
      | com store name nulo pt         | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":null}]}                | Campo stores[0].name é obrigatório.                   | 400.001 | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpoint}/${userId}/employees"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao               | body                                                                                                                                          | message                                          | code    | language |
      | com storeId inválido    | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"batatadoce","name":"${storeName}"}]} | Store [batatadoce] not found for the given user. | 422.020 | en-US    |
      | com storeId inválido pt | {"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"batatadoce","name":"${storeName}"}]} | Loja [batatadoce] não encontrada para o usuário. | 422.020 | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"
    When I set POST api endpoint as "${endpoint}/<userId>/employees"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  | batax                                |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario Outline: Cadastrar employee <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as <body>
    When I set POST api endpoint as "${endpoint}/${userId}/employees"
    Then I get response body
    And I get response status code
    And I save response header "location" as "location"
    And I save final value of header "location" as "employeeId"
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | descricao                        | body                                                                                                                                                            |
      | com role BOLETOS                 | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"BOLETOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"                 |
      | com role NOTAS_FISCAIS           | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"NOTAS_FISCAIS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"           |
      | com role TREINAMENTOS            | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"TREINAMENTOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"            |
      | com role HUB                     | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"HUB"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"                     |
      | com role PEDIDO_DE_ABASTECIMENTO | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"PEDIDO_DE_ABASTECIMENTO"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}" |
      | com role STATUS_DO_PEDIDO        | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"STATUS_DO_PEDIDO"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"        |
      | com role VALE_GAS                | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"VALE_GAS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"                |
      | com role CONTRATOS               | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"CONTRATOS"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"               |
      | com role MARKETING               | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"MARKETING"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"               |
      | com role DESAFIO_LAPIDAR         | "{"name":"${name}","login":"${login}","phone":${phone},"roles":[{"name":"DESAFIO_LAPIDAR"}],"stores":[{"storeId":"${storeId}","name":"${storeName}"}]}"         |
