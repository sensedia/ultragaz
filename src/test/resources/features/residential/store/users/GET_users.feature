@GET_users
Feature: GET_users.feature
  Operação responsável por consultar usuários do portal.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/store/v1"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "/users" as "endpoint"
    And I save "0013000001FnYmbAAF" as "storeIdAccess"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"
    And I save "Q4tu&pqk" as "password"
    And I save "testaddbyinvoice@sensedia.com" as "login"
    And I save "ultragazremedios@mailinator.com" as "login2"
    And I save "wellington.moura@sensedia.com" as "ownerLogin"
    And I save "f66cb524-7a48-4cc6-9e08-85f42b8c9265" as "ownerId"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?login=${ownerId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}?login=${ownerId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?login=${ownerId}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar usuários <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}<queries>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                   | queries          | message                                          | code      | language |
      | com _limit 0                | ?_limit=0        | "Field _limit value must be at least 1."         | "400.010" | "en-US"  |
      | com isActive inválido       | ?is-active=trues | "Field is-active has an invalid format."         | "400.003" | "en-US"  |
      | com _limit 0 em pt          | ?_limit=0        | "O valor do campo _limit deve ser pelo menos 1." | "400.010" | "pt-BR"  |
      | com isActive inválido em pt | ?is-active=trues | "Campo is-active tem um formato inválido."       | "400.003" | "pt-BR"  |

  @Positivo
  Scenario: Consultar usuários com login inexistente
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?login=batatinhax"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/userId" is not empty
    And I verify if response value "/0/storeId" is not empty
    And I verify if response value "/0/storeCode" is not empty
    And I verify if response value "/0/storeDocument" is not empty
    And I verify if response value "/0/login" is not empty
    And I verify if response value "/0/createdDate" is not empty
    And I verify if response value "/0/roles/0/name" is not empty
    And I verify if response value "/0/roles/0/description" is not empty
    And I verify if response value "/0/storesAccess/0/storeId" is not empty
    And I verify if response value "/0/storesAccess/0/name" is not empty
    And I verify if response value "/0/active" is not empty
    And Response code must be 200

    Examples: 
      | descricao               | query                                            |
      | com login do signatário | ?login=${ownerLogin}                             |
      | com owner-id            | ?owner-id=${ownerId}                             |
      | com store-id-access     | ?store-id-access=${storeIdAccess}&is-active=true |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/userId" is not empty
    And I verify if response value "/0/storeId" is not empty
    And I verify if response value "/0/storeCode" is not empty
    And I verify if response value "/0/storeDocument" is not empty
    And I verify if response value "/0/login" is not empty
    And I verify if response value "/0/createdDate" is not empty
    And I verify if response value "/0/roles/0/name" is not empty
    And I verify if response value "/0/roles/0/description" is not empty
    And I verify if response value "/0/storesAccess/0/storeId" is not empty
    And I verify if response value "/0/storesAccess/0/name" is not empty
    And I verify if response value "/0/storesAccess/0/document" is not empty
    And I verify if response value "/0/active" is not empty
    And Response code must be 200

    Examples: 
      | descricao          | query           |
      | com is-active true | ?is-active=true |

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/userId" is not empty
    And I verify if response value "/0/storeId" is not empty
    And I verify if response value "/0/storeCode" is not empty
    And I verify if response value "/0/storeDocument" is not empty
    And I verify if response value "/0/createdDate" is not empty
    And I verify if response value "/0/roles" is empty
    And I verify if response value "/0/storesAccess" is empty
    And I verify if response value "/0/active" is not empty
    And Response code must be 200

    Examples: 
      | descricao           | query            |
      | com is-active false | ?is-active=false |
      | com _limit 1        | ?_limit=1        |
      | com _offset 0       | ?offset=0        |

  @Positivo
  Scenario: Consultar user com expand contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?login=${ownerLogin}&_expand=CONTACT"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/userId" is not empty
    And I verify if response value "/0/storeId" is not empty
    And I verify if response value "/0/storeCode" is not empty
    And I verify if response value "/0/storeDocument" is not empty
    And I verify if response value "/0/login" is not empty
    And I verify if response value "/0/createdDate" is not empty
    And I verify if response value "/0/roles/0/name" is not empty
    And I verify if response value "/0/roles/0/description" is not empty
    And I verify if response value "/0/storesAccess/0/storeId" is not empty
    And I verify if response value "/0/storesAccess/0/name" is not empty
    And I verify if response value "/0/contacts/0/contactId" is not empty
    And I verify if response value "/0/contacts/0/contact" is not empty
    And I verify if response value "/0/contacts/0/contactType" is not empty
    And I verify if response value "/0/contacts/0/name" is not empty
    And I verify if response value "/0/contacts/0/isContactValidated" is not empty
    And I verify if response value "/0/contacts/0/contactIdSf" is not empty
    And I verify if response value "/0/privacyPolicy/version" is not empty
    And I verify if response value "/0/privacyPolicy/accepted" is not empty
    And I verify if response value "/0/privacyPolicy/acceptedDate" is not empty
    And I verify if response value "/0/active" is not empty
    And Response code must be 200

  @Positivo
  Scenario Outline: Consultar user <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/userId" is not empty
    And I verify if response value "/0/storeId" is not empty
    And I verify if response value "/0/storeCode" is not empty
    And I verify if response value "/0/storeDocument" is not empty
    And I verify if response value "/0/login" is not empty
    And I verify if response value "/0/createdDate" is not empty
    And I verify if response value "/0/roles/0/name" is not empty
    And I verify if response value "/0/roles/0/description" is not empty
    And I verify if response value "/0/storesAccess/0/storeId" is not empty
    And I verify if response value "/0/storesAccess/0/name" is not empty
    And I verify if response value "/0/privacyPolicy/version" is not empty
    And I verify if response value "/0/privacyPolicy/accepted" is not empty
    And I verify if response value "/0/privacyPolicy/acceptedDate" is not empty
    And I verify if response value "/0/active" is not empty
    And Response code must be 200

    Examples: 
      | descricao                       | query                                   |
      | com expand diverso              | ?login=${ownerLogin}&_expand=sexta      |
      | com expand vazio                | ?login=${ownerLogin}&_expand=           |
      | com expand numérico             | ?login=${ownerLogin}&_expand=1          |
      | com expand caracteres especiais | ?login=${ownerLogin}&_expand=#$%?@#@&@# |
