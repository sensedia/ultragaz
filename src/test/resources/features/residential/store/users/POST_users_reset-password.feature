@POST_users_reset-password
Feature: POST_users_reset-password.feature
  Operação responsável por enviar link para reset da senha

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/store/v1"
    And I save "/users" as "endpointUsers"
    And I save "https://www.google.com" as "url"
    And I save "wellington.moura@sensedia.com" as "email"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"
    And I save "CHANGE_PASSWORD" as "tokenType"

  @PreRequest
  Scenario Outline: Cadastrar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

    Examples: 
      | id | descricao | body                                                            |
      |  1 | com cpf   | {"storeCode":"${storeCode}","storeDocument":"${storeDocument}"} |

  @PreRequest
  Scenario: Consultar usuário criado
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpointUsers}/${userId}"
    Then I get response body
    And I get response status code
    And I compare response value "/userId" with "${userId}"
    And I compare response value "/storeDocument" with "${storeDocument}"
    And I verify if response value "/active" is not empty
    And I verify if response value "/storeId" is not empty
    And Response code must be 200

  @PreRequest
  Scenario: Consultar contacts
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/contactId" is not empty
    And I verify if response value "/0/contact" is not empty
    And I verify if response value "/0/contactType" is not empty
    And I save response value "/0/contactId" as "contactId"
    And Response code must be 200

  @PreRequest
  Scenario: Enviar token para telefone
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"${tokenType}","contactId":"${contactId}"}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Definition
  Scenario: Pegar token recém enviado para tabela do banco de dados
    Given I get token on database by contact_id "${contactId}"

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                | language |
      | sem body       | Invalid request body.  | en-US    |
      | sem body em pt | Requisição inválida.   | pt-BR    |

  @Negativo
  Scenario Outline: Enviar reset de senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be <responseCode>

    Examples: 
      | descricao             | body                            | message                      | code    | language | responseCode |
      | sem login             | {"url":"${url}"}                | Field login is required.     | 400.001 | en-US    |          400 |
      | com login vazio       | {"login":"","url":"${url}"}     | Field login is required.     | 400.001 | en-US    |          400 |
      | com login nulo        | {"login": null,"url":"${url}"}  | Field login is required.     | 400.001 | en-US    |          400 |
      | sem url               | {"login":"${email}"}            | Field url is required.       | 400.001 | en-US    |          400 |
      | com url vazio         | {"login":"${email}","url":""}   | Field url is required.       | 400.001 | en-US    |          400 |
      | com url nulo          | {"login":"${email}","url":null} | Field url is required.       | 400.001 | en-US    |          400 |
      | sem login em pt       | {"url":"${url}"}                | Campo login é obrigatório. | 400.001 | pt-BR    |          400 |
      | com login vazio em pt | {"login":"","url":"${url}"}     | Campo login é obrigatório. | 400.001 | pt-BR    |          400 |
      | com login nulo em pt  | {"login": null,"url":"${url}"}  | Campo login é obrigatório. | 400.001 | pt-BR    |          400 |
      | sem url em pt         | {"login":"${email}"}            | Campo url é obrigatório.   | 400.001 | pt-BR    |          400 |
      | com url vazio em pt   | {"login":"${email}","url":""}   | Campo url é obrigatório.   | 400.001 | pt-BR    |          400 |
      | com url nulo em pt    | {"login":"${email}","url":null} | Campo url é obrigatório.   | 400.001 | pt-BR    |          400 |

  @Negativo
  Scenario Outline: Enviar reset de senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be <responseCode>

    Examples: 
      | descricao                                       | body                                        | message                               | code    | language | responseCode |
      | com login incorreto                             | {"login":"1","url":"${url}"}                | Field login has an invalid format.    | 400.003 | en-US    |          400 |
      | com url com formato inválido sem http://        | {"login":"${email}","url":"teste.com"}      | Field url has an invalid format.      | 400.003 | en-US    |          400 |
      | com url com formato inválido sem .com           | {"login":"${email}","url":"htpps:teste.br"} | Field url has an invalid format.      | 400.003 | en-US    |          400 |
      | com login incorreto em pt                       | {"login":"1","url":"${url}"}                | Campo login tem um formato inválido.  | 400.003 | pt-BR    |          400 |
      | com url com formato inválido sem http://  em pt | {"login":"${email}","url":"teste.com"}      | Campo url tem um formato inválido.    | 400.003 | pt-BR    |          400 |
      | com url com formato inválido sem .com em pt     | {"login":"${email}","url":"htpps:teste.br"} | Campo url tem um formato inválido.    | 400.003 | pt-BR    |          400 |
      | com login incorreto sem arroba                  | {"login":"aline.email.com","url":"${url}"}  | Field login has an invalid format.    | 400.003 | en-US    |          400 |
      | com login incorreto sem .com                    | {"login":"aline@email","url":"${url}"}      | User not found for the given e-mail.    | 422.013 | en-US    |          422 |
      | com login incorreto sem arroba em pt            | {"login":"aline.email.com","url":"${url}"}  | Campo login tem um formato inválido.  | 400.003 | pt-BR    |          400 |
      | com login incorreto sem .com em pt              | {"login":"aline@email","url":"${url}"}      | Usuário não encontrado para o e-mail informado.  | 422.013 | pt-BR    |          422 |
      | com login não cadastrado                        | {"login":"teste_monday@sensedia.com","url":"${url}"}      | User not found for the given e-mail.  | 422.013 | en-US    |          422 |
      | com login não cadastrado em pt                  | {"login":"teste_monday@sensedia.com","url":"${url}"}      | Usuário não encontrado para o e-mail informado.  | 422.013 | pt-BR    |          422 |
      
  @Negativo
  Scenario Outline: Enviar reset de senha <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/1/message" with "<messageOne>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao            | body | language | message                      | code    | messageOne                 | messageTwo                   |
      | com body vazio       | {}   | en-US    | Field login is required.     | 400.001 | Field url is required.     | Field token is required.     |
      | com body vazio em pt | {}   | pt-BR    | Campo login é obrigatório. | 400.001 | Campo url é obrigatório. | Campo token é obrigatório. |

  @Positivo
  Scenario Outline: Resetar senha <descricao>  
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"login":"<email>","url":"${url}"}"
    When I set POST api endpoint as "${endpointUsers}/reset-password"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    
    Examples:
    | descricao            | email                      | 
    | email cadastrado     | aline.dias@sensedia.com    |
    
  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId}" and login "${email}"
