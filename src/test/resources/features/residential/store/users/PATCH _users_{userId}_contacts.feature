@PATCH_users_{userId}_contacts
Feature: PATCH_users_{userId}_contacts.feature
  Operação responsável por atualização de nome em todos os contatos do usuário.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/store/v1"
    And I save "/users" as "endpointUsers"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "RENATO LIMA DA SILVA" as "initialName"
    And I save "f66cb524-7a48-4cc6-9e08-85f42b8c9265" as "userPortalId"
    And I save "c7fba3b8-0b80-40e8-853c-eea667c1d68e" as "userPortalIdSemContatos"
    And I save "RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILV RENATO LIMa" as "nome241"
    And I save "RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILVA RENATO LIMA DA SILV RENATO LIM" as "nome240"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "en-US"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"name": "${initialName}"}"
    When I set PATCH api endpoint as "${endpointUsers}/${userPortalId}/contacts"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request header "client_id" as "${client_id}1"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"name": "${initialName}"}"
    When I set POST api endpoint as "${endpointUsers}/${userPortalId}/contacts"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Accept-Language" as "en-US"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"name": "${initialName}"}"
    When I set PATCH api endpoint as "${endpointUsers}/${userPortalId}/contacts"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com access_token inválido
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}1"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"name": "${initialName}"}"
    When I set PATCH api endpoint as "${endpointUsers}/${userPortalId}/contacts"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "en-US"
    And I set request body as "{"name": "${initialName}"}"
    When I set PATCH api endpoint as "${endpointUsers}/${userPortalId}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    When I set PATCH api endpoint as "${endpointUsers}/${userPortalId}/contacts"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message                 | language |
      | sem body       | "Invalid request body." | "en-US"  |
      | sem body em pt | "Requisição inválida."  | "pt-BR"  |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpointUsers}/${userPortalId}/contacts"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 400

    Examples: 
      | parameter                     | body                    | language | code      | message                                         |
      | sem name                      | "{}"                    | "en-US"  | "400.001" | "Field name is required."                       |
      | com name vazio                | "{"name":""}"           | "en-US"  | "400.001" | "Field name is required."                       |
      | com name nulo                 | "{"name":null}"         | "en-US"  | "400.001" | "Field name is required."                       |
      | sem name pt                   | "{}"                    | "pt-BR"  | "400.001" | "Campo name é obrigatório."                     |
      | com name vazio pt             | "{"name":""}"           | "pt-BR"  | "400.001" | "Campo name é obrigatório."                     |
      | com name nulo pt              | "{"name":null}"         | "pt-BR"  | "400.001" | "Campo name é obrigatório."                     |
      | com name de 241 caracteres    | "{"name":"${nome241}"}" | "en-US"  | "400.002" | "Field name must have up to 240 characters."    |
      | com name de 241 caracteres pt | "{"name":"${nome241}"}" | "pt-BR"  | "400.002" | "Campo name deve ter no máximo 240 caracteres." |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpointUsers}/${userPortalIdSemContatos}/contacts"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 422

    Examples: 
      | parameter                  | body                          | language | code      | message                                    |
      | com usuário sem contato    | "{"name":"Wellington Moura"}" | "en-US"  | "422.019" | "Contacts not found for the given user."   |
      | com usuário sem contato pt | "{"name":"Wellington Moura"}" | "pt-BR"  | "422.019" | "Contatos não encontrados para o usuário." |

  @Positivo
  Scenario Outline: Atualizar nomes de contato do usuário <parameter>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpointUsers}/${userPortalId}/contacts"
    Then I get response body
    And I get response status code
    And Response code must be 204

    Examples: 
      | parameter                    | body                        |
      | para Sensedia                | "{"name":"Sensedia"}"       |
      | para nome com 240 caracteres | "{"name":"${nome240}"}"     |
      | para o nome inicial          | "{"name":"${initialName}"}" |
