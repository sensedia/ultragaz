@POST_users_{userId}_validations
Feature: POST_users_{userId}_validations.feature
  Operação responsável por validar token.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/store/v1"
    And I save "/users" as "endpointUsers"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"

  @PreRequest
  Scenario: Cadastrar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"storeCode":"${storeCode}","storeDocument":"${storeDocument}"}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

  @PreRequest
  Scenario: Consultar contacts
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I save response value "/0/contactId" as "contactId"
    And Response code must be 200
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=EMAIL"
    Then I get response body
    And I get response status code
    And I save response value "/0/contactId" as "contactId2"
    And Response code must be 200

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message               | language |
      | sem body       | Invalid request body. | en-US    |
      | sem body em pt | Requisição inválida.  | pt-BR    |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao             | body           | message                    | code    | language |
      | sem token             | {}             | Field token is required.   | 400.001 | en-US    |
      | com token vazio       | {"token":""}   | Field token is required.   | 400.001 | en-US    |
      | com token nulo        | {"token":null} | Field token is required.   | 400.001 | en-US    |
      | sem token em pt       | {}             | Campo token é obrigatório. | 400.001 | pt-BR    |
      | com token vazio em pt | {"token":""}   | Campo token é obrigatório. | 400.001 | pt-BR    |
      | com token nulo em pt  | {"token":null} | Campo token é obrigatório. | 400.001 | pt-BR    |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                 | body          | message         | code    | language |
      | com token incorreto       | {"token":"1"} | Invalid token.  | 422.002 | en-US    |
      | com token incorreto em pt | {"token":"1"} | Token inválido. | 422.002 | pt-BR    |

  @Positivo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId}"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    And I wait 3 seconds
    Given I get token on database by contact_id "${contactId}"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                           | body                 | tokenType       | message         | code    | language |
      | com tokenType ADD_CONTACT           | {"token":"${TOKEN}"} | ADD_CONTACT     | Invalid token.  | 422.002 | en-US    |
      | com tokenType CHANGE_PASSWORD       | {"token":"${TOKEN}"} | CHANGE_PASSWORD | Invalid token.  | 422.002 | en-US    |
      | com tokenType ADD_CONTACT em pt     | {"token":"${TOKEN}"} | ADD_CONTACT     | Token inválido. | 422.002 | pt-BR    |
      | com tokenType CHANGE_PASSWORD em pt | {"token":"${TOKEN}"} | CHANGE_PASSWORD | Token inválido. | 422.002 | pt-BR    |

  @Negativo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"token":"${token}"}"
    When I set POST api endpoint as "${endpointUsers}/<userId>/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  |                                    1 |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Positivo
  Scenario Outline: Validar token <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"<tokenType>","contactId":"${contactId}","url":"https://www.google.com"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202
    And I wait 3 seconds
    Given I get token on database by contact_id "${contactId}"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

    Examples: 
      | descricao              | body                 | tokenType |
      | com tokenType REGISTER | {"token":"${TOKEN}"} | REGISTER  |
