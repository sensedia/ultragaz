@POST_users_{userId}_contacts
Feature: POST_users_{userId}_contacts.feature
  Operação responsável por criar novo contato para usuário no portal.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    #Given I use domain as "http://localhost:8088"
    And I use api name as "/store/v1"
    And I save "/users" as "endpointUsers"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "310708" as "storeCode"
    And I save "07227472000120" as "storeDocument"
    And I save "emailtest@sensedia.com" as "contact"
    And I save "EMAIL" as "contactType"
    And I save "token" as "token"
    And I save "testaddbyinvoice@sensedia.com" as "login"
    And I save "ADD_CONTACT" as "tokenType"

  @PreRequest
  Scenario: Cadastrar usuário
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"storeCode":"${storeCode}","storeDocument":"${storeDocument}"}"
    When I set POST api endpoint as "${endpointUsers}"
    Then I get response body
    And I get response status code
    And I save response header "Location" as "location"
    And I save final value of header "Location" as "userId"
    And Response code must be 201

  @PreRequest
  Scenario: Consultar contacts
    Given I set request header "client_id" as "${client_id}"
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=PHONE"
    Then I get response body
    And I get response status code
    And I save response value "/0/contactId" as "contactId"
    And Response code must be 200

  @PreRequest
  Scenario: Enviar token para telefone
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"tokenType":"${tokenType}","contactId":"${contactId}"}"
    And I wait 3 seconds
    When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 202

  @Definition
  Scenario: Pegar token recém enviado para tabela do banco de dados
    Given I get token on database by contact_id "${contactId}"

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointUsers}/${userId}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição com <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "<variable>"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

    Examples: 
      | parameter             | variable        | message                                                                       | language |
      | Content-Type inválido | application/pdf | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |
      | Content-Type vazio    |                 | Could not find a required APP in the request, identified by HEADER client_id. | en-US    |

  @Negativo
  Scenario Outline: Enviar requisição <parameter>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/contacts"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "400.000"
    And Response code must be 400

    Examples: 
      | parameter      | message               | language |
      | sem body       | Invalid request body. | en-US    |
      | sem body em pt | Requisição inválida.  | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/contacts<queries>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with "<message>"
    And I compare response value "/0/code" with "<code>"
    And Response code must be 400

    Examples: 
      | descricao                             | queries             | body                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | message                                             | code    | language |
      | sem contact                           | ?_limit=1&_offset=1 | {"contactType":"${contactType}","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | Field contact is required.                          | 400.001 | en-US    |
      | com contact vazio                     | ?_limit=1&_offset=1 | {"contact":"","contactType":"${contactType}","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | Field contact is required.                          | 400.001 | en-US    |
      | com contact nulo                      | ?_limit=1&_offset=1 | {"contact":null,"contactType":"${contactType}","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Field contact is required.                          | 400.001 | en-US    |
      | com contact inválido para EMAIL       | ?_limit=1&_offset=1 | {"contact":"019982722791","contactType":"EMAIL","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | Field contact has an invalid format.                | 400.003 | en-US    |
      | com contact EMAIL inválido            | ?_limit=1&_offset=1 | {"contact":"alinebendersensediacom","contactType":"EMAIL","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Field contact has an invalid format.                | 400.003 | en-US    |
      | com contact longo                     | ?_limit=1&_offset=1 | {"contact":"contactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongo@emaillongo.com","contactType":"${contactType}","token":"${TOKEN}"} | Field contact has an invalid format.                | 400.003 | en-US    |
      | sem contactType                       | ?_limit=1&_offset=1 | {"contact":"${contact}","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Field contactType has an invalid value or is empty. | 400.007 | en-US    |
      | com contactType vazio                 | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Field contactType has an invalid value or is empty. | 400.007 | en-US    |
      | com contactType nulo                  | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":null,"token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Field contactType has an invalid value or is empty. | 400.007 | en-US    |
      | com contactType PHONE                 | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"PHONE","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | Field contactType only accept value EMAIL.          | 400.008 | en-US    |
      | com contactType inválido              | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"EMAILS","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Field contactType has an invalid value or is empty. | 400.007 | en-US    |
      | sem token                             | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"${contactType}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Field token is required.                            | 400.001 | en-US    |
      | com token vazio                       | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"${contactType}","token":""}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Field token is required.                            | 400.001 | en-US    |
      | com token nulo                        | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"${contactType}","token":null}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | Field token is required.                            | 400.001 | en-US    |
      | com body vazio                        | ?_limit=1&_offset=1 | {}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Field token is required.                            | 400.001 | en-US    |
      | sem contact em pt                     | ?_limit=1&_offset=1 | {"contactType":"${contactType}","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | Campo contact é obrigatório.                        | 400.001 | pt-BR    |
      | com contact vazio em pt               | ?_limit=1&_offset=1 | {"contact":"","contactType":"${contactType}","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | Campo contact é obrigatório.                        | 400.001 | pt-BR    |
      | com contact nulo em pt                | ?_limit=1&_offset=1 | {"contact":null,"contactType":"${contactType}","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Campo contact é obrigatório.                        | 400.001 | pt-BR    |
      | com contact inválido para EMAIL em pt | ?_limit=1&_offset=1 | {"contact":"019982722791","contactType":"EMAIL","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | Campo contact tem um formato inválido.              | 400.003 | pt-BR    |
      | com contact EMAIL inválido em pt      | ?_limit=1&_offset=1 | {"contact":"alinebendersensediacom","contactType":"EMAIL","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Campo contact tem um formato inválido.              | 400.003 | pt-BR    |
      | com contact longo em pt               | ?_limit=1&_offset=1 | {"contact":"contactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongocontactlongo@emaillongo.com","contactType":"${contactType}","token":"${TOKEN}"} | Campo contact tem um formato inválido.              | 400.003 | pt-BR    |
      | sem contactType em pt                 | ?_limit=1&_offset=1 | {"contact":"${contact}","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Campo contactType tem um valor inválido ou vazio.   | 400.007 | pt-BR    |
      | com contactType vazio em pt           | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Campo contactType tem um valor inválido ou vazio.   | 400.007 | pt-BR    |
      | com contactType nulo em pt            | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":null,"token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Campo contactType tem um valor inválido ou vazio.   | 400.007 | pt-BR    |
      | com contactType inválido em pt        | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"EMAILS","token":"${TOKEN}"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Campo contactType tem um valor inválido ou vazio.   | 400.007 | pt-BR    |
      | com token vazio em pt                 | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"${contactType}","token":""}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Campo token é obrigatório.                          | 400.001 | pt-BR    |
      | com token nulo em pt                  | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"${contactType}","token":null}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | Campo token é obrigatório.                          | 400.001 | pt-BR    |
      | com body vazio em pt                  | ?_limit=1&_offset=1 | {}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Campo token é obrigatório.                          | 400.001 | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar contacts <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as "<language>"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/${userId}/contacts<queries>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "<message>"
    And I compare response value "/code" with "<code>"
    And Response code must be 422

    Examples: 
      | descricao                | queries             | body                                                                    | message         | code    | language |
      | com token inválido       | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"${contactType}","token":"token"} | Invalid token.  | 422.002 | en-US    |
      | com token inválido em pt | ?_limit=1&_offset=1 | {"contact":"${contact}","contactType":"${contactType}","token":"token"} | Token inválido. | 422.002 | pt-BR    |

  @Negativo
  Scenario Outline: Cadastrar contact <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contact":"aline.dias@sensedia.com","contactType":"EMAIL","token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/<userId>/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao            | userId                               |
      | com userId inválido  | batax                                |
      | com userId incorreto | b101c323-fe08-4bed-9d65-5cfc78003aac |

  @Negativo
  Scenario Outline: Cadastrar contact <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"contact":"${login}","contactType":"EMAIL","token":"${TOKEN}"}"
    When I set POST api endpoint as "${endpointUsers}/<userId>/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao           | userId     |
      | com userId inválido | ${userId}1 |

  @Positivo
  Scenario Outline: Cadastrar contact
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointUsers}/<userId>/contacts"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | descricao | userId    | body                                                                             |
      | por token | ${userId} | {"contact":"testaddbytoken@sensedia.com","contactType":"EMAIL","token":${TOKEN}} |

  @AfterRequest
  Scenario: Remove inserções
    Given I remove info from userId "${userId}" and login "testaddbytoken@sensedia.com"
