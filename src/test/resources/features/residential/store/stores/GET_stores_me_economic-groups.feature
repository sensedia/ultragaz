Feature: GET_stores_me_economic-groups.feature
  Consulta para consultar grupos economicos de uma loja/revenda.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I set request body as "{"grant_type":"password","password":"teste123!","username":"abender@outlook.com.br"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/store/v1"
    And I save "/stores" as "endpointStores"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointStores}/me/economic-groups"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointStores}/me/economic-groups"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Positivo
  Scenario: Consultar customer
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/me/economic-groups"
    Then I get response body
    And I get response status code
    And I verify if response value "/economicGroups/0/name" is not empty
    And I verify if response value "/economicGroups/0/members/0/memberId" is not empty
    And I verify if response value "/economicGroups/0/members/0/headQuarterId" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/storeId" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/storeDocument" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/storeCode" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/addressCode" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/addressId" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/name" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/active" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/isOperating" is not empty
    And I verify if response value "/headQuarter/stores/0/storeId" is not empty
    And I verify if response value "/headQuarter/stores/0/storeDocument" is not empty
    And I verify if response value "/headQuarter/stores/0/storeCode" is not empty
    And I verify if response value "/headQuarter/stores/0/addressCode" is not empty
    And I verify if response value "/headQuarter/stores/0/addressId" is not empty
    And I verify if response value "/headQuarter/stores/0/name" is not empty
    And I verify if response value "/headQuarter/stores/0/active" is not empty
    And I verify if response value "/headQuarter/stores/0/isOperating" is not empty
    And Response code must be 200

  @Negativo
  Scenario Outline: Consultar customer <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<access_token>"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointStores}/me/economic-groups"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

    Examples: 
      | descricao                  | access_token     | code      | message                                       |
      | por access_token incorreto | ${access_token}1 | "400.005" | "Field customer-code must have 8 characters." |
