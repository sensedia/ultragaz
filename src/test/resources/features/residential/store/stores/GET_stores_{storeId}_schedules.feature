Feature: GET_stores_{storeId}_schedules.feature
  Obter a agenda de uma revenda.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/stores" as "endpointStores"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "0013000001HvV2bAAF" as "storeId"

  @Positivo
  Scenario Outline: Consultar agenda <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/${storeId}/schedules"
    Then I get response body
    And I get response status code
    And I verify if response value "/channel" is not empty
    And I verify if response value "/0/schedule/0/scheduleType" is not empty
    And I verify if response value "/0/schedule/0/startTime" is not empty
    And I verify if response value "/0/schedule/0/endTime" is not empty
    And Response code must be 200
    And I wait 2 seconds

    Examples: 
      | descricao | channel           | scheduleType  | startTime  | endTime    |
      | por name  | "PedidoLicitacao" | "ATENDIMENTO" | "08:00:00" | "22:00:00" |

  @Negativo
  Scenario Outline: Consultar stores schedules <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointStores}/<storeId>/schedules"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao             | storeId | language |
      | por storeId incorreto |       1 | en-US    |

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401
