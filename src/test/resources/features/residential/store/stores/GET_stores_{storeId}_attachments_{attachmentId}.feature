Feature: GET_stores_{storeId}_attachments_{attachmentId}.feature
  Operação responsável por obter contrato na base64.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/stores" as "endpointStores"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "0013000001P448vAAB" as "storeId"
    And I save "00P0M00000ki3tMUAQ" as "attachmentId"
    And I save "00P0M00000hbdZIUAY" as "attachmentId2"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/${storeId}/attachments/${attachmentId}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpointStores}/${storeId}/attachments/${attachmentId}""
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <descricao>
    Given I set request header "client_id" as <client_id>
    Given I set request header "access_token" as <token>
    When I set GET api endpoint as "${endpointStores}/${storeId}/attachments/${attachmentId}""
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | descricao                 | client_id       | token              | message                                                                                    |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar contrato na base64 <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpointStores}/<storeId>/attachments/<attachmentId>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao                               | storeId    | attachmentId     | message                                       | code      | language |
      | storeId e attachmentId incompatíveis    | ${storeId} | ${attachmentId2} | "AttachmentId not found for the given store." | "422.023" | "en-US"  |
      | storeId e attachmentId incompatíveis pt | ${storeId} | ${attachmentId2} | "AttachmentId não encontrado para a revenda." | "422.023" | "pt-BR"  |
      | por storeId incorreto                   |          1 | ${attachmentId}  | "AttachmentId not found for the given store." | "422.023" | "en-US"  |
      | por storeId incorreto pt                |          1 | ${attachmentId}  | "AttachmentId não encontrado para a revenda." | "422.023" | "pt-BR"  |

  @Positivo
  Scenario: Consultar contrato na base64
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/${storeId}/attachments/${attachmentId}"
    Then I get response body
    And I get response status code
    And I verify if response value "/attachment" is not empty
    And Response code must be 200
