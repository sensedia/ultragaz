Feature: GET_stores.feature
  Obter revendas do segmento domiciliar da Ultragaz.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/stores" as "endpointStores"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "MANUELA DE MIRANDA SOUZA EIRELI EPP" as "name"
    And I save "SALVADOR" as "city"
    And I save "BA" as "state"
    And I save "19329336000120" as "document"
    And I save "2008316-2061372" as "codeStore"
    And I save "-21.7012938" as "latitude"
    And I save "-45.25653790000001" as "longitude"
    And I save "-23.9568137" as "latitude2"
    And I save "-46.3290069" as "longitude2"
    And I save "1" as "distanceRadius"
    And I save "0110060" as "productId"
    And I save "Disk" as "channel"

  @Positivo
  Scenario Outline: Consultar stores <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/nameStore" with <nameStore>
    And I compare response value "/0/storeId" with <storeId>
    And I compare response value "/0/codeStore" with <codeStore>
    And I compare response value "/0/thumbnail" with <thumbnail>
    And I compare response value "/0/statusStore" with <statusStore>
    And I compare response value "/0/availability" with <availability>
    And I compare response value "/0/contacts/0/phone" with <phone>
    And I compare response value "/0/contacts/0/lastName" with <lastName>
    And I compare response value "/0/contacts/0/name" with <name>
    And I compare response value "/0/contacts/0/email" with <email>
    And I compare response value "/0/contacts/0/mainContact" with <mainContact>
    And I compare response value "/0/channels/0/channel" with <channel>
    And I verify if response value "/0/channels/0/paymentMethods" is empty
    And I compare response value "/0/channels/0/schedule/0/scheduleType" with <scheduleType>
    And I compare response value "/0/channels/0/schedule/0/startTime" with <startTime>
    And I compare response value "/0/channels/0/schedule/0/endTime" with <endTime>
    And I verify if response value "/0/channels/0/schedule/0/weekdays/0" is not empty
    And I verify if response value "/0/images" is empty
    And I verify if response value "/0/transferMethods" is empty
    And I compare response value "/0/address/stateAbbreviation" with <stateAbbreviation>
    And I compare response value "/0/address/neighborhood" with <neighborhood>
    And I compare response value "/0/address/country" with <country>
    And I compare response value "/0/address/city" with <city>
    And I compare response value "/0/address/address" with <address>
    And I compare response value "/0/address/addressNumber" with <addressNumber>
    And I compare response value "/0/address/addressAddition" with <addressAddition>
    And I compare response value "/0/socialNetworks/whatsapp" with <whatsapp>
    And Response code must be 200
    And I wait 2 seconds

    Examples: 
      | descricao              | queryParameter                            | nameStore                             | storeId              | codeStore         | thumbnail                                        | statusStore | availability | phone        | name      | lastName  | email     | mainContact | channel | paymentMethod   | paymentMethodCode | price   | productId | nameProduct | scheduleType  | startTime  | endTime    | weekdays  | stateAbbreviation | neighborhood | country | city       | address         | addressNumber | addressAddition | whatsapp |
      | por name, city e state | ?name=${name}&city=${city}&state=${state} | "MANUELA DE MIRANDA SOUZA EIRELI EPP" | "0013000001HvV2bAAF" | "2008316-2061372" | "https://cdn.buttercms.com/wToOscZR8GCGUqjyaqw3" | "OFFLINE"   | "true"       | "3460641671" | "MANUELA" | "MANUELA" | "MANUELA" | "false"     | "0800"  | "VALE ULTRAGAZ" | "37"              | "74.99" | "0110035" | "GLP/P13"   | "ATENDIMENTO" | "07:00:00" | "18:00:00" | "Domingo" | "BA"              | "PERNAMBUES" | "BR"    | "SALVADOR" | "AVENIDA HILDA" | "80"          | "S C"           | "false"  |
      | por document           | ?document=${document}                     | "MANUELA DE MIRANDA SOUZA EIRELI EPP" | "0013000001HvV2bAAF" | "2008316-2061372" | "https://cdn.buttercms.com/wToOscZR8GCGUqjyaqw3" | "OFFLINE"   | "true"       | "3460641671" | "MANUELA" | "MANUELA" | "MANUELA" | "false"     | "0800"  | "VALE ULTRAGAZ" | "37"              | "74.99" | "0110035" | "GLP/P13"   | "ATENDIMENTO" | "07:00:00" | "18:00:00" | "Domingo" | "BA"              | "PERNAMBUES" | "BR"    | "SALVADOR" | "AVENIDA HILDA" | "80"          | "S C"           | "false"  |
      | por code-store         | ?code-store=${codeStore}                  | "MANUELA DE MIRANDA SOUZA EIRELI EPP" | "0013000001HvV2bAAF" | "2008316-2061372" | "https://cdn.buttercms.com/wToOscZR8GCGUqjyaqw3" | "OFFLINE"   | "true"       | "3460641671" | "MANUELA" | "MANUELA" | "MANUELA" | "false"     | "0800"  | "VALE ULTRAGAZ" | "37"              | "74.99" | "0110035" | "GLP/P13"   | "ATENDIMENTO" | "07:00:00" | "18:00:00" | "Domingo" | "BA"              | "PERNAMBUES" | "BR"    | "SALVADOR" | "AVENIDA HILDA" | "80"          | "S C"           | "false"  |
  		#| por name vazio         | ?city=${city}&state=${state}&name=        | "MANUELA DE MIRANDA SOUZA EIRELI EPP" | "0013000001HvV2bAAF" | "2008316-2061372" | "https://cdn.buttercms.com/wToOscZR8GCGUqjyaqw3" | "OFFLINE"   | "true"       | "3460641671" | "MANUELA" | "MANUELA" | "MANUELA" | "false"     | "0800"  | "VALE ULTRAGAZ" | "37"              | "74.99" | "0110035" | "GLP/P13"   | "ATENDIMENTO" | "07:00:00" | "18:00:00" | "Domingo" | "BA"              | "PERNAMBUES" | "BR"    | "SALVADOR" | "AVENIDA HILDA" | "80"          | "S C"           | "true"   |
  
  @Positivo
  Scenario Outline: Consultar stores <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/nameStore" is not empty
    And I verify if response value "/0/storeId" is not empty
    And I verify if response value "/0/codeStore" is not empty
    And I verify if response value "/0/thumbnail" is not empty
    And I verify if response value "/0/statusStore" is not empty
    And I verify if response value "/0/availability" is not empty
    And I verify if response value "/0/contacts/0/phone" is not empty
    And I verify if response value "/0/contacts/0/lastName" is not empty
    And I verify if response value "/0/contacts/0/name" is not empty
    And I verify if response value "/0/contacts/0/email" is not empty
    And I verify if response value "/0/contacts/0/mainContact" is not empty
    And I verify if response value "/0/channels/0/channel" is not empty
    And I verify if response value "/0/channels/0/paymentMethods" is empty
    And I verify if response value "/0/channels/0/schedule/0/scheduleType" is not empty
    And I verify if response value "/0/channels/0/schedule/0/startTime" is not empty
    And I verify if response value "/0/channels/0/schedule/0/endTime" is not empty
    And I verify if response value "/0/channels/0/schedule/0/weekdays/0" is not empty
    And I verify if response value "/0/images" is empty
    And I verify if response value "/0/transferMethods" is empty
    And I verify if response value "/0/address/stateAbbreviation" is not empty
    And I verify if response value "/0/address/country" is not empty
    And I verify if response value "/0/address/city" is not empty
    And I verify if response value "/0/address/address" is not empty
    And I verify if response value "/0/address/addressNumber" is not empty
    And I verify if response value "/0/address/addressAddition" is not empty
    And I verify if response value "/0/socialNetworks/whatsapp" is not empty
    And Response code must be 200

    Examples: 
      | descricao                        | queryParameter                                                                   |
      | por latitude e longitude         | ?latitude=${latitude2}&longitude=${longitude2}                                   |
      | por latitude, longitude e radius | ?latitude=${latitude2}&longitude=${longitude2}&distance-radius=${distanceRadius} |
      | por radius vazio                 | ?longitude=${longitude2}&latitude=${latitude2}&distance-radius=                  |

  @Positivo
  Scenario Outline: Consultar stores <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/nameStore" is not empty
    And I verify if response value "/0/storeId" is not empty
    And I verify if response value "/0/codeStore" is not empty
    And I verify if response value "/0/thumbnail" is not empty
    And I verify if response value "/0/statusStore" is not empty
    And I verify if response value "/0/availability" is not empty
    And I verify if response value "/0/contacts/0/phone" is not empty
    And I verify if response value "/0/contacts/0/lastName" is not empty
    And I verify if response value "/0/contacts/0/name" is not empty
    And I verify if response value "/0/contacts/0/email" is not empty
    And I verify if response value "/0/contacts/0/mainContact" is not empty
    And I verify if response value "/0/channels/0/channel" is not empty
    And I verify if response value "/0/channels/0/paymentMethods" is empty
    And I verify if response value "/0/channels/0/schedule/0/scheduleType" is not empty
    And I verify if response value "/0/channels/0/schedule/0/startTime" is not empty
    And I verify if response value "/0/channels/0/schedule/0/endTime" is not empty
    And I verify if response value "/0/channels/0/schedule/0/weekdays/0" is not empty
    And I verify if response value "/0/images" is empty
    And I verify if response value "/0/transferMethods" is empty
    And I verify if response value "/0/address/stateAbbreviation" is not empty
    And I verify if response value "/0/address/country" is not empty
    And I verify if response value "/0/address/city" is not empty
    And I verify if response value "/0/address/address" is not empty
    And I verify if response value "/0/address/addressNumber" is not empty
    And I verify if response value "/0/address/addressAddition" is not empty
    And I verify if response value "/0/socialNetworks/whatsapp" is not empty
    And Response code must be 200
    And I wait 2 seconds

    Examples: 
      | descricao      | queryParameter                                                         |
      | por channel    | ?longitude=${longitude2}&latitude=${latitude2}&channel=${channel}      |
      #| por product-id | ?longitude=${longitude2}&latitude=${latitude2}&product-id=${productId} |

  @Positivo
  Scenario Outline: Consultar stores <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200
    And I wait 2 seconds

    Examples: 
      | descricao                         | queryParameter                                            |
      | por name inexistente              | ?name=${name}s&city=${city}&state=${state}                |
      | por state inexistente             | ?name=${name}&city=${city}&state=${state}s                |
      | por city inexistente              | ?name=${name}&city=${city}s&state=${state}                |
      | por document inexistente          | ?document=45401035802                                     |
      | por longitude, latitude oceanicas | ?latitude=-22.8163929&longitude=-22.8163920               |
      | por channel incorreto             | ?latitude=${latitude}&longitude=${longitude}&channel=s    |
      | por product-id incorreto          | ?latitude=${latitude}&longitude=${longitude}&product-id=s |

  @Negativo
  Scenario Outline: Consultar stores <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                  | queryParameter                                                           | code      | message                                                 |
      | por document vazio         | ?document=                                                               | "400.006" | "One filter must be informed."                          |
      | por code-store vazio       | ?code-store=                                                             | "400.006" | "One filter must be informed."                          |
      | por radius inválido        | ?longitude=${longitude}&latitude=${latitude}&distance-radius=-1          | "400.003" | "Field distance-radius has an invalid format."          |
      | por radius muito grande    | ?longitude=${longitude}&latitude=${latitude}&distance-radius=10000000000 | "400.005" | "distance-radius must be less than or equal to 5."      |
      | por radius >5              | ?longitude=${longitude}&latitude=${latitude}&distance-radius=10          | "400.005" | "distance-radius must be less than or equal to 5."      |
      | por latitude               | ?latitude=${latitude}                                                    | "400.001" | "Field longitude is required."                          |
      | por latitude vazia         | ?longitude=${longitude}&latitude=                                        | "400.001" | "Field latitude is required."                           |
      | por longitude              | ?longitude=${longitude}                                                  | "400.001" | "Field latitude is required."                           |
      | por longitude vazia        | ?latitude=${latitude}&longitude=                                         | "400.001" | "Field longitude is required."                          |
      | por name                   | ?name=${name}                                                            | "400.004" | "Field city and state are required when name informed." |
      | por longitude incorreta    | ?latitude=${latitude}&longitude=Rua                                      | "400.003" | "Field longitude has an invalid format."                |
      | por latitude incorreta     | ?longitude=${longitude}&latitude=Rua                                     | "400.003" | "Field latitude has an invalid format."                 |
      | por channel                | ?channel=${channel}                                                      | "400.006" | "One filter must be informed."                          |
      | por product-id             | ?product-id=${productId}                                                 | "400.006" | "One filter must be informed."                          |
      | por code-store inexistente | ?code-store=45401035802                                                  | "400.003" | "Field code-store has an invalid format."               |

  @Negativo
  Scenario Outline: Consultar stores <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I compare response value "/1/code" with <code2>
    And I compare response value "/1/message" with <message2>
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                    | queryParameter                     | code      | message                                                 | code2     | message2                   |
      | por name e city              | ?name=${name}&city=${city}         | "400.004" | "Field city and state are required when name informed." | "400.001" | "Field state is required." |
      | por name e state             | ?name=${name}&state=${state}       | "400.004" | "Field city and state are required when name informed." | "400.001" | "Field city is required."  |
      | por name, city e state vazio | ?name=${name}&city=${city}&state=  | "400.004" | "Field city and state are required when name informed." | "400.001" | "Field state is required." |
      | por name, city vazio e state | ?name=${name}&state=${state}&city= | "400.004" | "Field city and state are required when name informed." | "400.001" | "Field city is required."  |

  @Negativo
  Scenario Outline: Consultar stores <descricao> em Português
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                  | queryParameter                                                           | code      | message                                                        |
      | por document vazio         | ?document=                                                               | "400.006" | "Um filtro deve ser informado pelo menos."                     |
      | por code-store vazio       | ?code-store=                                                             | "400.006" | "Um filtro deve ser informado pelo menos."                     |
      | por radius inválido        | ?longitude=${longitude}&latitude=${latitude}&distance-radius=-1          | "400.003" | "Campo distance-radius tem um formato inválido."               |
      | por radius muito grande    | ?longitude=${longitude}&latitude=${latitude}&distance-radius=10000000000 | "400.005" | "distance-radius deve ser menor ou igual a 5."                 |
      | por radius >5              | ?longitude=${longitude}&latitude=${latitude}&distance-radius=10          | "400.005" | "distance-radius deve ser menor ou igual a 5."                 |
      | por latitude               | ?latitude=${latitude}                                                    | "400.001" | "Campo longitude é obrigatório."                               |
      | por latitude vazia         | ?longitude=${longitude}&latitude=                                        | "400.001" | "Campo latitude é obrigatório."                                |
      | por longitude              | ?longitude=${longitude}                                                  | "400.001" | "Campo latitude é obrigatório."                                |
      | por longitude vazia        | ?latitude=${latitude}&longitude=                                         | "400.001" | "Campo longitude é obrigatório."                               |
      | por name                   | ?name=${name}                                                            | "400.004" | "Campo city e state são obrigatórios quando name é informado." |
      | por longitude incorreta    | ?latitude=${latitude}&longitude=Rua                                      | "400.003" | "Campo longitude tem um formato inválido."                     |
      | por latitude incorreta     | ?longitude=${longitude}&latitude=Rua                                     | "400.003" | "Campo latitude tem um formato inválido."                      |
      | por channel                | ?channel=${channel}                                                      | "400.006" | "Um filtro deve ser informado pelo menos."                     |
      | por product-id             | ?product-id=${productId}                                                 | "400.006" | "Um filtro deve ser informado pelo menos."                     |
      | por code-store inexistente | ?code-store=45401035802                                                  | "400.003" | "Campo code-store tem um formato inválido."                    |

  @Negativo
  Scenario Outline: Consultar stores <descricao> em Português
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And I compare response value "/1/code" with <code2>
    And I compare response value "/1/message" with <message2>
    And I verify if response value "/2/code" is empty
    And I verify if response value "/2/message" is empty
    And Response code must be 400
    And I wait 2 seconds

    Examples: 
      | descricao                    | queryParameter                     | code      | message                                                        | code2     | message2                     |
      | por name e city              | ?name=${name}&city=${city}         | "400.004" | "Campo city e state são obrigatórios quando name é informado." | "400.001" | "Campo state é obrigatório." |
      | por name e state             | ?name=${name}&state=${state}       | "400.004" | "Campo city e state são obrigatórios quando name é informado." | "400.001" | "Campo city é obrigatório."  |
      | por name, city e state vazio | ?name=${name}&city=${city}&state=  | "400.004" | "Campo city e state são obrigatórios quando name é informado." | "400.001" | "Campo state é obrigatório." |
      | por name, city vazio e state | ?name=${name}&state=${state}&city= | "400.004" | "Campo city e state são obrigatórios quando name é informado." | "400.001" | "Campo city é obrigatório."  |

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointStores}?name=${name}&city=${city}&state=${state}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointStores}?name=${name}&city=${city}&state=${state}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401
