Feature: PUT_stores_{storeId}_contacts_{contactIdSf}.feature
  Operação responsável por atualizar contato para a Revenda / Loja no Salesforce.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/stores" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0013000001HJAs6AAH" as "storeId"
    And I save "0030M00002Hb5FtQAJ" as "contactIdSf"
    And I save "wellington.moura@sensedia.com" as "email"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"
    When I set PUT api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"
    When I set PUT api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"
    When I set PUT api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario: Enviar requisição sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"
    When I set PUT api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Atualizar contato para a Revenda / Loja no Salesforce <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as <body>
    When I set PUT api endpoint as "${endpoint}/${storeId}/contacts/<contactIdSf>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 400

    Examples: 
      | description                              | body                                                                                                                                                                                    | message                                      | code      | language |
      | sem campo secondaryEmal                  | "{"cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"                             | "Field secondaryEmail is required."          | "400.001" | "en-US"  |
      | com campo secondaryEmal vazio            | "{"secondaryEmail":"","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"         | "Field secondaryEmail is required."          | "400.001" | "en-US"  |
      | com campo secondaryEmal nulo             | "{"secondaryEmail":null,"cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"       | "Field secondaryEmail is required."          | "400.001" | "en-US"  |
      | sem campo cellPhoneAreaCode              | "{"secondaryEmail":"${email}","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"                          | "Field cellPhoneAreaCode is required."       | "400.001" | "en-US"  |
      | com campo cellPhoneAreaCode vazio        | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"   | "Field cellPhoneAreaCode is required."       | "400.001" | "en-US"  |
      | sem campo cellPhoneAreaCode nulo         | "{"secondaryEmail":"${email}","cellPhoneAreaCode":null,"cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}" | "Field cellPhoneAreaCode is required."       | "400.001" | "en-US"  |
      | sem campo cellPhoneNumber                | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"                               | "Field cellPhoneNumber is required."         | "400.001" | "en-US"  |
      | com campo cellPhoneNumber vazio          | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"          | "Field cellPhoneNumber is required."         | "400.001" | "en-US"  |
      | com campo cellPhoneNumber nulo           | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":null,"landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"        | "Field cellPhoneNumber is required."         | "400.001" | "en-US"  |
      | sem campo landlinePhoneAreaCode          | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"                              | "Field landlinePhoneAreaCode is required."   | "400.001" | "en-US"  |
      | com campo landlinePhoneAreaCode vazio    | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"   | "Field landlinePhoneAreaCode is required."   | "400.001" | "en-US"  |
      | com campo landlinePhoneAreaCode nulo     | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":null,"landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}" | "Field landlinePhoneAreaCode is required."   | "400.001" | "en-US"  |
      | sem campo landlinePhone                  | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","position":"QA","birthDate":"1989-05-15"}"                            | "Field landlinePhone is required."           | "400.001" | "en-US"  |
      | com campo landlinePhone vazio            | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"","position":"QA","birthDate":"1989-05-15"}"         | "Field landlinePhone is required."           | "400.001" | "en-US"  |
      | com campo landlinePhone nulo             | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":null,"position":"QA","birthDate":"1989-05-15"}"       | "Field landlinePhone is required."           | "400.001" | "en-US"  |
      | sem campo position                       | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","birthDate":"1989-05-15"}"                 | "Field position is required."                | "400.001" | "en-US"  |
      | com campo position  vazio                | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"","birthDate":"1989-05-15"}"   | "Field position is required."                | "400.001" | "en-US"  |
      | com campo position nulo                  | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":null,"birthDate":"1989-05-15"}" | "Field position is required."                | "400.001" | "en-US"  |
      | sem campo birthDate                      | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA"}"                          | "Field birthDate is required."               | "400.001" | "en-US"  |
      | com campo birthDate vazio                | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":""}"           | "Field birthDate is required."               | "400.001" | "en-US"  |
      | com campo birthDate nulo                 | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":null}"         | "Field birthDate is required."               | "400.001" | "en-US"  |
      | sem campo secondaryEmal pt               | "{"cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"                             | "Campo secondaryEmail é obrigatório."        | "400.001" | "pt-BR"  |
      | com campo secondaryEmal vazio pt         | "{"secondaryEmail":"","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"         | "Campo secondaryEmail é obrigatório."        | "400.001" | "pt-BR"  |
      | com campo secondaryEmal nulo pt          | "{"secondaryEmail":null,"cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"       | "Campo secondaryEmail é obrigatório."        | "400.001" | "pt-BR"  |
      | sem campo cellPhoneAreaCode pt           | "{"secondaryEmail":"${email}","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"                          | "Campo cellPhoneAreaCode é obrigatório."     | "400.001" | "pt-BR"  |
      | com campo cellPhoneAreaCode vazio pt     | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"   | "Campo cellPhoneAreaCode é obrigatório."     | "400.001" | "pt-BR"  |
      | sem campo cellPhoneAreaCode nulo pt      | "{"secondaryEmail":"${email}","cellPhoneAreaCode":null,"cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}" | "Campo cellPhoneAreaCode é obrigatório."     | "400.001" | "pt-BR"  |
      | sem campo cellPhoneNumber pt             | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"                               | "Campo cellPhoneNumber é obrigatório."       | "400.001" | "pt-BR"  |
      | com campo cellPhoneNumber vazio pt       | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"          | "Campo cellPhoneNumber é obrigatório."       | "400.001" | "pt-BR"  |
      | com campo cellPhoneNumber nulo pt        | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":null,"landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"        | "Campo cellPhoneNumber é obrigatório."       | "400.001" | "pt-BR"  |
      | sem campo landlinePhoneAreaCode pt       | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"                              | "Campo landlinePhoneAreaCode é obrigatório." | "400.001" | "pt-BR"  |
      | com campo landlinePhoneAreaCode vazio pt | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"   | "Campo landlinePhoneAreaCode é obrigatório." | "400.001" | "pt-BR"  |
      | com campo landlinePhoneAreaCode nulo pt  | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":null,"landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}" | "Campo landlinePhoneAreaCode é obrigatório." | "400.001" | "pt-BR"  |
      | sem campo landlinePhone pt               | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","position":"QA","birthDate":"1989-05-15"}"                            | "Campo landlinePhone é obrigatório."         | "400.001" | "pt-BR"  |
      | com campo landlinePhone vazio pt         | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"","position":"QA","birthDate":"1989-05-15"}"         | "Campo landlinePhone é obrigatório."         | "400.001" | "pt-BR"  |
      | com campo landlinePhone nulo pt          | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":null,"position":"QA","birthDate":"1989-05-15"}"       | "Campo landlinePhone é obrigatório."         | "400.001" | "pt-BR"  |
      | sem campo position pt                    | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","birthDate":"1989-05-15"}"                 | "Campo position é obrigatório."              | "400.001" | "pt-BR"  |
      | com campo position  vazio pt             | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"","birthDate":"1989-05-15"}"   | "Campo position é obrigatório."              | "400.001" | "pt-BR"  |
      | com campo position nulo pt               | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":null,"birthDate":"1989-05-15"}" | "Campo position é obrigatório."              | "400.001" | "pt-BR"  |
      | sem campo birthDate pt                   | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA"}"                          | "Campo birthDate é obrigatório."             | "400.001" | "pt-BR"  |
      | com campo birthDate vazio pt             | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":""}"           | "Campo birthDate é obrigatório."             | "400.001" | "pt-BR"  |
      | com campo birthDate nulo pt              | "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":null}"         | "Campo birthDate é obrigatório."             | "400.001" | "pt-BR"  |

  @Negativo
  Scenario Outline: Atualizar contato para a Revenda / Loja no Salesforce <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "Accept-Language" as <language>
    And I set request body as "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"
    When I set PUT api endpoint as "${endpoint}/${storeId}/contacts/<contactIdSf>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | description                            | contactIdSf           | message                                                | code      | language |
      | com contactIdSf inválido string        | banana                | "Error while updating contact info."                   | "422.028" | "en-US"  |
      | com contactIdSf inválido inteiro       |              24242334 | "Error while updating contact info."                   | "422.028" | "en-US"  |
      | com contactIdSf inválido SQL injection | ${contactIdSf} OR 1=1 | "Error while updating contact info."                   | "422.028" | "en-US"  |
      | com contactIdSf inválido string pt     | banana                | "Erro ao realizar operação de atualização do contato." | "422.028" | "pt-BR"  |
      | com contactIdSf inválido inteiro pt    |              24242334 | "Erro ao realizar operação de atualização do contato." | "422.028" | "pt-BR"  |
      | com contactIdSf inválido SQL injection | ${contactIdSf} OR 1=1 | "Erro ao realizar operação de atualização do contato." | "422.028" | "pt-BR"  |

  @Positivo
  Scenario: Atualizar contato para a Revenda / Loja no Salesforce
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"secondaryEmail":"${email}","cellPhoneAreaCode":"19","cellPhoneNumber":"982911139","landlinePhoneAreaCode":"19","landlinePhone":"32561245","position":"QA","birthDate":"1989-05-15"}"
    When I set PUT api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And Response code must be 204
