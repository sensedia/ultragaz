Feature: GET_stores_{storeId}_business-characteristics.feature
  Obter características de negócio de logística domiciliar da loja/revenda.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  #@Definition
  #Scenario: Definir configurações de ambiente
  #Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
  #Given I use domain as "http://localhost:8001"
  #And I use api name as "/store/v1"
  #And I save "/users/authentications" as "endpointAuthentication"
  #And I save "/users" as "endpointUsers"
  #And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
  #And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
  #And I save "teste123!" as "password"
  #And I save "3196568" as "storeCode"
  #And I save "22036971000124" as "storeDocument"
  #And System generate random number
  #And I save "testauth${random}@sensedia.com" as "login"
  #
  #@PreRequest
  #Scenario: Cadastrar usuário
  #Given I set request header "client_id" as "${client_id}"
  #And I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #And I set request body as "{"storeCode":"${storeCode}","storeDocument":"${storeDocument}"}"
  #When I set POST api endpoint as "${endpointUsers}"
  #Then I get response body
  #And I get response status code
  #And I save response header "Location" as "location"
  #And I save final value of header "Location" as "userId"
  #And Response code must be 201
  #
  #@PreRequest
  #Scenario: Consultar contacts
  #Given I set request header "client_id" as "${client_id}"
  #Given I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #When I set GET api endpoint as "${endpointUsers}/${userId}/contacts?contactType=PHONE"
  #Then I get response body
  #And I get response status code
  #And I save response value "/0/contactId" as "contactId"
  #And Response code must be 200
  #
  #@PreRequest
  #Scenario: Enviar token para telefone
  #Given I set request header "client_id" as "${client_id}"
  #And I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #And I set request body as "{"tokenType":"ADD_CONTACT","contactId":"${contactId}"}"
  #And I wait 3 seconds
  #When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
  #Then I get response body
  #And I get response status code
  #And I verify if response body is empty
  #And Response code must be 202
  #
  #@Definition
  #Scenario: Pegar token recém enviado para tabela do banco de dados
  #Given I get token on database by contact_id "${contactId}"
  #
  #@PreRequest
  #Scenario Outline: Cadastrar contact
  #Given I set request header "client_id" as "${client_id}"
  #And I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #And I set request body as "<body>"
  #When I set POST api endpoint as "${endpointUsers}/<userId>/contacts"
  #Then I get response body
  #And I get response status code
  #And I verify if response body is empty
  #And I save response header "Location" as "contacts"
  #And I save final value of header "Location" as "contactId"
  #And Response code must be 201
  #
  #Examples:
  #| descricao    | userId    | body                                                            |
  #| por invoices | ${userId} | {"contact":"${login}","contactType":"EMAIL","token":"${TOKEN}"} |
  #
  #@PreRequest
  #Scenario Outline: Cadastrar senha
  #Given I set request header "client_id" as "${client_id}"
  #And I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #And I set request body as "<body>"
  #When I set POST api endpoint as "${endpointUsers}/${userId}/passwords"
  #Then I get response body
  #And I get response status code
  #And I verify if response body is empty
  #And Response code must be 201
  #
  #Examples:
  #| body                                                                                       |
  #| {"contactId":"${contactId}","password":"${password}","passwordConfirmation":"${password}"} |
  #
  #@PreRequest
  #Scenario: Enviar token para telefone
  #Given I set request header "client_id" as "${client_id}"
  #And I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #And I set request body as "{"tokenType":"REGISTER","contactId":"${contactId}","url":"https://www.google.com"}"
  #And I wait 3 seconds
  #When I set POST api endpoint as "${endpointUsers}/${userId}/tokens"
  #Then I get response body
  #And I get response status code
  #And I verify if response body is empty
  #And Response code must be 202
  #
  #@Definition
  #Scenario: Pegar token recém enviado para tabela do banco de dados
  #Given I get token on database by contact_id "${contactId}"
  #
  #@PreRequest
  #Scenario: Validar token
  #Given I get token on database by contact_id "${contactId}"
  #And I set request header "client_id" as "${client_id}"
  #And I set request header "access_token" as "${access_token}"
  #And I set request header "Content-Type" as "application/json"
  #And I set request header "Accept-Language" as "<language>"
  #And I set request body as "{"token":"${TOKEN}"}"
  #When I set POST api endpoint as "${endpointUsers}/${userId}/validations"
  #Then I get response body
  #And I get response status code
  #And I verify if response body is empty
  #And Response code must be 202
  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/stores" as "endpointStores"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "0013000001HvV2bAAF" as "storeId"

  @Positivo
  Scenario: Consultar business-characteristics
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/${storeId}/business-characteristics"
    Then I get response body
    And I get response status code
    And I compare response value "/businessCharacteristicId" with "a2j0M000000tvl9QAA"
    #And I verify if response value "/serviceType/0/" is not empty
    And I verify if response value "/standardServiceType" is not empty
    And I verify if response value "/standardSupplyBranchId" is not empty
    And I verify if response value "/standardShippingMode" is not empty
    And I verify if response value "/supplyBranches/0/id" is not empty
    And I verify if response value "/supplyBranches/0/name" is not empty
    And I verify if response value "/supplyBranches/0/products/0/code" is not empty
    And I verify if response value "/supplyBranches/0/products/0/name" is not empty
    And Response code must be 200

  @Negativo
  Scenario Outline: Consultar stores business-characteristics <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointStores}/<storeId>/business-characteristics"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao             | storeId     | language |
      | por storeId inválido  |           1 | en-US    |
      | por storeId incorreto | ${storeId}1 | en-US    |

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/${storeId}/business-characteristics"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpointStores}/${storeId}/business-characteristics"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpointStores}/${storeId}/business-characteristics"
    Then I get response body
    And I get response status code
    And Response body must be "<message>"
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                  |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | Could not find a required Access Token in the request, identified by HEADER access_token |
      | com client_id vazio       | ""              | "${access_token}"  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com access_token vazio    | "${client_id}"  | ""                 | Could not find a required Access Token in the request, identified by HEADER access_token |
