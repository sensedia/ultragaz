Feature: GET_stores_me.feature
  Consulta para obter dados de loja.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I set request body as "{"grant_type":"password","password":"teste123!","username":"aline.dias@sensedia.com"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I get response status code
    And I save response value "/access_token" as "access_token"
    And Response code must be 201

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/store/v1"
    And I save "/stores" as "endpointStores"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointStores}/me"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointStores}/me"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Positivo
  Scenario: Consultar customer
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/me"
    Then I get response body
    And I get response status code
    And I verify if response value "/storeId" is not empty
    And I verify if response value "/nameStore" is not empty
    And I verify if response value "/codeStore" is not empty
    And I verify if response value "/addressId" is not empty
    And I verify if response value "/thumbnail" is not empty
    And I verify if response value "/statusStore" is not empty
    And I verify if response value "/availability" is not empty
    And I verify if response value "/contacts/0/phone" is not empty
    And I verify if response value "/contacts/0/name" is not empty
    And I verify if response value "/contacts/0/lastName" is not empty
    And I verify if response value "/contacts/0/email" is not empty
    And I verify if response value "/contacts/0/mainContact" is not empty
    And I verify if response value "/channels/0/channel" is not empty
    And I verify if response value "/channels/0/schedule/0/scheduleType" is not empty
    And I verify if response value "/channels/0/schedule/0/startTime" is not empty
    And I verify if response value "/channels/0/schedule/0/endTime" is not empty
    And I verify if response value "/address/stateAbbreviation" is not empty
    And I verify if response value "/address/neighborhood" is not empty
    And I verify if response value "/address/country" is not empty
    And I verify if response value "/address/city" is not empty
    And I verify if response value "/address/address" is not empty
    And I verify if response value "/address/addressNumber" is not empty
    And I verify if response value "/address/addressAddition" is not empty
    And I verify if response value "/socialNetworks/whatsapp" is not empty
    
    And Response code must be 200

  @Negativo
  Scenario Outline: Consultar customer <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "<access_token>"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointStores}/me"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

    Examples: 
      | descricao                  | access_token     | code      | message                                       |
      | por access_token incorreto | ${access_token}1 | "400.005" | "Field customer-code must have 8 characters." |
