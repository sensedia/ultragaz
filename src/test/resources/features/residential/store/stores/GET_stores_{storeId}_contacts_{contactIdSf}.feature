Feature: GET_stores_{storeId}_contacts_{contactIdSf}.feature
  Operação responsável por retornar os contatos da Revenda / Loja que estão cadastrados no Salesforce.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/stores" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0013000001HJAs6AAH" as "storeId"
    And I save "0030M00002Hb5FtQAJ" as "contactIdSf"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar os contatos da Revenda / Loja que estão cadastrados no Salesforce <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${storeId}/contacts/<contactIdSf>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with "Internal server error."
    And I compare response value "/code" with "500.000"
    And Response code must be 500

    Examples: 
      | description               | contactIdSf |
      | por contactIdSf incorreto |           1 |

  @Positivo
  Scenario Outline: Consultar os contatos da Revenda / Loja que estão cadastrados no Salesforce <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${storeId}/contacts/${contactIdSf}"
    Then I get response body
    And I get response status code
    And I verify if response value "/contactIdSf" is not empty
    And I verify if response value "/email" is not empty
    And I verify if response value "/secondaryEmail" is not empty
    And I verify if response value "/cellPhoneAreaCode" is not empty
    And I verify if response value "/cellPhoneNumber" is not empty
    And I verify if response value "/landlinePhoneAreaCode" is not empty
    And I verify if response value "/landlinePhone" is not empty
    And I verify if response value "/position" is not empty
    And I verify if response value "/birthDate" is not empty
    And Response code must be 200

    Examples: 
      | description           | storeId    |
      | por storeId incorreto |          1 |
      | por storeId correto   | ${storeId} |
