Feature: GET_stores_{storeId}.feature
  Obter revenda específica do segmento domiciliar da Ultragaz.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    And I use api name as "/dev/residential/store/v1"
    And I save "/stores" as "endpointStores"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0013000001HvV2bAAF" as "storeId"
    And I save "-21.7012938" as "latitude"
    And I save "-45.25653790000001" as "longitude"

  @Positivo
  Scenario Outline: Consultar stores <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/${storeId}"
    Then I get response body
    And I get response status code
    And I compare response value "/nameStore" with <nameStore>
    And I compare response value "/storeId" with <storeId>
    And I compare response value "/codeStore" with <codeStore>
    And I compare response value "/thumbnail" with <thumbnail>
    And I compare response value "/statusStore" with <statusStore>
    And I compare response value "/availability" with <availability>
    And I compare response value "/contacts/0/phone" with <phone>
    And I compare response value "/contacts/0/lastName" with <lastName>
    And I compare response value "/contacts/0/name" with <name>
    And I compare response value "/contacts/0/email" with <email>
    And I compare response value "/contacts/0/mainContact" with <mainContact>
    And I compare response value "/channels/0/channel" with <channel>
    And I verify if response value "/channels/0/paymentMethods" is empty
    And I compare response value "/channels/0/schedule/0/scheduleType" with <scheduleType>
    And I compare response value "/channels/0/schedule/0/startTime" with <startTime>
    And I compare response value "/channels/0/schedule/0/endTime" with <endTime>
    And I verify if response value "/channels/0/schedule/0/weekdays/0" is not empty
    And I verify if response value "/images" is empty
    And I verify if response value "/transferMethods" is empty
    And I compare response value "/address/stateAbbreviation" with <stateAbbreviation>
    And I compare response value "/address/neighborhood" with <neighborhood>
    And I compare response value "/address/country" with <country>
    And I compare response value "/address/city" with <city>
    And I compare response value "/address/address" with <address>
    And I compare response value "/address/addressNumber" with <addressNumber>
    And I compare response value "/address/addressAddition" with <addressAddition>
    And I compare response value "/socialNetworks/whatsapp" with <whatsapp>
    And I verify if response value "/commercialConsultant/id" is not empty
    And I verify if response value "/commercialConsultant/name" is not empty
    And I verify if response value "/commercialConsultant/email" is not empty
    And I verify if response value "/commercialConsultant/phoneNumber" is not empty
    And I verify if response value "/commercialConsultant/photo" is not empty
    And Response code must be 200

    Examples: 
      | descricao   | nameStore                             | storeId              | codeStore         | thumbnail                                        | statusStore | availability | phone        | name      | lastName  | email     | mainContact | channel | paymentMethod   | paymentMethodCode | price   | productId | nameProduct | scheduleType  | startTime  | endTime    | weekdays  | stateAbbreviation | neighborhood | country | city       | address         | addressNumber | addressAddition | whatsapp |
      | com sucesso | "MANUELA DE MIRANDA SOUZA EIRELI EPP" | "0013000001HvV2bAAF" | "2008316-2061372" | "https://cdn.buttercms.com/wToOscZR8GCGUqjyaqw3" | "OFFLINE"   | "true"       | "3460641671" | "MANUELA" | "MANUELA" | "MANUELA" | "false"     | "0800"  | "VALE ULTRAGAZ" | "37"              | "74.99" | "0110035" | "GLP/P13"   | "ATENDIMENTO" | "07:00:00" | "18:00:00" | "Domingo" | "BA"              | "PERNAMBUES" | "BR"    | "SALVADOR" | "AVENIDA HILDA" | "80"          | "S C"           | "false"  |

  @Positivo
  Scenario Outline: Consultar stores por field <field>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/${storeId}?fields=<field>"
    Then I get response body
    And I get response status code
    And I compare response value "/<field>" with <value>
    And Response code must be 200

    Examples: 
      | field        | value                                 |
      | nameStore    | "MANUELA DE MIRANDA SOUZA EIRELI EPP" |
      | statusStore  | "OFFLINE"                             |
      | availability | "true"                                |

  @Positivo
  Scenario: Consultar stores por field inexistente
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/${storeId}?fields=Bla"
    Then I get response body
    And I get response status code
    And Response body must be "{}"
    And Response code must be 200

  @Negativo
  Scenario Outline: Consultar stores <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointStores}/<storeId>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao             | storeId |
      | por storeId incorreto |       1 |

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointStores}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401
