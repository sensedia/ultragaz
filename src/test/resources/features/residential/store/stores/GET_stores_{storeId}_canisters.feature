@GET_stores_{storeId}_canisters
Feature: GET_stores_{storeId}_canisters.feature
  Operação responsável por obter o histórico de vasilhames com defeito da loja no Salesforce.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/stores" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0013000001P448vAAB" as "storeId"
    And I save "2018-01-01" as "startDate"
    And I save "2019-11-14" as "endDate"
    And I save "APROVED" as "status"
    And I save "361027-1" as "sealNumber"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}/${storeId}/canisters"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}/${storeId}/canisters"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}/${storeId}/canisters"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar histórico de vasilhames com defeito <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as <language>
    When I set GET api endpoint as "${endpoint}/${storeId}/canisters<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with <code>
    And I compare response value "/0/message" with <message>
    And Response code must be 400

    Examples: 
      | descricao                            | query                                        | code      | message                                     | language |
      | por start-date contendo caractere    | ?start-date=${startDate}a                    | "400.003" | "Field start-date has an invalid format."   | "en-US"  |
      | por start-date contendo caractere pt | ?start-date=${startDate}a                    | "400.003" | "Campo start-date tem um formato inválido." | "pt-BR"  |
      | por start-date com dia inválido      | ?start-date=2018-01-32                       | "400.003" | "Field start-date has an invalid format."   | "en-US"  |
      | por start-date com dia inválido pt   | ?start-date=2018-01-32                       | "400.003" | "Campo start-date tem um formato inválido." | "pt-BR"  |
      | por start-date com mês inválido      | ?start-date=2018-13-01                       | "400.003" | "Field start-date has an invalid format."   | "en-US"  |
      | por start-date com mês inválido pt   | ?start-date=2018-13-01                       | "400.003" | "Campo start-date tem um formato inválido." | "pt-BR"  |
      | por intervado de datas inválido      | ?start-date=${endDate}&end-date=${startDate} | "400.015" | "Invalid date range."                       | "en-US"  |
      | por intervalo de datas inválido pt   | ?start-date=${endDate}&end-date=${startDate} | "400.015" | "Intervalo de datas inválido."              | "pt-BR"  |
      | por end-date contendo caractere      | ?end-date=${endDate}a                        | "400.003" | "Field end-date has an invalid format."     | "en-US"  |
      | por end-date contendo caractere pt   | ?end-date=${endDate}a                        | "400.003" | "Campo end-date tem um formato inválido."   | "pt-BR"  |
      | por status inválido                  | ?status=${status}a                           | "400.003" | "Field status has an invalid format."       | "en-US"  |
      | por status inválido pt               | ?status=${status}a                           | "400.003" | "Campo status tem um formato inválido."     | "pt-BR"  |
      | por _offset inválido                 | ?_offset=a                                   | "400.003" | "Field _offset has an invalid format."      | "en-US"  |
      | por _offset inválido pt              | ?_offset=a                                   | "400.003" | "Campo _offset tem um formato inválido."    | "pt-BR"  |

  @Positivo
  Scenario Outline: Consultar histórico de vasilhames com defeito <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${storeId}/canisters<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/storeId" is not empty
    And I verify if response value "/totalRecords" is not empty
    And I verify if response value "/totalPendingAnalysis" is not empty
    And I verify if response value "/totalApproved" is not empty
    And I verify if response value "/totalReproved" is not empty
    And I verify if response value "/totalApprovedPending" is not empty
    And I verify if response value "/canisterSeals/0/processCreatedDate" is not empty
    And I verify if response value "/canisterSeals/0/recipient" is not empty
    And I verify if response value "/canisterSeals/0/sealNumber" is not empty
    And I verify if response value "/canisterSeals/0/exchangeReason" is empty
    And I verify if response value "/canisterSeals/0/analysisDate" is not empty
    And I verify if response value "/canisterSeals/0/verifiedReason" is empty
    And I verify if response value "/canisterSeals/0/status" is not empty
    And I verify if response value "/canisterSeals/0/isReplacementRealized" is not empty
    And I verify if response value "/canisterSeals/0/orderNumber" is not empty
    And I verify if response value "/canisterSeals/0/orderDate" is not empty
    And Response code must be 200

    Examples: 
      | descricao           | query                                                                                            |
      | por start-date      | ?start-date=${startDate}                                                                         |
      | por status APPROVED | ?start-date=${startDate}&status=APPROVED                                                         |
      | por seal-number     | ?start-date=${startDate}&seal-number=${sealNumber}                                               |
      | por _offset 1       | ?start-date=${startDate}&_offset=1                                                               |
      | por query completa  | ?start-date=${startDate}&end-date=${endDate}&status=APPROVED&seal-number=${sealNumber}&_offset=1 |

  @Positivo
  Scenario Outline: Consultar histórico de vasilhames com defeito <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${storeId}/canisters<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/storeId" is not empty
    And I verify if response value "/totalRecords" is not empty
    And I verify if response value "/totalPendingAnalysis" is not empty
    And I verify if response value "/totalApproved" is not empty
    And I verify if response value "/totalReproved" is not empty
    And I verify if response value "/totalApprovedPending" is not empty
    And I verify if response value "/canisterSeals/0/processCreatedDate" is not empty
    And I verify if response value "/canisterSeals/0/recipient" is not empty
    And I verify if response value "/canisterSeals/0/sealNumber" is not empty
    And I verify if response value "/canisterSeals/0/exchangeReason" is empty
    And I verify if response value "/canisterSeals/0/analysisDate" is not empty
    And I verify if response value "/canisterSeals/0/verifiedReason" is empty
    And I verify if response value "/canisterSeals/0/status" is not empty
    And I verify if response value "/canisterSeals/0/isReplacementRealized" is not empty
    And I verify if response value "/canisterSeals/0/orderNumber" is empty
    And I verify if response value "/canisterSeals/0/orderDate" is empty
    And Response code must be 200

    Examples: 
      | descricao           | query                                    |
      | por status REPROVED | ?start-date=${startDate}&status=REPROVED |

  @Positivo
  Scenario Outline: Consultar histórico de vasilhames com defeito <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}/${storeId}/canisters<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is empty
    And I verify if response value "/_metadata/totalPages" is empty
    And I verify if response value "/storeId" is empty
    And I verify if response value "/totalRecords" is empty
    And I verify if response value "/totalPendingAnalysis" is empty
    And I verify if response value "/totalApproved" is empty
    And I verify if response value "/totalReproved" is empty
    And I verify if response value "/totalApprovedPending" is empty
    And I verify if response value "/canisterSeals/0/processCreatedDate" is empty
    And I verify if response value "/canisterSeals/0/recipient" is empty
    And I verify if response value "/canisterSeals/0/sealNumber" is empty
    And I verify if response value "/canisterSeals/0/exchangeReason" is empty
    And I verify if response value "/canisterSeals/0/analysisDate" is empty
    And I verify if response value "/canisterSeals/0/verifiedReason" is empty
    And I verify if response value "/canisterSeals/0/status" is empty
    And I verify if response value "/canisterSeals/0/isReplacementRealized" is empty
    And I verify if response value "/canisterSeals/0/orderNumber" is empty
    And I verify if response value "/canisterSeals/0/orderDate" is empty
    And Response code must be 200

    Examples: 
      | descricao                    | query                       |
      | sem query                    |                             |
      | por seal-number inválido     | ?seal-number=${sealNumber}x |
      | por status  PENDING_ANALYSIS | ?status=PENDING_ANALYSIS    |
      | por status PENDING_APPROVAL  | ?status=PENDING_APPROVAL    |
      | por end-date                 | ?end-date=${endDate}        |
