@GET_roles
Feature: GET_roles.feature
  Operação responsável por retornar as funções/permissões.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"client_credentials"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store/v1"
    And I save "/roles" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"

  @Negativo
  Scenario: Enviar requisição sem client_id
  	Given I set request header "access_token" as "${access_token}"	
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401
	
	@Negativo
  Scenario: Enviar requisição sem access_token
  	Given I set request header "client_id" as "${client_id}"	
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
    
  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Consultar roles <descricao> <language>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Accept-Language" as "<language>"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "<code>"
    And I compare response value "/0/message" with "<message>"
    And I verify if response value "/1/code" is empty
    And I verify if response value "/1/message" is empty
    And Response code must be 400

    Examples: 
      | descricao              | queryParameter    | code    | message                                        | language |
      | com is-active inválido | ?is-active=falses | 400.003 | Field is-active has an invalid format.         | en-US    |
      | com is-active inválido | ?is-active=falses | 400.003 | Campo is-active tem um formato inválido.       | pt-BR    |
      | com _offset string     | ?_offset=a        | 400.003 | Field _offset has an invalid format.           | en-US    |
      | com _offset string     | ?_offset=a        | 400.003 | Campo _offset tem um formato inválido.         | pt-BR    |
      | com _limit 0           | ?_limit=0         | 400.010 | Field _limit value must be at least 1.         | en-US    |
      | com _limit 0           | ?_limit=0         | 400.010 | O valor do campo _limit deve ser pelo menos 1. | pt-BR    |
      | com _limit string      | ?_limit=a         | 400.003 | Field _limit has an invalid format.            | en-US    |
      | com _limit string      | ?_limit=a         | 400.003 | Campo _limit tem um formato inválido.          | pt-BR    |

  @Positivo
  Scenario Outline: Consultar roles <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/name" is not empty
    And I verify if response value "/0/description" is not empty
    And I verify if response value "/0/active" is not empty
    And Response code must be 200

    Examples: 
      | descricao                                   | queryParameter                     |
      | sem query parameter                         |                                    |
      | com is-active true                          | ?is-active=true                    |
      | com is-active false                         | ?is-active=false                   |
      | com is-active 1                             | ?is-active=1                       |
      | com is-active vazio                         | ?is-active=                        |
      | com _offset 0                               | ?_offset=0                         |
      | com _offset vazio                           | ?_offset=                          |
      | com _limit 100                              | ?_limit=100                        |
      | com _limit vazio                            | ?_limit=                           |
      | com _limit, _offset e is-active preenchidos | ?_limit=1&_offset=0&is-active=true |

  @Positivo
  Scenario Outline: Consultar roles <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<queryParameter>"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

    Examples: 
      | descricao       | queryParameter |
      | com _offset 100 | ?_offset=100   |
