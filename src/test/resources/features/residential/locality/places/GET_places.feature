Feature: GET_places.feature 
	Consulta para obter geolocalização.


@PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"
    

@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com/dev" 
	And I use api name as "/locality/v1" 
	And I save "/places" as "endpointPlaces" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
	And I save "13181643" as "zipCode" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set GET api endpoint as "${endpointPlaces}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição sem access_token 
	Given I set request header "Accept-Language" as "pt-BR" 
	And I set request header "client_id" as "${client_id}" 
	When I set GET api endpoint as "${endpointPlaces}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição com client_id inválido 
	Given I set request header "client_id" as "${client_id}1" 
	When I set GET api endpoint as "${endpointPlaces}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição com access_token inválido 
	Given I set request header "Accept-Language" as "pt-BR" 
	And I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}1" 
	When I set GET api endpoint as "${endpointPlaces}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
	And Response code must be 401 
	
	
@Positivo 
Scenario Outline: Consultar place <descricao> 
	Given I set request header "client_id" as "${client_id}"
	And I set request header "access_token" as "${access_token}"
	When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
	Then I get response body 
	And I get response status code 
	And I compare response value "/0/stateAbbreviation" with <stateAbbreviation> 
	And I compare response value "/0/stateName" with <stateName>
	And I compare response value "/0/neighborhood" with <neighborhood> 
	And I compare response value "/0/zipCode" with <zipCode> 
	And I compare response value "/0/levelZipCode" with <levelZipCode> 
	And I compare response value "/0/addressType" with <addressType> 
	And I compare response value "/0/address" with <address> 
	And I compare response value "/0/addressAddition" with <addressAddition> 
	And I compare response value "/0/abbreviation" with <abbreviation> 
	And I compare response value "/0/locationType" with <locationType> 
	And I compare response value "/0/city" with <city> 
	And I verify if response value "/1/namePlace" is empty 
	And I verify if response value "/1/stateAbbreviation" is empty
	And I verify if response value "/1/stateName" is empty 
	And I verify if response value "/1/neighborhood" is empty 
	And I verify if response value "/1/levelZipCode" is empty 
	And I verify if response value "/1/fullAddress" is empty 
	And I verify if response value "/1/country" is empty 
	And I verify if response value "/1/addressType" is empty 
	And I verify if response value "/1/address" is empty 
	And I verify if response value "/1/addressAddition" is empty 
	And I verify if response value "/1/mailBox" is empty 
	And I verify if response value "/1/organizationName" is empty 
	And I verify if response value "/1/abbreviation" is empty 
	And Response code must be 200 
	And I wait 2 seconds 
	
	Examples: 
		| descricao                  | queryParameter       | placeId                                | stateAbbreviation | stateName   | neighborhood                      | zipCode    | levelZipCode | addressType | address                                   | addressAddition | abbreviation                 | locationType | city        |
		| por zip-code               | ?zip-code=${zipCode} | "87e20b0b-981f-11e9-9a47-0242ac160002" | "SP"              | "São Paulo" | "Jardim Bom Retiro (Nova Veneza)" | "13181643" | "ADDRESS"    | "Rua"       | "Domingos Conrado"                        | ""              | "R Domingos Conrado"         | "ROOFTOP"    | "Sumaré"    |
		| por zip-code comercial     | ?zip-code=13013160   | "75baffd8-981f-11e9-9a47-0242ac160002" | "SP"              | "São Paulo" | "Centro"                          | "13013160" | "ADDRESS"    | "Avenida"   | "Andrade Neves"                           | "- até 600/601" | "Av Andrade Neves"           | "ROOFTOP"    | "Campinas" |
		| por zip-code nome comprido | ?zip-code=04112080   | "8170d9dd-981f-11e9-9a47-0242ac160002" | "SP"              | "São Paulo" | "Vila Mariana"                    | "04112080" | "ADDRESS"    | "Avenida"   | "Engenheiro Luiz Gomes Cardim Sangirardi" | ""              | "Av Eng Luiz G C Sangirardi" | "ROOFTOP"    | "São Paulo" |
		
		
		@Positivo 
		Scenario Outline: Consultar place <descricao> 
			Given I set request header "client_id" as "${client_id}"
			And I set request header "access_token" as "${access_token}"
			When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
			Then I get response body 
			And I get response status code 
			And I compare response value "/0/namePlace" with <namePlace> 
			And I compare response value "/0/stateAbbreviation" with <stateAbbreviation>
			And I compare response value "/0/stateName" with <stateName>
			And I compare response value "/0/neighborhood" with <neighborhood> 
			And I compare response value "/0/zipCode" with <zipCode> 
			And I compare response value "/0/levelZipCode" with <levelZipCode> 
			And I compare response value "/0/address" with <address> 
			And I compare response value "/0/organizationName" with <organizationName> 
			And I compare response value "/0/abbreviation" with <abbreviation> 
			And I compare response value "/0/locationType" with <locationType>
			And I compare response value "/0/city" with <city>
			And I verify if response value "/0/streetNumber" is empty
			And I verify if response value "/1/namePlace" is empty 
			And I verify if response value "/1/stateAbbreviation" is empty
			And I verify if response value "/1/stateName" is empty 
			And I verify if response value "/1/neighborhood" is empty 
			And I verify if response value "/1/levelZipCode" is empty 
			And I verify if response value "/1/fullAddress" is empty 
			And I verify if response value "/1/country" is empty 
			And I verify if response value "/1/addressType" is empty 
			And I verify if response value "/1/address" is empty 
			And I verify if response value "/1/addressAddition" is empty 
			And I verify if response value "/1/mailBox" is empty 
			And I verify if response value "/1/organizationName" is empty 
			And I verify if response value "/1/abbreviation" is empty 
			And Response code must be 200 
			And I wait 2 seconds 
			
			Examples:  
				| descricao              | queryParameter     | placeId                                | namePlace                     | stateAbbreviation | stateName   | neighborhood     | zipCode    | levelZipCode     | address                     | organizationName              | abbreviation         | locationType | city         |
				| por zip-code comercial | ?zip-code=69923900 | "937818b5-981f-11e9-9a47-0242ac160002" | "Aeroporto Plácido de Castro" | "AC"              | "Acre"      | "Vila Aeroporto" | "69923900" | "CORPORATE_USER" | "Avenida Plácido de Castro" | "Aeroporto Plácido de Castro" | "Aer Plácido Castro" | "ROOFTOP"    | "Rio Branco" |
				
				
				@Positivo 
				Scenario Outline: Consultar place <descricao> 
					Given I set request header "client_id" as "${client_id}"
					And I set request header "access_token" as "${access_token}"
					When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/0/stateAbbreviation" with <stateAbbreviation>
					And I compare response value "/0/stateName" with <stateName>
					And I compare response value "/0/zipCode" with <zipCode> 
					And I compare response value "/0/levelZipCode" with <levelZipCode> 
					And I compare response value "/0/address" with <address> 
					And I compare response value "/0/abbreviation" with <abbreviation> 
					And I compare response value "/0/locationType" with <locationType>
					And I compare response value "/0/city" with <city>
					And I verify if response value "/0/streetNumber" is empty
					And I verify if response value "/1/namePlace" is empty 
					And I verify if response value "/1/stateAbbreviation" is empty
					And I verify if response value "/1/stateName" is empty 
					And I verify if response value "/1/neighborhood" is empty 
					And I verify if response value "/1/levelZipCode" is empty 
					And I verify if response value "/1/fullAddress" is empty 
					And I verify if response value "/1/country" is empty 
					And I verify if response value "/1/addressType" is empty 
					And I verify if response value "/1/address" is empty 
					And I verify if response value "/1/addressAddition" is empty 
					And I verify if response value "/1/mailBox" is empty 
					And I verify if response value "/1/organizationName" is empty 
					And I verify if response value "/1/abbreviation" is empty 
					And Response code must be 200 
					And I wait 2 seconds 
					
					Examples: 
						| descricao                     | queryParameter     | placeId                               | stateAbbreviation | stateName   | zipCode    | levelZipCode  | address | abbreviation | locationType | city     |
						| por zip-code único por cidade | ?zip-code=69390000 |"116ec22a-981f-11e9-9a47-0242ac160002" | "RR"              | "Roraima"   | "69390000" | "MAIN_LOCALE" | "Cantá" | "Cantá"      | "ROOFTOP"    | "Cantá"  |
						
						
						@Positivo 
						Scenario Outline: Consultar place <descricao> 
							Given I set request header "client_id" as "${client_id}"
							And I set request header "access_token" as "${access_token}"
							When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/0/address" with "Atlantic Ocean" 
							And I compare response value "/0/locationType" with "APPROXIMATE" 
							And Response code must be 200 
							And I wait 2 seconds 
							
							Examples: 
								| descricao                         | queryParameter                              |
								| por longitude, latitude oceanicas | ?latitude=-22.8163929&longitude=-22.8163920 |
								
								
								@Positivo 
								Scenario Outline: Consultar place <descricao> 
									Given I set request header "client_id" as "${client_id}"
									And I set request header "access_token" as "${access_token}"
									When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
									Then I get response body 
									And I get response status code 
									And I compare response value "/0/stateAbbreviation" with "SP" 
									And I compare response value "/0/stateName" with "São Paulo"
									And I compare response value "/0/neighborhood" with "Bosque das Palmeiras"
									And I compare response value "/0/streetNumber" with "1000"
									And I compare response value "/0/country" with "Brazil" 
									And I compare response value "/0/address" with "R. Dr. Ricardo Benetton Martins, 1000 - Bosque das Palmeiras, Campinas - SP, Brazil" 
									And I compare response value "/0/locationType" with "RANGE_INTERPOLATED" 
									And I compare response value "/0/streetNumber" with "1000"
									And I compare response value "/0/city" with "Campinas"
									And I compare response value "/1/stateAbbreviation" with "SP" 
									And I compare response value "/1/stateName" with "São Paulo"
									And I compare response value "/1/neighborhood" with "Bosque das Palmeiras" 
									And I compare response value "/1/country" with "Brazil" 
									And I compare response value "/1/address" with "R. Dr. Ricardo Benetton Martins - Bosque das Palmeiras, Campinas - SP, Brazil" 
									And I compare response value "/1/locationType" with "GEOMETRIC_CENTER"
									And I compare response value "/1/city" with "Campinas" 
									And I compare response value "/2/stateAbbreviation" with "SP"
									And I compare response value "/2/stateName" with "State of São Paulo"
									And I compare response value "/2/neighborhood" with "Bosque das Palmeiras" 
									And I compare response value "/2/country" with "Brazil" 
									And I compare response value "/2/address" with "Bosque das Palmeiras, Campinas - State of São Paulo, Brazil" 
									And I compare response value "/2/locationType" with "APPROXIMATE"
									And I compare response value "/2/city" with "Campinas" 
									And I compare response value "/3/stateAbbreviation" with "SP"
									And I compare response value "/3/stateName" with "State of São Paulo"
									And I compare response value "/3/country" with "Brazil" 
									And I compare response value "/3/address" with "Campinas, State of São Paulo, Brazil" 
									And I compare response value "/3/locationType" with "APPROXIMATE"
									And I compare response value "/3/city" with "Campinas" 
									And I compare response value "/4/stateAbbreviation" with "SP" 
									And I compare response value "/4/stateName" with "State of São Paulo"
									And I compare response value "/4/country" with "Brazil" 
									And I compare response value "/4/address" with "Campinas - State of São Paulo, Brazil" 
									And I compare response value "/4/locationType" with "APPROXIMATE"
									And I compare response value "/4/city" with "Campinas"
									And I compare response value "/5/stateAbbreviation" with "SP" 
									And I compare response value "/5/stateName" with "State of São Paulo"
									And I compare response value "/5/country" with "Brazil" 
									And I compare response value "/5/address" with "State of São Paulo, Brazil" 
									And I compare response value "/5/locationType" with "APPROXIMATE" 
									And I compare response value "/6/country" with "Brazil" 
									And I compare response value "/6/address" with "Brazil" 
									And I compare response value "/6/locationType" with "APPROXIMATE" 
									And Response code must be 200 
									And I wait 2 seconds 
									
									Examples: 
										| descricao                                         | queryParameter                                                   | 
										| por longitude e latitude CPQD                     | ?latitude=${latitude}&longitude=${longitude}                     |
										| por longitude, latitude CPQD e zip-code aleatório | ?latitude=${latitude}&longitude=${longitude}&zip-code=${zipCode} | 
										
										
										@Positivo 
										Scenario Outline: Consultar place <descricao> 
											Given I set request header "client_id" as "${client_id}"
											And I set request header "access_token" as "${access_token}"
											When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
											Then I get response body 
											And I get response status code 
											And I compare response value "/0/stateAbbreviation" with "AC"
											And I compare response value "/0/stateName" with "Acre" 
											And I compare response value "/0/zipCode" with "69902992" 
											And I compare response value "/0/levelZipCode" with "MAILBOX" 
											And I compare response value "/0/address" with "Ramal Benfica, s/n" 
											And I compare response value "/0/mailbox" with "Benfica" 
											And I compare response value "/0/locationType" with "ROOFTOP" 
											And I verify if response value "/0/streetNumber" is empty
											And Response code must be 200 
											And I wait 2 seconds 
											
											Examples: 
												| descricao           | queryParameter     | 
												| por cep com mailbox | ?zip-code=69902992 |
												
												
												@Negativo 
												Scenario Outline: Consultar place <descricao> 
													Given I set request header "client_id" as "${client_id}"
													And I set request header "access_token" as "${access_token}"
													When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
													Then I get response body 
													And I get response status code 
													And I compare response value "/0/code" with <code> 
													And I compare response value "/0/message" with <message> 
													And I verify if response value "/1/code" is empty 
													And I verify if response value "/1/message" is empty 
													And Response code must be 400 
													And I wait 2 seconds 
													
													Examples: 
														| descricao              | queryParameter                    | code      | message                                             |
														| por zip-code vazio     | ?zip-code=                        | "400.004" | "Field zip-code or latitude/longitude is required." |
														| por zip-code incorreto | ?zip-code=101010101               | "400.003" | "Field zip-code has an invalid format."             |
														| por zip-code inválido  | ?zip-code=1                       | "400.003" | "Field zip-code has an invalid format."             |
														| por latitude           | ?latitude=${latitude}             | "400.004" | "Field zip-code or longitude is required."          |
														| por latitude vazia     | ?longitude=${longitude}&latitude= | "400.004" | "Field zip-code or latitude is required."           | 
														| por longitude          | ?longitude=${longitude}           | "400.004" | "Field zip-code or latitude is required."           |
														| por longitude vazia    | ?latitude=${latitude}&longitude=  | "400.004" | "Field zip-code or longitude is required."          | 
														
														
														@Negativo 
														Scenario Outline: Consultar place <descricao> 
															Given I set request header "client_id" as "${client_id}"
															And I set request header "access_token" as "${access_token}"
															When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
															Then I get response body 
															And I get response status code 
															And I compare response value "/0/code" with <code> 
															And I compare response value "/0/message" with <message> 
															And Response code must be 400 
															
															Examples: 
																| descricao               | queryParameter                       | code      | message                                  |
																| por longitude incorreta | ?latitude=${latitude}&longitude=Rua  | "400.003" | "Field longitude has an invalid format." |
																| por latitude incorreta  | ?longitude=${longitude}&latitude=Rua | "400.003" | "Field latitude has an invalid format."  |
																
																
																@Negativo 
																Scenario Outline: Consultar place <descricao> em Português 
																	Given I set request header "client_id" as "${client_id}"
																	And I set request header "access_token" as "${access_token}"
																	And I set request header "Accept-Language" as "pt-BR" 
																	When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
																	Then I get response body 
																	And I get response status code 
																	And I compare response value "/0/code" with <code> 
																	And I compare response value "/0/message" with <message> 
																	And I verify if response value "/1/code" is empty 
																	And I verify if response value "/1/message" is empty 
																	And Response code must be 400 
																	
																	Examples: 
																		| descricao              | queryParameter                    | code      | message                                                 |
																		| por zip-code vazio     | ?zip-code=                        | "400.004" | "O campo zip-code ou latitude/longitude é obrigatório." |
																		| por zip-code incorreto | ?zip-code=101010101               | "400.003" | "O campo zip-code tem o formato inválido."              |
																		| por zip-code inválido  | ?zip-code=1                       | "400.003" | "O campo zip-code tem o formato inválido."              |
																		| por latitude           | ?latitude=${latitude}             | "400.004" | "O campo zip-code ou longitude é obrigatório."          |
																		| por latitude vazia     | ?longitude=${longitude}&latitude= | "400.004" | "O campo zip-code ou latitude é obrigatório."           | 
																		| por longitude          | ?longitude=${longitude}           | "400.004" | "O campo zip-code ou latitude é obrigatório."           |
																		| por longitude vazia    | ?latitude=${latitude}&longitude=  | "400.004" | "O campo zip-code ou longitude é obrigatório."          | 
																		
																		
																		@Negativo 
																		Scenario Outline: Consultar place <descricao> em Português 
																			Given I set request header "client_id" as "${client_id}"
																			And I set request header "access_token" as "${access_token}"
																			And I set request header "Accept-Language" as "pt-BR" 
																			When I set GET api endpoint as "${endpointPlaces}<queryParameter>" 
																			Then I get response body 
																			And I get response status code 
																			And I compare response value "/0/code" with <code> 
																			And I compare response value "/0/message" with <message> 
																			And Response code must be 400 
																			
																			Examples: 
																				| descricao               | queryParameter                       | code      | message                                    |
																				| por longitude incorreta | ?latitude=${latitude}&longitude=Rua  | "400.003" | "O campo longitude tem o formato inválido." |
																				| por latitude incorreta  | ?longitude=${longitude}&latitude=Rua | "400.003" | "O campo latitude tem o formato inválido."  |