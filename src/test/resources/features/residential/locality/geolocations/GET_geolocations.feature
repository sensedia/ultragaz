Feature: GET_geolocations.feature 
	Consulta para obter geolocalização.


@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com/dev" 
	And I use api name as "/locality/v1" 
	And I save "/geolocations" as "endpointGeolocations" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "13181643" as "zipCode" 
	And I save "Rua Domingos Conrado, 528" as "address" 
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set GET api endpoint as "${endpointGeolocations}?zip-code=13181643" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
@Negativo 
Scenario: Enviar requisição sem access_token 
	Given I set request header "Accept-Language" as "pt-BR" 
	Given I set request header "client_id" as "${client_id}" 
	When I set GET api endpoint as "${endpointGeolocations}?zip-code=13181643" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição com client_id inválido 
	Given I set request header "client_id" as "${client_id}1" 
	When I set GET api endpoint as "${endpointGeolocations}?zip-code=13181643" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
@Negativo 
Scenario: Enviar requisição com access_token inválido 
	Given I set request header "client_id" as "${client_id}" 
	Given I set request header "access_token" as "${access_token}1" 
	And I set request header "Accept-Language" as "pt-BR" 
	When I set GET api endpoint as "${endpointGeolocations}?zip-code=13181643" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
	And Response code must be 401 
	
@Positivo 
Scenario Outline: Consultar geolocation <descricao> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	When I set GET api endpoint as "${endpointGeolocations}<queryParameter>" 
	Then I get response body 
	And I get response status code 
	And I compare response value "/0/latitude" with <latitude> 
	And I compare response value "/0/longitude" with <longitude> 
	And I compare response value "/0/fullAddress" with <fullAddress> 
	And I compare response value "/0/locationType" with <locationType> 
	And I verify if response value "/1/latitude" is empty 
	And I verify if response value "/1/longitude" is empty 
	And I verify if response value "/1/fullAddress" is empty 
	And I verify if response value "/1/locationType" is empty 
	And Response code must be 200 
	And I wait 2 seconds 
	
	Examples: 
		| descricao              | queryParameter                          | latitude            | longitude     | fullAddress                                                                             | locationType       |
		| por zip-code           | ?zip-code=${zipCode}                    | "-22.8533912"       | "-47.1889629" | "R. Domingos Conrado - Jardim Bom Retiro (Nova Veneza), Sumaré - SP, 13181-643, Brazil" | "GEOMETRIC_CENTER" |
		| por address e zip-code | ?address=${address}&zip-code=${zipCode} | "-22.8533912"       | "-47.1889629" | "R. Domingos Conrado - Jardim Bom Retiro (Nova Veneza), Sumaré - SP, 13181-643, Brazil" | "GEOMETRIC_CENTER" |
		| por address aleatório  | ?address=Rua                            | "41.23692519999999" | "-85.8561118" | "108 E Market St, Warsaw, IN 46580, USA"                                                | "ROOFTOP"          |
		
		
		@Positivo 
		Scenario Outline: Consultar geolocation <descricao> 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			When I set GET api endpoint as "${endpointGeolocations}<queryParameter>" 
			Then I get response body 
			And I get response status code 
			And I compare response value "/0/latitude" with <latitude> 
			And I compare response value "/0/longitude" with <longitude> 
			And I compare response value "/0/fullAddress" with <fullAddress> 
			And I compare response value "/0/locationType" with <locationType> 
			And Response code must be 200 
			And I wait 2 seconds 
			
			Examples: 
				| descricao   | queryParameter      | latitude      | longitude    | fullAddress                                                                                  | locationType |
				| por address | ?address=${address} | "-22.8539579" | "-47.187451" | "R. Domingos Conrado, 528 - Jardim Bom Retiro (Nova Veneza), Sumaré - SP, 13181-643, Brazil" | "ROOFTOP"    |
				
				
				@Positivo 
				Scenario: Consultar geolocation inexistente 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set GET api endpoint as "${endpointGeolocations}?address=2u1h321uh3123" 
					Then I get response body 
					And I get response status code 
					And Response body must be "[]" 
					And Response code must be 200 
					
					
				@Negativo 
				Scenario Outline: Consultar geolocation <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set GET api endpoint as "${endpointGeolocations}<queryParameter>" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/0/code" with <code> 
					And I compare response value "/0/message" with <message> 
					And I verify if response value "/1/code" is empty 
					And I verify if response value "/1/message" is empty 
					And Response code must be 400 
					And I wait 2 seconds 
					
					Examples: 
						| descricao              | queryParameter      | code      | message                                  | 
						| por zip-code incorreto | ?zip-code=101010101 | "400.003" | "Field zip-code has an invalid format."  |
						| por zip-code inválido  | ?zip-code=1         | "400.003" | "Field zip-code has an invalid format."  |
						| por address vazio      | ?address=           | "400.004" | "Field zip-code or address is required." |
						| por zip-code vazio     | ?zip-code=          | "400.004" | "Field zip-code or address is required." |
						
						
						@Negativo 
						Scenario Outline: Consultar geolocation <descricao> em Português 
							Given I set request header "client_id" as "${client_id}" 
							And I set request header "access_token" as "${access_token}" 
							And I set request header "Accept-Language" as "pt-BR" 
							When I set GET api endpoint as "${endpointGeolocations}<queryParameter>" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/0/code" with <code> 
							And I compare response value "/0/message" with <message> 
							And I verify if response value "/1/code" is empty 
							And I verify if response value "/1/message" is empty 
							And Response code must be 400 
							And I wait 2 seconds 
							
							Examples: 
								| descricao              | queryParameter      | code      | message                                      | 
								| por zip-code incorreto | ?zip-code=101010101 | "400.003" | "O campo zip-code tem o formato inválido."   |
								| por zip-code inválido  | ?zip-code=1         | "400.003" | "O campo zip-code tem o formato inválido."   |
								| por address vazio      | ?address=           | "400.004" | "O campo zip-code ou address é obrigatório." |
								| por zip-code vazio     | ?zip-code=          | "400.004" | "O campo zip-code ou address é obrigatório." |