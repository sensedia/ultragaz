Feature: POST_locales_files.feature 
	Operação responsável por importação da base de dados de endereços(Correios).

	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/locality/v1" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "/locales/files" as "endpointLocalesFiles" 
	And I save "eDNE_Delta_Master_1411.zip" as "key" 
	And I save "ms-locality-bucket" as "bucket" 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set POST api endpoint as "${endpointLocalesFiles}" 
	Then I get response body 
	And I get response status code 
	And I compare response value "/error" with "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario Outline: Enviar requisição <descricao> 
	Given I set request header "client_id" as "<client_id>" 
	And I set request header "access_token" as "<access_token>" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"key":"${key}","bucket":"${bucket}"}" 
	When I set POST api endpoint as "${endpointLocalesFiles}" 
	Then I get response body 
	And I get response status code 
	And I compare response value "/error" with "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	Examples: 
		| descricao                 | client_id     | access_token     | message                                                                                  |
		| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
		| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
		
		
		@Positivo 
		Scenario: Cadastrar arquivo 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			And I set request body as "{"key":"${key}","bucket":"${bucket}"}" 
			When I set POST api endpoint as "${endpointLocalesFiles}" 
			Then I get response body 
			And I get response status code 
			And I verify if response body is empty 
			And Response code must be 202 
			And I wait 2 seconds 
			
			
		@Negativo 
		Scenario: Cadastrar arquivo sem body 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "Content-Type" as "application/json" 
			And I set request header "access_token" as "${access_token}" 
			When I set POST api endpoint as "${endpointLocalesFiles}" 
			Then I get response body 
			And I get response status code 
			And I compare response value "/0/code" with "400.000" 
			And I compare response value "/0/message" with "Invalid request body." 
			And Response code must be 400 
			
			
		@Negativo 
		Scenario: Cadastrar arquivo sem Content-Type 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request body as "{"key":"${key}","bucket":"${bucket}"}" 
			When I set POST api endpoint as "${endpointLocalesFiles}" 
			Then I get response body 
			And I get response status code 
			And I verify if response body is empty 
			And Response code must be 415 
			
			
		@Negativo 
		Scenario Outline: Postar arquivo <descricao> 
			Given I set request header "Content-Type" as "application/json" 
			And I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request body as "<body>" 
			When I set POST api endpoint as "${endpointLocalesFiles}" 
			Then I get response body 
			And I get response status code 
			And I compare response value "/0/code" with "<code>" 
			And I compare response value "/0/message" with <message> 
			And Response code must be <responseCode> 
			
			Examples: 
				| id | descricao                                  | body                                                  | message                                                         | code    | responseCode |
				| 1  | com key vazio                              | {"key":"","bucket":"${bucket}"}                       | "Field key is required."                                        | 400.001 | 400          |
				| 2  | com key null                               | {"key":null,"bucket":"${bucket}"}                     | "Field key is required."                                        | 400.001 | 400          |
				| 3  | sem key                                    | {"bucket":"${bucket}"}                                | "Field key is required."                                        | 400.001 | 400          |
				| 4  | com bucket vazio                           | {"key":"${key}","bucket":""}                          | "Field bucket is required."                                     | 400.001 | 400          |
				| 5  | com bucket nulo                            | {"key":"${key}","bucket":null}                        | "Field bucket is required."                                     | 400.001 | 400          |
				| 6  | sem bucket                                 | {"key":"${key}"}                                      | "Field bucket is required."                                     | 400.001 | 400          |
				
				
				@Positivo 
				Scenario Outline: Postar arquivo <descricao> 
					Given I set request header "Content-Type" as "application/json" 
					And I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "<body>" 
					When I set POST api endpoint as "${endpointLocalesFiles}" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 202 
					
					Examples: 
						| descricao                                  | body                                                  |
						| com key inválido                           | {"key":"eDNE_Delter_1411","bucket":"${bucket}"}       |
						| com bucket inválido                        | {"key":"${key}","bucket":"${bucket}1233"}             |
						
						
						@Negativo 
						Scenario Outline: Postar arquivo <descricao> 
							Given I set request header "Content-Type" as "application/json" 
							And I set request header "client_id" as "${client_id}" 
							And I set request header "access_token" as "${access_token}" 
							And I set request body as "<body>" 
							When I set POST api endpoint as "${endpointLocalesFiles}" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/0/code" with "<code>" 
							And I compare response value "/0/message" with <message> 
							And I compare response value "/1/code" with "<code1>" 
							And I compare response value "/1/message" with <message1> 
							And Response code must be <responseCode> 
							
							Examples: 
								| descricao                                  | body                                                  | message                                                         | code    | responseCode | message1                                                        | code1   |
								| com body vazio                             | {}                                                    | "Field key is required."                                        | 400.001 | 400          | "Field bucket is required."                                     | 400.001 |