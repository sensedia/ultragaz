Feature: GET_orders.feature 
	Consulta pedidos no segmento domiciliar da Ultragaz.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set GET api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	
	
@PreRequest 
Scenario: Cadastrar cliente com sucesso 
	Given System generate random number 
	And System generate random CPF 
	And I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set POST api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "customerId" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/order/v1" 
	And I save "/orders" as "endpointOrders" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "01uq0000005QTFg" as "productPriceId" 
	And I save "01uq0000005QTFg" as "productId" 
	And I save "001q0000017eE9U" as "storeId" 
	
	
@PreRequest 
Scenario Outline: Cadastrar pedido <descricao> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "<body>" 
	When I set POST api endpoint as "${endpointOrders}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "orderId<id>" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	Examples: 
		| id | descricao                                  | body                                                                                                                                                                                                                                         |
		| 1  | com sucesso                                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		
		
		@Negativo 
		Scenario: Enviar requisição sem client_id 
			When I set GET api endpoint as "${endpointOrders}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario: Enviar requisição sem access_token 
			Given I set request header "client_id" as "${client_id}" 
			When I set GET api endpoint as "${endpointOrders}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario Outline: Enviar requisição <descricao> 
			Given I set request header "client_id" as "<client_id>" 
			And I set request header "access_token" as "<access_token>" 
			When I set GET api endpoint as "${endpointOrders}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "<message>" 
			And Response code must be 401 
			
			Examples: 
				| descricao                 | client_id     | access_token     | message                                                                          |
				| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
				| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
				| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required APP in the request, identified by HEADER access_token. |
				| com access_token vazio    | ${client_id}  |                  | Could not find a required APP in the request, identified by HEADER access_token. |
				
				
				@Positivo 
				Scenario: Consultar pedido 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?customer-id=${customerId}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/0/customerId" with "${customerId}" 
					And I verify if response value "/0/orderId" is not empty 
					And I verify if response value "/0/nameCustomer" is not empty 
					And I verify if response value "/0/valueOrder" is not empty 
					And I verify if response value "/0/statusOrder" is not empty 
					And I verify if response value "/0/products/0/price" is not empty 
					And I verify if response value "/0/products/0/quantity" is not empty 
					And I verify if response value "/0/acknowledge" is not empty 
					And I verify if response value "/0/salesChannel" is not empty 
					And I verify if response value "/0/address/stateAbbreviation" is not empty 
					And I verify if response value "/0/address/address" is not empty 
					And I verify if response value "/0/requestDate" is not empty 
					And I verify if response value "/0/numberOrder" is not empty 
					And I verify if response value "/0/storeId" is not empty 
					And I verify if response value "/0/products/0/productPriceId" is not empty 
					And I verify if response value "/0/products/0/productId" is not empty 
					And Response code must be 200 
					
					
				@Positivo 
				Scenario: Consultar pedido 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?customer-id=${customerId}&store-id=${storeId}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/0/storeId" with "001q0000017eE9UAAU" 
					And I verify if response value "/0/orderId" is not empty 
					And I verify if response value "/0/nameCustomer" is not empty 
					And I verify if response value "/0/valueOrder" is not empty 
					And I verify if response value "/0/statusOrder" is not empty 
					And I verify if response value "/0/products/0/price" is not empty 
					And I verify if response value "/0/products/0/quantity" is not empty 
					And I verify if response value "/0/acknowledge" is not empty 
					And I verify if response value "/0/salesChannel" is not empty 
					And I verify if response value "/0/address/stateAbbreviation" is not empty 
					And I verify if response value "/0/address/address" is not empty 
					And I verify if response value "/0/requestDate" is not empty 
					And I verify if response value "/0/numberOrder" is not empty 
					And I verify if response value "/0/customerId" is not empty 
					And I verify if response value "/0/products/0/productPriceId" is not empty 
					And I verify if response value "/0/products/0/productId" is not empty 
					And Response code must be 200 
					
					
				@Positivo 
				Scenario: Consultar pedido 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?customer-id=${customerId}&status-order=Novo" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/0/statusOrder" with "Novo" 
					And I verify if response value "/0/orderId" is not empty 
					And I verify if response value "/0/nameCustomer" is not empty 
					And I verify if response value "/0/valueOrder" is not empty 
					And I verify if response value "/0/storeId" is not empty 
					And I verify if response value "/0/products/0/price" is not empty 
					And I verify if response value "/0/products/0/quantity" is not empty 
					And I verify if response value "/0/acknowledge" is not empty 
					And I verify if response value "/0/salesChannel" is not empty 
					And I verify if response value "/0/address/stateAbbreviation" is not empty 
					And I verify if response value "/0/address/address" is not empty 
					And I verify if response value "/0/requestDate" is not empty 
					And I verify if response value "/0/numberOrder" is not empty 
					And I verify if response value "/0/customerId" is not empty 
					And I verify if response value "/0/products/0/productPriceId" is not empty 
					And I verify if response value "/0/products/0/productId" is not empty 
					And Response code must be 200 
					
					
				@Positivo 
				Scenario: Consultar pedido 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?customer-id=${customerId}&payment-method=Dinheiro" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/0/paymentMethod" with "Dinheiro" 
					And I verify if response value "/0/statusOrder" is not empty 
					And I verify if response value "/0/orderId" is not empty 
					And I verify if response value "/0/nameCustomer" is not empty 
					And I verify if response value "/0/valueOrder" is not empty 
					And I verify if response value "/0/storeId" is not empty 
					And I verify if response value "/0/products/0/price" is not empty 
					And I verify if response value "/0/products/0/quantity" is not empty 
					And I verify if response value "/0/acknowledge" is not empty 
					And I verify if response value "/0/salesChannel" is not empty 
					And I verify if response value "/0/address/stateAbbreviation" is not empty 
					And I verify if response value "/0/address/address" is not empty 
					And I verify if response value "/0/requestDate" is not empty 
					And I verify if response value "/0/numberOrder" is not empty 
					And I verify if response value "/0/customerId" is not empty 
					And I verify if response value "/0/products/0/productPriceId" is not empty 
					And I verify if response value "/0/products/0/productId" is not empty 
					And Response code must be 200 
					
					
				@Positivo 
				Scenario: Consultar pedido 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?customer-id=${customerId}&start-date=2019-08-15" 
					Then I get response body 
					And I get response status code 
					And I verify if response value "/0/statusOrder" is not empty 
					And I verify if response value "/0/orderId" is not empty 
					And I verify if response value "/0/nameCustomer" is not empty 
					And I verify if response value "/0/valueOrder" is not empty 
					And I verify if response value "/0/storeId" is not empty 
					And I verify if response value "/0/products/0/price" is not empty 
					And I verify if response value "/0/products/0/quantity" is not empty 
					And I verify if response value "/0/acknowledge" is not empty 
					And I verify if response value "/0/salesChannel" is not empty 
					And I verify if response value "/0/address/stateAbbreviation" is not empty 
					And I verify if response value "/0/address/address" is not empty 
					And I verify if response value "/0/requestDate" is not empty 
					And I verify if response value "/0/numberOrder" is not empty 
					And I verify if response value "/0/customerId" is not empty 
					And I verify if response value "/0/products/0/productPriceId" is not empty 
					And I verify if response value "/0/products/0/productId" is not empty 
					And Response code must be 200 
					
					
				@Positivo 
				Scenario: Consultar pedido 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?customer-id=${customerId}&end-date=2019-10-15" 
					Then I get response body 
					And I get response status code 
					And I verify if response value "/0/statusOrder" is not empty 
					And I verify if response value "/0/orderId" is not empty 
					And I verify if response value "/0/nameCustomer" is not empty 
					And I verify if response value "/0/valueOrder" is not empty 
					And I verify if response value "/0/storeId" is not empty 
					And I verify if response value "/0/products/0/price" is not empty 
					And I verify if response value "/0/products/0/quantity" is not empty 
					And I verify if response value "/0/acknowledge" is not empty 
					And I verify if response value "/0/salesChannel" is not empty 
					And I verify if response value "/0/address/stateAbbreviation" is not empty 
					And I verify if response value "/0/address/address" is not empty 
					And I verify if response value "/0/requestDate" is not empty 
					And I verify if response value "/0/numberOrder" is not empty 
					And I verify if response value "/0/customerId" is not empty 
					And I verify if response value "/0/products/0/productPriceId" is not empty 
					And I verify if response value "/0/products/0/productId" is not empty 
					And Response code must be 200 
					
					
				@Positivo 
				Scenario: Consultar pedido 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?customer-id=${customerId}&origin=0800" 
					Then I get response body 
					And I get response status code 
					And I verify if response value "/0/statusOrder" is not empty 
					And I verify if response value "/0/orderId" is not empty 
					And I verify if response value "/0/nameCustomer" is not empty 
					And I verify if response value "/0/valueOrder" is not empty 
					And I verify if response value "/0/storeId" is not empty 
					And I verify if response value "/0/products/0/price" is not empty 
					And I verify if response value "/0/products/0/quantity" is not empty 
					And I verify if response value "/0/acknowledge" is not empty 
					And I verify if response value "/0/salesChannel" is not empty 
					And I verify if response value "/0/address/stateAbbreviation" is not empty 
					And I verify if response value "/0/address/address" is not empty 
					And I verify if response value "/0/requestDate" is not empty 
					And I verify if response value "/0/numberOrder" is not empty 
					And I verify if response value "/0/customerId" is not empty 
					And I verify if response value "/0/products/0/productPriceId" is not empty 
					And I verify if response value "/0/products/0/productId" is not empty 
					And Response code must be 200 
					
					@Positivo 
				Scenario: Consultar pedido 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?customer-id=001q0000019QQ9dAAG&store-id=001q0000017eE9UAAU&status-order=Novo&payment-method=Dinheiro&start-date=2019-08-15&end-date=2019-08-15&origin=0800" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/0/paymentMethod" with "Dinheiro"
					And I compare response value "/0/statusOrder" with "Novo"
					And I compare response value "/0/storeId" with "001q0000017eE9UAAU"
					And I compare response value "/0/salesChannel" with "0800"
					And I verify if response value "/0/orderId" is not empty 
					And I verify if response value "/0/nameCustomer" is not empty 
					And I verify if response value "/0/valueOrder" is not empty 
					And I verify if response value "/0/products/0/price" is not empty 
					And I verify if response value "/0/products/0/quantity" is not empty 
					And I verify if response value "/0/acknowledge" is not empty 
					And I verify if response value "/0/address/stateAbbreviation" is not empty 
					And I verify if response value "/0/address/address" is not empty 
					And I verify if response value "/0/requestDate" is not empty 
					And I verify if response value "/0/numberOrder" is not empty 
					And I verify if response value "/0/customerId" is not empty 
					And I verify if response value "/0/products/0/productPriceId" is not empty 
					And I verify if response value "/0/products/0/productId" is not empty 
					And Response code must be 200 
				
					
				@Positivo 
				Scenario Outline: Consultar pedido <descricao>
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?<query>" 
					Then I get response body 
					And I get response status code
					And Response body must be "[]" 
					And Response code must be 200
					
					Examples: 
					| descricao               | query                   |
					| customerId incorreto    | customer-id=1           |
					| storeId incorreto       | customer-id=${customerId}&store-id=1              |
					| statuaOrder incorreto   | customer-id=${customerId}&status-order=Novos      |
					| paymentMethod incorreto | customer-id=${customerId}&payment-method=D        |
					| startDate futura        | customer-id=${customerId}&start-date=2019-08-20&end-date=2019-08-15 |
					| endDate passado         | customer-id=${customerId}&end-date=2010-08-15     |
					| origin incorreto        | customer-id=${customerId}&origin=08             |
					
					
					
					@Negativo 
				Scenario Outline: Consultar pedido <descricao>
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}?<query>" 
					Then I get response body 
					And I get response status code
					And I compare response value "/code" with "422.000"
					And I compare response value "/message" with "<message>"
					And Response code must be 422
					
					Examples: 
					| descricao               | query                                       | message            |
					| startDate incorreto     | customer-id=${customerId}&start-date=2019   | Invalid date: 2019 |
					| endDate incorreto       | customer-id=${customerId}&end-date=2019     | Invalid date: 2019 |