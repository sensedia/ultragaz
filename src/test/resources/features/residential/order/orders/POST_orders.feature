Feature: POST_orders.feature 
	Operação responsável por cadastrar um pedido.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	
	
@PreRequest 
Scenario: Cadastrar cliente com sucesso 
	Given System generate random number 
	And System generate random CPF 
	And I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set POST api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "customerId" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/order/v1" 
	And I save "/orders" as "endpointOrders" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "01uq0000005QTFg" as "productPriceId" 
	And I save "01uq0000005QTFg" as "productId" 
	And I save "001q0000017eE9U" as "storeId" 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set POST api endpoint as "${endpointOrders}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição sem access_token 
	Given I set request header "client_id" as "${client_id}" 
	When I set POST api endpoint as "${endpointOrders}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
	And Response code must be 401 
	
	
@Negativo 
Scenario Outline: Enviar requisição <descricao> 
	Given I set request header "client_id" as "<client_id>" 
	And I set request header "access_token" as "<access_token>" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}"}]}" 
	When I set POST api endpoint as "${endpointOrders}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "<message>" 
	And Response code must be 401 
	
	Examples: 
		| descricao                 | client_id     | access_token     | message                                                                                  |
		| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
		| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
		| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
		| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
		
		
		@Positivo 
		Scenario Outline: Cadastrar pedido <descricao> 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			And I set request body as "<body>" 
			When I set POST api endpoint as "${endpointOrders}" 
			Then I get response body 
			And I get response status code 
			And I save response header "Location" as "location" 
			And I save final value of header "Location" as "orderId<id>" 
			And I verify if response body is empty 
			And Response code must be 201 
			And I wait 2 seconds 
			
			Examples: 
				| id | descricao                                  | body                                                                                                                                                                                                                                         |
				| 1  | com sucesso                                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 2  | com nome repetido                          | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.98,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 3  | com deliveryNotes vazio                    | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 4  | com deliveryNotes nulo                     | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":null,"changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 5  | sem deliveryNotes                          | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 6  | com changeFor nulo                         | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":null,"products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 7  | sem changeFor                              | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 8  | com products.productId vazio               | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": ""}]} |
				| 9  | com products.productId nulo                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": null}]} |
				| 10 | sem products.productId                     | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}"}]} |
				| 11 | com changeFor 0                            | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":0,"products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 12 | com products.price 0                       | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":0,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
				| 13 | com products.productId inválido            | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "1"}]} |
				| 14 | com products.productId incorreto           | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "01uq0000004HTFg"}]} |
				
				
				
				@Negativo 
				Scenario: Cadastrar pedido sem body 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set POST api endpoint as "${endpointOrders}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/code" with "400.000" 
					And I compare response value "/message" with "Body Inválido" 
					And Response code must be 400 
					
				@Negativo 
				Scenario: Cadastrar pedido sem Content-Type 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "{"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}" 
					When I set POST api endpoint as "${endpointOrders}" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 415 
					
					
				@Negativo 
				Scenario Outline: Cadastrar pedido <descricao> 
					Given I set request header "Content-Type" as "application/json" 
					And I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "<body>" 
					When I set POST api endpoint as "${endpointOrders}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/code" with "<code>" 
					And I compare response value "/message" with <message> 
					And Response code must be <responseCode> 
					
					Examples: 
						| id | descricao                                  | body                                                                                                                                                                                                                                    | message                                                       | code    | responseCode |
						| 3  | com customerId vazio                       | {"customerId":"","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}                | "É necessário informar o Id da conta do cliente, 'customerId'." | 400.002 | 400          |
						| 4  | com customerId null                        | {"customerId":null,"storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}              | "É necessário informar o Id da conta do cliente, 'customerId'." | 400.002 | 400          |
						| 5  | sem customerId                             | {"storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}                                | "É necessário informar o Id da conta do cliente, 'customerId'." | 400.002 | 400          |
						| 6  | com storeId vazio                         | {"customerId":"${customerId}","storeId":"","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}              | "É necessário informar o Id do revendedor, 'storeId'."         | 400.002 | 400          |
						| 7  | com storeId nulo                          | {"customerId":"${customerId}","storeId":null,"paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}            | "É necessário informar o Id do revendedor, 'storeId'."         | 400.002 | 400          |
						| 8  | sem storeId                               | {"customerId":"${customerId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}                            | "É necessário informar o Id do revendedor, 'storeId'."         | 400.002 | 400          |
						| 9  | com paymentMethod vazio                    | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}           | "Valor inválido informado."    | 400.001 | 400          |
						| 10 | com paymentMethod nulo                     | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":null,"deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}         | "Valor inválido informado."    | 400.001 | 400          |
						| 11 | sem paymentMethod                          | {"customerId":"${customerId}","storeId":"${storeId}","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}                              | "Valor inválido informado."    | 400.001 | 400          |
						| 15 | com changeFor vazio                        | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}      | "Valor inválido informado."                                     | 400.001 | 400          |
						| 18 | com products vazio                         | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[]}                                                                                              | "É necessário informar a lista de products do pedido."        | 400.004 | 400          |
						| 19 | com products nulo                          | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":null}                                                                                            | "É necessário informar a lista de products do pedido."        | 400.004 | 400          |
						| 20 | sem products                               | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100"}                                                                                                            | "É necessário informar a lista de products do pedido."        | 400.004 | 400          |
						| 21 | com products.price vazio                   | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":"","quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}   | "Valor inválido informado."                                     | 400.001 | 400          |
						| 22 | com products.price nulo                    | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":null,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Os Produtos do pedido devem ter um preço unitário."            | 422.002 | 422          |
						| 23 | sem products.price                         | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}              | "Os Produtos do pedido devem ter um preço unitário."            | 422.002 | 422          |
						| 24 | com products.productPriceId vazio          | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"", "productId": "${productId}"}]}                    | "Não foi encontrado o Catálogo de Preço da Entrada da Tabela de Preços informada : " | 422.000 | 422          |
						| 25 | com products.productPriceId nulo           | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":null, "productId": "${productId}"}]}                  | "Não foi encontrado o Catálogo de Preço da Entrada da Tabela de Preços informada : null" | 422.000 | 422          |
						| 26 | sem products.productPriceId                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1, "productId": "${productId}"}]}                                        | "Não foi encontrado o Catálogo de Preço da Entrada da Tabela de Preços informada : null" | 422.000 | 422          |
						| 30 | com products.quantity vazio                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":"","productPriceId":"${productPriceId}", "productId": "${productId}"}]}  | "Valor inválido informado."       | 400.001 | 400          |
						| 31 | com products.quantity nulo                 | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":null,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Campos obrigatórios ausente: quantity."       | 400.003 | 400          |
						| 32 | sem products.quantity                      | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}                | "Campos obrigatórios ausente: quantity."       | 400.003 | 400          |
						| 33 | com customerId longo                       | {"customerId":"${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido para o identificador."       | 400.002 | 400          |								
						| 34 | com customerId inválido                    | {"customerId":"1","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido para o identificador."       | 400.002 | 400          |
						| 35 | com customerId incorreto                   | {"customerId":"001q0000019QQ8d","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |  "O Cliente não possui contato relacionado."       | 422.000 | 422          |				
						| 36 | com storeId inválido                      | {"customerId":"${customerId}","storeId":"1","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido para o identificador."       | 400.002 | 400          |
						| 37 | com storeId incorreto                     | {"customerId":"${customerId}","storeId":"001q0000017eE8U","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 38 | com paymentMethod incorreto para changeFor | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiros","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 39 | com paymentMethod incorreto                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiros","deliveryNote":"Testes","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 40 | com deliveryNotes longo                    | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. Delivery Note long. ","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 42 | com changeFor longo                        | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"2321321321893712837129372198379218738173981738173712893719837219837892173982173981273817382173981279372198371298372198378192738217398271983721837219837219371298372198739821379218378921371289372891739218732813792817391287321987381927","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 43 | com changeFor texto                        | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"abobrinha","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 45 | com products.price longo                   | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":"8138279372189372183791287398127398127387129387218937218937817381729372183728173981","quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 46 | com products.price texto                   | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":"abobrinha","quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 47 | com products.productPriceId inválido       | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"1", "productId": "${productId}"}]} | "Não foi encontrado o Catálogo de Preço da Entrada da Tabela de Preços informada : 1" | 422.000 | 422          |
						| 48 | com products.productPriceId incorreto      | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"01uq0000006QTFh", "productId": "${productId}"}]} | "Não foi encontrado o Catálogo de Preço da Entrada da Tabela de Preços informada : 01uq0000006QTFh" | 422.000 | 422          |
						| 51 | com products.quantity 0                    | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":0,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Não é possível salvar products de pedido com quantity zero."       | 422.001 | 422          |
						| 52 | com products.quantity string               | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":"abobrinha","productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 53 | com products.quantity longo                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":"132183792187328173981273928173171321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821731321837921873281739812739281731723821732382173","productPriceId":"${productPriceId}", "productId": "${productId}"}]} | "Valor inválido informado."       | 400.001 | 400          |
						| 54 | com body vazio                             | {} | "É necessário informar a lista de products do pedido."        | 400.004 | 400          |