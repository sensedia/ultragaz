Feature: PATCH_orders_{orderId}.feature 
	Operação responsável por alterar um pedido.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	
	
@PreRequest 
Scenario: Cadastra cliente com sucesso 
	Given System generate random number 
	And System generate random CPF 
	And I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set POST api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "customerId" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/order/v1" 
	And I save "/orders" as "endpointOrders" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "01uq0000005QTFg" as "productPriceId" 
	And I save "01uq0000005QTFg" as "productId" 
	And I save "001q0000017eE9U" as "storeId"
	
	
@PreRequest 
Scenario Outline: Cadastrar pedido <descricao> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "<body>" 
	When I set POST api endpoint as "${endpointOrders}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "orderId<id>" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	Examples: 
		| id | descricao                                  | body                                                                                                                                                                                                                                         |
		| 1  | com sucesso                                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 2  | com sucesso                                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 3  | com sucesso                                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 4  | com sucesso                                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 5  | com sucesso                                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		
		
		@Negativo 
		Scenario: Enviar requisição sem client_id 
			When I set PATCH api endpoint as "${endpointOrders}/${orderId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario: Enviar requisição sem access_token 
			Given I set request header "client_id" as "${client_id}" 
			When I set PATCH api endpoint as "${endpointOrders}/${orderId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario Outline: Enviar requisição <descricao> 
			Given I set request header "client_id" as "<client_id>" 
			And I set request header "access_token" as "<access_token>" 
			And I set request header "Content-Type" as "application/json" 
			And I set request body as "{"statusOrder":"RECEBIDO","serviceNote":"string"}" 
			When I set PATCH api endpoint as "${endpointOrders}/${orderId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "<message>" 
			And Response code must be 401 
			
			Examples: 
				| descricao                 | client_id     | access_token     | message                                                                                  |
				| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
				| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
				| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
				| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
				
				
				@Negativo 
				Scenario: Alterar pedido sem body 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set PATCH api endpoint as "${endpointOrders}/${orderId1}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/code" with "400.000" 
					And I compare response value "/message" with "Body Inválido" 
					And Response code must be 400 
					
					
				@Negativo 
				Scenario: Alterar pedido sem Content-Type 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "{"statusOrder":"RECEBIDO","serviceNote":"string"}" 
					When I set PATCH api endpoint as "${endpointOrders}/${orderId1}" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 415 
					
					
				@Positivo 
				Scenario Outline: Alterar pedido <descricao> 
					Given I set request header "Content-Type" as "application/json" 
					And I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "<body>" 
					When I set PATCH api endpoint as "${endpointOrders}/${orderId1<id>}" 
					Then I get response body 
					And I get response status code 
					And Response code must be 204 
					
					Examples: 
						| id | descricao                                         | body                                                              |
						| 2  | com serviceNote vazio                             | {"statusOrder":"RECEBIDO","serviceNote":""}                       |
						| 3  | com serviceNote nulo                              | {"statusOrder":"RECEBIDO","serviceNote":null}                     |
						| 4  | com serviceNote vazio                             | {"statusOrder":"RECEBIDO","serviceNote":"Service Note"}           |
						| 5  | sem serviceNote                                   | {"statusOrder":"ENTREGUE"}                                        |
						
						
						@Negativo 
						Scenario Outline: Alterar pedido <descricao> 
							Given I set request header "Content-Type" as "application/json" 
							And I set request header "client_id" as "${client_id}" 
							And I set request header "access_token" as "${access_token}" 
							And I set request body as "<body>" 
							When I set PATCH api endpoint as "${endpointOrders}/${orderId1}" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/code" with "<code>" 
							And I compare response value "/message" with <message> 
							And Response code must be <responseCode> 
							
							Examples: 
								| id | descricao                                  | body                                                                                                                                                                                                                                    | message                                                       | code    | responseCode |
								| 1  | com body vazio                             | {}                                                         | "O campo statusOrder é obrigatório." | 400.001 | 400 |
								| 2  | sem statusOrder                            | {"serviceNote": "string"}                                  | "O campo statusOrder é obrigatório." | 400.001 | 400 |
								| 3  | com statusOrder vazio                      | {"statusOrder": "","serviceNote": "string"}                |  "Valor inválido informado para o campo statusOrder."            | 400.002 | 400 |
								| 4  | com statusOrder nulo                       | {"statusOrder": null,"serviceNote": "string"}              | "O campo statusOrder é obrigatório." | 400.001 | 400 |
								| 5  | com statusOrder inválido                   | {"statusOrder": "status","serviceNote": "string"}          | "Valor inválido informado para o campo statusOrder."            | 400.002 | 400 |
								| 6  | com serviceNote entregue                   | {"statusOrder":"RECEBIDO","serviceNote":"Service Note"}    | "Este pedido já foi entregue."                                  | 422.000 | 422 |