Feature: GET_orders_{orderId}.feature 
	Consulta de um pedido no segmento domiciliar da Ultragaz.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set GET api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	
	
@PreRequest 
Scenario: Cadastrar cliente com sucesso 
	Given System generate random number 
	And System generate random CPF 
	And I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set POST api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "customerId" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/order/v1" 
	And I save "/orders" as "endpointOrders" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "01uq0000005QTFg" as "productPriceId" 
	And I save "01uq0000005QTFg" as "productId" 
	And I save "001q0000017eE9U" as "storeId" 
	
	
@PreRequest 
Scenario Outline: Cadastrar pedido <descricao> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "<body>" 
	When I set POST api endpoint as "${endpointOrders}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "orderId<id>" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	Examples: 
		| id | descricao                                  | body                                                                                                                                                                                                                                         |
		| 1  | com sucesso                                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 2  | com nome repetido                          | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 3  | com deliveryNotes vazio                    | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 4  | com deliveryNotes nulo                     | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":null,"changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 5  | sem deliveryNotes                          | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 6  | com changeFor nulo                         | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":null,"products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 7  | sem changeFor                              | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 8  | com products.productId vazio               | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": ""}]} |
		| 9  | com products.productId nulo                | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": null}]} |
		| 10 | sem products.productId                     | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}"}]} |
		| 11 | com changeFor 0                            | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":0,"products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 12 | com products.price 0                       | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":0,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]} |
		| 13 | com products.productId inválido            | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "1"}]} |
		| 14 | com products.productId incorreto           | {"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}", "productId": "01uq0000004HTFg"}]} |
		
		
		@Negativo 
		Scenario: Enviar requisição sem client_id 
			When I set GET api endpoint as "${endpointOrders}/${orderId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario: Enviar requisição sem access_token 
			Given I set request header "client_id" as "${client_id}" 
			When I set GET api endpoint as "${endpointOrders}/${orderId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario Outline: Enviar requisição <descricao> 
			Given I set request header "client_id" as "<client_id>" 
			And I set request header "access_token" as "<access_token>" 
			When I set GET api endpoint as "${endpointOrders}/${orderId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "<message>" 
			And Response code must be 401 
			
			Examples: 
				| descricao                 | client_id     | access_token     | message                                                                          |
				| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
				| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.    |
				| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required APP in the request, identified by HEADER access_token. |
				| com access_token vazio    | ${client_id}  |                  | Could not find a required APP in the request, identified by HEADER access_token. |
				
				
				@Negativo 
				Scenario: Consultar pedido com customerId inexistente 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set GET api endpoint as "${endpointOrders}/batatinhax" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 404 
					
					
				@Positivo 
				Scenario Outline: Consultar pedido <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointOrders}/${orderId<id>}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/customerId" with "${customerId}" 
					And I compare response value "/orderId" with "${orderId<id>}" 
					And I compare response value "/nameCustomer" with "Aline ${random}" 
					And I compare response value "/paymentMethod" with "Dinheiro" 
					And I compare response value "/deliveryNote" with "Testes" 
					And I compare response value "/changeFor" with "100.99" 
					And I compare response value "/valueOrder" with "10.99" 
					And I compare response value "/statusOrder" with "Novo" 
					And I compare response value "/products/0/price" with "10.99" 
					And I compare response value "/products/0/quantity" with "1" 
					And I compare response value "/acknowledge" with "false" 
					And I compare response value "/salesChannel" with "0800" 
					And I compare response value "/address/stateAbbreviation" with "SP" 
					And I compare response value "/address/address" with "Rua Domingõs Conradô" 
					And I compare response value "/address/addressReference" with "Praça dõ Bom Retiro" 
					And I compare response value "/address/numberAddress" with "528SC" 
					And I compare response value "/address/city" with "Sumaré" 
					And I compare response value "/address/additionAddress" with "Jardim Bom Retirõ" 
					And I compare response value "/serviceChannel" with "0800" 
					And I verify if response value "/requestDate" is not empty 
					And I verify if response value "/numberOrder" is not empty 
					And I compare response value "/storeId" with "${storeId}" 
					And I compare response value "/products/0/productPriceId" with "${productPriceId}" 
					And I compare response value "/products/0/productId" with "${productId}" 
					And Response code must be 200 
					
					Examples: 
						| id | descricao                                  | 
						| 1  | com sucesso                                | 
						| 2  | com nome repetido                          | 
						| 8  | com products.productId vazio               | 
						| 9  | com products.productId nulo                | 
						| 10 | sem products.productId                     | 
						| 13 | com products.productId inválido            | 
						| 14 | com products.productId incorreto           |
						
						
						@Positivo 
						Scenario Outline: Consultar pedido <descricao> 
							Given I set request header "client_id" as "${client_id}" 
							And I set request header "access_token" as "${access_token}" 
							And I set request header "Content-Type" as "application/json" 
							When I set GET api endpoint as "${endpointOrders}/${orderId<id>}" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/customerId" with "${customerId}" 
							And I compare response value "/orderId" with "${orderId<id>}" 
							And I compare response value "/nameCustomer" with "Aline ${random}" 
							And I compare response value "/paymentMethod" with "Dinheiro" 
							And I verify if response value "/deliveryNote" is empty 
							And I compare response value "/changeFor" with "100.99" 
							And I compare response value "/valueOrder" with "10.99" 
							And I compare response value "/statusOrder" with "Novo" 
							And I compare response value "/products/0/price" with "10.99" 
							And I compare response value "/products/0/quantity" with "1" 
							And I compare response value "/acknowledge" with "false" 
							And I compare response value "/salesChannel" with "0800" 
							And I compare response value "/address/stateAbbreviation" with "SP" 
							And I compare response value "/address/address" with "Rua Domingõs Conradô" 
							And I compare response value "/address/addressReference" with "Praça dõ Bom Retiro" 
							And I compare response value "/address/numberAddress" with "528SC" 
							And I compare response value "/address/city" with "Sumaré" 
							And I compare response value "/address/additionAddress" with "Jardim Bom Retirõ" 
							And I compare response value "/serviceChannel" with "0800" 
							And I verify if response value "/requestDate" is not empty 
							And I verify if response value "/numberOrder" is not empty 
							And I compare response value "/storeId" with "${storeId}" 
							And I compare response value "/products/0/productPriceId" with "${productPriceId}" 
							And I compare response value "/products/0/productId" with "${productId}" 
							And Response code must be 200 
							
							Examples: 
								| id | descricao                                  | 
								| 3  | com deliveryNotes vazio                    | 
								| 4  | com deliveryNotes nulo                     | 
								| 5  | sem deliveryNotes                          | 
								
								
								@Positivo 
								Scenario Outline: Consultar pedido <descricao> 
									Given I set request header "client_id" as "${client_id}" 
									And I set request header "access_token" as "${access_token}" 
									And I set request header "Content-Type" as "application/json" 
									When I set GET api endpoint as "${endpointOrders}/${orderId<id>}" 
									Then I get response body 
									And I get response status code 
									And I compare response value "/customerId" with "${customerId}" 
									And I compare response value "/orderId" with "${orderId<id>}" 
									And I compare response value "/nameCustomer" with "Aline ${random}" 
									And I compare response value "/paymentMethod" with "Dinheiro" 
									And I compare response value "/deliveryNote" with "Testes" 
									And I verify if response value "/changeFor" is empty 
									And I compare response value "/valueOrder" with "10.99" 
									And I compare response value "/statusOrder" with "Novo" 
									And I compare response value "/products/0/price" with "10.99" 
									And I compare response value "/products/0/quantity" with "1" 
									And I compare response value "/acknowledge" with "false" 
									And I compare response value "/salesChannel" with "0800" 
									And I compare response value "/address/stateAbbreviation" with "SP" 
									And I compare response value "/address/address" with "Rua Domingõs Conradô" 
									And I compare response value "/address/addressReference" with "Praça dõ Bom Retiro" 
									And I compare response value "/address/numberAddress" with "528SC" 
									And I compare response value "/address/city" with "Sumaré" 
									And I compare response value "/address/additionAddress" with "Jardim Bom Retirõ" 
									And I compare response value "/serviceChannel" with "0800" 
									And I verify if response value "/requestDate" is not empty 
									And I verify if response value "/numberOrder" is not empty 
									And I compare response value "/storeId" with "${storeId}" 
									And I compare response value "/products/0/productPriceId" with "${productPriceId}" 
									And I compare response value "/products/0/productId" with "${productId}" 
									And Response code must be 200 
									
									Examples: 
										| id | descricao                                  | 
										| 6  | com changeFor nulo                         | 
										| 7  | sem changeFor                              | 
										
										
										@Positivo 
										Scenario Outline: Consultar pedido <descricao> 
											Given I set request header "client_id" as "${client_id}" 
											And I set request header "access_token" as "${access_token}" 
											And I set request header "Content-Type" as "application/json" 
											When I set GET api endpoint as "${endpointOrders}/${orderId<id>}" 
											Then I get response body 
											And I get response status code 
											And I compare response value "/customerId" with "${customerId}" 
											And I compare response value "/orderId" with "${orderId<id>}" 
											And I compare response value "/nameCustomer" with "Aline ${random}" 
											And I compare response value "/paymentMethod" with "Dinheiro" 
											And I compare response value "/deliveryNote" with "Testes" 
											And I compare response value "/changeFor" with "0" 
											And I compare response value "/valueOrder" with "10.99" 
											And I compare response value "/statusOrder" with "Novo" 
											And I compare response value "/products/0/price" with "10.99" 
											And I compare response value "/products/0/quantity" with "1" 
											And I compare response value "/acknowledge" with "false" 
											And I compare response value "/salesChannel" with "0800" 
											And I compare response value "/address/stateAbbreviation" with "SP" 
											And I compare response value "/address/address" with "Rua Domingõs Conradô" 
											And I compare response value "/address/addressReference" with "Praça dõ Bom Retiro" 
											And I compare response value "/address/numberAddress" with "528SC" 
											And I compare response value "/address/city" with "Sumaré" 
											And I compare response value "/address/additionAddress" with "Jardim Bom Retirõ" 
											And I compare response value "/serviceChannel" with "0800" 
											And I verify if response value "/requestDate" is not empty 
											And I verify if response value "/numberOrder" is not empty 
											And I compare response value "/storeId" with "${storeId}" 
											And I compare response value "/products/0/productPriceId" with "${productPriceId}" 
											And I compare response value "/products/0/productId" with "${productId}" 
											And Response code must be 200 
											
											Examples: 
												| id | descricao                                  | 
												| 11 | com changeFor 0                            | 
												
												
												@Positivo 
												Scenario Outline: Consultar pedido <descricao> 
													Given I set request header "client_id" as "${client_id}" 
													And I set request header "access_token" as "${access_token}" 
													And I set request header "Content-Type" as "application/json" 
													When I set GET api endpoint as "${endpointOrders}/${orderId<id>}" 
													Then I get response body 
													And I get response status code 
													And I compare response value "/customerId" with "${customerId}" 
													And I compare response value "/orderId" with "${orderId<id>}" 
													And I compare response value "/nameCustomer" with "Aline ${random}" 
													And I compare response value "/paymentMethod" with "Dinheiro" 
													And I compare response value "/deliveryNote" with "Testes" 
													And I compare response value "/changeFor" with "100.99" 
													And I compare response value "/valueOrder" with "10.99" 
													And I compare response value "/statusOrder" with "Novo" 
													And I compare response value "/products/0/price" with "0" 
													And I compare response value "/products/0/quantity" with "1" 
													And I compare response value "/acknowledge" with "false" 
													And I compare response value "/salesChannel" with "0800" 
													And I compare response value "/address/stateAbbreviation" with "SP" 
													And I compare response value "/address/address" with "Rua Domingõs Conradô" 
													And I compare response value "/address/addressReference" with "Praça dõ Bom Retiro" 
													And I compare response value "/address/numberAddress" with "528SC" 
													And I compare response value "/address/city" with "Sumaré" 
													And I compare response value "/address/additionAddress" with "Jardim Bom Retirõ" 
													And I compare response value "/serviceChannel" with "0800" 
													And I verify if response value "/requestDate" is not empty 
													And I verify if response value "/numberOrder" is not empty 
													And I compare response value "/storeId" with "${storeId}" 
													And I compare response value "/products/0/productPriceId" with "${productPriceId}" 
													And I compare response value "/products/0/productId" with "${productId}" 
													And Response code must be 200 
													
													Examples: 
														| id | descricao                                  | 
														| 12 | com products.price 0                       | 