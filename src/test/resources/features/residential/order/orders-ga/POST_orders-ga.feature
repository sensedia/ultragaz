Feature: POST_orders-ga.feature
  Operação responsável por cadastrar um pedido de abastecimento no GA.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "16aa22fe-4aef-3900-b559-642745e09178" and APP secretKey = "d24168f6-0b1e-35a3-b607-5c305e7b19f3"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    And I use api name as "/dev/residential/order/v1"
    And I save "/stores" as "endpointStores"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "87e96209-a767-3426-8c4f-a1c9c8eae05a" as "client_secret"
    And I save "/orders-ga" as "endpointOrders"
    And I save "0013000001P4aQH" as "storeId"
    And I save "a0k300000032VrbAAE" as "supplyBrancheId"
    And I save "ENTREGA" as "serviceType"
    And I save "0030M00001zlgsV" as "contactId"
    And I save "GAS_ENVASADO" as "orderType"
    And I save "1" as "quantity"
    And I save "0110027" as "code"
    And I add 3 days on local date

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set POST api endpoint as "${endpointOrders}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set POST api endpoint as "${endpointOrders}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401
    
  @Negativo
  Scenario: Cadastrar pedido sem body
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    When I set POST api endpoint as "${endpointOrders}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with "422.001"
    And I compare response value "/message" with "No content to map to Object due to end of input"
    And Response code must be 422

  @Negativo
  Scenario: Cadastrar pedido sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}"
    When I set POST api endpoint as "${endpointOrders}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Enviar requisição <descricao>
    Given I set request header "client_id" as "<client_id>"
    And I set request header "access_token" as "<access_token>"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}"
    When I set POST api endpoint as "${endpointOrders}"
    Then I get response body
    And I get response status code
    And Response body must be "<message>"
    And Response code must be 401

    Examples: 
      | descricao                 | client_id     | access_token     | message                                                                                  |
      | com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
      | com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |

  @Positivo
  Scenario Outline: Cadastrar pedido <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointOrders}"
    Then I get response body
    And I get response status code
    #And I save response header "Location" as "location"
    #And I save final value of header "Location" as "orderId<id>"
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | descricao                 | body                                                                                                                                                                                                                                                                                                                                                      |
      | completo                  | {"observation":"Observação, observação. Observação, observação. Observação, observação. Observação, observação.","storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]} |
      | com observation vazio     | {"observation":"","storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                |
      | com observation nulo      | {"observation":null,"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                              |
      | sem observation           | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                 |
      | com dois products         | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"},{"code":"0110035","quantity":"50"}]}                                                                              |
      | com serviceType vazio     | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                               |
      | com serviceType nulo      | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":null,"contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                             |
      | com serviceType inválido  | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}1","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                |
      | com serviceType ENTREGA   | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"ENTREGA","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                        |
      | com serviceType RETIRA    | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"RETIRA","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                         |
      | com serviceType incorreto | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"1231231","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                        |
      | sem serviceType           | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                                                |
      | com contactId nulo        | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":null,"orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                           |
      | sem contactId             | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                                                                                                                            |
      | com products.quantity 100 | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":100}]}                                                                                                                           |

  @Negativo
  Scenario Outline: Cadastrar pedido <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointOrders}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And I compare response value "/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao                     | body                                                                                                                                                                                                                                       | message                                                                                   | code      |
      | com storeId vazio             | {"storeId":"","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}            | "Invalid id: "                                                                            | "422.001" |
      | com storeId nulo              | {"storeId":null,"supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}          | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com storeId inválido          | {"storeId":"${storeId}1","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]} | "Invalid id: 0013000001P4aQH1"                                                            | "422.001" |
      | com storeId incorreto         | {"storeId":"131321d","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}     | "Invalid id: 131321d"                                                                     | "422.001" |
      | sem storeId                   | {"supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                         | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com supplyBrancheId vazio     | {"storeId":"${storeId}","supplyBrancheId":"","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                    | "Invalid id: "                                                                            | "422.001" |
      | com supplyBrancheId nulo      | {"storeId":"${storeId}","supplyBrancheId":null,"serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                  | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com supplyBrancheId inválido  | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}1","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]} | "Invalid id: a0k300000032VrbAAE1"                                                         | "422.001" |
      | com supplyBrancheId incorreto | {"storeId":"${storeId}","supplyBrancheId":"13123131","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}            | "Invalid id: 13123131"                                                                    | "422.001" |
      | sem supplyBrancheId           | {"storeId":"${storeId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                                         | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com contactId vazio           | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}              | "Invalid id: "                                                                            | "422.001" |
      | com contactId inválido        | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}1","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]} | "Invalid id: 0030M00001zlgsV1"                                                            | "422.001" |
      | com contactId incorreto       | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"1231231","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}       | "Invalid id: 1231231"                                                                     | "422.001" |
      | com orderType vazio           | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}              | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com orderType nulo            | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":null,"deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}            | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com orderType inválido        | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}1","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]} | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com orderType incorreto       | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"1231231","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}       | "Attempt to de-reference a null object"                                                   | "422.001" |
      | sem orderType                 | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"${quantity}"}]}                             | "Attempt to de-reference a null object"                                                   | "422.001" |
      #| com dueDate vazio         | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"","products":[{"code":"${code}","quantity":"${quantity}"}]}                 | "Invalid format: \\"\\" at [line:1, column:172]"                                          | "422.001" |
      | com dueDate nulo              | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":null,"products":[{"code":"${code}","quantity":"${quantity}"}]}          | "Attempt to de-reference a null object"                                                   | "422.001" |
      #| com dueDate inválido      | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}1","products":[{"code":"${code}","quantity":"${quantity}"}]} | "Invalid format: \\"2020-05-111\\" is malformed at \\"1\\" at [line:1, column:172]"       | "422.001" |
      | com dueDate incorreto         | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"1231231","products":[{"code":"${code}","quantity":"${quantity}"}]}     | "A data informada ultrapassa o prazo para gerar pedido com antecedência. Prazo: 10 dias"  | "422.001" |
      | sem dueDate                   | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","products":[{"code":"${code}","quantity":"${quantity}"}]}                              | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com products vazio            | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[]}                                             | "Informe a quantidade de pelo menos um item.;Informe a quantidade de pelo menos um item." | "422.001" |
      | com products nulo             | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":null}                                           | "Attempt to de-reference a null object"                                                   | "422.001" |
      | sem products                  | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}"}                                                           | "Attempt to de-reference a null object"                                                   | "422.001" |
      | com products.code vazio       | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"","quantity":"${quantity}"}]}         | "Informe a quantidade de pelo menos um item.;Informe a quantidade de pelo menos um item." | "422.001" |
      | com products.code nulo        | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":null,"quantity":"${quantity}"}]}       | "Produto da filial não encontrado."                                                       | "422.001" |
      | com products.code inválido    | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}1","quantity":"${quantity}"}]} | "Informe a quantidade de pelo menos um item.;Informe a quantidade de pelo menos um item." | "422.001" |
      | com products.code incorreto   | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"132321","quantity":"${quantity}"}]}   | "Informe a quantidade de pelo menos um item.;Informe a quantidade de pelo menos um item." | "422.001" |
      | sem products.code             | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"quantity":"${quantity}"}]}                   | "Produto da filial não encontrado."                                                       | "422.001" |
      #| com products.quantity vazio    | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":""}]}             | "For input string: \\"\\" at [line:1, column:231]"                                        | "422.001" |
      | com products.quantity nulo    | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":null}]}           | "Informe a quantidade de pelo menos um item.;Informe a quantidade de pelo menos um item." | "422.001" |
      #| com products.quantity inválido | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":"a"}]}            | "For input string: \\"a\\" at [line:1, column:231]"                                       | "422.001" |
      | com products.quantity 0       | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}","quantity":0}]}              | "Informe a quantidade de pelo menos um item.;Informe a quantidade de pelo menos um item." | "422.001" |
      | sem products.quantity         | {"storeId":"${storeId}","supplyBrancheId":"${supplyBrancheId}","serviceType":"${serviceType}","contactId":"${contactId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"${code}"}]}                           | "Informe a quantidade de pelo menos um item.;Informe a quantidade de pelo menos um item." | "422.001" |
