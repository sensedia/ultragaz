Feature: GET_orders-ga.feature
  Operação responsável por retornar os pedidos de abastecimento de uma loja/revenda.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" and APP secretKey = "4f9971fd-5892-3c51-a982-cf780f96acd9"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/order/v1"
    And I save "/orders-ga" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0010M00001TmUNf" as "storeId"
    And I save "2020-11-10" as "startDate"
    And I save "2020-11-11" as "endDate"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <descricao>
    Given I set request header "client_id" as "<client_id>"
    And I set request header "access_token" as "<access_token>"
    When I set GET api endpoint as "${endpoint}"
    Then I get response body
    And I get response status code
    And Response body must be "<message>"
    And Response code must be 401

    Examples: 
      | descricao                 | client_id     | access_token     | message                                                                                  |
      | com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
      | com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |

  @Negativo
  Scenario Outline: Consultar pedido <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404

    Examples: 
      | descricao                     | query                                                            |
      | store-id vazio                | ?store-id=                                                       |
      | store-id incorreto            | ?store-id=${storeId}1                                            |
      | end-date ano incorreto        | ?store-id=${storeId}&end-date=202-12-30                          |
      | end-date menor que start-date | ?store-id=${storeId}&end-date=${startDate}&start-date=${endDate} |

  @Negativo
  Scenario Outline: Consultar pedido com <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/code" with "400.001"
    And I compare response value "/0/message" with <message>
    And Response code must be 400

    Examples: 
      | descricao              | query | message                       |
      | store-id não informado |       | "Field store-id is required." |

  @Positivo
  Scenario Outline: Consultar pedido <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/1/id" is not empty
    And I verify if response value "/1/number" is not empty
    And I verify if response value "/1/occupationStatus" is not empty
    And I verify if response value "/1/cycleStatus" is not empty
    And I verify if response value "/1/status" is not empty
    And I verify if response value "/1/supplyBranche" is not empty
    And I verify if response value "/1/supplyBrancheId" is not empty
    And I verify if response value "/1/source" is not empty
    And I verify if response value "/1/serviceType" is not empty
    And I verify if response value "/1/deliveryDate" is not empty
    And I verify if response value "/1/cutDate" is not empty
    And I verify if response value "/1/products/0/code" is not empty
    And I verify if response value "/1/products/0/quantity" is not empty
    And Response code must be 200

    Examples: 
      | description              | query                                                            |
      | store-id                 | ?store-id=${storeId}                                             |
      | start-date vazio         | ?store-id=${storeId}&start-date=                                 |
      | start-date dia incorreto | ?store-id=${storeId}&start-date=2020-12-33                       |
      | start-date mes incorreto | ?store-id=${storeId}&start-date=2020-13-30                       |
      | start-date ano incorreto | ?store-id=${storeId}&start-date=202-12-30                        |
      | start-date inválido      | ?store-id=${storeId}&start-date=a                                |
      | start-date               | ?store-id=${storeId}&start-date=${startDate}                     |
      | end-date vazio           | ?store-id=${storeId}&end-date=                                   |
      | end-date dia incorreto   | ?store-id=${storeId}&end-date=2020-12-33                         |
      | end-date mes incorreto   | ?store-id=${storeId}&end-date=2020-13-30                         |
      | end-date inválido        | ?store-id=${storeId}&end-date=a                                  |
      | end-date                 | ?store-id=${storeId}&end-date=${endDate}                         |
      | end-date e start-date    | ?store-id=${storeId}&end-date=${endDate}&start-date=${startDate} |
