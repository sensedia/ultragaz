Feature: PATCH_orders-ga_{orderId}.feature
  Operação responsável por atualizar um pedido de abastecimento no GA.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "16aa22fe-4aef-3900-b559-642745e09178" and APP secretKey = "d24168f6-0b1e-35a3-b607-5c305e7b19f3"
    And I read file body "/features/access-token/body/access_token.json"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev/residential"
    And I use api name as "/order/v1"
    And I save "/orders-ga" as "endpointOrders"
    And I save "16aa22fe-4aef-3900-b559-642745e09178" as "client_id"
    And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret"
    And I save "0013000001P448vAAB" as "storeId"
    And I save "a0k300000032VrbAAE" as "supplyBrancheId"
    And I save "ENTREGA" as "serviceType"
    And I save "0030M00001zlgsV" as "contactId"
    And I save "GAS_ENVASADO" as "orderType"
    And I save "1" as "quantity"
    And I save "0110027" as "code"
    #And I add 3 days on local date
    And I save "2020-09-05" as "dueDate"

  @PreRequest
  Scenario Outline: Cadastrar pedido <descricao> <id>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "<body>"
    When I set POST api endpoint as "${endpointOrders}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 201

    Examples: 
      | id | descricao   | body                                                                                                                                                                                                                           |
      |  0 | com sucesso | {"storeId":"${storeId}","supplyBrancheId":"a0k300000032VqZAAU","serviceType":"ENTREGA","contactId":"0033000001w6ctHAAQ","orderType":"GAS_ENVASADO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]} |
      |  1 | com sucesso | {"storeId":"${storeId}","supplyBrancheId":"a0k300000032VqZAAU","serviceType":"ENTREGA","contactId":"0033000001w6ctHAAQ","orderType":"GAS_ENVASADO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]} |
      |  2 | com sucesso | {"storeId":"${storeId}","supplyBrancheId":"a0k300000032VqZAAU","serviceType":"ENTREGA","contactId":"0033000001w6ctHAAQ","orderType":"GAS_ENVASADO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]} |
      |  3 | com sucesso | {"storeId":"${storeId}","supplyBrancheId":"a0k300000032VqZAAU","serviceType":"ENTREGA","contactId":"0033000001w6ctHAAQ","orderType":"GAS_ENVASADO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]} |
      |  4 | com sucesso | {"storeId":"${storeId}","supplyBrancheId":"a0k300000032VqZAAU","serviceType":"ENTREGA","contactId":"0033000001w6ctHAAQ","orderType":"GAS_ENVASADO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]} |

  @PreRequest
  Scenario Outline: Consultar pedido <description>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpointOrders}<query>"
    Then I get response body
    And I get response status code
    And I save response value "/<id>/id" as "orderId<id>"
    And Response code must be 200

    Examples: 
      | id | description | query                                                          |
      |  0 | com sucesso | ?store-id=${storeId}&start-date=${dueDate}&end-date=${dueDate} |
      |  1 | com sucesso | ?store-id=${storeId}&start-date=${dueDate}&end-date=${dueDate} |
      |  2 | com sucesso | ?store-id=${storeId}&start-date=${dueDate}&end-date=${dueDate} |
      |  3 | com sucesso | ?store-id=${storeId}&start-date=${dueDate}&end-date=${dueDate} |
      |  4 | com sucesso | ?store-id=${storeId}&start-date=${dueDate}&end-date=${dueDate} |

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set PATCH api endpoint as "${endpointOrders}/${orderId0}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId0}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <descricao>
    Given I set request header "client_id" as "<client_id>"
    And I set request header "access_token" as "<access_token>"
    And I set request header "Content-Type" as "application/json"
    And I set request body as "{"statusOrder":"RECEBIDO","serviceNote":"string"}"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId0}"
    Then I get response body
    And I get response status code
    And Response body must be "<message>"
    And Response code must be 401

    Examples: 
      | descricao                 | client_id     | access_token     | message                                                                                  |
      | com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
      | com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
      | com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |

  @Negativo
  Scenario: Alterar pedido sem body
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId0}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with "400.000"
    And I compare response value "/message" with "Request body inválido."
    And Response code must be 400

  @Negativo
  Scenario: Alterar pedido sem Content-Type
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "{"cancellationReason":"Outros","cancellationReasonDetail":"Outros detalhes"}"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId0}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 415

  @Negativo
  Scenario Outline: Alterar pedido <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId1}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And I compare response value "/message" with <message>
    And Response code must be 400

    Examples: 
      | descricao                                  | body                                                                                                                                            | code      | message                                                      |
      | com cancellationReasonDetail vazio         | {"cancellationReason":"Outros","cancellationReasonDetail":""}                                                                                   | "400.001" | "Campo cancellationReasonDetail é obrigatório."              |
      | com cancellationReasonDetail nulo          | {"cancellationReason":"Outros","cancellationReasonDetail":null}                                                                                 | "400.001" | "Campo cancellationReasonDetail é obrigatório."              |
      | com cancellationReasonDetail não informado | {"cancellationReason":"Outros"}                                                                                                                 | "400.001" | "Campo cancellationReasonDetail é obrigatório."              |
      | com body vazio                             | {}                                                                                                                                              | "400.001" | "Campo products é obrigatório."                              |
      | com cancellationReason vazio               | {"cancellationReason":"","cancellationReasonDetail":"Outros"}                                                                                   | "400.001" | "Campo cancellationReason é obrigatório."                    |
      | com cancellationReason nulo                | {"cancellationReason":null,"cancellationReasonDetail":"Outros"}                                                                                 | "400.001" | "Campo cancellationReason é obrigatório."                    |
      | com cancellationReason não informado       | {"cancellationReasonDetail":"Outros"}                                                                                                           | "400.001" | "Campo cancellationReason é obrigatório."                    |
      | sem supplyBrancheId                        | {"orderType":"GAS_ENVASADO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]}                                         | "400.001" | "Campo supplyBrancheId é obrigatório."                       |
      | com supplyBrancheId vazio                  | {"supplyBrancheId":"","orderType":"GAS_ENVASADO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]}                    | "400.001" | "Campo supplyBrancheId é obrigatório."                       |
      | com supplyBrancheId nulo                   | {"supplyBrancheId":null,"orderType":"GAS_ENVASADO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]}                  | "400.001" | "Campo supplyBrancheId é obrigatório."                       |
      | sem orderType                              | {"supplyBrancheId":"a0k300000032VqZAAU","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]}                             | "400.001" | "Campo orderType é obrigatório."                             |
      | com orderType vazio                        | {"supplyBrancheId":"a0k300000032VqZAAU","orderType":"","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]}              | "400.001" | "Campo orderType é obrigatório."                             |
      | com orderType nulo                         | {"supplyBrancheId":"a0k300000032VqZAAU","orderType":null,"deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]}            | "400.001" | "Campo orderType é obrigatório."                             |
      | sem deliveryDate                           | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","products":[{"code":"0110027","quantity":"1"}]}                              | "400.001" | "Campo deliveryDate é obrigatório."                          |
      | com deliveryDate vazio                     | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"","products":[{"code":"0110027","quantity":"1"}]}            | "400.001" | "Campo deliveryDate é obrigatório."                          |
      | com deliveryDate nulo                      | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":null,"products":[{"code":"0110027","quantity":"1"}]}          | "400.001" | "Campo deliveryDate é obrigatório."                          |
      | sem products                               | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}"}                                                 | "400.001" | "Campo products é obrigatório."                              |
      | com products vazio                         | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[]}                                   | "400.001" | "Campo products não pode ser vazio."                         |
      | com products nulo                          | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":null}                                 | "400.001" | "Campo products é obrigatório."                              |
      | sem products.code                          | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"quantity":"1"}]}                   | "400.001" | "Campos products.code e products.quantity são obrigatórios." |
      | com products.code vazio                    | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"","quantity":"1"}]}         | "400.001" | "Campos products.code e products.quantity são obrigatórios." |
      | com products.code nulo                     | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":null,"quantity":"1"}]}       | "400.001" | "Campos products.code e products.quantity são obrigatórios." |
      | sem products.quantity                      | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"0110027"}]}                 | "400.001" | "Campos products.code e products.quantity são obrigatórios." |
      | com products.quantity vazio                | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":""}]}   | "400.001" | "Campos products.code e products.quantity são obrigatórios." |
      | com products.quantity nulo                 | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":null}]} | "400.001" | "Campos products.code e products.quantity são obrigatórios." |

  @Negativo
  Scenario Outline: Alterar pedido <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId4}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And I compare response value "/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao                   | body                                                                                                                                           | code      | message                             |
      | com deliveryDate retroativa | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"2020-05-11","products":[{"code":"0110027","quantity":"1"}]} | "422.001" | "O pedido não pode ser retroativo." |

  @Negativo
  Scenario Outline: Alterar pedido <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId1}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And Response code must be 422

    Examples: 
      | descricao                      | body                                                                                                                                                 | code      |
      | com supplyBrancheId inválido   | {"supplyBrancheId":"xuxu","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]}                     | "422.001" |
      | com products.code inválido     | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"0110027777777","quantity":"1"}]} | "422.001" |
      | com orderType inválido         | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}OOOOOO","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]} | "422.001" |
      | com deliveryDate inválido      | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"xuxu","products":[{"code":"0110027","quantity":"1"}]}             | "422.001" |
      | com products.quantity inválido | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"xuxu"}]}    | "422.001" |
      | com products.code inválido     | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"xuxu","quantity":"1"}]}          | "422.001" |

  @Negativo
  Scenario Outline: <descricao> pedido com orderId inexistente
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as <body>
    When I set PATCH api endpoint as "${endpointOrders}/${orderId0}1"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with <code>
    And I compare response value "/message" with <message>
    And Response code must be 422

    Examples: 
      | descricao | body                                                                                                                                             | code      | message                                                                                                |
      | Cancelar  | "{"cancellationReason":"Outros","cancellationReasonDetail":"Outros detalhes"}"                                                                   | "422.001" | "Não foi possível efetuar o cancelamento do pedido. Favor entrar em contato com a logística da filial" |
      | Atualizar | "{"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":"1"}]}" | "422.001" | "Attempt to de-reference a null object"                                                                |

  @Negativo
  Scenario Outline: Alterar pedido <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId0}"
    Then I get response body
    And I get response status code
    And I compare response value "/code" with "422.001"
    And I compare response value "/message" with "Não foi possível efetuar o cancelamento do pedido. Favor entrar em contato com a logística da filial"
    And Response code must be 422

    Examples: 
      | descricao                                                       | body                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
      | com cancellationReasonDetail ultrapassando limite de caracteres | {"cancellationReason":"Outros","cancellationReasonDetail":"Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. Outros. "} |

  @Positivo
  Scenario Outline: Alterar pedido <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | id | descricao                                         | body                                                                                                         |
      |  0 | com cancellationReazon Outros                     | {"cancellationReason":"Outros","cancellationReasonDetail":"Outros detalhes"}                                 |
      |  1 | com cancellationReazon Produto errado             | {"cancellationReason":"Produto errado","cancellationReasonDetail":"Produto errados"}                         |
      |  2 | com cancellationReazon Data de solicitação errada | {"cancellationReason":"Data de solicitação errada","cancellationReasonDetail":"Datas de solicitação errada"} |
      |  3 | com cancellationReason inválido                   | {"cancellationReason":"Outross","cancellationReasonDetail":"Outros"}                                         |

  @Positivo
  Scenario Outline: Atualizar pedido <descricao>
    Given I set request header "Content-Type" as "application/json"
    And I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    And I set request header "access_token" as "${access_token}"
    And I set request body as "<body>"
    When I set PATCH api endpoint as "${endpointOrders}/${orderId<id>}"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 204

    Examples: 
      | id | descricao                | body                                                                                                                                         |
      |  4 | para products.quantity 2 | {"supplyBrancheId":"${supplyBrancheId}","orderType":"${orderType}","deliveryDate":"${dueDate}","products":[{"code":"0110027","quantity":2}]} |
