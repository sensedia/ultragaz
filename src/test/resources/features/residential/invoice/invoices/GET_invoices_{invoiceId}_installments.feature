Feature: GET_invoices_{invoiceId}_installments.feature
  Consulta para obter dados da fatura.

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with APP clientId = "e21a3470-af98-3d69-915e-66f56e0da336" and APP secretKey = "87e96209-a767-3426-8c4f-a1c9c8eae05a"
    And I set request body as "{"grant_type":"password","username":"allyne.bender@hotmail.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com/dev"
    And I use api name as "/industrial/invoice/v1"
    And I save "/invoices" as "endpointInvoices"
    And I save "e21a3470-af98-3d69-915e-66f56e0da336" as "client_id"
    And I save "A4FD77B524192A7DE053166114AC9927" as "invoiceId"

  @Negativo
  Scenario: Enviar requisição sem client_id
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}/installments"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem client_id em Português
    Given I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}/installments"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido
    Given I set request header "client_id" as "${client_id}1"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}/installments"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição com client_id inválido em Português
    Given I set request header "client_id" as "${client_id}1"
    And I set request header "Accept-Language" as "pt-BR"
    When I set GET api endpoint as "${endpointInvoices}/${invoiceId}/installments"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/<invoiceId>/installments?_limit=1&_offset=0"
    Then I get response body
    And I get response status code
    And I verify if response body is empty
    And Response code must be 404
    And I wait 2 seconds

    Examples: 
      | descricao                 | invoiceId    |
      | por invoiceId inexistente | 123131311231 |

  @Positivo
  Scenario Outline: Consultar invoice <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpointInvoices}/<invoiceId>/installments?_limit=1&_offset=0"
    Then I get response body
    And I get response status code
    And I verify if response value "/0/installmentId" is not empty
    And I verify if response value "/0/installmentNumber" is not empty
    And I verify if response value "/0/amountOriginal" is not empty
    And I verify if response value "/0/amountRemaining" is not empty
    And I verify if response value "/0/dueDate" is not empty
    And I verify if response value "/0/paymentStatus" is not empty
    #And I verify if response value "/0/typeableLine" is not empty
    And Response code must be 200

    Examples: 
      | descricao            | invoiceId    |
      | por invoiceId válido | ${invoiceId} |
