Feature: POST_services_{serviceId}_surveys.feature 
	Operação responsável por cadastrar um pesquisa de satisfação do atendimento.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	
	
@PreRequest 
Scenario: Cadastrar cliente com sucesso 
	Given System generate random number 
	And System generate random CPF 
	And I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set POST api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "customerId" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/service/v1" 
	And I save "/services" as "endpointServices" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "01uq0000005QTFg" as "productPriceId" 
	And I save "01uq0000005QTFg" as "productId" 
	And I save "001q0000017eE9U" as "storeId" 
	
	And I save "13422876" as "numberOrder" 
	And I save "4f9971fd" as "source" 
	And I save "4f9971fd" as "notes" 
	And I save "4f9971fd" as "phase" 
	And I save "4f9971fd" as "reasonService" 
	And I save "20190731/5204585" as "relatedServiceCode" 
	And I save "Assistência Técnica Domiciliar" as "serviceType" 
	And I save "2019-01-01" as "expectedDate" 
	
	
@PreRequest 
Scenario Outline: Cadastrar atendimento <descricao> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "<body>" 
	When I set POST api endpoint as "${endpointServices}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "serviceId" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	Examples: 
		| id | descricao                                  | body                                                                                                                                                                                                                                         |
		| 1  | com numberOrder vazio                      | {"numberOrder":"","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		
		
		@Negativo 
		Scenario: Enviar requisição sem client_id 
			When I set POST api endpoint as "${endpointServices}/${serviceId}/surveys" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario: Enviar requisição sem access_token 
			Given I set request header "client_id" as "${client_id}" 
			When I set POST api endpoint as "${endpointServices}/${serviceId}/surveys" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario Outline: Enviar requisição <descricao> 
			Given I set request header "client_id" as "<client_id>" 
			And I set request header "access_token" as "<access_token>" 
			And I set request header "Content-Type" as "application/json" 
			And I set request body as "{"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100","products":[{"price":10,"quantity":1,"productPriceId":"${productPriceId}"}]}" 
			When I set POST api endpoint as "${endpointServices}/${serviceId}/surveys" 
			Then I get response body 
			And I get response status code 
			And Response body must be "<message>" 
			And Response code must be 401 
			
			Examples: 
				| descricao                 | client_id     | access_token     | message                                                                                  |
				| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
				| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
				| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
				| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
				
				
				@Negativo 
				Scenario: Cadastrar pesquisa sem body 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set POST api endpoint as "${endpointServices}/${serviceId}/surveys" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/code" with "400.000" 
					And I compare response value "/message" with "Body Inválido" 
					And Response code must be 400 
					
				@Negativo 
				Scenario: Cadastrar pesquisa sem Content-Type 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "{"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}" 
					When I set POST api endpoint as "${endpointServices}/${serviceId}/surveys" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 415 
					
					
				@Positivo 
				Scenario Outline: Cadastrar avaliação <descricao> 
					Given I set request header "Content-Type" as "application/json" 
					And I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "<body>" 
					When I set POST api endpoint as "${endpointServices}/${serviceId}/surveys" 
					Then I get response body 
					And I get response status code 
					And Response code must be 201 
					
					Examples: 
						| id | descricao                   | body                          |						
						| 1  | sem note                    | {"rating":0}                  |
						| 2  | com sucesso                 | {"rating":10,"note":"string"} |
						| 3  | com note vazio              | {"rating":0,"note":""}        |
						| 4  | com note nulo               | {"rating":0,"note":null}      |
						
						
						@Negativo 
						Scenario Outline: Cadastrar avaliação <descricao> 
							Given I set request header "Content-Type" as "application/json" 
							And I set request header "client_id" as "${client_id}" 
							And I set request header "access_token" as "${access_token}" 
							And I set request body as "<body>" 
							When I set POST api endpoint as "${endpointServices}/${serviceId}/surveys" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/code" with "<code>" 
							And I compare response value "/message" with <message> 
							And Response code must be <responseCode> 
							
							Examples: 
								| id | descricao                   | body                                                                                                                                                                                                                                    | message                                                       | code    | responseCode |
								| 1  | com body vazio              | {}                | "O campo 'rating' é obrigatório." | 400.000 | 400          |
								| 2  | com rating vazio            | {"rating":"","note":"string"}              | "O campo 'rating' é obrigatório." | 400.000 | 400          |
								| 3  | com rating nulo             | {"rating":null,"note":"string"}     | "O campo 'rating' é obrigatório." | 400.000 | 400          |
								| 4  | sem rating                  | {"note":"string"}     | "O campo 'rating' é obrigatório." | 400.000 | 400          |
								| 5  | com rating incorreto        | {"rating":110,"note":"string stringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstring stringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstringstring"}     | "Valor inválido informado." | 400.000 | 400          |