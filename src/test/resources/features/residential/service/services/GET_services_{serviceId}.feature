Feature: GET_services_{serviceId}.feature 
	Consulta de um atendimento no segmento domiciliar da Ultragaz.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	
	
@PreRequest 
Scenario: Cadastrar cliente com sucesso 
	Given System generate random number 
	And System generate random CPF 
	And I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set POST api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "customerId" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/service/v1" 
	And I save "/services" as "endpointServices" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "13422876" as "numberOrder" 
	And I save "4f9971fd" as "source" 
	And I save "4f9971fd" as "notes" 
	And I save "4f9971fd" as "phase" 
	And I save "4f9971fd" as "reasonService" 
	And I save "20190731/5204585" as "relatedServiceCode" 
	And I save "Assistência Técnica Domiciliar" as "serviceType" 
	And I save "2019-01-01" as "expectedDate" 
	
	
@PreRequest 
Scenario Outline: Cadastrar atendimento <descricao> 
	Given I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "<body>" 
	When I set POST api endpoint as "${endpointServices}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "serviceId<id>" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	Examples: 
		| id | descricao                                  | body                                                                                                                                                                                                                                         |
		| 1  | com numberOrder vazio                      | {"numberOrder":"","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 2  | com numberOrder nulo                       | {"numberOrder":null,"source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 3  | sem numberOrder                            | {"source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 4  | com source vazio                           | {"numberOrder":"${numberOrder}","source":"","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 5  | com source nulo                            | {"numberOrder":"${numberOrder}","source":null,"notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 6  | com source inválido                        | {"numberOrder":"${numberOrder}","source":"Inválido","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 7  | sem source                                 | {"numberOrder":"${numberOrder}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 8  | com notes vazio                            | {"numberOrder":"${numberOrder}","source":"${source}","notes":"","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 9  | com notes nulo                             | {"numberOrder":"${numberOrder}","source":"${source}","notes":null,"phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 10 | sem notes                                  | {"numberOrder":"${numberOrder}","source":"${source}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 11 | com phase vazio                            | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 12 | com phase nulo                             | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":null,"reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 13 | com phase inválido                         | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"Inválido","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 14 | sem phase                                  | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 15 | com reasonService vazio                    | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 16 | com reasonService nulo                     | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":null,"customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 17 | com reasonService inválido                 | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"s","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 18 | sem reasonService                          | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 19 | com relatedServiceCode vazio               | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 20 | com relatedServiceCode nulo                | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":null,"serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 21 | sem relatedServiceCode                     | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		| 22 | com expectedDate nulo                      | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":null}  |
		| 23 | sem expectedDate                           | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}"}  |
		| 24 | com notes longo                            | {"numberOrder":"${numberOrder}","source":"${source}","notes":"Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo.","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
		
		
		@Negativo 
		Scenario: Enviar requisição sem client_id 
			When I set GET api endpoint as "${endpointServices}/${serviceId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario: Enviar requisição sem access_token 
			Given I set request header "client_id" as "${client_id}" 
			When I set GET api endpoint as "${endpointServices}/${serviceId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
			And Response code must be 401 
			
			
		@Negativo 
		Scenario Outline: Enviar requisição <descricao> 
			Given I set request header "client_id" as "<client_id>" 
			And I set request header "access_token" as "<access_token>" 
			When I set GET api endpoint as "${endpointServices}/${serviceId1}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "<message>" 
			And Response code must be 401 
			
			Examples: 
				| descricao                 | client_id     | access_token     | message                                                                                  |
				| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
				| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
				| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
				| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
				
				
				@Negativo 
				Scenario: Consultar pedido com customerId inexistente 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set GET api endpoint as "${endpointServices}/batatinhax" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 404 
					
					
				@Positivo 
				Scenario Outline: Consultar pedido <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request header "Content-Type" as "application/json" 
					When I set GET api endpoint as "${endpointServices}/${serviceId<id>}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/statusService" with "Aberto" 
					And I compare response value "/responsible" with "Administrador Ultragaz TI" 
					And I compare response value "/numberOrder" with "<numberOrder>" 
					And I compare response value "/source" with "<source>" 
					And I compare response value "/notes" with "<notes>" 
					And I compare response value "/cancellationReason" with "" 
					And I compare response value "/reasonService" with "<reasonService>" 
					And I compare response value "/serviceId" with "${serviceId<id>}" 
					And I compare response value "/phase" with "Aguardando Localização de Revenda" 
					And I compare response value "/providerId" with "" 
					And I compare response value "/subject" with "Assistência Técnica Domiciliar" 
					And I verify if response value "/requestDate" is not empty 
					And I verify if response value "/expectedDate" is not empty 
					And I compare response value "/relatedServiceCode" with "<relatedServiceCode>" 
					And I verify if response value "/codeService" is not empty
					And I compare response value "/serviceType" with "" 
					And I compare response value "/customerId" with "${customerId}" 
					And I compare response value "/changeHistory/0/field" with "ownerAssignment" 
					And I compare response value "/changeHistory/0/newValue" with "00530000004TlwbAAC" 
					And I compare response value "/changeHistory/0/oldValue" with "005q0000005PJFnAAO" 
					And I verify if response value "/changeHistory/0/changeDate" is not empty 
					And Response code must be 200 
					
					Examples: 
						| id | descricao                                  | numberOrder    | source   | notes    | reasonService | relatedServiceCode |
						| 1  | com numberOrder vazio                      |                | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 2  | com numberOrder nulo                       |                | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 3  | sem numberOrder                            |                | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 4  | com source vazio                           | ${numberOrder} |          | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 5  | com source nulo                            | ${numberOrder} |          | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 6  | com source inválido                        | ${numberOrder} | Inválido | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 7  | sem source                                 | ${numberOrder} |          | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 8  | com notes vazio                            | ${numberOrder} | 4f9971fd |          | 4f9971fd      | 20190731/5204585   |
						| 9  | com notes nulo                             | ${numberOrder} | 4f9971fd |          | 4f9971fd      | 20190731/5204585   |
						| 10 | sem notes                                  | ${numberOrder} | 4f9971fd |          | 4f9971fd      | 20190731/5204585   |
						| 11 | com phase vazio                            | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 12 | com phase nulo                             | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 13 | com phase inválido                         | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 14 | sem phase                                  | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 15 | com reasonService vazio                    | ${numberOrder} | 4f9971fd | 4f9971fd |               | 20190731/5204585   |
						| 16 | com reasonService nulo                     | ${numberOrder} | 4f9971fd | 4f9971fd |               | 20190731/5204585   |
						| 17 | com reasonService inválido                 | ${numberOrder} | 4f9971fd | 4f9971fd | s             | 20190731/5204585   |
						| 18 | sem reasonService                          | ${numberOrder} | 4f9971fd | 4f9971fd |               | 20190731/5204585   |
						| 19 | com relatedServiceCode vazio               | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      |                    |
						| 20 | com relatedServiceCode nulo                | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      |                    |
						| 21 | sem relatedServiceCode                     | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      |                    |
						| 22 | com expectedDate nulo                      | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 23 | sem expectedDate                           | ${numberOrder} | 4f9971fd | 4f9971fd | 4f9971fd      | 20190731/5204585   |
						| 24 | com notes longo                            | ${numberOrder} | 4f9971fd | Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. Notes longo. | 4f9971fd      | 20190731/5204585   |