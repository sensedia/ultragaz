Feature: POST_services.feature 
	Operação responsável por cadastrar um atendimento.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/customer/v1" 
	And I save "/customers" as "endpointCustomers" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	
	
@PreRequest 
Scenario: Cadastrar cliente com sucesso 
	Given System generate random number 
	And System generate random CPF 
	And I set request header "client_id" as "${client_id}" 
	And I set request header "access_token" as "${access_token}" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"firstName":"Aline","lastName":"${random}","document":"${randomCPF}","documentType":"CPF","businessType":"IT","company":"Sensedia","source":"QA","contact":{"phones":[{"ddd":"019","number":"982722791","phoneType":1}],"email":"aline.dias@sensedia.com"},"socialNetwork":{"instagram":"1abender","facebook":"1abender","twitter":"1abender"},"addresses":[{"mainAddress":true,"city":"Sumaré","stateAbbreviation":"SP","neighborhood":"Jardim Bom Retirõ","zipCode":"13181643","country":"Brazilã","referenceAddress":"Praça dõ Bom Retiro","address":"Rua Domingõs Conradô","additionAddress":"Cãsa","numberAddress":"528","latitude":${latitude},"longitude":${longitude},"descriptionAddress":"Minha casa maravilhosa"}]}" 
	When I set POST api endpoint as "${endpointCustomers}" 
	Then I get response body 
	And I get response status code 
	And I save response header "Location" as "location" 
	And I save final value of header "Location" as "customerId" 
	And I verify if response body is empty 
	And Response code must be 201 
	And I wait 2 seconds 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/service/v1" 
	And I save "/services" as "endpointServices" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "13422876" as "numberOrder" 
	And I save "4f9971fd" as "source" 
	And I save "4f9971fd" as "notes" 
	And I save "4f9971fd" as "phase" 
	And I save "4f9971fd" as "reasonService" 
	And I save "20190731/5204585" as "relatedServiceCode" 
	And I save "Assistência Técnica Domiciliar" as "serviceType" 
	And I save "2019-01-01" as "expectedDate" 
	
	
@Negativo 
Scenario: Enviar requisição sem client_id 
	When I set POST api endpoint as "${endpointServices}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
	And Response code must be 401 
	
	
@Negativo 
Scenario: Enviar requisição sem access_token 
	Given I set request header "client_id" as "${client_id}" 
	When I set POST api endpoint as "${endpointServices}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token" 
	And Response code must be 401 
	
	
@Negativo 
Scenario Outline: Enviar requisição <descricao> 
	Given I set request header "client_id" as "<client_id>" 
	And I set request header "access_token" as "<access_token>" 
	And I set request header "Content-Type" as "application/json" 
	And I set request body as "{"numberOrder":"string","source":"string","notes":"string","phase":"string","reasonService":"string","customerId":"string","relatedServiceCode":"string","serviceType":"string","expectedDate":"string"}" 
	When I set POST api endpoint as "${endpointServices}" 
	Then I get response body 
	And I get response status code 
	And Response body must be "<message>" 
	And Response code must be 401 
	
	Examples: 
		| descricao                 | client_id     | access_token     | message                                                                                  |
		| com client_id inválido    | ${client_id}1 | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
		| com client_id vazio       |               | ${access_token}  | Could not find a required APP in the request, identified by HEADER client_id.            |
		| com access_token inválido | ${client_id}  | ${access_token}1 | Could not find a required Access Token in the request, identified by HEADER access_token |
		| com access_token vazio    | ${client_id}  |                  | Could not find a required Access Token in the request, identified by HEADER access_token |
		
		
		@Positivo 
		Scenario Outline: Cadastrar atendimento <descricao> 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}" 
			And I set request header "Content-Type" as "application/json" 
			And I set request body as "<body>" 
			When I set POST api endpoint as "${endpointServices}" 
			Then I get response body 
			And I get response status code 
			And I save response header "Location" as "location" 
			And I save final value of header "Location" as "serviceId<id>" 
			And I verify if response body is empty 
			And Response code must be 201 
			And I wait 2 seconds 
			
			Examples: 
				| id | descricao                                  | body                                                                                                                                                                                                                                         |
				| 1  | com numberOrder vazio                          | {"numberOrder":"","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 2  | com numberOrder nulo                           | {"numberOrder":null,"source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 3  | sem numberOrder                                | {"source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 4  | com source vazio                           | {"numberOrder":"${numberOrder}","source":"","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 5  | com source nulo                            | {"numberOrder":"${numberOrder}","source":null,"notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 6  | com source inválido                        | {"numberOrder":"${numberOrder}","source":"Inválido","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 7  | sem source                                 | {"numberOrder":"${numberOrder}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 8  | com notes vazio                            | {"numberOrder":"${numberOrder}","source":"${source}","notes":"","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 9  | com notes nulo                             | {"numberOrder":"${numberOrder}","source":"${source}","notes":null,"phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 10 | sem notes                                  | {"numberOrder":"${numberOrder}","source":"${source}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 11 | com phase vazio                            | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 12 | com phase nulo                             | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":null,"reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 13 | com phase inválido                         | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"Inválido","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 14 | sem phase                                  | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 15 | com reasonService vazio                    | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 16 | com reasonService nulo                     | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":null,"customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 17 | com reasonService inválido                 | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"s","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 18 | sem reasonService                          | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 19 | com relatedServiceCode vazio               | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 20 | com relatedServiceCode nulo                | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":null,"serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 21 | sem relatedServiceCode                     | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  |
				| 22 | com expectedDate nulo                      | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":null}  |
				| 23 | sem expectedDate                           | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}"}  |
				
				
				@Negativo 
				Scenario: Cadastrar pedido sem body 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					When I set POST api endpoint as "${endpointServices}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/code" with "400.000" 
					And I compare response value "/message" with "Body Inválido" 
					And Response code must be 400 
					
					
				@Negativo 
				Scenario: Cadastrar pedido sem Content-Type 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "{"customerId":"${customerId}","storeId":"${storeId}","paymentMethod":"Dinheiro","deliveryNote":"Testes","changeFor":"100.99","products":[{"price":10.99,"quantity":1,"productPriceId":"${productPriceId}", "productId": "${productId}"}]}" 
					When I set POST api endpoint as "${endpointServices}" 
					Then I get response body 
					And I get response status code 
					And I verify if response body is empty 
					And Response code must be 415 
					
					
				@Negativo 
				Scenario Outline: Cadastrar atendimento <descricao> 
					Given I set request header "Content-Type" as "application/json" 
					And I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}" 
					And I set request body as "<body>" 
					When I set POST api endpoint as "${endpointServices}" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/code" with "<code>" 
					And I compare response value "/message" with <message> 
					And Response code must be <responseCode> 
					
					Examples: 
						| descricao                                  | body                                                                                                                                                                                                                                                              | message                                                | code    | responseCode |
						| com numberOrder inválido                       | {"numberOrder":"Inválido","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}    | "O campo numberOrder informado é inválido ou inexistente." | 400.002 | 400          |
						| com customerId vazio                       | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  | "Não existe um Cliente para o customerId informado."    | 422.000 | 422          |
						| com customerId nulo                        | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":null,"relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  | "Não existe um Cliente para o customerId informado."    | 422.000 | 422          |
						| com customerId inválido                    | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"s","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  | "Não existe um Cliente para o customerId informado."    | 422.000 | 422          |
						| sem customerId                             | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  | "Não existe um Cliente para o customerId informado."    | 422.000 | 422          |
						| com relatedServiceCode inválido            | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"s","serviceType":"${serviceType}","expectedDate":"${expectedDate}"}  | "O relatedServiceCode informado não existe."    | 422.001 | 422          |
						| com serviceType vazio                      | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"","expectedDate":"${expectedDate}"}  | "O campo serviceType informado é inválido ou inexistente."    | 400.000 | 400          |
						| com serviceType nulo                       | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":null,"expectedDate":"${expectedDate}"}  | "O campo serviceType informado é inválido ou inexistente."    | 400.000 | 400          |
						| com serviceType inválido                   | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"s","expectedDate":"${expectedDate}"}  | "O campo serviceType informado é inválido ou inexistente."    | 400.000 | 400          |
						| sem serviceType                            | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","expectedDate":"${expectedDate}"}  | "O campo serviceType informado é inválido ou inexistente."    | 400.000 | 400          |
						| com expectedDate vazio                     | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":""}  | "Valor inválido informado."    | 400.001 | 400          |
						| com expectedDate inválido                  | {"numberOrder":"${numberOrder}","source":"${source}","notes":"${notes}","phase":"${phase}","reasonService":"${reasonService}","customerId":"${customerId}","relatedServiceCode":"${relatedServiceCode}","serviceType":"${serviceType}","expectedDate":"s"}  | "Valor inválido informado."    | 400.001 | 400          |