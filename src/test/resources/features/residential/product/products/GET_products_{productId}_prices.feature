Feature: GET_products_{productId}_prices.feature 
	Obter preço de um produto específico do segmento domiciliar da Ultragaz.



@PreRequest 
Scenario: Gerar token 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "" 
	And System generate Authorization with default APP information 
	And I read file body "/features/access-token/body/access_token.json" 
	And I set request header "Authorization" as "${authorization}" 
	And I set request header "Content-Type" as "application/json" 
	When I set POST api endpoint as "/oauth/access-token" 
	And I get response body 
	And I save response value "/access_token" as "access_token" 
	
	
@Definition 
Scenario: Definir configurações de ambiente 
	Given I use domain as "https://api-ultragaz.sensedia.com" 
	And I use api name as "/dev/residential/product/v1" 
	And I save "/products" as "endpointProducts" 
	And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id" 
	And I save "4f9971fd-5892-3c51-a982-cf780f96acd9" as "client_secret" 
	And I save "0110035" as "productId" 
	And I save "-22.8163929" as "latitude" 
	And I save "-47.0421092" as "longitude" 
	And I save "2019-09-10T17:00-03:00" as "delivery-date" 
	
	
@Positivo 
Scenario: Consultar prices 
	Given I set request header "client_id" as "${client_id}"
	And I set request header "access_token" as "${access_token}" 
	When I set GET api endpoint as "${endpointProducts}/${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=${delivery-date}" 
	Then I get response body 
	And I get response status code 
	And I compare response value "/0/paymentMethod" with "Pagamento Online" 
	And I compare response value "/0/paymentMethodCode" with "11" 
	And I compare response value "/0/price" with "200" 
	And I compare response value "/0/storeId" with "001q00000143I7UAAU" 
	And I compare response value "/0/productPriceId" with "01uq0000005QppmAAC" 
	And Response code must be 200 
	And I wait 2 seconds 
	
	
@Positivo 
Scenario Outline: Consultar prices <descricao>
	Given I set request header "client_id" as "${client_id}"
	And I set request header "access_token" as "${access_token}"
	When I set GET api endpoint as "${endpointProducts}/${productId}/prices<queryParameter>" 
	Then I get response body 
	And I get response status code 
	And Response body must be "[]" 
	And Response code must be 200 
	And I wait 2 seconds 
	
	Examples: 
		| descricao                         | queryParameter                                                             |
		| por longitude, latitude oceanicas | ?latitude=-22.8163929&longitude=-22.8163920&delivery-date=${delivery-date} |
		
		
		@Negativo 
		Scenario Outline: Consultar prices <descricao> 
			Given I set request header "client_id" as "${client_id}" 
			And I set request header "access_token" as "${access_token}"
			When I set GET api endpoint as "${endpointProducts}/<productId>/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=${delivery-date}" 
			Then I get response body 
			And I get response status code 
			And Response body must be "[]" 
			And Response code must be 200 
			And I wait 2 seconds 
			
			Examples: 
				| descricao               | productId |
				| por productId incorreto | 1         |
				
				
				@Negativo 
				Scenario Outline: Consultar price <descricao> 
					Given I set request header "client_id" as "${client_id}" 
					And I set request header "access_token" as "${access_token}"
					When I set GET api endpoint as "${endpointProducts}/<queryParameter>" 
					Then I get response body 
					And I get response status code 
					And I compare response value "/0/code" with <code> 
					And I compare response value "/0/message" with <message> 
					And I verify if response value "/1/code" is empty 
					And I verify if response value "/1/message" is empty 
					And Response code must be 400 
					And I wait 2 seconds 
					
					Examples: 
						| descricao                     | queryParameter                                                                                    | code      | message                                              |
						| por delivery-date vazio       | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=                    | "400.001" | "Field delivery-date is required."                   |
						| por delivery-date incorreto   | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=0000-00-00T21:55:00 | "400.002" | "Field delivery-date has an invalid format."         |
						| por delivery-date inválido    | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=1                   | "400.002" | "Field delivery-date has an invalid format."         |
						| por latitude                  | ${productId}/prices?delivery-date=${delivery-date}&latitude=${latitude}                           | "400.001" | "Field longitude is required."                       |
						| por latitude vazia            | ${productId}/prices?delivery-date=${delivery-date}&longitude=${longitude}&latitude=               | "400.001" | "Field latitude is required."                        | 
						| por longitude                 | ${productId}/prices?delivery-date=${delivery-date}&longitude=${longitude}                         | "400.001" | "Field latitude is required."                        |
						| por longitude vazia           | ${productId}/prices?delivery-date=${delivery-date}&latitude=${latitude}&longitude=                | "400.001" | "Field longitude is required."                       | 
						
						
						@Negativo 
						Scenario Outline: Consultar price <descricao> 
							Given I set request header "client_id" as "${client_id}" 
							And I set request header "access_token" as "${access_token}"
							When I set GET api endpoint as "${endpointProducts}/<queryParameter>" 
							Then I get response body 
							And I get response status code 
							And I compare response value "/code" with <code> 
							And I compare response value "/message" with <message> 
							And Response code must be 422 
							And I wait 2 seconds 
							
							Examples: 
								| descricao                     | queryParameter                                                                                       | code      | message                                                                                                                                                 |
								| por delivery-date vencida     | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=2000-08-09T08:00-03:00 | "422.001" | "Field deliveryDate is invalid for the period. A valid period is the one between the current date and time and a week after the current date and time." |
								| por delivery-date muito longe | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=2040-08-09T08:00-03:00 | "422.001" | "Field deliveryDate is invalid for the period. A valid period is the one between the current date and time and a week after the current date and time." |
								
								
								@Negativo 
								Scenario Outline: Consultar prices <descricao> 
									Given I set request header "client_id" as "${client_id}" 
									And I set request header "access_token" as "${access_token}"
									When I set GET api endpoint as "${endpointProducts}/<queryParameter>" 
									Then I get response body 
									And I get response status code 
									And I compare response value "/0/code" with <code> 
									And I compare response value "/0/message" with <message> 
									And I verify if response value "/1/code" is empty 
									And I verify if response value "/1/message" is empty 
									And Response code must be 400 
									
									Examples: 
										| descricao               | queryParameter                                          | code      | message                                  |
										| por longitude incorreta | ${productId}/prices?latitude=${latitude}&delivery-date=${delivery-date}&longitude=Rua  | "400.002" | "Field longitude has an invalid format." |
										| por latitude incorreta  | ${productId}/prices?longitude=${longitude}&delivery-date=${delivery-date}&latitude=Rua | "400.002" | "Field latitude has an invalid format."  |
										
										
										@Negativo 
										Scenario Outline: Consultar prices <descricao> em Português 
											Given I set request header "client_id" as "${client_id}"
											And I set request header "access_token" as "${access_token}"
											And I set request header "Accept-Language" as "pt-BR" 
											When I set GET api endpoint as "${endpointProducts}/<queryParameter>" 
											Then I get response body 
											And I get response status code 
											And I compare response value "/0/code" with <code> 
											And I compare response value "/0/message" with <message> 
											And I verify if response value "/1/code" is empty 
											And I verify if response value "/1/message" is empty 
											And Response code must be 400 
											
											Examples: 
												| descricao                     | queryParameter                                                                                    | code      | message                                              |
												| por delivery-date vazio       | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=                    | "400.001" | "O campo delivery-date é obrigatório."               |
												| por delivery-date incorreto   | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=0000-00-00T21:55:00 | "400.002" | "O campo delivery-date tem o formato inválido."      |
												| por delivery-date inválido    | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=1                   | "400.002" | "O campo delivery-date tem o formato inválido."      |
												| por latitude                  | ${productId}/prices?delivery-date=${delivery-date}&latitude=${latitude}                           | "400.001" | "O campo longitude é obrigatório."                   |
												| por latitude vazia            | ${productId}/prices?delivery-date=${delivery-date}&longitude=${longitude}&latitude=               | "400.001" | "O campo latitude é obrigatório."                    | 
												| por longitude                 | ${productId}/prices?delivery-date=${delivery-date}&longitude=${longitude}                         | "400.001" | "O campo latitude é obrigatório."                    |
												| por longitude vazia           | ${productId}/prices?delivery-date=${delivery-date}&latitude=${latitude}&longitude=                | "400.001" | "O campo longitude é obrigatório."                   | 
												
												
												@Negativo 
												Scenario Outline: Consultar price <descricao> em Português 
													Given I set request header "client_id" as "${client_id}" 
													And I set request header "access_token" as "${access_token}"
													And I set request header "Accept-Language" as "pt-BR" 
													When I set GET api endpoint as "${endpointProducts}/<queryParameter>" 
													Then I get response body 
													And I get response status code 
													And I compare response value "/code" with <code> 
													And I compare response value "/message" with <message> 
													And Response code must be 422 
													And I wait 2 seconds 
													
													Examples: 
														| descricao                     | queryParameter                                                                                       | code      | message                                                                                                                                        |
														| por delivery-date vencida     | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=2000-08-09T08:00-03:00 | "422.001" | "O campo deliveryDate é uma data inválida para o período. Um período válido deve estar entre a data e horas atuais e uma semana após a mesma." |
														| por delivery-date muito longe | ${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=2040-08-09T08:00-03:00 | "422.001" | "O campo deliveryDate é uma data inválida para o período. Um período válido deve estar entre a data e horas atuais e uma semana após a mesma." |
														
														
														
														@Negativo 
														Scenario Outline: Consultar prices <descricao> em Português 
															Given I set request header "client_id" as "${client_id}" 
															And I set request header "access_token" as "${access_token}"
															And I set request header "Accept-Language" as "pt-BR" 
															When I set GET api endpoint as "${endpointProducts}/<queryParameter>" 
															Then I get response body 
															And I get response status code 
															And I compare response value "/0/code" with <code> 
															And I compare response value "/0/message" with <message> 
															And I verify if response value "/1/code" is empty 
															And I verify if response value "/1/message" is empty 
															And Response code must be 400 
															
															Examples: 
																| descricao               | queryParameter                                          | code      | message                                     |
																| por longitude incorreta | ${productId}/prices?latitude=${latitude}&delivery-date=${delivery-date}&longitude=Rua  | "400.002" | "O campo longitude tem o formato inválido." |
																| por latitude incorreta  | ${productId}/prices?longitude=${longitude}&delivery-date=${delivery-date}&latitude=Rua | "400.002" | "O campo latitude tem o formato inválido."  |
																
																
																@Negativo 
																Scenario: Enviar requisição sem client_id 
																	When I set GET api endpoint as "${endpointProducts}/${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=${delivery-date}" 
																	Then I get response body 
																	And I get response status code 
																	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
																	And Response code must be 401 
																	
																	
																@Negativo 
																Scenario: Enviar requisição com client_id inválido 
																	Given I set request header "client_id" as "${client_id}1" 
																	When I set GET api endpoint as "${endpointProducts}/${productId}/prices?latitude=${latitude}&longitude=${longitude}&delivery-date=${delivery-date}" 
																	Then I get response body 
																	And I get response status code 
																	And Response body must be "Could not find a required APP in the request, identified by HEADER client_id." 
																	And Response code must be 401