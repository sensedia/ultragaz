Feature: GET_/pending-return-nfs.feature
  Operação responsável por listar a quantidade de ocorrências que uma revenda/loja deixou de emitir Nota Fiscal de retorno de vasilhame. (Provedor SalesForce)

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store-loyalty/v1"
    And I save "/pending-return-nfs" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0013000001FnYmbAAF" as "storeId"
    And I save "2020-09-01" as "startDate"
    And I save "2020-10-01" as "endDate"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1&start-date=${startDate}&end-date=${endDate}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1&start-date=${startDate}&end-date=${endDate}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?pg=1&start-date=${startDate}&end-date=${endDate}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Retornar quantidade de ocorrências <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                 | query                                               | message                                                  | code      |
      | sem query                 |                                                     | "O parâmetro pg é obrigatório."                          | "400.001" |
      | sem pg                    | ?start-date=${startDate}&end-date=${endDate}        | "O parâmetro pg é obrigatório."                          | "400.001" |
      | sem start-date            | ?pg=1&end-date=${endDate}                           | "O parâmetro start-date é obrigatório."                  | "400.001" |
      | sem end-date              | ?pg=1&start-date=${startDate}                       | "O parâmetro end-date é obrigatório."                    | "400.001" |
      | com pg inválido           | ?pg=1a&start-date=${startDate}&end-date=${endDate}  | "O parâmetro pg tem o formato inválido."                 | "400.002" |
      | com pg igual a zero       | ?pg=0&start-date=${startDate}&end-date=${endDate}   | "O parâmetro 'pg' deve ser maior que 0."                 | "400.004" |
      | com pg negativo           | ?pg=-1&start-date=${startDate}&end-date=${endDate}  | "O parâmetro 'pg' deve ser maior que 0."                 | "400.004" |
      | com start-date inválido   | ?pg=1&start-date=a${startDate}&end-date=${endDate}  | "O formato do parâmetro start-date deve ser yyyy-mm-dd." | "400.003" |
      | com start-date 0000-00-00 | ?pg=1&start-date=0000-00-00&end-date=${endDate}     | "O formato do parâmetro start-date deve ser yyyy-mm-dd." | "400.003" |
      | com start-date 9999-99-99 | ?pg=1&start-date=9999-99-99&end-date=${endDate}     | "O formato do parâmetro start-date deve ser yyyy-mm-dd." | "400.003" |
      | com start-date 2020-13-01 | ?pg=1&start-date=2020-13-01&end-date=${endDate}     | "O formato do parâmetro start-date deve ser yyyy-mm-dd." | "400.003" |
      | com start-date 2020-10-32 | ?pg=1&start-date=2020-13-01&end-date=${endDate}     | "O formato do parâmetro start-date deve ser yyyy-mm-dd." | "400.003" |
      | com end-date inválido     | ?pg=1&start-date=${startDate}&end-date=a${endDate}  | "O formato do parâmetro end-date deve ser yyyy-mm-dd."   | "400.003" |
      | com end-date 2020-11-32   | ?pg=1&start-date=${startDate}&end-date=2020-11-32   | "O formato do parâmetro end-date deve ser yyyy-mm-dd."   | "400.003" |

  @Positivo
  Scenario: Listar quantidade de ocorrências com store-id inválido
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?pg=1&start-date=${startDate}&end-date=${endDate}&store-id=0013000001CQUtLAAXaa"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

  @Positivo
  Scenario Outline: Retornar quantidade de ocorrências <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/stores/0/id" is not empty
    And I verify if response value "/stores/0/casesQuantity" is not empty
    And Response code must be 200

    Examples: 
      | descricao                                      | query                                                                   |
      | sem store-id                                   | ?pg=1&start-date=${startDate}&end-date=${endDate}                       |
      | com store-id                                   | ?pg=1&start-date=${startDate}&end-date=${endDate}&store-id=${storeId}   |
      | com start-date posterior a end-date do formato | ?pg=1&start-date=2021-09-01&end-date=${endDate}                         |
      | com start-date 1111-01-01                      | ?pg=1&start-date=1111-01-01&end-date=${endDate}                         |
      | com end-date 1111-01-01                        | ?pg=1&start-date=${startDate}&end-date=1111-01-01                       |
      
