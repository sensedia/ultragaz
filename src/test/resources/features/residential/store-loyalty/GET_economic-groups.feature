Feature: GET_economic-groups.feature
  Operação responsável por retornar o(s) grupo(s) econômico(s) da(s) revenda(s). (Provedor SalesForce)

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store-loyalty/v1"
    And I save "/economic-groups" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "0013000001CQUtLAAX" as "storeId"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?pg=1"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Retornar o grupo econômico da revenda <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao           | query                | message                                  | code      |
      | sem query           |                      | "O parâmetro pg é obrigatório."          | "400.001" |
      | sem pg              | ?store-id=${storeId} | "O parâmetro pg é obrigatório."          | "400.001" |
      | com pg inválido     | ?pg=1a               | "O parâmetro pg tem o formato inválido." | "400.002" |
      | com pg igual a zero | ?pg=0                | "O parâmetro 'pg' deve ser maior que 0." | "400.004" |
      | com pg negativo     | ?pg=-1               | "O parâmetro 'pg' deve ser maior que 0." | "400.004" |

  @Positivo
  Scenario: Retornar o grupo econômico da revenda com store-id inválido
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}?pg=1&store-id=0013000001CQUtLAAXa"
    Then I get response body
    And I get response status code
    And Response body must be "[]"
    And Response code must be 200

  @Positivo
  Scenario Outline: Retornar o grupo econômico da revenda <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/economicGroups/0/id" is not empty
    And I verify if response value "/economicGroups/0/members/0/id" is not empty
    And I verify if response value "/economicGroups/0/members/0/headquarterId" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/storeId" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/name" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/document" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/storeCode" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/addressCode" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/customerId" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/addressId" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/market" is not empty
    And I verify if response value "/economicGroups/0/members/0/stores/0/core" is not empty
    And Response code must be 200

    Examples: 
      | descricao    | query                     |
      | com pg       | ?pg=1                     |
      | com store-id | ?pg=1&store-id=${storeId} |
