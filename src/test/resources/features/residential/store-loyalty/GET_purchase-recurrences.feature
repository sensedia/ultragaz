Feature: GET_/purchase-recurrences.feature
  Operação responsável por retornar a última data de compra da(s) loja(s)/revenda(s) do UGAPEX

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store-loyalty/v1"
    And I save "/purchase-recurrences" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "298229" as "siteCode"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Retornar última data de compra da loja/revenda <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao              | query                     | message                                        | code      |
      | sem query              |                           | "Campos obrigatórios ausentes"                 | "400.001" |
      | sem pg                 | ?lim=2                    | "Campos obrigatórios ausentes"                 | "400.001" |
      | sem lim                | ?pg=1                     | "Campos obrigatórios ausentes"                 | "400.001" |
      | com pg igual a zero    | ?pg=0&lim=2               | "O atributo 'pg' deve ser maior que 0"         | "400.002" |
      | com pg negativo        | ?pg=-1&lim=2              | "O atributo 'pg' deve ser maior que 0"         | "400.002" |
      | com lim negativo       | ?pg=1&lim=-1              | "O atributo 'lim' deve ser maior que 0"        | "400.003" |
      | com lim igual a zero   | ?pg=1&lim=0               | "O atributo 'lim' deve ser maior que 0"        | "400.003" |
      | com pg inválido        | ?pg=1a&lim=2              | "Um dos atributos da requisição está inválido" | "400.004" |
      | com lim inválido       | ?pg=1&lim=2a              | "Um dos atributos da requisição está inválido" | "400.004" |
      | com site-code inválido | ?pg=1&lim=2&site-code=12a | "Um dos atributos da requisição está inválido" | "400.004" |

  @Positivo
  Scenario Outline: Retornar última data de compra da loja/revenda <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/recurrences/0/customerCode" is empty
    And I verify if response value "/recurrences/0/siteCode" is empty
    And I verify if response value "/recurrences/0/lastPurchaseAt" is empty
    And Response code must be 200

    Examples: 
      | descricao                                       | query                    |
      | com site-code não pertencente aos inadimplentes | ?pg=1&lim=2&site-code=12 |

  @Positivo
  Scenario Outline: Retornar última data de compra da loja/revenda <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/recurrences/0/customerCode" is not empty
    And I verify if response value "/recurrences/0/siteCode" is not empty
    And I verify if response value "/recurrences/0/lastPurchaseAt" is not empty
    And Response code must be 200

    Examples: 
      | descricao     | query                             |
      | sem site-code | ?pg=1&lim=2                       |
      | com site-code | ?pg=1&lim=2&site-code=${siteCode} |
