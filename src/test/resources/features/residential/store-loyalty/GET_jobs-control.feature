@GET_jobs-control
Feature: GET_jobs-control.feature
  Operação responsável por retornar as metricas de uso do Job Control, ferrramenta de marketing. (Provedor RAI)

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store-loyalty/v1"
    And I save "/jobs-control" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "00125223000185" as "storeDocument"
    And I save "2021-01-25" as "startDate"
    And I save "2021-02-25" as "endDate"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2&start-date=${startDate}&end-date=${endDate}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2&start-date=${startDate}&end-date=${endDate}"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2&start-date=${startDate}&end-date=${endDate}"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Retornar metricas de uso do Job Control <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/0/message" with <message>
    And I compare response value "/0/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao                      | query                                                                      | message                                          | code      |
      | sem pg                         | ?lim=1&start-date=${startDate}&end-date=${endDate}                         | "Parâmetro pg inválido."                         | "400.001" |
      | sem lim                        | ?pg=1&start-date=${startDate}&end-date=${endDate}                          | "Parâmetro lim inválido."                        | "400.001" |
      | sem start-date                 | ?pg=1&lim=2&end-date=${endDate}                                            | "Parâmetro start-date inválido."                 | "400.001" |
      | sem end-date                   | ?pg=1&lim=2&start-date=${startDate}                                        | "Parâmetro end-date inválido."                   | "400.001" |
      | com pg inválido                | ?pg=1a&lim=2&start-date=${startDate}&end-date=${endDate}                   | "O parâmetro pg tem o formato inválido."         | "400.002" |
      | com lim inválido               | ?pg=1&lim=2a&start-date=${startDate}&end-date=${endDate}                   | "O parâmetro lim tem o formato inválido."        | "400.002" |
      | com store-document inválido    | ?pg=1&lim=2&start-date=${startDate}&end-date=${endDate}&store-document=12a | "Parâmetro store-document inválido."             | "400.001" |
      | com start-date inválido        | ?pg=1&lim=2&start-date=a${startDate}&end-date=${endDate}                   | "O parâmetro start-date tem o formato inválido." | "400.002" |
      | com end-date inválido          | ?pg=1&lim=2&start-date=${startDate}&end-date=a${endDate}                   | "O parâmetro end-date tem o formato inválido."   | "400.002" |
      | com end-date DD maior que 31   | ?pg=1&lim=2&start-date=${startDate}&end-date=2021-02-32                    | "O parâmetro end-date tem o formato inválido."   | "400.002" |
      | com end-date MM maior que 12   | ?pg=1&lim=2&start-date=${startDate}&end-date=2021-13-15                    | "O parâmetro end-date tem o formato inválido."   | "400.002" |
      | com start-date DD maior que 31 | ?pg=1&lim=2&start-date=2021-01-32&end-date=${endDate}                      | "O parâmetro start-date tem o formato inválido." | "400.002" |
      | com start-date MM maior que 12 | ?pg=1&lim=2&start-date=2021-13-15&end-date=${endDate}                      | "O parâmetro start-date tem o formato inválido." | "400.002" |

  @Positivo
  Scenario Outline: Retornar metricas de uso do Job Control <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/jobsControl/0/storeDocument" is empty
    And I verify if response value "/jobsControl/0/downloadsNumber" is empty
    And I verify if response value "/jobsControl/0/customizationsNumber" is empty
    And I verify if response value "/jobsControl/0/accessNumber" is empty
    And Response code must be 200

    Examples: 
      | descricao                           | query                                                      |
      | com start-date posterior a end-date | ?pg=1&lim=2&start-date=${endDate}&end-date=${startDate}    |
      | com pg maior que totalPages         | ?pg=1000&lim=2&start-date=${endDate}&end-date=${startDate} |

  @Positivo
  Scenario Outline: Retornar metricas de uso do Job Control <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/jobsControl/0/storeDocument" is not empty
    And I verify if response value "/jobsControl/0/downloadsNumber" is not empty
    And I verify if response value "/jobsControl/0/customizationsNumber" is not empty
    And I verify if response value "/jobsControl/0/accessNumber" is not empty
    And Response code must be 200

    Examples: 
      | descricao          | query                                                                                   |
      | sem store-document | ?pg=1&lim=2&start-date=${startDate}&end-date=${endDate}                                 |
      | com store-document | ?pg=1&lim=2&start-date=${startDate}&end-date=${endDate}&store-document=${storeDocument} |
