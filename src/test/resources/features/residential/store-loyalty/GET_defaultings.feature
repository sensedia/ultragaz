@GET_defaultings
Feature: GET_defaultings.feature
  Operação responsável por retornar a(s) loja(s)/revenda(s) inadimplentes no fechamento mensal do UGAPEX

  @PreRequest
  Scenario: Gerar token
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as ""
    And System generate Authorization with default APP information
    And I set request body as "{"grant_type":"password","username":"wellington.moura@sensedia.com","password":"teste123!"}"
    And I set request header "Authorization" as "${authorization}"
    And I set request header "Content-Type" as "application/json"
    When I set POST api endpoint as "/dev/oauth/access-token"
    And I get response body
    And I save response value "/access_token" as "access_token"

  @Definition
  Scenario: Definir configurações de ambiente
    Given I use domain as "https://api-ultragaz.sensedia.com"
    And I use api name as "/dev/residential/store-loyalty/v1"
    And I save "/defaultings" as "endpoint"
    And I save "1da17c99-6816-31f7-b3a8-9a5b64aa2f29" as "client_id"
    And I save "7380949" as "siteCode"

  @Negativo
  Scenario: Enviar requisição sem client_id
    Given I set request header "access_token" as "${access_token}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2&start-date=01-2020&end-date=12-2020"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required APP in the request, identified by HEADER client_id."
    And Response code must be 401

  @Negativo
  Scenario: Enviar requisição sem access_token
    Given I set request header "client_id" as "${client_id}"
    And I set request header "Content-Type" as "application/json"
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2&start-date=01-2020&end-date=12-2020"
    Then I get response body
    And I get response status code
    And Response body must be "Could not find a required Access Token in the request, identified by HEADER access_token"
    And Response code must be 401

  @Negativo
  Scenario Outline: Enviar requisição <description>
    Given I set request header "client_id" as <client_id>
    And I set request header "access_token" as <access_token>
    When I set GET api endpoint as "${endpoint}?pg=1&lim=2&start-date=01-2020&end-date=12-2020"
    Then I get response body
    And I get response status code
    And Response body must be <message>
    And Response code must be 401

    Examples: 
      | description               | client_id       | access_token       | message                                                                                    |
      | com client_id vazio       | ""              | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com client_id inválido    | "${client_id}1" | "${access_token}"  | "Could not find a required APP in the request, identified by HEADER client_id."            |
      | com access_token vazio    | "${client_id}"  | ""                 | "Could not find a required Access Token in the request, identified by HEADER access_token" |
      | com access_token inválido | "${client_id}"  | "${access_token}1" | "Could not find a required Access Token in the request, identified by HEADER access_token" |

  @Negativo
  Scenario Outline: Retornar lojas/revendas inadimplentes <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I compare response value "/message" with <message>
    And I compare response value "/code" with <code>
    And Response code must be 400

    Examples: 
      | descricao               | query                                                         | message                                        | code      |
      | sem query               |                                                               | "Campos obrigatórios ausentes"                 | "400.001" |
      | sem pg                  | ?lim=2&start-date=01-2020&end-date=12-2020                    | "Campos obrigatórios ausentes"                 | "400.001" |
      | sem lim                 | ?pg=1&start-date=01-2020&end-date=12-2020                     | "Campos obrigatórios ausentes"                 | "400.001" |
      | sem start-date          | ?pg=1&lim=2&end-date=12-2020                                  | "Campos obrigatórios ausentes"                 | "400.001" |
      | sem end-date            | ?pg=1&lim=2&start-date=01-2020                                | "Campos obrigatórios ausentes"                 | "400.001" |
      | com pg igual a zero     | ?pg=0&lim=2&start-date=01-2020&end-date=12-2020               | "O atributo 'pg' deve ser maior que 0"         | "400.002" |
      | com pg negativo         | ?pg=-1&lim=2&start-date=01-2020&end-date=12-2020              | "O atributo 'pg' deve ser maior que 0"         | "400.002" |
      | com lim negativo        | ?pg=1&lim=-1&start-date=01-2020&end-date=12-2020              | "O atributo 'lim' deve ser maior que 0"        | "400.003" |
      | com lim igual a zero    | ?pg=1&lim=0&start-date=01-2020&end-date=12-2020               | "O atributo 'lim' deve ser maior que 0"        | "400.003" |
      | com pg inválido         | ?pg=1a&lim=2&start-date=01-2020&end-date=12-2020              | "Um dos atributos da requisição está inválido" | "400.005" |
      | com lim inválido        | ?pg=1&lim=2a&start-date=01-2020&end-date=12-2020              | "Um dos atributos da requisição está inválido" | "400.005" |
      | com start-date inválido | ?pg=1a&lim=2&start-date=a01-2020&end-date=12-2020             | "Um dos atributos da requisição está inválido" | "400.005" |
      | com end-date inválido   | ?pg=1&lim=2&start-date=01-2020&end-date=a12-2020              | "Um dos atributos da requisição está inválido" | "400.005" |
      | com site-code inválido  | ?pg=1&lim=2&start-date=01-2020&end-date=12-2020&site-code=12a | "Um dos atributos da requisição está inválido" | "400.005" |

  @Positivo
  Scenario Outline: Retornar lojas/revendas inadimplentes <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/defaultingCustomers/0/customerCode" is empty
    And I verify if response value "/defaultingCustomers/0/siteCode" is empty
    And I verify if response value "/defaultingCustomers/0/defaultings/0/date" is empty
    And I verify if response value "/defaultingCustomers/0/defaultings/0/value" is empty
    And Response code must be 200

    Examples: 
      | descricao                                       | query                                                        |
      | com site-code não pertencente aos inadimplentes | ?pg=1&lim=2&start-date=01-2020&end-date=12-2020&site-code=12 |
      | com start-date posterior a end-date             | ?pg=1&lim=2&start-date=12-2020&end-date=11-2020              |
      | com MM maior que 12 em start-date               | ?pg=1&lim=2&start-date=13-2020&end-date=11-2020              |
      | com MM maior que 12 em end-date                 | ?pg=1&lim=2&start-date=12-2020&end-date=13-2020              |
      | com end-date fora do formato                    | ?pg=1&lim=2&start-date=01-2020&end-date=132-2020             |

  @Positivo
  Scenario Outline: Retornar lojas/revendas inadimplentes <descricao>
    Given I set request header "client_id" as "${client_id}"
    And I set request header "access_token" as "${access_token}"
    When I set GET api endpoint as "${endpoint}<query>"
    Then I get response body
    And I get response status code
    And I verify if response value "/_metadata/currentPage" is not empty
    And I verify if response value "/_metadata/totalPages" is not empty
    And I verify if response value "/defaultingCustomers/0/customerCode" is not empty
    And I verify if response value "/defaultingCustomers/0/siteCode" is not empty
    And I verify if response value "/defaultingCustomers/0/defaultings/0/date" is not empty
    And I verify if response value "/defaultingCustomers/0/defaultings/0/value" is not empty
    And Response code must be 200

    Examples: 
      | descricao                      | query                                                                 |
      | sem site-code                  | ?pg=1&lim=2&start-date=01-2020&end-date=12-2020                       |
      | com site-code                  | ?pg=1&lim=2&start-date=01-2020&end-date=12-2020&site-code=${siteCode} |
      | com start-date fora do formato | ?pg=1&lim=2&start-date=132-2020&end-date=12-2020                      |
